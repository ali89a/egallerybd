<?php
//$vendor_product=$this->db->select('*')
//        ->from('product')
//        ->where('vendor_id',$this->session->userdata('vendor_id'))
//        ->get()->result();

?>

<h2 class="text-center" style="margin: 0px; padding: 20px;">Products</h2>
  <div class="panel panel-default">
    <div class="panel-heading">
        <div class="text-right">
            <a href="<?php echo base_url()?>Vendor_dashboard/add_product"  class="btn btn-primary btn-sm"><i class="f_a fa-plus-circle" aria-hidden="true"></i>&nbsp;Add Product</a>
        </div>
       
        
        
    </div>
    <div class="panel-body">
        <div class="">
            <form class="form-horizontal" action="<?php echo base_url();?>Vendor_dashboard/view_product" method="post">
                <div class="form-group">
                    <div class="col-sm-4 col-md-offset-5">
                        <input type="text" class="form-control" name="search_text" id="search_text" placeholder="Search By Product Name or Code">
                    </div>
                    <div class="col-sm-3">
                        <button  class="btn btn-primary btn-block">Search</button>
                    </div>
                </div>
            </form>
        </div> 
    <div class="table-responsive"> 
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>#</th>
                <th>Image</th>
                <th>Name</th>
                <th>Code</th>
                <th>Price</th>
                <th>Discount</th>    
                <th>Sale Price</th>
                <th>Status</th>
                <th class="text-right">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php 
            $i=1;
            foreach ($vendor_product as $value) {
                
            
            ?>            
            <tr>
                <td><?php echo $i;?></td>
                <td><img width="40" class="img-thumbnail" src="<?php echo base_url().$value->product_img_master;?>" alt=""></td>
               
                <td><?php echo $value->product_name;?></td> 
                <td><?php echo $value->product_code;?></td>
                   <td>BDT&nbsp;<?php echo $value->product_price; ?></td>
                   <td><?php echo $value->discount; ?>%</td>
                <td>BDT&nbsp;<?php echo $value->current_sale_price;?></td>
            
                <td>
                        <?php
                        if ($value->is_vendor_product == 1) {
                            echo '<span class="label label-warning">Pending</span>';
                        } elseif ($value->is_vendor_product == 2) {

                            echo '<span class="label label-success">Accepted</span>';
                        } elseif ($value->is_vendor_product == 3) {
                            echo '<span class="label label-danger">Rejected</span>';
                        }
                        ?>
                    </td>
              
                <td class="text-right">
                        <a href="<?php echo site_url("Vendor_dashboard/product_details/$value->product_id") ?>" class="btn btn-primary btn-xs">  <i class="fa fa-eye" aria-hidden="true"></i>
</a>    <?php if ($value->is_vendor_product == 1) { ?>
                        <a href="<?php echo site_url("Vendor_dashboard/edit_product/$value->product_id") ?>" class="btn btn-success btn-xs"><i class="fa fa-edit" aria-hidden="true"></i></a>
                        <?php } else {
                            ?>
                            <a class="btn btn-success btn-xs disabled"><i class="fa fa-edit" aria-hidden="true"></i></a>
                        <?php } ?>


                    </td>
            </tr>
          <?php 
          $i++;
            }
            ?> 
        </tbody>
    </table>
</div>
    
    
    </div>
  </div>


<style>
    
    .hcolor{
        background-color: #0088cc;
        color: white;
    }
    #generic_search_result_ul{
        background-color:#0088CC;
        color:white;
        list-style:none;
        margin:0;
        padding:0;
        min-height:50px;
        max-height:200px;
        overflow-y: scroll;
    }
    .generic_search_result_li{
       padding:2px 10px;
    }
    .generic_search_result_li:hover{
        background-color:yellowgreen;
        color:white;
      cursor: pointer;
    }
</style>

