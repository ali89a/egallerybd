<?php

//echo '<pre>';
//print_r($select_product_by_id);
?>
 

 
  <div class="panel panel-default">
      <div class="panel-heading">
           <div class="col-md-3">
            <a  href="<?php echo site_url('Vendor_dashboard/view_product'); ?>" class="btn btn-default" style="background-color: #00A65A;color: white;margin-top: 10px; "><i class="fa fa-step-backward" aria-hidden="true"></i>
Back To Product List</a>

           </div> 
          <div class="col-md-9">
              <h3 style="padding: 15px;">Product Information</h3>
          </div>
      </div>
    <div class="panel-body">
        <div class="row">
            
        <div class="col-md-4">
              <div class="table-responsive">          
  <table class="table table-bordered">
   
     
      <tr>
      
        <td><img src="<?php echo base_url().$select_product_by_id->product_img_master;?>">
         </td>
      </tr>
    
   
  </table>
  </div>
        </div>
        <div class="col-md-8">
                  <div class="table-responsive">          
  <table class="table table-bordered">
   
     
    
      <tr class="bg-info">
        <td>Category Name</td>
        <td>
            <?php 
            $category_name=$this->db->get_where('category',array('category_id'=>"$select_product_by_id->category_id"))->row()->category_name;
        echo $category_name;
        ?>
        </td>
      </tr>
    
      <tr>
          <td >Sub Category Name</td>
        <td>
            <?php 
         
            $sub_category_name=$this->db->get_where('sub_category',array('sub_category_id'=>"$select_product_by_id->sub_category_id"))->row()->sub_cat_name;
        echo $sub_category_name;
       
            
           ?>
        </td>
      </tr>
    
      <tr class="bg-info">
        <td>Brand Name</td>
        <td>
            
             <?php 
         
            $brand_name=$this->db->get_where('brand',array('brand_id'=>"$select_product_by_id->brand_id"))->row()->brand_name;
        echo $brand_name;
       
            
           ?>
          
            
        </td>
      </tr>
    
      <tr>
        <td>Name</td>
        <td><?php echo $select_product_by_id->product_name;?></td>
      </tr>
      <tr class="bg-info">
        <td>Code</td>
        <td><?php echo $select_product_by_id->product_code;?></td>
      </tr>
      <tr>
        <td>Price</td>
        <td><?php echo $select_product_by_id->current_sale_price;?></td>
      </tr>
      <tr class="bg-info">
        <td>discount</td>
        <td><?php echo $select_product_by_id->discount;?></td>
      </tr>
      <tr>
        <td>Short Description</td>
        <td><?php echo $select_product_by_id->product_short_des;?></td>
      </tr>
      <tr>
        <td>Description</td>
        <td><?php echo $select_product_by_id->product_long_des;?></td>
      </tr>
       <tr class="bg-info">
        <td>Status</td>
        <td><?php 
           if ($select_product_by_id->is_active==1) {
               echo 'Active';  
           } else {
                 echo 'Inactive'; 
           }
      
        ?></td>
      </tr>
       <tr class="bg-info">
        <td>Product?</td>
        <td><?php 
        if ($select_product_by_id->vendor_id==NULL) {
               echo 'Admin Product';  
           } else {
                 echo 'Sub Admin Product'; 
           }
        echo $select_product_by_id->vendor_id;
        ?></td>
      </tr>
   
  </table>
  </div>
        </div>

    
    </div>
    </div>
    <div class="panel-footer"> </div>
  </div>
