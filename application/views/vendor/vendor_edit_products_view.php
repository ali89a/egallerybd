
<script type="text/javascript">
    

    // Ajax post
    $(document).ready(function () {
        
          $("#discount").on('keyup', function (e) {
		//if(e.which!==37){
			//console.log(String.fromCharCode(e.which));
		console.log(e.target.value);
		


			var price = $("#price").val();
                      var discount = e.target.value.match(/\d/g);
			discount = discount.join("");
                      var totalvalue = price * ((100 - discount) / 100);
                      var bdtvalue = price * (discount / 100);

                      $("#total_price_amount").val(totalvalue.toFixed(2));
                      $("#bdtdiscount").val(bdtvalue.toFixed(2));
		//}
   
                      
   
              
          });
          
        
          $("#bdtdiscount").on('keyup', function (e) {

                      var price = $("#price").val();
                      var bdtdiscount = $(this).val();
                      var discount = (bdtdiscount*100) / price;
                      var total_price_amount = price - bdtdiscount;
			if((discount-Math.floor(discount)).toFixed(2)==0){
				$("#discount").val(discount.toFixed(0)+'%');
			}else{
				$("#discount").val(discount.toFixed(2));
			}
                      
                      $("#total_price_amount").val(total_price_amount.toFixed(2));
   
              
          });
        
//        ----------------
$("#category_id").on('change', function () {
            var category_id = $('#category_id').val();
            // alert(category_id);
            console.log(category_id);
$('#sub_category_id').val("");
$('#brand_id').val("");
$('#sub3_id').val("");
$("#brand_id").prop('disabled', true);

            if (category_id == '')
            {
                $("#sub_category_id").prop('disabled', true);
                $('#brand_id').prop('disabled', true);
		$('#sub3_id').prop('disabled', true);
            } else
            {
                $("#sub_category_id").prop('disabled', false);
                $.ajax({

                    // url: "<?php echo base_url(); ?>brand/get_subcat_by_cat_id", 
                    url: "<?php echo base_url(); ?>" + "Vendor_dashboard/get_subcat_by_cat_id",
                    type: "POST",
                    data: {'category_id': category_id},
                    dataType: 'json',
                    success: function (data) {
console.log(data);
                        $("#sub_category_id").html(data);
                    },
                    error: function () {
                          $("#sub_category_id").html(' <option value="">Not Found</option>');
                    }
                });

            }

        });
    });
</script>
<script type="text/javascript">
    // Ajax post
    $(document).ready(function () {
        $("#sub_category_id").on('change', function () {
            var sub_category_id = $('#sub_category_id').val();
            // alert(category_id);
            console.log(sub_category_id);
$('#brand_id').val("");
$('#sub3_id').val("");

            if (sub_category_id == '')
            {
                $("#brand_id").prop('disabled', true);
                $("#sub3_id").prop('disabled', true);
            } else
            {
                $("#brand_id").prop('disabled', false);
                $.ajax({

                    // url: "<?php echo base_url(); ?>brand/get_subcat_by_cat_id", 
                    url: "<?php echo base_url(); ?>" + "Vendor_dashboard/get_brand_by_sub_cat_id",
                    type: "POST",
                    data: {'sub_category_id': sub_category_id},
                    dataType: 'json',
                    success: function (data) {
console.log(data);
                        $("#brand_id").html(data);
                    },
                    error: function () {
                          $("#brand_id").html(' <option value="">Not Found</option>');
                    }
                });

            }

        });
    });
</script>

<script type="text/javascript">
    // Ajax post
    $(document).ready(function () {
        $("#brand_id").on('change', function () {
            var brand_id = $('#brand_id').val();
            // alert(category_id);
            console.log(brand_id);
$('#sub3_id').val("");

            if (sub_category_id == '')
            {
                $("#sub3_id").prop('disabled', true);
            } else
            {
                $("#sub3_id").prop('disabled', false);
                $.ajax({

                    // url: "<?php echo base_url(); ?>brand/get_subcat_by_cat_id", 
                    url: "<?php echo base_url(); ?>" + "Vendor_dashboard/get_sub3_by_brand_id",
                    type: "POST",
                    data: {'brand_id': brand_id},
                    dataType: 'json',
                    success: function (data) {
console.log(data);
                        $("#sub3_id").html(data);
                    },
                    error: function () {
                          $("#sub3_id").html(' <option value="">Not Found</option>');
                    }
                });

            }

        });
    });
</script>
<h2 class="text-center" style="margin: 0px; padding: 20px;">Edit Products</h2>
  <div class="panel panel-default">
    <div class="panel-heading">
        <div class="text-right">
            <a href="<?php echo site_url('Vendor_dashboard/view_product'); ?>" class="btn btn-default btn-sm" ><i class="fa fa-step-backward" aria-hidden="true"></i>
Back To Product List</a>  <a href="<?php echo base_url()?>Vendor_dashboard/add_product"  class="btn btn-primary btn-sm"><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;Add Product</a>
        </div>
       
        
        
    </div>
    <div class="panel-body">
   
    <form id="edit_form" name="edit_form" action="<?php echo base_url(); ?>Vendor_dashboard/save_product" method="post" class="form-horizontal" enctype="multipart/form-data">
           <div class="box-body">
                <div class="form-group">
                    <label  class="col-sm-3 control-label">Vendor <span style="color: red;">*</span></label>


                    <div class="col-sm-6">

                        <select class="form-control selectpicker" id="vendor_store_id" name="vendor_store_id"  data-live-search="true" data-live-search-style="startsWith">
                            <option value="">--choose Vendor--</option>
                            <?php 
                            foreach ($select_all_pub_vendor as $vendor) {
                                
                         
                            ?>
                            <option value="<?php echo $vendor->vendor_store_id;?>"><?php echo $vendor->vendor_store_name;?></option>
<?php } ?>
                        </select>

                    </div>
                      
                </div>
                <div class="form-group">
                    <label  class="col-sm-3 control-label">Brand</label>


                    <div class="col-sm-6">

                        <select class="form-control selectpicker" id="top_brand_id" name="top_brand_id"  data-live-search="true" data-live-search-style="startsWith">
                            <option value="">--choose Brand--</option>
                            <?php 
                            foreach ($select_all_pub_top_brand as $top_brand) {
                                
                         
                            ?>
                            <option value="<?php echo $top_brand->top_brand_id;?>"><?php echo $top_brand->top_brand_name;?></option>
<?php } ?>
                        </select>

                    </div>
                      
                </div>
                <div class="form-group">
                    <label  class="col-sm-3 control-label">Category <span style="color: red;">*</span></label>


                    <div class="col-sm-6">

                        <select class="form-control selectpicker" id="category_id" name="category_id"  data-live-search="true" data-live-search-style="startsWith" required>
                            <option value="">--choose--</option>
                            <?php 
                            foreach ($select_all_pub_category as $value) {
                                
                         
                            ?>
                            <option value="<?php echo $value->category_id;?>"><?php echo $value->category_name;?></option>
<?php } ?>
                        </select>

                    </div>
                      
                </div>
                <div class="form-group">
                    <label  class="col-sm-3 control-label">Sub-Category <span style="color: red;">*</span></label>


                    <div class="col-sm-6">

                        <select class="form-control" id="sub_category_id" name="sub_category_id" required >
                            <option>--Select Sub Category--</option>
<?php 
                            foreach ($select_all_pub_sub_cat as $sbvalue) {
                                
                         
                            ?>
                                 <option value="<?php echo $sbvalue->sub_category_id;?>"><?php echo $sbvalue->sub_cat_name;?></option>
<?php } ?>
                        </select>

                    </div>
                </div>
                <div class="form-group">
                    <label  class="col-sm-3 control-label">Sub Category2: <span style="color: red;">*</span></label>

                    <div class="col-sm-6">

                        <select class="form-control" id="brand_id" name="brand_id" required >
                            <option>--Select Sub Category2--</option>
<?php 
                            foreach ($select_all_pub_brand as $bvalue) {
                                
                         
                            ?>
                                 <option value="<?php echo $bvalue->brand_id;?>"><?php echo $bvalue->brand_name;?></option>
<?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label  class="col-sm-3 control-label">Sub-Category3: <span style="color: red;">*</span></label>

                    <div class="col-sm-6">

                        <select class="form-control" id="sub3_id" name="sub3_id"  required>
                            <option>--Select Sub-Category3--</option>
<?php 
                            foreach ($select_all_pub_sub3 as $sub3) {
                                
                         
                            ?>
                                 <option value="<?php echo $sub3->sub_category_three_id;?>"><?php echo $sub3->sub_category_three_name;?></option>
<?php } ?>
            
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label  class="col-sm-3 control-label">Product Name <span style="color: red;">*</span></label>


                    <div class="col-sm-6">

                        <?php
                        $data1 = array(
                            'type' => 'text',
                            'name' => 'txt_product',
                                 'value' => "$select_product_by_id->product_name",
                            'id' => 'txt_product',
                            'class' => 'form-control'
                        );

                        echo form_input($data1);
                        ?>

                    </div>
                </div>





                <div class="form-group">
                    <label  class="col-sm-3 control-label">Product Code <span style="color: red;">*</span>:</label>

                    <div class="col-sm-6">

                        <?php
                        $data2 = array(
                            'type' => 'text',
                            'name' => 'txt_product_item_code','value' => "$select_product_by_id->product_code",
                            'id' => 'txt_product_item_code',
                            'class' => 'form-control'
                        );

                        echo form_input($data2);
                        ?>

                    </div>
                </div>
                <div class="form-group">
                    <label  class="col-sm-3 control-label"> Price <span style="color: red;">*</span>:</label>

                    <div class="col-sm-6">

                        <?php
                        $data2 = array(
                            'type' => 'text',
                            'name' => 'price', 'value' => "$select_product_by_id->product_price",
                            'id' => 'price',
                            'class' => 'form-control'
                        );

                        echo form_input($data2);
                        ?>

                    </div>
                </div>
                <div class="form-group">
                    <label  class="col-sm-3 control-label"> Discount(%): <span style="color: red;">*</span></label>

                    <div class="col-sm-2">
<input class="form-control" id="discount" name="discount" size="30" type="text" value="<?php echo $select_product_by_id->discount;?>" >
<input class="form-control" id="product_id" name="product_id" size="30" type="hidden" value="<?php echo $select_product_by_id->product_id;?>" >



                    </div>
               
                    <label  class="col-sm-2 control-label"> Discount(BDT): <span style="color: red;">*</span></label>

                    <div class="col-sm-2">
<input class="form-control" id="bdtdiscount" name="bdtdiscount" size="30" type="text" value="<?php echo $select_product_by_id->bdt_discount;?>">

                    </div>
                </div>

                <div class="form-group">
                    <label  class="col-sm-3 control-label">New Price: <span style="color: red;">*</span></label>

                    <div class="col-sm-6">
                        <div class="">
                             <input class="form-control" id="total_price_amount" name="new_price" readonly="readonly" value="<?php echo $select_product_by_id->current_sale_price;?>">​
                      </div>
                    </div>
                </div>
                <div class="form-group">
                    <label  class="col-sm-3 control-label">Short Description: <span style="color: red;">*</span></label>

                    <div class="col-sm-6">
                        <div class="">


                            <?php echo form_textarea(array('id' => 'txt_short_description','value' => "$select_product_by_id->product_short_des", 'name' => 'txt_short_description', 'class' => 'ckeditor form-control', 'type' => 'text()', 'rows' => 5, 'cols' => 21,)); ?>
                     </div>
                    </div>
                </div>
                <div class="form-group">
                    <label  class="col-sm-3 control-label">Long Description: <span style="color: red;">*</span></label>

                    <div class="col-sm-6">
                        <div class="">


                             <?php echo form_textarea(array('id' => 'txt_long_description', 'name' => 'txt_long_description','value' => "$select_product_by_id->product_long_des", 'class' => 'ckeditor form-control', 'type' => 'text()', 'rows' => 7, 'cols' => 21,)); ?>
                            </div>
                    </div>
                </div>


                 <div class="form-group">
                    <label  class="col-sm-3 control-label">Product Master Image: <span style="color: red;">*</span></label>

                    <div class="col-sm-4">
                        <?php echo form_upload(array('id' => 'micon', 'name' => 'micon', 'class' => 'form-control')); ?>
                    </div>
                    <div class="col-sm-2">
                        <img height="50" src="<?php echo base_url().$select_product_by_id->product_img_master;?>" alt="a">
                        
                     </div>
                </div>
                <div class="form-group">
                    <label  class="col-sm-3 control-label">Product Images:</label>

                    <div class="col-sm-6">
                           <input type="file" class="form-control" name="userFiles[]" multiple/>
                         </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-6"> 

                        <?php echo form_submit(array('id' => 'submit', 'value' => 'Save', 'class' => 'btn btn-success')); ?>
                        <?php // echo form_submit(array('id' => 'submit', 'value' => 'update', 'class' => 'btn btn-primary'));  ?>
                        <?php echo form_submit(array('id' => 'submit', 'value' => 'delete', 'class' => 'btn btn-danger')); ?>

                    </div>
                </div>
            </div>

        </form>
    
    </div>
  </div>


 <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/css/bootstrap-select.css" />
<script type="text/javascript">
    document.forms['edit_form'].elements['category_id'].value ='<?php echo $select_product_by_id->category_id; ?>';
</script>
<script type="text/javascript">
    document.forms['edit_form'].elements['sub_category_id'].value ='<?php echo $select_product_by_id->sub_category_id; ?>';
</script>
<script type="text/javascript">
    document.forms['edit_form'].elements['brand_id'].value ='<?php echo $select_product_by_id->brand_id ;?>';
</script>
<script type="text/javascript">
    document.forms['edit_form'].elements['vendor_store_id'].value ='<?php echo $select_product_by_id->vendor_store_id;?>';
</script>
<script type="text/javascript">
    document.forms['edit_form'].elements['top_brand_id'].value ='<?php echo $select_product_by_id->top_brand_id;?>';
</script>
 <script src="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/js/bootstrap-select.js"></script>
<script type="text/javascript">
    document.forms['edit_form'].elements['sub3_id'].value ='<?php echo $select_product_by_id->sub_category_three_id;?>';
</script>
 <script type="text/javascript">

 $('#edit_form').submit(function() {
   // alert("ULA ULA");
    $('#edit_form select').prop('disabled',false);
    return true; // return false to cancel form action
});

$(function () {
    //Initialize Select2 Elements
    $(".select2").select2();
    });

</script>