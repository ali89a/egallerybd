<!DOCTYPE html>


<html>
    <head>
        <title>Online Shopping | Egallerybd</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="icon" href="img/index.ico" type="image/x-icon">
        <link rel="shortcut icon" href="<?php echo base_url() ?>front_asset/img/favicon.ico" type="image/x-icon">
	<link rel="icon" href="<?php echo base_url() ?>front_asset/img/favicon.ico" type="image/x-icon">
        <link href="<?php echo base_url() ?>front_asset/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>front_asset/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>front_asset/css/style.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>front_asset/themes/default/default.css" type="text/css" media="screen" />
        <link href="<?php echo base_url() ?>front_asset/css/responsive_for_all_device.css" rel="stylesheet" type="text/css"/>


        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>

        <style>

            /* Note: Try to remove the following lines to see the effect of CSS positioning */
            .affix {
                top: 0;
                width: 100%;
                
            }

            .affix + .container-fluid {
                padding-top: 70px;
            }
        </style>
        <style>
            #loading {
                position: absolute; width: 100%; height: 350px;background: url('img/load.gif') no-repeat center;
            }
            #totop{
                position: fixed;
                bottom: 10px;
                right: 5px;
                cursor: pointer;
                display: none;
            }
        </style>
         <style>
            .msg_fadeout a{
                color: red!important;
}
            div.fixed {
                position: fixed;
                top: 0;
                right: 500px;
                width: 400px;
                z-index: 2000;
            }
            
            #search_result_ul{
/*                background-color: #7FA538;*/
                max-height:400px;
               
                padding: 0;margin: 0;
                z-index: 2001;
            }
            .search_result_li{
                 background-color: #00a65a;
                  padding: 10px;
                 list-style: none; overflow: auto;
            }
            .search_result_li a{
               color: white!important;
            }
            .search_result_li:hover{
                  background-color: #7FA43A;
                 
            }
            .search_result_li a:hover{
               
                  color: white!important;
            }
        </style>
        <script type="text/javascript">
        $(function() {
            $('.msg_fadeout').delay(1000).show().fadeOut('slow');
        });
        
        
        
        function search_product(){
            var search_text=$('#search_text').val();
         //   alert(search_text);
             if(search_text !=''){
      $.ajax({
                type: 'POST',
                url: '<?php echo site_url('welcome/ajax_search_product_name') ?>',
                data: {
                    search_text: search_text
                  
                }
            }).done(function (result) {
               
                $('#search_result').html(result);
              
                console.log(result);
            });
            }
            else{
             
             $('#search_result').html('');
            }
        
            
            
        }
        </script>
       
        
        <script src="<?php echo base_url() ?>front_asset/js/jssor.slider-23.1.0.min.js" type="text/javascript"></script>
        <script type="text/javascript">
            jssor_1_slider_init = function () {

                var jssor_1_SlideshowTransitions = [
                    {$Duration: 1200, x: 0.2, y: -0.1, $Delay: 20, $Cols: 8, $Rows: 4, $Clip: 15, $During: {$Left: [0.3, 0.7], $Top: [0.3, 0.7]}, $Formation: $JssorSlideshowFormations$.$FormationStraightStairs, $Assembly: 260, $Easing: {$Left: $Jease$.$InWave, $Top: $Jease$.$InWave, $Clip: $Jease$.$OutQuad}, $Outside: true, $Round: {$Left: 1.3, $Top: 2.5}},
                    {$Duration: 1500, x: 0.3, y: -0.3, $Delay: 20, $Cols: 8, $Rows: 4, $Clip: 15, $During: {$Left: [0.1, 0.9], $Top: [0.1, 0.9]}, $SlideOut: true, $Formation: $JssorSlideshowFormations$.$FormationStraightStairs, $Assembly: 260, $Easing: {$Left: $Jease$.$InJump, $Top: $Jease$.$InJump, $Clip: $Jease$.$OutQuad}, $Outside: true, $Round: {$Left: 0.8, $Top: 2.5}},
                    {$Duration: 1500, x: 0.2, y: -0.1, $Delay: 20, $Cols: 8, $Rows: 4, $Clip: 15, $During: {$Left: [0.3, 0.7], $Top: [0.3, 0.7]}, $Formation: $JssorSlideshowFormations$.$FormationStraightStairs, $Assembly: 260, $Easing: {$Left: $Jease$.$InWave, $Top: $Jease$.$InWave, $Clip: $Jease$.$OutQuad}, $Outside: true, $Round: {$Left: 0.8, $Top: 2.5}},
                    {$Duration: 1500, x: 0.3, y: -0.3, $Delay: 80, $Cols: 8, $Rows: 4, $Clip: 15, $During: {$Left: [0.3, 0.7], $Top: [0.3, 0.7]}, $Easing: {$Left: $Jease$.$InJump, $Top: $Jease$.$InJump, $Clip: $Jease$.$OutQuad}, $Outside: true, $Round: {$Left: 0.8, $Top: 2.5}},
                    {$Duration: 1800, x: 1, y: 0.2, $Delay: 30, $Cols: 10, $Rows: 5, $Clip: 15, $During: {$Left: [0.3, 0.7], $Top: [0.3, 0.7]}, $SlideOut: true, $Reverse: true, $Formation: $JssorSlideshowFormations$.$FormationStraightStairs, $Assembly: 2050, $Easing: {$Left: $Jease$.$InOutSine, $Top: $Jease$.$OutWave, $Clip: $Jease$.$InOutQuad}, $Outside: true, $Round: {$Top: 1.3}},
                    {$Duration: 1000, $Delay: 30, $Cols: 8, $Rows: 4, $Clip: 15, $SlideOut: true, $Formation: $JssorSlideshowFormations$.$FormationStraightStairs, $Assembly: 2049, $Easing: $Jease$.$OutQuad},
                    {$Duration: 1000, $Delay: 80, $Cols: 8, $Rows: 4, $Clip: 15, $SlideOut: true, $Easing: $Jease$.$OutQuad},
                    {$Duration: 1000, y: -1, $Cols: 12, $Formation: $JssorSlideshowFormations$.$FormationStraight, $ChessMode: {$Column: 12}},
                    {$Duration: 1000, x: -0.2, $Delay: 40, $Cols: 12, $SlideOut: true, $Formation: $JssorSlideshowFormations$.$FormationStraight, $Assembly: 260, $Easing: {$Left: $Jease$.$InOutExpo, $Opacity: $Jease$.$InOutQuad}, $Opacity: 2, $Outside: true, $Round: {$Top: 0.5}},
                    {$Duration: 2000, y: -1, $Delay: 60, $Cols: 15, $SlideOut: true, $Formation: $JssorSlideshowFormations$.$FormationStraight, $Easing: $Jease$.$OutJump, $Round: {$Top: 1.5}}
                ];

                var jssor_1_options = {
                    $AutoPlay: 1,
                    $SlideshowOptions: {
                        $Class: $JssorSlideshowRunner$,
                        $Transitions: jssor_1_SlideshowTransitions,
                        $TransitionsOrder: 1
                    },
                    $ArrowNavigatorOptions: {
                        $Class: $JssorArrowNavigator$
                    },
                    $BulletNavigatorOptions: {
                        $Class: $JssorBulletNavigator$
                    }
                };

                var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

                /*responsive code begin*/
                /*remove responsive code if you don't want the slider scales while window resizing*/
                function ScaleSlider() {
                    var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
                    if (refSize) {
                        refSize = Math.min(refSize, 600);
                        jssor_1_slider.$ScaleWidth(refSize);
                    } else {
                        window.setTimeout(ScaleSlider, 30);
                    }
                }
                ScaleSlider();
                $Jssor$.$AddEvent(window, "load", ScaleSlider);
                $Jssor$.$AddEvent(window, "resize", ScaleSlider);
                $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
                /*responsive code end*/
            };
        </script>
        <style>
            /* jssor slider bullet navigator skin 01 css */
            /*
            .jssorb01 div           (normal)
            .jssorb01 div:hover     (normal mouseover)
            .jssorb01 .av           (active)
            .jssorb01 .av:hover     (active mouseover)
            .jssorb01 .dn           (mousedown)
            */
            .jssorb01 {
                position: absolute;
            }
            .jssorb01 div, .jssorb01 div:hover, .jssorb01 .av {
                position: absolute;
                /* size of bullet elment */
                width: 12px;
                height: 12px;
                filter: alpha(opacity=70);
                opacity: .7;
                overflow: hidden;
                cursor: pointer;
                border: #000 1px solid;
            }
            .jssorb01 div { background-color: gray; }
            .jssorb01 div:hover, .jssorb01 .av:hover { background-color: #d3d3d3; }
            .jssorb01 .av { background-color: #fff; }
            .jssorb01 .dn, .jssorb01 .dn:hover { background-color: #555555; }

            /* jssor slider arrow navigator skin 05 css */
            /*
            .jssora05l                  (normal)
            .jssora05r                  (normal)
            .jssora05l:hover            (normal mouseover)
            .jssora05r:hover            (normal mouseover)
            .jssora05l.jssora05ldn      (mousedown)
            .jssora05r.jssora05rdn      (mousedown)
            .jssora05l.jssora05lds      (disabled)
            .jssora05r.jssora05rds      (disabled)
            */
            .jssora05l, .jssora05r {
                display: block;
                position: absolute;
                /* size of arrow element */
                width: 40px;
                height: 40px;
                cursor: pointer;
                background: url('img/a17.png') no-repeat;
                overflow: hidden;
            }
            .jssora05l { background-position: -10px -40px; }
            .jssora05r { background-position: -70px -40px; }
            .jssora05l:hover { background-position: -130px -40px; }
            .jssora05r:hover { background-position: -190px -40px; }
            .jssora05l.jssora05ldn { background-position: -250px -40px; }
            .jssora05r.jssora05rdn { background-position: -310px -40px; }
            .jssora05l.jssora05lds { background-position: -10px -40px; opacity: .3; pointer-events: none; }
            .jssora05r.jssora05rds { background-position: -70px -40px; opacity: .3; pointer-events: none; }
        </style>


    </head>
    <body>
        <div class="container-fluid" style="background-color: #7FA43A">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <div class="user-menu">
                            <ul>
                                <li><a href="#">Everyday (9am-10pm)</a></li><li>|</li>
                                <li><a href="#"><b>Email:</b> info@egallerybd.com</a></li>

                            </ul>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="user-menu pull-right">
                            <ul>
                                <li><a href="#"><i class="fa fa-phone"></i> +8801881202020</a></li>

                                <li>|</li>

                                <li><a href="#"><i class="fa fa-envelope"></i> 01881117777</a></li>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="pull-right msg_fadeout fixed">
          <?php 
          if($this->session->flashdata('success'))
              { 
              ?>
        <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
        </div>

    <?php } 
    else if($this->session->flashdata('error'))
        {  
        ?>
 
            <div class="alert alert-danger alert-dismissable">
                 <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                 <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
            </div>

    <?php 
    
        } 
    else if($this->session->flashdata('warning'))
        {  
        ?>

        <div class="alert alert-warning">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Warning!</strong> <?php echo $this->session->flashdata('warning'); ?>
        </div>

    <?php } 
    else if($this->session->flashdata('info'))
        {  
        ?>

        <div class="alert alert-info">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>
        </div>
    <?php 
    
        } 
    ?>

        </div>
        <div class="container" style="color:#fff;">
            <div class="row">
                <div class="col-md-3 col-xs-3 logopad">
                    <div style="padding-top: 6px; padding-bottom: 6px;">
                        <a href="<?php echo base_url() ?>"><img src="<?php echo base_url() ?>front_asset/img/finallogo.png" alt="" class="img-responsive"></a>
                    </div>

                </div>
                <div class="col-md-6 col-xs-9">

                    <div style="padding-top: 6px; padding-bottom: 6px;">
                       <?php
                        $top_ad = $this->db->get_where('ad_management', array('ad_position' => 1, 'is_active' => 1))->row();
                        if (count($top_ad) > 0) {
                            ?>
                        <a href="<?php echo base_url() ?>whole_supply"><img src="<?php echo base_url() . $top_ad->ad_file; ?>" alt="" class="img-responsive"></a>
                        <?php } else {
                            ?>
                        <a href="#"> <img src="<?php echo base_url() ?>front_asset/img/add.jpg" alt="" class="img-responsive"></a>
                        <?php } ?>
                    </div>
                </div>

                
                <div class="col-md-3 col-xs-12">
                    <div style="padding-top: 6px; padding-bottom: 6px;">
                        <div style="float: left;">
                            <img src="<?php echo base_url() ?>front_asset/img/cu.png"  alt="" class="img-responsive" style="">  
                        </div>
                        <div style="float: left;margin-left: 10px;margin-bottom: 10px;">
                            <?php 
                            if ($this->session->userdata('customer_id')) {?>
                            <a href="#" class=""><span  style="color: red"><?php echo $this->session->userdata('customer_name');?></span></a><span style="color: black">&nbsp;or&nbsp;</span><a class="" href="<?php echo base_url() ?>welcome/customer_logout" ><span style="color: red">Logout</span> </a>
                        
                        <?php }else{?>
                             <a href="#" class="" data-toggle="modal" data-target="#login-modal"><span  style="color: red">Sign in</span></a><span style="color: black">&nbsp;or&nbsp;</span><a class="" href="#" data-toggle="modal" data-target="#register-modal"><span style="color: red">Register</span> </a>
                        
                            
                        <?php }?>
                        </div>
                        <div style="margin-top:5px">
                            <?php 
                            if($this->session->userdata('customer_id')== NULL){
                            ?>
                            <a  href="<?php echo base_url(); ?>">
                                <span style="margin-left:12px;padding: 5px;" class="label label-success">
                                    My Account
                                </span>
                            </a>
  <?php 
                            }else{
                            ?>
                               <a href="<?php echo base_url(); ?>welcome/account">
                                <span style="margin-left:12px;padding: 5px;" class="label label-success">
                                    My Account
                                </span>
                            </a>
                            
                              <?php 
                            }
                            ?>
                        </div>
                    </div>

                </div>
            </div>

        </div>
  

        <div class="container-fluid">



            <div id="login-modal" class="modal fade" role="dialog">
                <div class="modal-dialog margin-auto">
                    <div class="modal-content">
                        <div class="modal-body">
                            <h2 class="text-center text-uppercase">Login to Your Account</h2>
                            <form  action="<?php echo base_url();?>welcome/customer_login_check" method="post">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="email" id="email" placeholder="Email Address/ Mobile">
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" name="password" id="password" placeholder="Password">
                                </div>
                                <div class="form-group">
                                    <input type="submit" class="form-control btn-primary" value="Login">
                                </div>
                            </form>
                            <p><a class="small modal-links" href="#" data-dismiss="modal" data-toggle="modal" data-target="#register-modal">Register</a><a class="small modal-links" href="#" data-dismiss="modal" data-toggle="modal" data-target="#forget-modal">Forget Password</a></p>
                        </div>
                    </div>
                </div>
            </div>
            <div id="register-modal" class="modal fade" role="dialog">
                <div class="modal-dialog margin-auto">
                    <div class="modal-content">
                        <div class="modal-body">
                            <h2 class="text-center text-uppercase">Register to Your Account</h2>
                            <form action="<?php echo base_url();?>welcome/customer_registration" method="post">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="name" placeholder="Name" name="name">
                                </div> 
                                <div class="form-group">
                                    <input type="text" class="form-control" id="mobile" placeholder="Mobile" name="mobile">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="address" placeholder="address" name="address">
                                </div>
                                <div class="form-group">
                                    <input type="date" class="form-control" id="dob" placeholder="Date Of Birth" name="dob">
                                </div>
                              
                                <div class="form-group">
                                    <input type="text" class="form-control" id="email" placeholder="Email" name="email">
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" id="password" placeholder="Password" name="password">
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" id="confirm_password" placeholder="Confirm Password" name="confirm_password">
                                </div>
                                <div class="form-group">
                                    <input type="submit" class="form-control btn-primary" value="Register">
                                </div>
                            </form>
                            <p><a class="small modal-links" href="#" data-dismiss="modal" data-toggle="modal" data-target="#login-modal">Login</a><a class="small modal-links" href="#" data-dismiss="modal" data-toggle="modal" data-target="#forget-modal">Forget Password</a></p>
                        </div>
                    </div>
                </div>
            </div>
            <div id="forget-modal" class="modal fade" role="dialog">
                <div class="modal-dialog margin-auto">
                    <div class="modal-content">
                        <div class="modal-body">
                            <h2 class="text-center text-uppercase">Forget Your Password</h2>
                            <form>
                                <p class="small">Enter your Email Address that you used to register. We'll send you an email with your username and a link to reset your password.</p>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="email" placeholder="Email">
                                </div>
                                <div class="form-group">
                                    <input type="submit" class="form-control btn-primary" value="Submit">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>


        </div>

        <nav class="navbar navbar-default" data-spy="affix" data-offset-top="195" style="z-index: 1000;">
            <div class="container">

                <div class="navbar-header">
                    <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".js-navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a style="color:#82A33A;" class="navbar-brand" href="<?php echo base_url() ?>">eGallerybd</a>
                </div>


                
                <div class="collapse navbar-collapse js-navbar-collapse">
                    <ul class="nav navbar-nav">

                        <li class="active">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Shop All Categories  <b class="caret"></b></a>
                           <ul class="dropdown-menu">
                               <?php foreach ($all_published_category as $category) { ?>

                                   <li class="dropdown-submenu">
                                       <a href="#" class="dropdown-toggle" data-toggle="dropdown">

                                      <?php echo $category->category_name;?>

                                       </a>
                                       <ul class="dropdown-menu">
                                           
                                             <?php
                                            $sub_categorys = $this->Home_model->get_sub_category_by_cat_id($category->category_id);
                                            foreach ($sub_categorys as $sub_category) {
                                                ?>
                                           
                                           <li class="dropdown-submenu">
                                               <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                <?php echo $sub_category->sub_cat_name;?>
                                               </a>
                                               <ul class="dropdown-menu">
                                                    <?php
                                                        $brands = $this->Home_model->get_brand_by_sub_cat_id($sub_category->sub_category_id);
                                                        foreach ($brands as $brand) {
                                                            ?>
                                                   
                                                   <li class="dropdown-submenu">
                                                       <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                        <?php echo $brand->brand_name;?>
                                                       </a>
                                                       <ul class="dropdown-menu">
                                                           
                                                            <?php
                                                        $sub_categorytree = $this->Home_model->get_sub_category_three_by_brand_id($brand->brand_id);
                                                        foreach ($sub_categorytree as $sub_cat_tree) {
                                                            ?>
                                                           <li>
                                                               <a href="<?php echo base_url();?>welcome/brand_product/<?php echo $sub_cat_tree->sub_category_three_id;?>">
                                                                   <?php echo $sub_cat_tree->sub_category_three_name;?>
                                                               </a>
                                                           </li>
                                                            <?php } ?>
                                                       </ul>
                                                   </li>
                                                      <?php } ?>
                                               </ul>
                                           </li>
                                           <?php } ?>
                                       </ul>
                                   </li>
                               <?php } ?>
                    </ul>
              
                        </li>



                        <li class=""> <a href="<?php echo base_url() ?>welcome/gallery">Gallery</a> </li>

                        <li class=""> <a href="<?php echo base_url() ?>welcome/contact">Contact Us</a> </li>
                        <li class=""> <a href="<?php echo base_url() ?>welcome/cart">Cart(<span style="color:red"><?php echo $this->cart->total_items(); ?></span>)</a> </li>






                    </ul>




                    <form class="navbar-form" >
                        <div class="input-group">     

                            <input id="search_text" onkeyup="search_product()" type="text" class="form-control" style=""  placeholder="Search by Product">                            
                             <div id="search_result" style="position: absolute; top: 50px;left: 20px;"></div>
                        </div> 
                      
                    </form>   

                </div>
                <!-- /.nav-collapse -->
                <style>
                    #search_text{
                        width:400px; margin-left: 20px;border-radius: 5px; 
                    }
                    
                </style>
            </div>
        </nav>

        <!--------------->
     
            <?php echo $home_content; ?>
       
        


        <!--------------->

        <div class="container-fluid big-footer-area"  style="background: #140f15; margin-top: 50px;">
            <div class="row">

                <div class="col-md-3 col-xs-12">
                    <!--		    <h4 class="text-center"  style="padding: 10px 5px 5px;">Follow US</h4>-->
                    <div class="footer-menu">
                        <h3 class="footer-wid-title">EgalleryBD</h3>
                        <p>Sign up to our newsletter and get exclusive deals you wont find anywhere else straight to your inbox!</p>                   
                        <div class="newsletter-form">
                            <form class="form-horizontal" action="<?php echo base_url();?>Welcome/newslatter_save" method="post" id="form">
                        
                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <input type="email" class="form-control" id="email" name="email" placeholder="Enter email">
                                    </div>

                                </div>

                                <div class="form-group"> 
                                    <div class=" col-sm-12">
                                        <button id="btn" type="submit" class="btn btn-success btn-block">Submit</button>
                                    </div>
                                </div>
                            </form>
                             <div class="success">
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-xs-12">
                    <!--		    <h4 class="text-center"  style="padding: 10px 5px 5px;">Follow US</h4>-->
                    <div class="footer-menu">
                        <h3 class="footer-wid-title">Quick Links</h3>
                        <div class="text-center" >
                            <span ><a style="color: #fff!important;"  href="<?php echo base_url() ?>whole_supply">Whole Supply</a></span><br>
                            <span ><a style="color: #fff!important;"  href="<?php echo base_url() ?>welcome/about">About us</a></span><br>
                            <span><a style="color: #fff!important;" href="<?php echo base_url() ?>welcome/career">Career</a></span><br>
                            <span><a style="color: #fff!important;" href="<?php echo base_url() ?>welcome/return_policy">Return Policy</a></span><br>
                            <span><a style="color: #fff!important;" href="<?php echo base_url() ?>welcome/term_condition">Term Condition</a></span><br>
                            
                            <span><a style="color: #fff!important;" href="<?php echo base_url() ?>welcome/other_business">Our Other Business</a></span><br>
                        </div>

                    </div>
                </div>

                <div class="col-md-3 col-xs-12">
                    <h3 class="footer-wid-title">Contact</h3>

                    <p>Trusted online market place with home delivery service. </p>
                    <p class="text-center" style="border-bottom:1px #7FA43A solid; "><b><< Our Office/Display Center >></b></p>
                    <p class="text-center">Muktobangla Shopping Complex(3rd Floor)</p> <p class="text-center">Mirpur-1, Dhaka-1216.</p>

                </div>

                <div class="col-md-3 col-xs-12">
                    <!--		    <h4 class="text-center"  style="padding: 10px 5px 5px;">Follow US</h4>-->
                    <div class="footer-menu">
                        <h3 class="footer-wid-title">Total Visitors</h3>
                        <div class="newsletter-form">
                            <div style="text-align: center;background-color: #7FA538;padding: 15px 60px;margin: 5px 20px; box-shadow: 0px 0px 5px 1px rgba(15,11,15,1); ">
                                <span style="font-size: 28px;text-align: center;color: white;"><?php echo $visitor_count->total_count; ?></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid footer-bottom-area ">

            <div class="row">
                <div class="col-md-12">
                    <div class="copyright">
                        <p>&copy; 2017 EgalleryBD.com. All Rights Reserved. Design & Development By <a href="http://protivait.com/" target="_blank"> Protiva IT</a></p>
                    </div>
                </div>


            </div> <!-- End footer bottom area -->
        </div>


        <script src="<?php echo base_url() ?>front_asset/js/bootstrap.min.js"></script>
        <script type="text/javascript">

            $(document).ready(function () {
                $("#myBtn").click(function () {
                    $("#myModal").modal();
                });
            });
        </script>
        <script type="text/javascript">
            $(window).onload(function () {
                $('#loading').hide();
            });
        </script>

        <script type="text/javascript">
            jQuery(document).on('click', '.mega-dropdown', function (e) {
                e.stopPropagation()
            })
        </script> 
<script>
    $(document).ready(function () {
        $('#btn').click(function (event) {
            event.preventDefault();
            var email = $('#form').serializeArray();

            $.post(
                    $('#form').attr('action'),
                    email,
                    function (data) {
                        $('.success').html(data);
                        $('.success').fadeOut(3000);
                        $('input').val("");
                    }
            )
        })
    });
</script>
        <script>
            $(document).ready(function () {
                $('body').append(
                        '<a href="https://www.facebook.com/" target="_blank"><div id="facebook" title="Facebook" class=""><i class="fa fa-facebook" aria-hidden="true"></i></div></a>',
                        '<a href="https://www.youtube.com/" target="_blank"><div id="youtube"  title="Youtube" class=""><i class="fa fa-youtube" aria-hidden="true"></i></div></a>',
                        '<a href="https://www.linkedin.com/" target="_blank"><div id="linkedin" title="Linkedin" class=""><i class="fa fa-linkedin" aria-hidden="true"></i></div></a>',
                        '<a href="https://www.twitter.com/" target="_blank"><div id="twitter" title="Twitter" class=""><i class="fa fa-twitter" aria-hidden="true"></i></div></a>',
                        '<a  href="<?php echo base_url()?>welcome/cart" id="btncart" class="cart"><div id="cart" title="Shopping-Cart" class="" style="text-align:center;"><i class="fa fa-shopping-cart" aria-hidden="true"></i><br>Item(<?php echo $this->cart->total_items(); ?>)<br><span>BDT <?php echo $this->cart->total(); ?></span></div></a>'
                        );
            });
        </script>
        <script>
            $(document).ready(function () {
                $('body').append('<div id="totop" class="btn btn-primary"><span class="glyphicon glyphicon-arrow-up"></span>Back to top</div>');
                $(window).scroll(function () {
                    if ($(this).scrollTop() !== 0) {
                        $('#totop').fadeIn();
                    } else {
                        $('#totop').fadeOut();
                    }
                });
                $('#totop').click(function () {
                    $('html, body').animate({scrollTop: 0}, 600);
                });
            });
        </script>

      
        <script type="text/javascript">
            $(document).ready(function () {
                if (!isNaN(currentVal)) {
                    // Increment
                    $('input[name=' + fieldName + ']').val(currentVal + 1);
                } else {
                    // Otherwise put a 0 there
                    $('input[name=' + fieldName + ']').val(0);
                }
            });
            // This button will decrement the value till 0
            $(".qtyminus").click(function (e) {
            // Stop acting like a button
            e.preventDefault();
                    // Get the field name
                    fieldName = $(this).attr('field');
                    // Get its current value
                    var currentVal = parseInt($('input[name=' + fieldName + ']').val());
                    // If it isn't undefined or its greater than 0
                    if (!isNaN(currentVal) && currentVal > 0) {
            // Decrement one
            $('input[name=' + fieldName + ']').val(currentVal - 1);
            } else {
            // Otherwise put a 0 there
            $('input[name=' + fieldName + ']').val(0);
            }
            }
            );
            }
            );

        </script>

    </body>
</html>
