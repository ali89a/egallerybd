<style>
    .jumbotron {
        background: #7FA43A;
        color: #FFF;
        border-radius: 0px;
        margin-bottom: 0;
    }
    .jumbotron-sm { padding-top: 24px;
                    padding-bottom: 24px; }
    .jumbotron small {
        color: #FFF;
    }
    .h1 small {
        font-size: 24px;
    }
</style>
<div class="jumbotron jumbotron-sm">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-lg-12">
                <h1 class="h1">
                    Return Policy <small>Feel free to Read More</small></h1>
            </div>
        </div>
    </div>
</div>	
<div class="container">
    <div class="col-md-12">
        <?php
echo $all_career->return_policy_text;
?>
    </div>
</div>
