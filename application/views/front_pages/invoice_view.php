<div class="container">
    <div class="row">
        
        
        <?php 
        $order_info=$this->db->select('*')->from('customer_order')->where('order_id',$this->session->userdata('order_id'))->get()->row();
//        echo '<pre>';
//        print_r($order_info);
        ?>
          <?php
                                         
         $shipping_details=$this->db->select('*')->from('shipping')->where('ship_id',$order_info->shipping_id)->get()->row();
         $payment_details=$this->db->select('*')->from('payment')->where('payment_id',$order_info->payment_id)->get()->row();
//                           echo '<pre>';
//        print_r($shipping_details);
                              $shipping_area=$this->db->select('*')->from('shipping_area')->where('shipping_area_id', $shipping_details->shipping_area_id)->get()->row()->shipping_area;
                            ?>
        <div class="col-xs-12">
            <div class="text-center">
                <i class="fa fa-search-plus pull-left icon"></i>
                <h2>Invoice for purchase #<?php echo $this->session->userdata('order_id');?></h2>
            </div>
            <hr>
            <div class="row">
                <div class="col-xs-12 col-md-3 col-lg-3 pull-left">
                    <div class="panel panel-default height">
                        <div class="panel-heading">Billing Details</div>
                        <div class="panel-body">
                            
                            <strong><?php echo $shipping_details->ship_name;?>:</strong><br>
                           <?php echo $shipping_details->ship_email;?><br>
                           <?php echo $shipping_details->ship_phone;?><br>
                           <?php echo $shipping_details->ship_address;?><br>
                           <?php 
                                                           //  $shipping_area=$this->db->select('*')->from('shipping_area')->where('shipping_area_id', $shipping_details->shipping_area_id)->get()->row()->shipping_area;
                           echo $shipping_area;
                           ?><br>
                           <?php echo $shipping_details->country;?><br>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-3 col-lg-3">
                    <div class="panel panel-default height">
                        <div class="panel-heading">Payment Information</div>
                        <div class="panel-body">
                            <?php 
//                                                       echo '<pre>';
//                                                       print_r($payment_details);
                            ?>
                            <strong>Type Name:</strong> 
                                <?php 
                                if($payment_details->payment_type==1)
                                    {
                                    echo 'Cash On Delivery';
                                }elseif ($payment_details->payment_type==2) {
                                     echo 'Rocket';
                                }elseif ($payment_details->payment_type==3) {
                                     echo 'Bkash';
                                }
                                    ?><br>
                            <strong>Trx Number:</strong><?php echo $payment_details->trx_id?><br>
                            <strong>Date:</strong> <?php echo $payment_details->payment_date?><br>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-3 col-lg-3">
                    <div class="panel panel-default height">
                        <div class="panel-heading">Order Preferences</div>
                        <div class="panel-body">
                            <strong>Discount:</strong> Yes<br>
                            <strong>Express Delivery:</strong> Yes<br>
                            <strong>Order Date:</strong> <?php echo $payment_details->payment_date?><br>
                            <strong>Coupon:</strong> No<br>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-3 col-lg-3 pull-right">
                    <div class="panel panel-default height">
                        <div class="panel-heading">Shipping Address</div>
                        <div class="panel-body">
                          
                            
                            
                            <strong><?php echo $shipping_details->ship_name;?>:</strong><br>
                           <?php echo $shipping_details->ship_email;?><br>
                           <?php echo $shipping_details->ship_phone;?><br>
                           <?php echo $shipping_details->ship_address;?><br>
                           <?php 
                                                           
                           echo $shipping_area;
                           ?><br>
                           <?php echo $shipping_details->country;?><br>
                           
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="text-center"><strong>Order summary</strong></h3>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-condensed">
                            <thead>
                                <tr>
                                    <td><strong>Item Name</strong></td>
                                    <td class="text-center"><strong>Item Price</strong></td>
                                    <td class="text-center"><strong>Item Quantity</strong></td>
                                    <td class="text-right"><strong>Total</strong></td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                        
                                  $order_details=$this->db->select('*')->from('customer_order_details')->where('order_id',$order_info->order_id)->get()->result();
                                foreach ($order_details as $value) {
 //       echo '<pre>';
// print_r($value);
                                ?>
                                <tr>
                                    <td><?php echo $value->product_name;?></td>
                                    <td class="text-center"><?php echo $value->product_price;?></td>
                                    <td class="text-center"><?php echo $value->product_sales_qty;?></td>
                                    <td class="text-right"><?php echo $value->product_price*$value->product_sales_qty;?></td>
                                </tr>
                                   <?php } ?>
                            
                                <tr>
                                    <td class="highrow"></td>
                                    <td class="highrow"></td>
                                    <td class="highrow text-center"><strong>Subtotal</strong></td>
                                    <td class="highrow text-right"><?php echo $order_info->order_total;?></td>
                                </tr>
                                <tr>
                                    <td class="emptyrow"></td>
                                    <td class="emptyrow"></td>
                                    <td class="emptyrow text-center"><strong>Shipping(+)</strong></td>
                                    <td class="emptyrow text-right">
                                        <?php 
                                         $shipping_charge=$this->db->select('*')->from('shipping_cost')->where('shipping_area_id',$shipping_details->shipping_area_id)->get()->row();
                             
                                    echo $shipping_charge->ship_cost_amt;
                                    
                                    ?>
                                    
                                    
                                    </td>
                                </tr>
                                <tr>
                                    <td class="emptyrow"></td>
                                    <td class="emptyrow"></td>
                                    <td class="emptyrow text-center"><strong>Discount(-)</strong></td>
                                    <td class="emptyrow text-right"><?php echo $order_info->coupon_discount;?></td>
                                </tr>
                                <tr>
                                    <td class="emptyrow"><i class="fa fa-barcode iconbig"></i></td>
                                    <td class="emptyrow"></td>
                                    <td class="emptyrow text-center"><strong>Total</strong></td>
                                    <td class="emptyrow text-right"><?php echo $order_info->order_total-$order_info->coupon_discount+$shipping_charge->ship_cost_amt;?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<style>
.height {
    min-height: 200px;
}

.icon {
    font-size: 47px;
    color: #5CB85C;
}

.iconbig {
    font-size: 77px;
    color: #5CB85C;
}

.table > tbody > tr > .emptyrow {
    border-top: none;
}

.table > thead > tr > .emptyrow {
    border-bottom: none;
}

.table > tbody > tr > .highrow {
    border-top: 3px solid;
}
</style>