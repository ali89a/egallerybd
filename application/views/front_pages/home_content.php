
<div class="container">
    <div class="row">
        <div class="col-md-2">
            <?php
            if (!empty($bigoffer_products)) {
                ?>

                <div class="row">
                <?php
                foreach ($bigoffer_products as $bigoffer_product) {
                    ?>
                        <div class="col-md-12" style=" margin-bottom: 2px;">
                            <div class="single-product">
                                <div class="product-f-image">
                                    <img src="<?php echo base_url() . $bigoffer_product->product_img_master ?>" alt="" class="img-responsive">
                                    <div class="product-hover">
                                        <!--				<a href="" class="add-to-cart-link"><i class="fa fa-shopping-cart"></i> Add to cart</a>-->
                                        <a href="<?php echo base_url() ?>welcome/product_details/<?php echo $bigoffer_product->product_id; ?>" class="view-details-link"><i class="fa fa-eye"></i>View</a>
                                    </div>
                                </div>

                                <a href="<?php echo base_url() ?>welcome/product_details/<?php echo $bigoffer_product->product_id; ?>" class="product-carousel-price"> <h5  style=" margin-top: 5px; font-size:12px; "><?php echo $bigoffer_product->product_name; ?></h5></a>
                                <span>Code:  <?php echo $bigoffer_product->product_code; ?></span>	
                            </div>
                            <div class="product-carousel-price">

                                <span  style=" font-size:12px; "> <ins>৳&nbsp;<?php echo $bigoffer_product->current_sale_price; ?></ins> 
        <?php
        if ($bigoffer_product->discount > 0) {
            ?>
                                        <del style="color:red;"><?php echo $bigoffer_product->product_price; ?></del>
        <?php }
        ?>
                                </span>

                            </div> 
                            <a href="<?php echo base_url(); ?>welcome/add_to_cart/<?php echo $bigoffer_product->product_id ?>" class="btn btn-default addcart" style="padding: 1px 5px;margin-bottom: 10px;"><i class="fa fa-shopping-cart"></i> Add to cart</a>
                            <br>
                            <div style="border-top: 3px solid #D9230F;padding: 5px;">
                            </div>
                            <a href="<?php echo base_url(); ?>welcome/view_all_big_offer_product" class="label label-success" style="border-top: 1px solid #DAEAE0 !important;">All Big Offer Products >></a>
        <?php
        if ($bigoffer_product->discount > 0) {
            ?>
                                <div class="dis">
                                    <p style=" padding: 2px 7px; color: white;text-align: center ">
                                        <span  style=" font-size:12px; ">
                                <?php echo $bigoffer_product->discount ?>%&nbsp;
                                        </span> <br>OFF
                                    </p>
                                </div>
        <?php }
        ?>
                        </div>

        <?php
    }
    ?>
                </div>

                <?php } else { ?>

                <div class="row">
                    <div class="col-md-12" style=" margin-bottom: 2px;">
                        <div class="single-product">
                            <div class="product-f-image">
                                <img src="<?php echo base_url() ?>front_asset/default_image/product_image/EG-01.jpg" alt="" class="img-responsive">
                                <div class="product-hover">
                                    <!--				<a href="" class="add-to-cart-link"><i class="fa fa-shopping-cart"></i> Add to cart</a>-->
                                    <a href="" class="view-details-link"><i class="fa fa-eye"></i>View</a>
                                </div>
                            </div>


                            <a href="" class="product-carousel-price"> <h5  style=" margin-top: 5px; font-size:12px; ">Product Name</h5></a>
                            <span>Code:  101</span>	
                        </div>
                        <div class="product-carousel-price">

                            <span  style=" font-size:12px; "> <ins>৳&nbsp;4000</ins> 

                                <del style="color:red;">5000</del>
                            </span>

                        </div> 
                        <a href="" class="btn btn-default addcart" style="padding: 1px 5px;margin-bottom: 10px;"><i class="fa fa-shopping-cart"></i> Add to cart</a>
                        <br>
                        <div style="border-top: 3px solid #D9230F;padding: 5px;">
                        </div>
                        <a href="" class="label label-success" style="border-top: 1px solid #DAEAE0 !important;">All Big Offer Products >></a>

                        <div class="dis">
                            <p style=" padding: 2px 7px; color: white;text-align: center ">
                                <span  style=" font-size:12px; ">
                                    50%&nbsp;
                                </span> <br>OFF
                            </p>
                        </div>

                    </div>
                </div>
<?php } ?>
        </div>

        <div class="col-md-7 hidden-xs">
            <div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:500px;height:250px;overflow:hidden;visibility:hidden;" class="img-responsive">
                <!-- Loading Screen -->
                <div data-u="loading" style="position:absolute;top:0px;left:0px;background-color:rgba(0,0,0,0.7);">
                    <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
                    <div style="position:absolute;display:block;background:url('<?php echo base_url() ?>front_asset/img/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
                </div>
                <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:500px;height:250px;overflow:hidden;">
<?php
if (!empty($slider_images)) {
    ?>
    <?php foreach ($slider_images as $slider_image) { ?>
                            <div>
                                <img data-u="image" src="<?php echo base_url() . $slider_image->file_url ?>" alt="">
                            </div>
                        <?php } ?>

                    <?php } else { ?>
                        <div>
                            <img data-u="image" src="<?php echo base_url() ?>front_asset/default_image/slider_image/04.jpg" alt="">
                        </div>

                    <?php } ?>
                </div>
                <!-- Bullet Navigator -->
                <div data-u="navigator" class="jssorb01" style="bottom:5px;right:170px;">
                    <div data-u="prototype" style="width:10px;height:10px;">

                    </div>
                </div>
                <!-- Arrow Navigator -->
                <!--                        <span data-u="arrowleft" class="jssora05l" style="top:0px;left:8px;width:40px;height:40px;" data-autocenter="2"></span>
                                        <span data-u="arrowright" class="jssora05r" style="top:0px;right:33px;width:40px;height:40px;" data-autocenter="2"></span>-->
            </div>
            <script type="text/javascript">jssor_1_slider_init();</script>
        </div>
        <div class="col-md-3" >

            <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fegallerybd%2F&tabs=timeline&width=340&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="280" height="340" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
        </div>
    </div>


</div>


<div class="container" >
    <div class="row">
        <div class="col-md-9">
            <?php 
            if(!empty($letest_products)){
            ?>
            <div class="row" style="">
                <div></div>
                <h4 class="section-title">Latest Products</h4>
<?php
foreach ($letest_products as $letest_product) {
    ?>

                    <div class="col-md-3" style=" margin-bottom: 20px;">
                        <div class="single-product">
                            <div class="product-f-image">
                                <img src="<?php echo base_url() . $letest_product->product_img_master ?>" alt="" class="img-responsive">
                                <div class="product-hover">
                                    <!--				<a href="" class="add-to-cart-link"><i class="fa fa-shopping-cart"></i> Add to cart</a>-->
                                    <a href="<?php echo base_url() ?>welcome/product_details/<?php echo $letest_product->product_id; ?>" class="view-details-link"><i class="fa fa-eye"></i>View</a>
                                </div>
                            </div>

                            <a href="<?php echo base_url() ?>welcome/product_details/<?php echo $letest_product->product_id; ?>" class="product-carousel-price"> <h5  style=" margin-top: 5px;"><?php echo $letest_product->product_name; ?></h5></a>
                            <span>Code: <?php echo $letest_product->product_code; ?></span>		
                        </div>
                        <div class="product-carousel-price">
                            <span  style=" font-size:12px; "> <ins>৳&nbsp;<?php echo $letest_product->current_sale_price; ?></ins> 
    <?php
    if ($letest_product->discount > 0) {
        ?>
                                    <del style="color:red;"><?php echo $letest_product->product_price; ?></del>
    <?php }
    ?>
                            </span>


                        </div> 
                        <a href="<?php echo base_url(); ?>welcome/add_to_cart/<?php echo $letest_product->product_id ?>" class="btn btn-default addcart"><i class="fa fa-shopping-cart"></i> Add to cart</a>
                                <?php
                                if ($letest_product->discount > 0) {
                                    ?>
                            <div class="dis">
                                <p style=" padding: 2px 7px; color: white;text-align: center ">
                                    <span  style=" font-size:12px; ">
        <?php echo $letest_product->discount ?>%
                                    </span> <br>OFF
                                </p>
                            </div>
                        <?php }
                        ?>
                    </div>

<?php } ?>

            </div>
            <?php }?>
<?php
foreach ($all_pub_category as $category) {

    // echo '<pre>';
    //  print_r($category);
    //  exit();
    ?>

                <div class="row" style="">
                    <h4 class="section-title"><?php echo $category->category_name; ?></h4>

                <?php
                $products = $this->Home_model->get_four_product_by_cat_id($category->category_id);
//                        echo '<pre>';
//                print_r($products);
//                exit();     
                foreach ($products as $product) {
                    ?>

                        <div class="col-md-3" style=" margin-bottom: 20px;">
                            <div class="single-product">
                                <div class="product-f-image">
                                    <img src="<?php echo base_url() . $product->product_img_master ?>" alt="" class="img-responsive">
                                    <div class="product-hover">
                                        <!--				<a href="" class="add-to-cart-link"><i class="fa fa-shopping-cart"></i> Add to cart</a>-->
                                        <a href="<?php echo base_url() ?>welcome/product_details/<?php echo $product->product_id; ?>" class="view-details-link"><i class="fa fa-eye"></i>View</a>
                                    </div>
                                </div>

                                <a href="<?php echo base_url() ?>welcome/product_details/<?php echo $product->product_id; ?>" class="product-carousel-price"> <h5  style=" margin-top: 5px;"><?php echo $product->product_name; ?></h5></a>
                                <span>Code: <?php echo $product->product_code; ?></span>		
                            </div>
                            <div class="product-carousel-price">
                                <span  style=" font-size:12px; "> <ins>৳&nbsp;<?php echo $product->current_sale_price; ?></ins> 
        <?php
        if ($product->discount > 0) {
            ?>
                                        <del style="color:red;"><?php echo $product->product_price; ?></del>
        <?php }
        ?>
                                </span>


                            </div> 
                            <a href="<?php echo base_url(); ?>welcome/add_to_cart/<?php echo $product->product_id ?>" class="btn btn-default addcart"><i class="fa fa-shopping-cart"></i> Add to cart</a>

        <?php
        if ($product->discount > 0) {
            ?>
                                <div class="dis">
                                    <p style=" padding: 2px 7px; color: white;text-align: center ">
                                        <span  style=" font-size:12px; ">
                                        <?php echo $product->discount ?>%
                                        </span> <br>OFF
                                    </p>
                                </div>
        <?php }
        ?>
                        </div>

                        <?php } ?> 

                </div>
                <div class="row text-center" style=" margin-top: 20px;margin-bottom: 20px;">
                    <a class="vmore" href="<?php echo base_url(); ?>welcome/category_product/<?php echo $category->category_id ?>" style="border-radius: 5px; margin: 20px;border: 1px solid #7FA43A; padding: 10px 15px;">View More</a>
                </div>
                                    <?php
                                }
                                ?>


        </div>

        <div class="col-md-3">
            <div class="row">
                <h4 class="section-title-video text-center">Video Products</h4>
                <div style="padding:5px 15px;">
                    <div class="">
<?php
if (!empty($youtube_video->file)) {
    ?>
                            <iframe id="ytvideo" src="http://www.youtube.com/embed/<?php echo $youtube_video->file; ?>"
                                    width="260" height="320" frameborder="0" allowfullscreen></iframe>



<?php } else { ?>
                            <iframe id="ytvideo" width="260" height="320" src="https://www.youtube.com/embed/NfrxSSpfjTY?list=RDNfrxSSpfjTY" frameborder="0" allowfullscreen></iframe>
<?php } ?>
                    </div>
                </div>
            </div>
            <style>
                #ytvideo{
                    width: 280px;
                }
            </style>
            <div class="row">

                <div class="col-md-12">


                        <?php
                        $right_ad = $this->db->get_where('ad_management', array('ad_position' => 2, 'is_active' => 1))->result();
                        if (count($right_ad) > 0) {
                            ?>
    <?php
    foreach ($right_ad as $rad) {
        ?>
                            <img src="<?php echo base_url() . $rad->ad_file; ?>" alt="" class="img-responsive">

    <?php } ?>

<?php
} else {
    ?>
                        <img src="<?php echo base_url() ?>front_asset/img/add_image (2).jpg" alt="add image" class="img-responsive">
                    <?php } ?>



                </div>
            </div>
        </div>
    </div>

</div>
<div class="container">
    <div class="row">

        <div class="col-md-12 ">
            <h4 class="section-title">All Categories</h4>
        </div>
                    <?php
                    foreach ($all_pub_category as $category) {
                        ?>
            <div  class="col-md-3 cat_div" style="margin-bottom: 10px;margin-top: 10px;">
                <a href="<?php echo base_url(); ?>welcome/category_product/<?php echo $category->category_id ?>">


                    <div class="well">
    <?php if ($category->category_icon) { ?>
                            <img width="200" height="200" src="<?php echo base_url() . $category->category_icon ?>" class="img-responsive img-rounded center-block" alt="a" />

    <?php } else { ?>
                            <img  src="http://placehold.it/200x200" class="img-responsive img-rounded center-block" alt="a" />
    <?php } ?>
                    </div>        
                    <div class="text-center"><!--                <div style="position: absolute; top:50px;left: 50px;">     
                        -->
                        <h4><?php echo $category->category_name ?></h4>
                    </div>
                </a>
            </div>
        <?php } ?>
    </div>

</div>

<!--        ------------product carosel start-------------------->

<div class="container">
    <div class="row" style="margin: 0;">



        <div class="row">
            <div class="col-md-9">
                <h3>
                    Brand</h3>
            </div>
            <div class="col-md-3">
                <!-- Controls -->
                <div class="controls pull-right hidden-xs">
                    <a class="left fa fa-chevron-left btn btn-success" href="#carousel-example"
                       data-slide="prev"></a><a class="right fa fa-chevron-right btn btn-success" href="#carousel-example"
                       data-slide="next"></a>
                </div>
            </div>
        </div>
        <div id="carousel-example" class="carousel slide hidden-xs" data-ride="carousel">
            <!-- Wrapper for slides -->
            <div class="carousel-inner">

<?php
$all_brand = $this->db->get('top_brand')->result_array();
$i = 1;
$j = 1;
foreach ($all_brand as $brand) {

    if ($i == 1 && $j == 1) {
        echo '<div class="item active"><div class="row">';
    } else if ($i == 1) {
        echo '<div class="item"><div class="row">';
    }
    $j++;
    ?>

                    <div class="col-sm-2">
                        <div class="col-item">
                            <div class="photo">
                                <a href="<?php echo base_url(); ?>welcome/top_brand_product/<?php echo $brand['top_brand_id'] ?>">
                                    <img width="100%" src="<?php echo base_url() . $brand['top_brand_icon'] ?>" class="img-responsive" alt="a">

                                </a>


                            </div>

                        </div>
                    </div>

                    <?php
                    if ($i == 6)
                        echo '</div></div>';
                    $i++;
                    if ($i == 7)
                        $i = 1;
                }
                ?>



            </div>
        </div>
    </div>
</div>


</div>

</div>


<!--        ----------------product carosel end---------------->

<div class="container">
    <div class="row">
        <div class="col-md-12 ">
            <h4 class="section-title">Location Map</h4>
            <div>
                <iframe id="locationmap" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3650.554936286971!2d90.35093481498225!3d23.798857984565377!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755c0e62895f857%3A0x1f085acba0f71e63!2sMukto+Bangla+Shopping+Complex!5e0!3m2!1sen!2s!4v1492367192985" width="1155" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
    </div>

</div>
<style>
    .cat_div:hover{
        transition: all 2s;
        background-color: #7FA43A;color: white!important;
    }
    .vmore:hover{
        color: white;
        background-color: #449D44;
    }
</style>