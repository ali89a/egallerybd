<div class="container">
    <div class="row">
        <div class="col-md-3">
            <div class="table-responsive"> 


                <table class="table table-bordered" width="100%">
                    <thead>
                        <tr style="font-size:12px; background-color: #00A65A;color: white;border-collapse: collapse;">
                            <th colspan="2">
                             
                                 <?php
                                if ($customer_active->img) {
                                    ?>
                                    <img width="100%" alt="" class="img-thumbnail" src="<?php echo base_url() . $customer_active->img; ?>">  

                                    <?php
                                } else {
                                    ?>

                                    <img width="100%" alt="" class="img-thumbnail" src="http://simpleicon.com/wp-content/uploads/user1.svg">  
                                <?php } ?>
                            
                            </th>

                        </tr>
                    </thead>
                    <tbody>

                        <tr>
                            <td colspan="2">
                                <h4 class="text-center">
                                    Personal Information 
                                </h4> 
                            </td>



                        </tr>
                        <tr>
                            <td>
                                Name
                            </td>

                            <td>
                               <?php echo $customer_active->customer_name;?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Email
                            </td>

                            <td>
                                <?php echo $customer_active->customer_email;?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Phone
                            </td>

                            <td>
                               <?php echo $customer_active->mobile;?>
                            </td>



                        </tr>
                        <tr>
                            <td>
                                Address
                            </td>

                            <td>
                                   <?php echo $customer_active->address;?>
                            </td>



                        </tr>


                    </tbody>
                </table>
            </div>

        </div>
        <div class="col-md-9">

<!--            <h2>Dynamic Pills</h2>-->
              <ul class="nav nav-pills">
                <li class="active"><a data-toggle="pill" href="#home">Home</a></li>
                <li><a data-toggle="pill" href="#menu1">Order Info</a></li>
                <li><a data-toggle="pill" href="#menu2">Edit Info</a></li>
                <li><a data-toggle="pill" href="#menu3">Change Password</a></li>
            </ul>

            <div class="tab-content">
                <div id="home" class="tab-pane fade in active">
                    <h3>HOME</h3>
 </div>
                    <div id="menu1" class="tab-pane fade">
                        <h3>Order Manage</h3>
                        <div class="table-responsive"> 


                            <table class="table table-bordered" width="100%">
                                <thead>
                                    <tr style="font-size:12px; background-color: #00A65A;color: white;border-collapse: collapse;">
                                        <th>Order No</th>
                                        <th>Order date</th>
                                        <th>Order Total</th>
                                        <th>Delivery Status</th>
                                        <th class="text-right">Option</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 1;
                                    foreach ($order_customer_active as $value) {
                                        ?>
                                        <tr>
                                            <td><?php echo $value->order_id; ?></td>
                                            <td><?php echo $value->order_date; ?></td>
                                            <td>&#2547; <?php 
                                            
                                             $shipping_area_id=$this->db->get_where('shipping',array('ship_id'=>$value->shipping_id))->row()->shipping_area_id;
                          $amont_ship=$this->db->get_where('shipping_cost',array('shipping_area_id'=>$shipping_area_id))->row()->ship_cost_amt;
                               
                           echo $value->order_total+$amont_ship-$value->coupon_discount; 
                                           // echo $value->order_total; 
                                            
                                            ?>
                                            
                                            </td>
                                            <td style="">
                                                <?php
                                                if ($value->order_status == 0) {
                                                    ?>
                                                    <span class="label label-primary">Pending </span>
                                                    <?php
                                                } else if ($value->order_status == 1) {
                                                    ?> 
                                                    <span class="label label-primary ">Confirmed</span>

                                                <?php } else if ($value->order_status == 2) {
                                                    ?> 
                                                    <span class="label label-success">Delivered</span>

                                                <?php } ?>

                                                <?php
                                                if ($value->order_cancle_req == 1) {
                                                    ?>
                                                    <span class="label label-danger" title="You Sent Order Cancel Request">Cancel</span>
                                                <?php } ?>
                                            </td>
                                            <td class="text-right">
                                                <button title="View Order" data-toggle="modal" data-target="#viewModal" class="btn btn-success btn-xs xz" m_idd="<?php echo $value->order_id; ?>" id="branch_view_id" ><i class="fa fa-eye" aria-hidden="true"></i></button>
                                                <a href="<?php echo site_url("Welcome/download_invoice/$value->order_id") ?>" class="" title="Invoice Download"> <button class="btn btn-primary btn-xs xz" ><i class="fa fa-download" aria-hidden="true"></i></button></a>
                                                <?php
                                                if ($value->order_cancle_req != 1) {
                                                    ?>
                                                    <a title="Cancle Order Request" href="<?php echo site_url("Welcome/cancle_order_request/$value->order_id") ?>" class="" onclick="return confirm('Are you sure?');">  <button class="btn btn-danger btn-xs xz"><i class="fa fa-trash" aria-hidden="true"></i></button></a>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                        <?php
                                        $i++;
                                    }
                                    ?>

                                </tbody>
                            </table>
                        </div> 
                        
                        
                                          <!-- Modal View Order -->
  <div class="modal fade" id="viewModal" role="dialog">
      <div class="modal-dialog modal-lg" style="min-width: 900px!important;">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title text-center">View Order</h4>
        </div>
          <div class="">
               <div id="b_view_show" style="">
<!--                <div class="modal-body">
                    <p>This is a large modal.</p>
                </div>-->
                </div>
          </div>
           
      
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
                    </div>
               
                <div id="menu2" class="tab-pane fade">
                    <h3>Personal Information Edit</h3>

                    <div class="row">
                   
                        <div class="col-sm-6 col-md-8">
                            
                            
                            <form action="<?php echo base_url(); ?>welcome/update_userinfo" method="post" class="form-horizontal">
  <div class="form-group">
    <label class="control-label col-sm-4" for="name">Name:</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="name" name="customer_name" value=" <?php echo $customer_active->customer_name;?>">
    </div>
  </div>
  
  <div class="form-group">
    <label class="control-label col-sm-4" for="email">Email:</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="email" name="customer_email" value=" <?php echo $customer_active->customer_email;?>">
    </div>
  </div>
  
  <div class="form-group">
    <label class="control-label col-sm-4" for="email">Mobile:</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="mobile" name="mobile" value=" <?php echo $customer_active->mobile;?>">
    </div>
  </div>
  
  <div class="form-group">
    <label class="control-label col-sm-4" for="email">Address:</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="address" name="address"  value="<?php echo $customer_active->address;?>">
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-sm-4" for="email">Date Of Birth:</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="dob" name="dob" value="<?php echo $customer_active->dob;?>">
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-sm-4" for="email">Blood Group:</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="blood_group" name="blood_group" value="<?php echo $customer_active->blood_group;?>">
    </div>
  </div>
  
 
  <div class="form-group">
    <div class="col-sm-offset-4 col-sm-8">
      <button type="submit" class="btn btn-success">Update</button>
    </div>
  </div>
</form>
                            
                            
                        </div>
                             <div class="col-sm-6 col-md-4">
                            <div>
                                <?php
                                if ($customer_active->img) {
                                    ?>
                                    <img width="200" alt="" class="img-thumbnail" src="<?php echo base_url() . $customer_active->img; ?>">  

                                    <?php
                                } else {
                                    ?>

                                    <img width="200" alt="" class="img-thumbnail" src="http://simpleicon.com/wp-content/uploads/user1.svg">  
                                <?php } ?>
<!--                                <img src="http://placehold.it/300x300" alt="" class="img-rounded img-responsive" />-->

                            </div> <br>  
                            <div>
                                <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal1">Update Image</button>  </div>

                            <div class="modal fade" id="myModal1" role="dialog">
                                <div class="modal-dialog">
                                    <form action="<?php echo base_url(); ?>welcome/update_userimg" method="post" enctype="multipart/form-data" class="form-horizontal">



                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Update Image</h4>
                                            </div>
                                            <div class="modal-body">
                                           
                                                <div class="form-group">
                                                    <label class="control-label col-sm-2" for="email">Image:</label>
                                                    <div class="col-sm-10">
                                                        <input type="file" name="userFiles[]" multiple id="file">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="clear" class="btn btn-default" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-success">Save</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="menu3" class="tab-pane fade">
                    <h3>Your Change Password</h3>


                    <div class="col-md-12">
                        <div class="row">
                           
                            <div class="col-xs-12 col-sm-12 col-md-8">
                                <form action="<?php echo base_url(); ?>Welcome/customer_password_change" method="post" >
                                    <div style="margin-top: 25px;" class="col-xs-12 col-sm-12 col-md-12 login-box">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></div>
                                                <input class="form-control" type="password" placeholder="Current Password" name="cpass" autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-addon"><span class="glyphicon glyphicon-log-in"></span></div>
                                                <input class="form-control" type="password" placeholder="New Password" name="npass"  autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <button class="btn icon-btn-save btn-success" type="submit">
                                                    <span class="btn-save-label"><i class="glyphicon glyphicon-floppy-disk"></i></span>Update Password</button>
                                            </div>
                                        </div>

                                    </div>
                                </form>

                            </div>



                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>





<style>
    small {
        display: block;
        line-height: 1.428571429;
        color: #999;
    }
    .glyphicon {  margin-bottom: 10px;margin-right: 10px;}
</style>
<script>
    $(document).ready(function () {
     
        $("body").on("click", "#branch_view_id", function () {
            // alert(base_url);
            $(".modal-body").html("--Loading--");
            var b_id = $(this).attr('m_idd');
           // alert(b_id);
            $.ajax({
                url: "<?php echo site_url('welcome/view_order_info');?>",
                data:
                        {
                            b_id: b_id
                        },
                type: "POST",
                success: function (data) {
                    console.log(data);
                    $("#b_view_show").html(data);
                }
            });
        });

    });
</script>