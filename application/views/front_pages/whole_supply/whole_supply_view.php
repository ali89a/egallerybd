<style>
    .jumbotron {
        background: #7FA43A;
        color: #FFF;
        border-radius: 0px;
        margin-bottom: 5px;
    }
    .jumbotron-sm { padding-top: 12px;
                    padding-bottom: 12px; }
    .jumbotron small {
        color: #FFF;
    }
    .h1 {
        text-align: center;
    }
     .jumbotron h3{
           font-size: 35px!important;
    }
</style>

<div class="jumbotron jumbotron-sm">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-lg-12">
                <h3 class="h1">
                  Industrial Product</h3>
            </div>
        </div>
    </div>
</div>

<div class="container">
<ul class="nav nav-pills nav-stacked col-md-2">
  <li class="active"><a href="#tab_a" data-toggle="pill">Pill A</a></li>
  <li><a href="#tab_b" data-toggle="pill">Pill B</a></li>
  <li><a href="#tab_c" data-toggle="pill">Pill C</a></li>
  <li><a href="#tab_d" data-toggle="pill">Pill D</a></li>
   <li><a href="#tab_E" data-toggle="pill">Pill E</a></li>
 
  </ul>
<div class="tab-content col-md-10">
        <div class="tab-pane active" id="tab_a">
             <h4>Pane A</h4>
         
              <div class="row">
                    <div class="col-md-3" style=" margin-bottom: 2px;">
                        <div class="single-product">
                            <div class="product-f-image">
                                <img src="<?php echo base_url() ?>front_asset/default_image/product_image/EG-01.jpg" alt="" class="img-responsive">
                                <div class="product-hover">
                                    <!--				<a href="" class="add-to-cart-link"><i class="fa fa-shopping-cart"></i> Add to cart</a>-->
                                    <a href="" class="view-details-link"><i class="fa fa-eye"></i>View</a>
                                </div>
                            </div>


                            <a href="" class="product-carousel-price"> <h5  style=" margin-top: 5px; font-size:12px; ">Product Name</h5></a>
                            <span>Code:  101</span>	
                        </div>
                        <div class="product-carousel-price">

                            <span  style=" font-size:12px; "> <ins>৳&nbsp;4000</ins> 

                                <del style="color:red;">5000</del>
                            </span>

                        </div> 
                        <a href="" class="btn btn-default addcart" style="padding: 1px 5px;margin-bottom: 10px;"><i class="fa fa-shopping-cart"></i> Add to cart</a>
                       

                       

                    </div>
                    <div class="col-md-3" style=" margin-bottom: 2px;">
                        <div class="single-product">
                            <div class="product-f-image">
                                <img src="<?php echo base_url() ?>front_asset/default_image/product_image/EG-01.jpg" alt="" class="img-responsive">
                                <div class="product-hover">
                                    <!--				<a href="" class="add-to-cart-link"><i class="fa fa-shopping-cart"></i> Add to cart</a>-->
                                    <a href="" class="view-details-link"><i class="fa fa-eye"></i>View</a>
                                </div>
                            </div>


                            <a href="" class="product-carousel-price"> <h5  style=" margin-top: 5px; font-size:12px; ">Product Name</h5></a>
                            <span>Code:  101</span>	
                        </div>
                        <div class="product-carousel-price">

                            <span  style=" font-size:12px; "> <ins>৳&nbsp;4000</ins> 

                                <del style="color:red;">5000</del>
                            </span>

                        </div> 
                        <a href="" class="btn btn-default addcart" style="padding: 1px 5px;margin-bottom: 10px;"><i class="fa fa-shopping-cart"></i> Add to cart</a>
                       

                      

                    </div>
                    <div class="col-md-3" style=" margin-bottom: 2px;">
                        <div class="single-product">
                            <div class="product-f-image">
                                <img src="<?php echo base_url() ?>front_asset/default_image/product_image/EG-01.jpg" alt="" class="img-responsive">
                                <div class="product-hover">
                                    <!--				<a href="" class="add-to-cart-link"><i class="fa fa-shopping-cart"></i> Add to cart</a>-->
                                    <a href="" class="view-details-link"><i class="fa fa-eye"></i>View</a>
                                </div>
                            </div>


                            <a href="" class="product-carousel-price"> <h5  style=" margin-top: 5px; font-size:12px; ">Product Name</h5></a>
                            <span>Code:  101</span>	
                        </div>
                        <div class="product-carousel-price">

                            <span  style=" font-size:12px; "> <ins>৳&nbsp;4000</ins> 

                                <del style="color:red;">5000</del>
                            </span>

                        </div> 
                        <a href="" class="btn btn-default addcart" style="padding: 1px 5px;margin-bottom: 10px;"><i class="fa fa-shopping-cart"></i> Add to cart</a>
                       

                        <div class="dis">
                            <p style=" padding: 2px 7px; color: white;text-align: center ">
                                <span  style=" font-size:12px; ">
                                    50%&nbsp;
                                </span> <br>OFF
                            </p>
                        </div>

                    </div>
                    <div class="col-md-3" style=" margin-bottom: 2px;">
                        <div class="single-product">
                            <div class="product-f-image">
                                <img src="<?php echo base_url() ?>front_asset/default_image/product_image/EG-01.jpg" alt="" class="img-responsive">
                                <div class="product-hover">
                                    <!--				<a href="" class="add-to-cart-link"><i class="fa fa-shopping-cart"></i> Add to cart</a>-->
                                    <a href="" class="view-details-link"><i class="fa fa-eye"></i>View</a>
                                </div>
                            </div>


                            <a href="" class="product-carousel-price"> <h5  style=" margin-top: 5px; font-size:12px; ">Product Name</h5></a>
                            <span>Code:  101</span>	
                        </div>
                        <div class="product-carousel-price">

                            <span  style=" font-size:12px; "> <ins>৳&nbsp;4000</ins> 

                                <del style="color:red;">5000</del>
                            </span>

                        </div> 
                        <a href="" class="btn btn-default addcart" style="padding: 1px 5px;margin-bottom: 10px;"><i class="fa fa-shopping-cart"></i> Add to cart</a>
                       

                       

                    </div>
                    <div class="col-md-3" style=" margin-bottom: 2px;">
                        <div class="single-product">
                            <div class="product-f-image">
                                <img src="<?php echo base_url() ?>front_asset/default_image/product_image/EG-01.jpg" alt="" class="img-responsive">
                                <div class="product-hover">
                                    <!--				<a href="" class="add-to-cart-link"><i class="fa fa-shopping-cart"></i> Add to cart</a>-->
                                    <a href="" class="view-details-link"><i class="fa fa-eye"></i>View</a>
                                </div>
                            </div>


                            <a href="" class="product-carousel-price"> <h5  style=" margin-top: 5px; font-size:12px; ">Product Name</h5></a>
                            <span>Code:  101</span>	
                        </div>
                        <div class="product-carousel-price">

                            <span  style=" font-size:12px; "> <ins>৳&nbsp;4000</ins> 

                                <del style="color:red;">5000</del>
                            </span>

                        </div> 
                        <a href="" class="btn btn-default addcart" style="padding: 1px 5px;margin-bottom: 10px;"><i class="fa fa-shopping-cart"></i> Add to cart</a>
                       

                        <div class="dis">
                            <p style=" padding: 2px 7px; color: white;text-align: center ">
                                <span  style=" font-size:12px; ">
                                    50%&nbsp;
                                </span> <br>OFF
                            </p>
                        </div>

                    </div>
                    <div class="col-md-3" style=" margin-bottom: 2px;">
                        <div class="single-product">
                            <div class="product-f-image">
                                <img src="<?php echo base_url() ?>front_asset/default_image/product_image/EG-01.jpg" alt="" class="img-responsive">
                                <div class="product-hover">
                                    <!--				<a href="" class="add-to-cart-link"><i class="fa fa-shopping-cart"></i> Add to cart</a>-->
                                    <a href="" class="view-details-link"><i class="fa fa-eye"></i>View</a>
                                </div>
                            </div>


                            <a href="" class="product-carousel-price"> <h5  style=" margin-top: 5px; font-size:12px; ">Product Name</h5></a>
                            <span>Code:  101</span>	
                        </div>
                        <div class="product-carousel-price">

                            <span  style=" font-size:12px; "> <ins>৳&nbsp;4000</ins> 

                                <del style="color:red;">5000</del>
                            </span>

                        </div> 
                        <a href="" class="btn btn-default addcart" style="padding: 1px 5px;margin-bottom: 10px;"><i class="fa fa-shopping-cart"></i> Add to cart</a>
                       

                       

                    </div>
                    <div class="col-md-3" style=" margin-bottom: 2px;">
                        <div class="single-product">
                            <div class="product-f-image">
                                <img src="<?php echo base_url() ?>front_asset/default_image/product_image/EG-01.jpg" alt="" class="img-responsive">
                                <div class="product-hover">
                                    <!--				<a href="" class="add-to-cart-link"><i class="fa fa-shopping-cart"></i> Add to cart</a>-->
                                    <a href="" class="view-details-link"><i class="fa fa-eye"></i>View</a>
                                </div>
                            </div>


                            <a href="" class="product-carousel-price"> <h5  style=" margin-top: 5px; font-size:12px; ">Product Name</h5></a>
                            <span>Code:  101</span>	
                        </div>
                        <div class="product-carousel-price">

                            <span  style=" font-size:12px; "> <ins>৳&nbsp;4000</ins> 

                                <del style="color:red;">5000</del>
                            </span>

                        </div> 
                        <a href="" class="btn btn-default addcart" style="padding: 1px 5px;margin-bottom: 10px;"><i class="fa fa-shopping-cart"></i> Add to cart</a>
                       </div>
                </div>
             
             
             
        </div>
        <div class="tab-pane" id="tab_b">
             <h4>Pane B</h4>
            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames
                ac turpis egestas.</p>
        </div>
        <div class="tab-pane" id="tab_c">
             <h4>Pane C</h4>
            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames
                ac turpis egestas.</p>
        </div>
        <div class="tab-pane" id="tab_d">
             <h4>Pane D</h4>
            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames
                ac turpis egestas.</p>
        </div>
        <div class="tab-pane" id="tab_E">
             <h4>Pane E</h4>
            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames
                ac turpis egestas.</p>
        </div>
      
             
</div><!-- tab content -->
</div><!-- end of container -->
<style>
    .nav>li>a:hover {
  
    color: #fff!important;
}
.nav-pills>li.active>a, .nav-pills>li.active>a:focus, .nav-pills>li.active>a:hover {
    color: #fff;
    background-color: #7FA43A;
}
.nav-pills>li, .nav-pills>li>a, .nav-pills>li>a:hover{
     color: #fff;
    background-color: #004444;
}
</style>