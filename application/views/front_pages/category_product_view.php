<style>
    .col-item
{
    border: 1px solid #E1E1E1;
    border-radius: 5px;
    background: #FFF;
}
.col-item .photo img
{
    margin: 0 auto;
    width: 100%;
}

.col-item .info
{
    padding: 10px;
    border-radius: 0 0 5px 5px;
    margin-top: 1px;
}

.col-item:hover .info {
    background-color: #F5F5DC;
}
.col-item .price
{
    /*width: 50%;*/
    float: left;
    margin-top: 5px;
}

.col-item .price h5
{
    line-height: 20px;
    margin: 0;
}

.price-text-color
{
    color: #219FD1;
}

.col-item .info .rating
{
    color: #777;
}

.col-item .rating
{
    /*width: 50%;*/
    float: left;
    font-size: 17px;
    text-align: right;
    line-height: 52px;
    margin-bottom: 10px;
    height: 52px;
}

.col-item .separator
{
    border-top: 1px solid #E1E1E1;
}

.clear-left
{
    clear: left;
}

.col-item .separator p
{
    line-height: 20px;
    margin-bottom: 0;
    margin-top: 10px;
    text-align: center;
}

.col-item .separator p i
{
    margin-right: 5px;
}
.col-item .btn-add
{
    width: 50%;
    float: left;
}

.col-item .btn-add
{
    border-right: 1px solid #E1E1E1;
}

.col-item .btn-details
{
    width: 50%;
    float: left;
    padding-left: 10px;
}
.controls
{
    margin-top: 20px;
}
[data-slide="prev"]
{
    margin-right: 10px;
}



</style>
<?php 
//echo '<pre>';
//print_r($products);
?>
<style>
    .jumbotron {
        background: #7FA43A;
        color: #FFF;
        border-radius: 0px;
        margin-bottom: 0;
    }
    .jumbotron-sm { padding-top: 12px;
                    padding-bottom: 12px; }
    .jumbotron small {
        color: #FFF;
    }
    .h3  {
        text-align: center;
    }
</style>
<div class="jumbotron jumbotron-sm">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-lg-12">
                <h2 class="h3">
                    Categories Product</h2>
            </div>
        </div>
    </div>
</div>	
<div class="container">
    <div class="row">
        <h3 class="text-center"></h3>
    </div>
    <?php 
    if ($products) {
        

    ?>
    <div class="row">
        <?php 
        foreach ($products as $product) {
            
       
        ?>
        <div class="col-sm-3" style="margin-top: 20px">
            <div class="col-item">
                <div class="photo">
                    <a href="<?php echo base_url();?>welcome/product_details/<?php echo $product->product_id;?>" class=""><img src="<?php echo base_url().$product->product_img_master ?>" class="img-responsive" alt="a" /></a>
                </div>
                <div class="info">
                    <div class="row">
                        <div class="price col-md-12">
                            <h5>
                                <?php echo $product->product_name;?></h5>
                            <h5 class="price-text-color">BDT&nbsp;
                            <?php echo $product->current_sale_price;?></h5>
                        </div>
                       
                    </div>
                    <div class="separator clear-left">
                        <p class="btn-add">
                            <i class="fa fa-shopping-cart"></i><a href="<?php echo base_url(); ?>welcome/add_to_cart/<?php echo $product->product_id ?>" class="hidden-sm">Add to cart</a></p>
                        <p class="btn-details">
                            <i class="fa fa-list"></i><a href="<?php echo base_url();?>welcome/product_details/<?php echo $product->product_id;?>" class="hidden-sm">More details</a></p>
                    </div>
                    <div class="clearfix">
                    </div>
                </div>
            </div>
        </div>
        
       <?php 
       }?>
    </div>
    <?php }
    else { ?>
     <div class="row">
        <h3 class="text-center">Not Found</h3>
    </div>
    
    <?php 
    }
    ?>
    </div>
