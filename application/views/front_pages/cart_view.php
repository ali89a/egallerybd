<style>
    .jumbotron {
        background: #7FA43A;
        color: #FFF;
        border-radius: 0px;
        margin-bottom: 5px;
    }
    .jumbotron-sm { padding-top: 12px;
                    padding-bottom: 12px; }
    .jumbotron small {
        color: #FFF;
    }
    .h1 small {
        font-size: 18px;
    }
     .jumbotron h3{
           font-size: 35px!important;
    }
</style>
<div class="jumbotron jumbotron-sm">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-lg-12">
                <h3 class="h1">
                   Shopping  <small> Cart </small></h3>
            </div>
        </div>
    </div>
</div>	
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-md-12 nopad">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <div class="panel-title">
                        <div class="row">
                            <div class="col-xs-12 col-md-9 nopad">
                                <h5><span class="glyphicon glyphicon-shopping-cart"></span> Shopping Cart</h5>
                            </div>
                            <div class="col-xs-12 col-md-3 nopad">
                           
                                    <a class="btn btn-primary btn-sm btn-block" href="<?php echo base_url();?>"><span class="glyphicon glyphicon-share-alt"></span> Continue shopping
                               </a> 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                     <?php 
                                        foreach ($this->cart->contents() as $items){
//                                            echo '<pre>';
//                                        print_r($items);
//                                        exit();
//                                          
                                             ?>
                    <div class="row">
                        <div class="col-xs-6 col-md-1 nopad"><img height="50" width="50" class="img-responsive" src="<?php echo base_url() . $items['image']?>">
                        </div>
                
                        <div class="col-xs-6 col-md-4 nopad">
                            <h4 class="product-name"><strong><?php echo $items['name'] ?></strong></h4><h4><small>Product description</small></h4>
                        </div>
                        <div class="col-xs-12 col-md-7 nopad">
                            <div class="col-xs-4 col-md-4 text-right nopad">
                                <h6><strong>BDT <?php echo $items['price'] ?> <span class="text-danger">&nbsp;X</span></strong></h6>
                            </div>
                            <form action="<?php echo base_url();?>welcome/update_cart" method="post">
                                    <div class="col-xs-3 col-md-2 nopad">
                                        <input type="number" class="form-control input-sm" name="qty" value="<?php echo $items['qty'] ?>" min="1" step="1">
                                    <input type="hidden"  value="<?php echo $items['rowid'] ?>" name="rowid"> 
                                    </div>
                                    <div class="col-xs-3 col-md-2 nopad">
                                        <button type="submit" class="btn btn-info btn-sm">update</button>
                                    </div>
                                </form>
                            
                             <div class="col-xs-3 col-md-2 nopad">
                                <h6><strong>BDT <?php echo $items['price']*$items['qty'] ?> </strong></h6>
                            </div>
                            <div class="col-xs-2 col-md-2 nopad">
                                <a href="<?php echo base_url(); ?>welcome/cart_remove/<?php echo $items['rowid'] ?>" class="btn btn-danger btn-sm">
                                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <hr>
                     <?php  } ?>
                    
                   
                    <div class="row">
                        <div class="">
                            <div class="col-xs-6 col-md-6 nopad">
                                <form action="<?php echo base_url(); ?>welcome/check_coupon_discount" method="post">
                                    <div class="">
                                        <input type="text" placeholder="Coupon code" value="" id="coupon_code" class="input-text" name="coupon_code">
                                        <input type="submit" value="Apply Coupon" name="apply_coupon" class="btn btn-success">
                                    </div> 
                                
                                </form>
                                   
                            </div>
                            <div class="col-xs-6 col-md-6 nopad">
                                 <h4 class="text-right">Sub Total :<strong>&#2547;&nbsp;<?php echo $this->cart->total()?></strong></h4>
                                 
                                  <?php if ($this->session->userdata('coupon_discount') > 0 && $this->cart->total() > 0) {
                                     ?>
                                     <h4 class="text-right">Coupon Discount: <strong>&#2547;&nbsp;<?php echo $this->session->userdata('coupon_discount'); ?></strong></h4>

                                 <?php } ?>
                                 <h4 class="text-right">Total :<strong>&#2547;&nbsp;<?php echo $this->cart->total() - $this->session->userdata('coupon_discount')?></strong></h4>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <div class="row text-center">
                        <div class="col-xs-6 col-md-9 nopad">
                          
                        </div>
                        <div class="col-xs-6 col-md-3 nopad">
                            <a href="<?php echo base_url()?>welcome/checkout" class="btn btn-success btn-block">
                                Checkout
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>