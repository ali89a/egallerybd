<div class="jumbotron jumbotron-sm">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-lg-12">
                <h3 class="h1">
                    Contact us <small>Feel free to contact us</small></h3>
            </div>
        </div>
    </div>
</div>	
<section id="contact" style="">
            <div class="container">
                <div class="row">
                    <div class="about_our_company" style="margin-bottom: 20px;">
                        <h1 style="color:#fff;">Write Your Message</h1>
                        <div class="titleline-icon"></div>
                        <p style="color:#fff;"></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                       <form action="<?php echo base_url();?>Welcome/save_contact" class="form-horizontal" method="post">
                    <fieldset>
                        <legend id="cont" class="text-center header" style="color:#fff;">Contact us</legend>
                        <h4 style="color:green;text-align: center">
                            <?php
                            $msg = $this->session->userdata('message');
                            if ($msg) {
                                echo $msg;
                                $this->session->unset_userdata('message');
                            }
                            ?>
                        </h4>
                        <div class="col-md-6">
                        <div class="form-group">
                            <div class="col-md-10 col-md-offset-1">
                                <input style="background-color: #7FA43A;color: white;" id="fname" name="name" type="text" placeholder="Name" class="form-control" required>
                            </div>
                        </div>
                         <div class="form-group">
                            <div class="col-md-10 col-md-offset-1">
                                <input style="background-color: #7FA43A;color: white;" id="email" name="email" type="text" placeholder="Email Address" class="form-control" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-10 col-md-offset-1">
                                <input style="background-color: #7FA43A;color: white;" id="phone" name="phone" type="text" placeholder="Phone" class="form-control" required>
                            </div>
                        </div></div>
 <div class="col-md-6">
                        <div class="form-group">
                            <div class="col-md-10 col-md-offset-1">
                                <textarea style="background-color: #7FA43A; color: white;" class="form-control" id="message" name="message" placeholder="Enter your massage for us here. We will get back to you within 2 business days." rows="10" required></textarea>
                            </div>
                        </div>
</div>
                        <div class="form-group">
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </fieldset>
                </form>
                    </div>
                    <div class="col-md-4">
                        <p style="color:#fff;">
                            <strong><i class="fa fa-map-marker"></i> Address</strong><br>
                            Muktobangla Shopping Complex(3rd Floor)

Mirpur-1, Dhaka-1216.
                        </p>
                        <p style="color:#fff;"><strong><i class="fa fa-phone"></i> Phone Number</strong><br>
                            

    +8801881202020</p>
                        <p style="color:#fff;">
                            <strong><i class="fa fa-envelope"></i>  Email Address</strong><br>
                            info@egallerybd.com</p>
                        <p></p>
                    </div>
                </div>
            </div>
        </section>
<style>
    section#contact {
    background-color: #222222;
    background-image: url('http://artdnaswitchbd.com/componants/images/map-image.png');
    background-position: center;
    background-repeat: no-repeat;
}
section {
    padding: 30px 0PX;
}
section#contact .section-heading {
    color: white;
}
section#contact .form-group {
    margin-bottom: 25px;
}
section#contact .form-group input,
section#contact .form-group textarea {
    padding: 20px;
}
section#contact .form-group input.form-control {
    height: auto;
}
section#contact .form-group textarea.form-control {
    height: 236px;
}
section#contact .form-control:focus {
    border-color: #fed136;
    box-shadow: none;
}
section#contact ::-webkit-input-placeholder {
    font-family: "Montserrat", "Helvetica Neue", Helvetica, Arial, sans-serif;
    text-transform: uppercase;
    font-weight: 700;
    color: #eeeeee;
}
.gellary_bg_none img{
	width: 100%;
	height: 250px;
}
section#contact :-moz-placeholder {
    /* Firefox 18- */
    font-family: "Montserrat", "Helvetica Neue", Helvetica, Arial, sans-serif;
    text-transform: uppercase;
    font-weight: 700;
    color: #eeeeee;
}
section#contact ::-moz-placeholder {
    /* Firefox 19+ */
    font-family: "Montserrat", "Helvetica Neue", Helvetica, Arial, sans-serif;
    text-transform: uppercase;
    font-weight: 700;
    color: #eeeeee;
}
section#contact :-ms-input-placeholder {
    font-family: "Montserrat", "Helvetica Neue", Helvetica, Arial, sans-serif;
    text-transform: uppercase;
    font-weight: 700;
    color: #eeeeee;
}
section#contact .text-danger {
    color: #e74c3c;
}

.about_our_company{
    text-align: center;
}
.about_our_company h1{
    font-size: 25px;
}
.titleline-icon {
    position: relative;
    max-width: 100px;
    border-top: 4px double #F99700;
    margin: 20px auto 20px;
}
.titleline-icon:after {
    position: absolute;
    top: -11px;
    left: 0;
    right: 0;
    margin: auto;
    font-family: 'FontAwesome';
    content: "\f141";
    font-size: 20px;
    line-height: 1;
    color: #F99700;
    text-align: center;
    vertical-align: middle;
    width: 40px;
    height: 20px;
    background: #ffffff;
}
.jumbotron {
background: #7FA43A;
color: #FFF;
border-radius: 0px;
margin-bottom: 0;
}
.jumbotron-sm { padding-top: 12px;
padding-bottom: 12px; }
.jumbotron small {
color: #FFF;
}
.h1 small {
font-size: 18px;
}
.jumbotron h3{
           font-size: 35px!important;
    }
</style>