<style>
    .jumbotron {
        background: #7FA43A;
        color: #FFF;
        border-radius: 0px;
        margin-bottom: 5px;
    }
    .jumbotron-sm { padding-top: 12px;
                    padding-bottom: 12px; }
    .jumbotron small {
        color: #FFF;
    }
    .h1 small {
        font-size: 18px;
    }
     .jumbotron h3{
           font-size: 35px!important;
    }
</style>
<div class="jumbotron jumbotron-sm">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-lg-12">
                <h3 class="h1">
                  About us  <small> Feel free to Read More </small></h3>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="col-md-12">
        <?php
echo $all_career->about_new_text;
?>
    </div>
</div>


