<?php
//echo '<pre>';
//print_r($product_details_by_id);
?>
<!-------------------------------->

<style>
    .jumbotron {
        background: #7FA43A;
        color: #FFF;
        border-radius: 0px;
        margin-bottom: 0;
    }
    .jumbotron-sm { padding-top: 2px;
                    padding-bottom: 2px; }
    .jumbotron small {
        color: #FFF;
    }
    .h1 small {
        font-size: 18px;
    }
    .jumbotron h3{
        font-size: 35px!important;
    }
</style>

<link rel="stylesheet" href="<?php echo base_url(); ?>front_asset/zoom/css/pygments.css" />
<link rel="stylesheet" href="<?php echo base_url(); ?>front_asset/zoom/css/easyzoom.css" />
<div class="jumbotron jumbotron-sm">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-lg-12">
                <h3 class="h1">
                    View Product Details <small>Feel free to See More</small></h3>
            </div>
        </div>
    </div>
</div>

<link rel="stylesheet" href="<?php echo base_url(); ?>front_asset/xzoom/css/normalize.css" />
<link rel="stylesheet" href="<?php echo base_url(); ?>front_asset/xzoom/css/foundation.css" />
<!--<link rel="stylesheet" href="<?php echo base_url(); ?>front_asset/xzoom/css/demo.css" />-->
<script src="<?php echo base_url(); ?>front_asset/xzoom/js/vendor/modernizr.js"></script>
<script src="<?php echo base_url(); ?>front_asset/xzoom/js/vendor/jquery.js"></script>
<!-- xzoom plugin here -->
<script type="text/javascript" src="<?php echo base_url(); ?>front_asset/xzoom/xzoom.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>front_asset/xzoom/css/xzoom.css" media="all" /> 
<!-- hammer plugin here -->
<script type="text/javascript" src="hammer.js/1.0.5/jquery.hammer.min.js"></script>  
<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<link type="text/css" rel="stylesheet" media="all" href="<?php echo base_url(); ?>front_asset/xzoom/fancybox/source/jquery.fancybox.css" />

<script type="text/javascript" src="<?php echo base_url(); ?>front_asset/xzoom/fancybox/source/jquery.fancybox.js"></script>


<div class="container">




    <?php
    $img = $data = explode(',', $product_details_by_id->product_img);
    ?>

    <!-- fancy start -->
    <section id="fancy" style="margin-top: 30px;">
        <div class="row">

            <div class="large-5 column">
                <div class="xzoom-container">
                    <img class="xzoom4" id="xzoom-fancy" src="<?php echo base_url() . $product_details_by_id->product_img_master; ?>" xoriginal="<?php echo base_url() . $product_details_by_id->product_img_master; ?>" />
                    <div class="xzoom-thumbs">

                        <a href="<?php echo base_url() . $product_details_by_id->product_img_master; ?>"><img class="xzoom-gallery4" width="80" src="<?php echo base_url() . $product_details_by_id->product_img_master; ?>"  xpreview="<?php echo base_url() . $product_details_by_id->product_img_master; ?>" title="The description goes here"></a>
                        <?php
                        if (!empty($img['0'])) {
                            ?>
                            <a href=" <?php echo base_url() . $img['0']; ?>"><img class="xzoom-gallery4" width="80" src=" <?php echo base_url() . $img['0']; ?>" title="The description goes here"></a>
                        <?php } ?>
                        <?php
                        if (!empty($img['1'])) {
                            ?>
                            <a href=" <?php echo base_url() . $img['1']; ?>"><img class="xzoom-gallery4" width="80" src=" <?php echo base_url() . $img['1']; ?>" title="The description goes here"></a>
                        <?php } ?>
                        <?php
                        if (!empty($img['2'])) {
                            ?>
                            <a href=" <?php echo base_url() . $img['2']; ?>"><img class="xzoom-gallery4" width="80" src=" <?php echo base_url() . $img['2']; ?>" title="The description goes here"></a>
                        <?php } ?>

                    </div>
                </div>          
            </div>
            <div class="large-7 column">

                <div class="row">
                    <div class="col-md-12">
                        <div id="exTab1" class="">
                            <div class="row">
                                <div class=" col-md-12">
                                    <h2 class="product-name"><?php echo $product_details_by_id->product_name; ?></h2>
                                    <div class="product-inner-price">
                                        <b>Price:</b> &nbsp;<ins>&#2547;&nbsp;<?php echo $product_details_by_id->current_sale_price; ?></ins> <del>&#2547;100.00</del>
                                    </div>    

                                    <form action="<?php echo base_url(); ?>welcome/add_to_cart/<?php echo $product_details_by_id->product_id ?>" class="cart" method="post">
                                        <div class="quantity">
                                            <input type="number" class="input-text qty text" title="Qty" value="1" name="quantity" min="1" step="1">
                                        </div>
                                        <button class="btn btn-primary" type="submit">Add to cart</button>
                                    </form>  
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-12">
                        <h3>Product Description</h3> 
                        <?php echo $product_details_by_id->product_long_des; ?>
                    </div>
                    <div class="col-md-12">
                        <h3>Reviews</h3>
                        <form action="<?php echo base_url(); ?>Welcome/save_review" method="post">
                            <div class="form-group">
                                <label >Name</label>
                                <input type="text" class="form-control" name="name" id="name" placeholder="Enter Name" required>
                                <input type="hidden" class="form-control" value="<?php echo $product_details_by_id->product_name; ?>" name="product_name" id="product_name" placeholder="Enter Name" >
                                <input type="hidden" class="form-control" value="<?php echo $product_details_by_id->product_id; ?>" name="product_id" id="product_id" placeholder="Enter Name" >
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" class="form-control" name="email" id="email" placeholder="Enter Email" required>
                            </div>
                            <div class="form-group">
                                <label>Your review</label>
                                <textarea class="form-control" name="review"  placeholder="Enter Your review" rows="4" required></textarea>
                            </div>

                            <button type="submit" class="btn btn-default">Submit</button>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </section>   
    <!-- fancy end -->


    <section>

    </section> 
</div>
<script src="<?php echo base_url(); ?>front_asset/xzoom/js/foundation.min.js"></script>
<script src="<?php echo base_url(); ?>front_asset/xzoom/js/setup.js"></script>

