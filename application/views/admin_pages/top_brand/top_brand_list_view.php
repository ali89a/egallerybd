<?php
//echo '<pre>';
//print_r($all_customer);
?>


<h2 class="text-center" style="margin: 0px; padding: 10px;">Top Brand</h2>
<div class="text-right">
    <a  onclick="open_modal($(this))" style="margin-top: 10px;margin-bottom: 10px;" class="btn btn-info btn-sm"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;Create New</a>

</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <div class="">
            <form class="form-horizontal" action="<?php echo base_url(); ?>Brand/search_top_brand" method="post">
                <div class="form-group">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="search_name" id="search_text" placeholder="Search By Name or Email or Contact" required>
                    </div>

                    <div class="col-sm-2">
                        <button  class="btn btn-primary btn-block"><i class="fa fa-search" aria-hidden="true"></i>&nbsp;Search</button>
                    </div>
                    <div class="col-sm-2">
                        <a href="<?php echo base_url(); ?>Brand/top_brand_view"  class="btn btn-primary btn-block"><i class="fa fa-refresh" aria-hidden="true"></i>&nbsp;Refresh</a>
                    </div>

                </div>
            </form>
        </div> 



    </div>

    <div class="panel-body">
        <div id="msg_all"></div>
        <div class="table-responsive"> 
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th> Logo</th>
                        <th> Name</th>
                        <th>email</th>
                        <th>contact</th>
                        <th>Address</th>

                        <th class="text-right">Action</th>
                    </tr>
                </thead>
                <tbody>

                    <?php
                    if (count($all_top_brand) > 0) {

                        $i = 1;
                        foreach ($all_top_brand as $value) {
                            ?>    

                            <tr>
                                <td><?php echo $i; ?></td>
                                <td>
                                    <img class="" height="50" src="<?php echo base_url() . $value->top_brand_icon; ?>" alt="Logo">

                                </td>
                                <td><?php echo $value->top_brand_name; ?></td>
                                <td><?php echo $value->top_brand_email; ?></td>
                                <td><?php echo $value->top_brand_contact; ?></td>
                                <td><?php echo $value->top_brand_address; ?></td>



                                <td class="text-right">

                                    <a class="btn btn-success btn-xs" onclick="edit_vendor_store($(this))" vendor_store_id="<?php echo $value->top_brand_id ?>"><i class="fa fa-edit" aria-hidden="true"></i></a>
                                    <a class="btn btn-danger btn-xs" onclick="delete_vendor_store($(this))" vendor_store_id="<?php echo $value->top_brand_id ?>"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                </td>
                            </tr>
                            <?php
                            $i++;
                        }
                        ?> 

                    <?php } else { ?>

                        <tr>
                            <td colspan="7">

                                Not Found Results

                            </td>

                        </tr>

                    <?php } ?>
                </tbody>
            </table>
        </div>


    </div>
</div>

<!-- generic_modal-->
<div class="modal fade" id="product_modal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header hcolor">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="edit_modal_title">Add Brand Store</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal">
                    <div class="form-group">

                        <label class="control-label col-sm-4">Top Brand Name<span style="color:red; padding: 5px;">*</span> :</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control cleang" id="vendor_name" name="vendor_name" placeholder="Enter Brand Name">
                            <input type="hidden" class="form-control cleang" id="vendor_store_id" name="vendor_store_id" >
                        </div>
                    </div>
                    <div class="form-group">

                        <label class="control-label col-sm-4">Top Brand Email<span style="color:red; padding: 5px;">*</span> :</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control cleang" id="vendor_email" name="vendor_email" placeholder="Enter Brand Email">
                        </div>
                    </div>
                    <div class="form-group">

                        <label class="control-label col-sm-4">Top Brand Contact<span style="color:red; padding: 5px;">*</span> :</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control cleang" id="vendor_contact" name="vendor_contact" placeholder="Enter Brand Contact">
                        </div>
                    </div>
                    <div class="form-group">

                        <label class="control-label col-sm-4">Top Brand Address<span style="color:red; padding: 5px;">*</span> :</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control cleang" id="vendor_address" name="vendor_address" placeholder="Enter Brand Address">
                        </div>
                    </div>
                    <div class="form-group">


                        <label class="control-label col-sm-4">Top Brand Logo<span style="color:red; padding: 5px;">*</span> :</label>
                        <div class="col-sm-8">
                            <input type="file" class="form-control cleang" id="vendor_icon" name="vendor_icon">
                            <span style="color:red">Required fixed size width 182px, height 68px</span>
                        </div>
                    </div>

                    <div class="form-group">

                        <label class="control-label col-sm-4"></label>
                        <div class="col-sm-8">
                            <span id="msggm"></span>
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <a type="submit" onclick="save_update_all_Vendor_info()" class="btn btn-success"><span id="btn">Save Brand</span></a>
            </div>
        </div>

    </div>
</div>
<script type="text/javascript">




    function save_update_all_Vendor_info() {

        var vendor_store_id = $('#vendor_store_id').val();
        var formData = new window.FormData();
        var vendor_name = $('#vendor_name').val();
        var vendor_email = $('#vendor_email').val();
        var vendor_contact = $('#vendor_contact').val();
        var vendor_address = $('#vendor_address').val();
        var vendor_icon = document.getElementById("vendor_icon");

        formData.append('vendor_store_id', vendor_store_id);
        formData.append('vendor_name', vendor_name);
        formData.append('vendor_email', vendor_email);
        formData.append('vendor_contact', vendor_contact);
        formData.append('vendor_address', vendor_address);
        formData.append('vendor_icon', vendor_icon.files[0]);


        if (vendor_name == '' || vendor_email == '' || vendor_contact == '' || vendor_address == '') {

            $('#msggm').html('<div class="alert alert-danger"><strong>Error!</strong> Please Required Field Fillup</div>');
            $('#msggm').delay(1000).show().fadeOut('slow');
        } else {

            $.ajax({
                type: 'POST',
                url: '<?php echo site_url('Customer/ajax_save_update_top_brand_info') ?>',
                data: formData,
                dataType: 'json',
                processData: false,
                contentType: false
            }).done(function (result) {
                $('#msggm').html(result.msgg);
                location.reload();
                console.log(result);
            });


        }



    }

    function delete_vendor_store(element) {

        var vendor_store_id = element.attr('vendor_store_id');

//       alert(id_medicine_generic_groups);
        if (!confirm('Are you sure you want to delete this?')) {

            return false;
        }
        $.ajax({
            type: 'POST',
            url: '<?php echo site_url('Customer/ajax_delete_top_brand_name') ?>',
            data: {
                vendor_store_id: vendor_store_id

            }
        }).done(function (result) {
            $('#msg_all').html(result.msgg);
            location.reload();
            $('#msg_all').delay(2000).show().fadeOut('slow');
            console.log(result);
        });





    }
    function edit_vendor_store(element) {

        var vendor_store_id = element.attr('vendor_store_id');
        alert(vendor_store_id);
        $('.clean').val('');


        $.ajax({
            type: 'POST',
            url: '<?php echo site_url('Customer/get_top_brand_info') ?>',
            data: {vendor_store_id: vendor_store_id}
        }).done(function (vendor_store_info) {
            console.log(vendor_store_info);

            $('input#vendor_store_id').val(vendor_store_info.top_brand_id);
            $('input#vendor_name').val(vendor_store_info.top_brand_name);
            $('input#vendor_email').val(vendor_store_info.top_brand_email);
            $('input#vendor_contact').val(vendor_store_info.top_brand_contact);
            $('input#vendor_address').val(vendor_store_info.top_brand_address);

            $('#edit_modal_title').html('Edit vendor_store Name');
            $('#btn').html('Update');

        });




        $('#product_modal').modal({
            keyboard: false
        });
    }
    function open_modal(element) {


        $('.clean').val('');
        $('#product_modal').modal({
            keyboard: false
        });

    }
</script>
<style>

    .hcolor{
        background-color: #0088cc;
        color: white;
    }

</style>
