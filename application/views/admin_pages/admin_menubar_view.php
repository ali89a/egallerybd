<li class="header">MAIN NAVIGATION</li>
<li 
    <?php if ($admin_main_content == "dashboard") { ?> class="active" <?php } ?>  >
    <a href="<?php echo base_url(); ?>Admin_Dashboard">
        <i class="fa fa-dashboard"></i> <span>Dashboard</span>
    </a>
</li>

<li>
    <a href="<?php echo base_url(); ?>Common/manage_order">
        <i class="fa fa-circle-o text-aqua"></i>
        <span>Manage Order</span>
    </a>
</li>
<li class="treeview">
    <a href="#">
        <i class="fa fa-edit"></i> <span>Product</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="treeview"<?php if ($admin_main_content == "admin_pages/pages/category/category_list_view") { ?> class="active" <?php } ?> >
            <a href="<?php echo base_url(); ?>Category/view">
                <i class="fa fa-pie-chart"></i>
                <span>Category</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>

        </li>
        <li class="treeview">
            <a href="<?php echo base_url(); ?>Sub_Category/view">
                <i class="fa fa-pie-chart"></i>
                <span>Sub-Category</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>

        </li>
        <li class="treeview">
            <a href="<?php echo base_url(); ?>Brand/view">
                <i class="fa fa-pie-chart"></i>
                <span>Sub-Category 2</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>

        </li>
        <li class="treeview">
            <a href="<?php echo base_url(); ?>Sub_Category_Three/view">
                <i class="fa fa-pie-chart"></i>
                <span>Sub-Category 3</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>

        </li>
        <li class="treeview">
            <a href="<?php echo base_url(); ?>Brand/top_brand_view">
                <i class="fa fa-pie-chart"></i>
                <span>Brand</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>

        </li>
        <li class="treeview">
            <a href="<?php echo base_url(); ?>Product/view">
                <i class="fa fa-pie-chart"></i>
                <span>Product</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>

        </li>
    </ul>
</li>


<li class="treeview">
    <a href="<?php echo base_url(); ?>Coupon/manage_coupon">
        <i class="fa fa-pie-chart"></i>
        <span>Coupon</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>

</li>
<li class="treeview">
    <a href="<?php echo base_url(); ?>Customer/manage_customer">
        <i class="fa fa-pie-chart"></i>
        <span>Customer</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>

</li>
<li class="treeview">
    <a href="<?php echo base_url(); ?>Customer/manage_vendor">
        <i class="fa fa-pie-chart"></i>
        <span>Vendor</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>

</li>
<li class="treeview">
    <a href="<?php echo base_url(); ?>Customer/manage_subadmin">
        <i class="fa fa-pie-chart"></i>
        <span>Sub-Admin</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>

</li>
<li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Sub-Admin Request</span>
            <span class="pull-right-container">
                
              <span class="label label-danger pull-right"> <?php echo $this->db->get_where('product',array('is_vendor_product'=>'1'))->num_rows();?></span>
            </span>
          </a>
            <ul class="treeview-menu">
                <li class="treeview">
                    <a href="<?php echo base_url(); ?>Customer/manage_vendor_request">
                        <i class="fa fa-circle-o"></i>
                        <span>Sub Admin Product Pending</span>
                       
                    </a>
                 

                </li>
                <li class="treeview">
                   
                    <a href="<?php echo base_url(); ?>Customer/manage_vendor_accept_product">
                        <i class="fa fa-circle-o"></i>
                        <span>Sub Admin Product Accept</span>
                       
                    </a>

                </li>
                <li class="treeview">
                   
                    <a href="<?php echo base_url(); ?>Customer/manage_vendor_reject_product">
                        <i class="fa fa-circle-o"></i>
                        <span>Sub Admin Product Reject</span>
                       
                    </a>

                </li>
            </ul>
        </li>

<li class="treeview">
    <a href="<?php echo base_url(); ?>Shipping/manage_shipping_cost">
        <i class="fa fa-pie-chart"></i>
        <span>Shipping Cost</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>

</li>
<li class="treeview">
    <a href="<?php echo base_url(); ?>Shipping/manage_shipping_area">
        <i class="fa fa-pie-chart"></i>
        <span>Shipping Area</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>

</li>
<li class="treeview">
    <a href="<?php echo base_url(); ?>Common/newsletter">
        <i class="fa fa-pie-chart"></i>
        <span>Newsletter</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>

</li>
<li class="treeview">
    <a href="<?php echo base_url(); ?>Common/manage_review">
        <i class="fa fa-pie-chart"></i>
        <span>Manage Review</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>

</li>

<li class="treeview">
    <a href="#">
        <i class="fa fa-pie-chart"></i>
        <span>Website Setting</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">

        <li class="treeview">
            <a href="<?php echo base_url(); ?>Slider_Manage/slider">
                <i class="fa fa-pie-chart"></i>
                <span>slider</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>

        </li>
        <li class="treeview">
            <a href="<?php echo base_url(); ?>Youtube/youtube_video">
                <i class="fa fa-pie-chart"></i>
                <span>Youtube</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>

        </li>
        <li class="treeview">
            <a href="<?php echo base_url(); ?>Ad/advertisement">
                <i class="fa fa-pie-chart"></i>
                <span> advertisement</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>

        </li>

<!--        <li>
            <a href="<?php echo base_url(); ?>Common/about">
                <i class="fa fa-circle-o"></i>
                View  about
            </a>
        </li>-->
        <li>
            <a href="<?php echo base_url(); ?>Common/new_about">
                <i class="fa fa-circle-o"></i>
                About
            </a>
        </li>

        <li>
            <a href="<?php echo base_url(); ?>Common/contact">
                <i class="fa fa-circle-o"></i>
                Contact Request
            </a>
        </li>
        <li>
            <a href="<?php echo base_url(); ?>Common/career">
                <i class="fa fa-circle-o"></i>
                Career
            </a>
        </li>
        <li>
            <a href="<?php echo base_url(); ?>Common/return_policy">
                <i class="fa fa-circle-o"></i>
              Return Policy
            </a>
        </li>
        <li>
            <a href="<?php echo base_url(); ?>Common/term_condition">
                <i class="fa fa-circle-o"></i>
       Term Condition
            </a>
        </li>
        <li>
            <a href="<?php echo base_url(); ?>Common/other_business">
                <i class="fa fa-circle-o"></i>
                    Other Business
            </a>
        </li>
    </ul>
</li>





