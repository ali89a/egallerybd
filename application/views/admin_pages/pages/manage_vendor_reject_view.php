<?php
$vendor_product=$this->db->select('*')
        ->from('product')
        ->where('is_vendor_product',3)
        ->get()->result();

?>



<h2 class="text-center" style="margin: 0px; padding: 20px;">Vendor Request Product</h2>
  <div class="panel panel-default">
    <div class="panel-heading">
      
       
        
        
    </div>
    <div class="panel-body">
      
    <div class="table-responsive"> 
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>#</th>
                <th>image</th>
                <th>Product Code</th>
                <th>Product Name</th>
                <th>Price</th>
                <th>Discount</th>
                <th>Status</th>
                <th class="text-right">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php 
            $i=1;
            foreach ($vendor_product as $value) {
                
            
            ?>            
            <tr>
                <td><?php echo $i;?></td>
                <td>
                    <a href="<?php echo site_url("Product/product_details/$value->product_id") ?>" class="">
                    <img width="40" class="img-thumbnail" src="<?php echo base_url().$value->product_img_master;?>" alt=""></a></td>
                <td><?php echo $value->product_code;?></td>
                <td><?php echo $value->product_name;?></td>
                <td><?php echo $value->current_sale_price;?></td>
                <td><?php echo $value->discount;?></td>
                <td>
                        <?php
                        if ($value->is_vendor_product == 1) {
                            echo '<span class="label label-info">Pending</span>';
                        } elseif ($value->is_vendor_product == 2) {

                            echo '<span class="label label-success">Success Label</span>';
                        } elseif ($value->is_vendor_product == 3) {
                            echo '<span class="label label-danger">Rejected</span>';
                        }
                        ?>
                    </td>
              
                <td class="text-right">
                        <button onclick="open_modal($(this))" class="btn btn-primary btn-xs"><i class="fa fa-eye" aria-hidden="true"></i></button>
                        <?php if ($value->is_vendor_product == 1) { ?>
                            <a href="<?php echo site_url("Customer/vendor_request_accept/$value->product_id") ?>" class="btn btn-success btn-xs"><i class="fa fa-plus" aria-hidden="true"></i></a>
                            <a href="<?php echo site_url("Customer/vendor_request_reject/$value->product_id") ?>" class="btn btn-danger btn-xs"><i class="fa fa-times" aria-hidden="true"></i></a>
                        <?php } else {
                            ?>
                            <a class="btn btn-success btn-xs disabled"><i class="fa fa-edit" aria-hidden="true"></i></a>
                        <?php } ?>


                    </td>
            </tr>
          <?php 
          $i++;
            }
            ?> 
        </tbody>
    </table>
</div>
    
    
    </div>
  </div>









