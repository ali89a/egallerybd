<style>
    .error {
        color:red;
        font-size:13px;
        margin-bottom:-15px
    }
</style>
<?php
//echo '<pre>';
//print_r($about_us_info);
?>
<section class="content">

    <!-- SELECT2 EXAMPLE -->
    <div class="box box-default">

        <div class="box-header with-border">
            <h3 class="">Update About Us</h3>

            <div class="">
                <a href="<?php echo site_url("Common/about"); ?>" class="btn btn-default" style="background-color: #00A65A;color: white;"><i class="fa fa-eye">&nbsp;</i>View About</a>
            </div>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <form action="<?php echo site_url("Common/about_us_edit/$about_us_info->about_us_id") ?>" method="post" class="form-horizontal well" style="margin: 0px;" enctype="multipart/form-data">

                <div class="form-group">
                    <label class="control-label col-sm-2" for="category_name">Short Description:</label>
                    <div class="col-sm-10">
                        <?php echo form_textarea(array('id' => 'txt_short_description', 'value' => "$about_us_info->short_des", 'name' => 'txt_short_description', 'class' => 'textarea form-control', 'type' => 'text()', 'rows' => 4, 'cols' => 21,)); ?>
                        <?php echo form_error('txt_short_description'); ?>
                    </div>

                </div>
                <div class="form-group">

                    <label class="control-label col-sm-2" for="category_name">Long Description:</label>
                    <div class="col-sm-10">
                        <?php echo form_textarea(array('id' => 'txt_long_description','value' => "$about_us_info->long_des", 'name' => 'txt_long_description', 'class' => 'textarea form-control', 'type' => 'text()', 'rows' => 6, 'cols' => 21,)); ?>
                        <?php echo form_error('txt_long_description'); ?>
                    </div>
                </div>


                <div class="form-group">        
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success">Submit</button>
                    </div>
                </div>
                <?php echo form_close(); ?>

        </div>
        <!-- /.box-body -->
    </div>
</section>
<!-- /.content -->
