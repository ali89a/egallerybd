

<?php 
//echo '<pre>';
//print_r($all_ctegory);
//exit();
?>


<section class="content">
    <div class="row">
        <div class="col-xs-12">


            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Slider Image Manage</h3>
                </div>
                <div class="">
                   <a href="<?php echo site_url('Slider_Manage/slider_img_add'); ?>" class="btn btn-default" style="background-color: #00A65A;color: white;"><i class="fa fa-plus-circle">&nbsp;</i>Add New</a>
                   
                </div>
                <div class="text-center">
                  
                       <?php 
                       $excp=$this->session->userdata('slider_message');
                        if ($excp) {
                        echo "<span style='color: white; background:green;padding: 5px;'>$excp</style>";
                       
                        $this->session->unset_userdata('slider_message');}
                       ?>
                  
                </div>
                 <div class="text-center">
                  
                       <?php 
                       $excp=$this->session->userdata('sli_message');
                        if ($excp) {
                        echo "<span style='color: white; background:green;padding: 5px;'>$excp</style>";
                       
                        $this->session->unset_userdata('sli_message');}
                       ?>
                  
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped text-center">
                        <thead>
                            <tr>
                                <th style="font-size: 11px; width: 16px">SL</th>
                                <th>Title</th>
                                <th>Slider Image</th>
                                <th>STATUS</th>
                                <th>OPTION</th>
                         
                            </tr>
                        </thead>
                        <tbody>


<?php 
$i=1;
foreach ($all_slider_image as $value) {
//    echo '<pre>';
//print_r($value);
//exit();

?>

                            <tr>
                                <td><?php echo $i;?></td>
                                <td><?php echo $value->title;;?></td>
                                <td>
                                    <img width="100" src="<?php echo base_url(). $value->file_url;?>" alt="" >
                                </td>
                                <td style="font-size: 11px;">
                                    <?php 
                                    if($value->is_active==1){
                                        ?>
                               <a href="<?php echo site_url("Slider_Manage/slider_inactive/$value->photo_id") ?>" class="">  <input style="color: white; background-color: #00a65a" type="submit" value="Active" name="active" class="button"></a>
                                    <?php }
                                    else {
                                    ?> 
                                   <a href="<?php echo site_url("Slider_Manage/slider_active/$value->photo_id") ?>" class="">  <input style="color: white; background-color: #a94236" type="submit" value="Inactive" name="published" class="button"></a>
                                    
                                    <?php }?>
                                </td>
                                <td style="font-size: 11px; width: 220px">
                                    <a href="<?php echo site_url("Slider_Manage/slider_edit_update/$value->photo_id") ?>" class="">  <input style="color: white; background-color: #3c5e76" type="submit" value="Edit" name="edit" class="button"></a>
                                    <a href="<?php echo site_url("Slider_Manage/slider_delete/$value->photo_id") ?>" class="" onclick="return confirm('Are you sure?');">  <input style="color: white; background-color: #a94236" type="submit" value="Delete" name="delete" class="button"></a>

                                </td>
                            </tr>
                        
                   <?php 
    $i++;
}

?>      
                        </tbody>
                      
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>


