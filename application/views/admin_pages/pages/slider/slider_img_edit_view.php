<style>
      .error {
                color:red;
                font-size:13px;
                margin-bottom:-15px
            }
</style>
<section class="content">

    <!-- SELECT2 EXAMPLE -->
    <div class="box box-default">
     
        <div class="box-header with-border">
            <h3 class="">Update Slider Image</h3>
             
            <div class="pull-left">
                <a href="<?php echo site_url('Slider_Manage/slider'); ?>" class="btn btn-default" style="background-color: #00A65A;color: white;"><i class="fa fa-eye">&nbsp;</i>View Slider Image</a>
            </div>
            <div class="pull-left">
                <a href="<?php echo site_url('Slider_Manage/slider_img_add'); ?>" class="btn btn-default" style="background-color: #00A65A;color: white;"><i class="fa fa-eye">&nbsp;</i>Add Slider Image</a>
            </div>
          
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <form action="<?php echo site_url("Slider_Manage/slider_edit_update/$select_slider_img->photo_id")?>" method="post" class="form-horizontal well" style="margin: 0px;" enctype="multipart/form-data">
                <div class="form-group">
                    
                    <label class="control-label col-sm-8" for="file"></label>
                    <div class="col-sm-4">
                        <img width="200" height="100" src="<?php echo base_url().$select_slider_img->file_url ;?>">
                    </div>
                    
                  
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="name">Title:</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $select_slider_img->title ;?>" placeholder="Enter title">   
                            <?php echo form_error('name'); ?>
                    </div>
                    <label class="control-label col-sm-2" for="file">Image:</label>
                    <div class="col-sm-4">
                        <input type="file" class="form-control" id="file" name="userFiles[]" multiple>
                           <?php echo form_error('userFiles[]'); ?>
                    </div>
                    
                  
                </div>
                    <div class="form-group">   
                    <label class="control-label col-sm-8" ></label>
                    <div class="col-sm-4">
                         <span style="color:red;text-align: center;">("Required fixed size width 600px, height 300px")</span> 
                    </div>
               </div>
            
            
                <div class="form-group">        
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success">Update</button>
                    </div>
                </div>
             <?php //echo form_close(); ?>
            </form>
            
        </div>
        <!-- /.box-body -->
    </div>
</section>
<!-- /.content -->
