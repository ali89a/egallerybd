
<div class="col-md-12">


    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Coupon Manage</h3>
        </div>
        <div class="">
       <a href="<?php echo base_url();?>Coupon/add_coupon"><button class="btn btn-primary"><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;Add New</button></a>
         </div>
        <div class="text-center">
                 <?php 
                       $excp=$this->session->userdata('co_message');
                        if ($excp) {
                        echo "<span style='color: white; background:green;padding: 5px;'>$excp</style>";
                       
                        $this->session->unset_userdata('co_message');}
                       ?>
                    </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered">
                <tr>
                    <th>Sl</th>
                    <th>Coupon Number</th>
                    <th>Discount</th>
                    <th>Status</th>
                    <th class="text-right">Action</th>
                </tr>
                 <?php
//                        echo '<pre>';
//                        print_r($all_coupon_discount);
//                        exit();
                        ?>
                <?php
                $i=1;
                        foreach ($all_coupon_discount as $value) {


               
                ?>
                <tr>
                    <td> <?php echo $i;?></td>
                    <td><?php echo $value->coupon_number;?> </td>
                    <td>&#2547;&nbsp;<?php echo $value->coupon_dis_tk;?> </td>
                  
                    <td style="font-size: 11px;">
                            <?php
                            if ($value->status == 0) {
                                ?>
                                <span class="label label-danger">Inactive</span>
                                <?php
                            } elseif ($value->status == 1) {
                                ?> 
                                <span class="label label-success">Active</span>

                                <?php
                            }
                            ?>

                        </td>
                        <td class="text-right">
                          <?php
                                if ($value->status == 0) {
                                    ?>  
                                  <a href="<?php echo site_url("Coupon/active_coupon/$value->coupon_id") ?>" class=""><button class="btn btn-success btn-xs"><i class="fa fa-check" aria-hidden="true"></i></button></a>

                                <?php
                                } else {
                                    ?>
                                      <a href="<?php echo site_url("Coupon/inactive_coupon/$value->coupon_id") ?>" class=""><button class="btn btn-danger btn-xs"><i class="fa fa-times" aria-hidden="true"></i></button></a>
   
                                <?php } ?>

                                <a href="<?php echo site_url("Coupon/edit_coupon/$value->coupon_id") ?>" class=""> <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                <a href="<?php echo site_url("Coupon/delete_coupon/$value->coupon_id") ?>" class="" onclick="return confirm('Are you sure?');">  <button class="btn btn-danger btn-xs"><i class="fa fa-trash" aria-hidden="true"></i></button></a>
                            </td>
                  
                </tr>
<?php
$i++;
} 
?>
            </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix">
            <ul class="pagination pagination-sm no-margin pull-right">
<!--                <li><a href="#">&laquo;</a></li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">&raquo;</a></li>-->
            </ul>
        </div>
    </div>
</div>  <!-- /.box -->
