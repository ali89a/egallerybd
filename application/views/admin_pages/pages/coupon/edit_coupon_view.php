<div class="col-md-12">
<?php
//echo '<pre>';
//print_r($coupon_by_id);
//exit();
?>

    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Coupon Discount</h3>
        </div>
          <a href="<?php echo base_url();?>Coupon/manage_coupon"><button class="btn btn-primary"><i class="fa fa-tasks" aria-hidden="true"></i>&nbsp;Manage View</button></a>
            <a href="<?php echo base_url();?>Coupon/add_coupon"><button class="btn btn-primary"><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;Add New</button></a>
        <!-- /.box-header -->
        <!-- form start -->
        <form action="<?php echo base_url();?>Coupon/edit_coupon" method="post" class="form-horizontal">
            <div class="box-body">
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Coupon Code</label>

                    <div class="col-sm-8">
                        <input type="text" name="coupon_number" value="<?php echo $coupon_by_id->coupon_number;?>" class="form-control" id="inputEmail3" placeholder="Coupon Code">
                        <input type="hidden" name="coupon_id" value="<?php echo $coupon_by_id->coupon_id;?>" class="form-control" id="inputEmail3" placeholder="Coupon Code">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">Discount</label>

                    <div class="col-sm-8">
                        <input type="text" name="discount" value="<?php echo $coupon_by_id->coupon_dis_tk;?>"  class="form-control" id="inputPassword3" placeholder="Discount">
                    </div>
                </div>
          
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <div class="col-sm-2"></div>
                    <div class="col-sm-8">
                       <button type="submit" class="btn btn-default">Cancel</button>
                <button type="submit" class="btn btn-info">Update</button>
                    </div>
             
            </div>
            <!-- /.box-footer -->
        </form>
    </div>
</div>