<div class="col-md-12">


    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Shipping Cost Manage</h3>
        </div>
        <div class="">
                <a href="<?php echo base_url(); ?>Shipping/add_shipping_cost"><button class="btn btn-primary"><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;Add New</button></a>
        </div>
        <div class="text-center">
                 <?php 
                       $excp=$this->session->userdata('sc_message');
                        if ($excp) {
                        echo "<span style='color: white; background:green;padding: 5px;'>$excp</style>";
                       
                        $this->session->unset_userdata('sc_message');}
                       ?>
                    </div>
                     <div class="text-center">
                 <?php 
                       $excp=$this->session->userdata('scup_message');
                        if ($excp) {
                        echo "<span style='color: white; background:green;padding: 5px;'>$excp</style>";
                       
                        $this->session->unset_userdata('scup_message');}
                       ?>
                    </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered">
                <tr>
                    <th>Sl</th>
                    <th>Shipping Area</th>
                    <th>Shipping Cost</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
                 <?php
//                        echo '<pre>';
//                        print_r($all_shipping_cost);
//                        exit();
                        ?>
                <?php
                $i=1;
                        foreach ($all_shipping_cost as $value) {
                            
                            
$shipping_area =$this->db->get_where('shipping_area',array('shipping_area_id'=>$value->shipping_area_id))->row()->shipping_area;
              

               
                ?>
                <tr>
                    <td> <?php echo $i;?></td>
                    <td>
                        
                   <?php 
                   echo $shipping_area;
                  // $res=$this->db->get_where('shipping_area', array('shipping_area_id',$value->shipping_area_id))->row();
                 // echo $res->shipping_area;
                   ?>
                               
                    </td>
                    
                    <td>&#2547;&nbsp;<?php echo $value->ship_cost_amt;?> </td>
                  
                    <td style="font-size: 11px;">
                            <?php
                            if ($value->status == 0) {
                                ?>
                                <span class="label label-primary">Inactive</span>
                                <?php
                            } elseif ($value->status == 1) {
                                ?> 
                                <span class="label label-info">Active</span>

                                <?php
                            }
                            ?>

                        </td>
                        <td>
                            <?php
                            if ($value->status == 0) {
                                ?>
                                <a href="<?php echo site_url("Shipping/active_shipping_cost/$value->shipping_cost_id") ?>" class=""><button class="btn btn-success btn-xs"><i class="fa fa-chevron-up" aria-hidden="true"></i></button></a>
                            <?php }else{?>
                                <a href="<?php echo site_url("Shipping/inactive_shipping_cost/$value->shipping_cost_id") ?>" class=""><button class="btn btn-danger btn-xs"><i class="fa fa-chevron-down" aria-hidden="true"></i></button></a>
                            <?php }?>
                                <a href="<?php echo site_url("Shipping/edit_shipping_cost/$value->shipping_cost_id") ?>" class=""> <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                <a href="<?php echo site_url("Shipping/delete_shipping_cost/$value->shipping_cost_id") ?>" class="" onclick="return confirm('Are you sure?');">  <button class="btn btn-danger btn-xs"><i class="fa fa-trash" aria-hidden="true"></i></button></a>
                            </td>
                  
                </tr>
<?php
$i++;
} 
?>
            </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix">
          
            
        </div>
    </div>
</div>  <!-- /.box -->
