<style>
      .error {
                color:red;
                font-size:13px;
                margin-bottom:-15px
            }
</style>
<div class="col-md-12">


    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Edit Shipping Cost</h3>
        </div>
          <a href="<?php echo base_url();?>Shipping/manage_shipping_cost"><button class="btn btn-primary"><i class="fa fa-tasks" aria-hidden="true"></i>&nbsp;Manage View</button></a>
          
        <!-- /.box-header -->
        <!-- form start -->
        <form name="edit_form" action="<?php echo base_url();?>Shipping/edit_shipping_cost/<?php echo $shipping_cost_by_id->shipping_cost_id;?>" method="post" class="form-horizontal">
            <div class="box-body">
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Shipping Area</label>

                    <div class="col-sm-8">
                       
                          <select name="shipping_area_id" class="form-control" id="">
                            <option value="">--Select Area--</option>
                            <?php 
                            $all_area=$this->db->get("shipping_area")->result();
                            foreach ($all_area as $area){
                            ?>
                            <option value="<?php echo $area->shipping_area_id;?>"><?php echo $area->shipping_area;?></option>
                            <?php }?>
                        </select>
                       
                        
                       
                          <?php echo form_error('shipping_area_id'); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">Shipping Cost( &#2547;)</label>

                    <div class="col-sm-8">
                        <input type="text" value="<?php echo $shipping_cost_by_id->ship_cost_amt;?>" name="shipping_cost" class="form-control" id="inputPassword3" placeholder="Enter Shipping Cost">
                            <?php echo form_error('shipping_cost'); ?>
                    </div>
                </div>
          
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <div class="col-sm-2"></div>
                    <div class="col-sm-8">
                       <button type="submit" class="btn btn-default">Cancel</button>
                <button type="submit" class="btn btn-info">Update</button>
                    </div>
             
            </div>
            <!-- /.box-footer -->
        </form>
    </div>
</div>

<script type="text/javascript">
    document.forms['edit_form'].elements['shipping_area_id'].value ='<?php echo $shipping_cost_by_id->shipping_area_id;?>';
</script>