<!-- <script>
        $(function() {
           $('#msg_all').delay(1000).show().fadeOut('slow');
        });
        </script>-->
<script type="text/javascript">

 function delete_generic_name(element){
        
        var shipping_area_id=element.attr('shipping_area_id');

//       alert(id_medicine_generic_groups);
        if(!confirm('Are you sure you want to delete this?')){
    
    return false;
}
        $.ajax({
                type: 'POST',
                url: '<?php echo site_url('Shipping/ajax_delete_generic_name') ?>',
                data: {
                    shipping_area_id: shipping_area_id
                  
                }
            }).done(function (result) {
                $('#msg_all').html(result.msgg);
                location.reload();
                $('#msg_all').delay(2000).show().fadeOut('slow');
                console.log(result);
            });
        
        
        
        
        
    }
 function inactive(element){
        
        var shipping_area_id=element.attr('shipping_area_id');

//        alert(id_medicine_generic_groups);
        
        $.ajax({
                type: 'POST',
                url: '<?php echo site_url('Shipping/ajax_inactive_generic_name') ?>',
                data: {
                    shipping_area_id: shipping_area_id
                  
                }
            }).done(function (result) {
                $('#msg_all').html(result.msgg);
                location.reload();
                $('#msg_all').delay(2000).show().fadeOut('slow');
                console.log(result);
            });
        
        
        
        
        
    }
    function active(element){
        
        var shipping_area_id=element.attr('shipping_area_id');

//        alert(id_medicine_generic_groups);
        
        $.ajax({
                type: 'POST',
                url: '<?php echo site_url('Shipping/ajax_active_generic_name') ?>',
                data: {
                    shipping_area_id: shipping_area_id
                  
                }
            }).done(function (result) {
                $('#msg_all').html(result.msgg);
                location.reload();
                $('#msg_all').delay(2000).show().fadeOut('slow');
                console.log(result);
            });
        
        
        
        
        
    }
    function save_update_all_shipping_area_info(){

//alert('test_icon');
        var shipping_area_id = $("#shipping_area_id").val();
        var shipping_area_name = $("#shipping_area_name").val();
       
      
//alert(id_pathology_test_category);
//alert(test_name);
//alert(test_description);
//alert(test_icon);
        if (shipping_area_name == '') {

            $('#msgg').html('<div class="alert alert-danger"><strong>Error!</strong> Please Required Field Fillup</div>');
          $('#msgg').delay(1000).show().fadeOut('slow');
} else {

            $.ajax({
                type: 'POST',
                url: '<?php echo site_url('Shipping/ajax_save_update_shipping_area_name_info') ?>',
                data: {
                    shipping_area_id: shipping_area_id,
                    shipping_area_name: shipping_area_name
                 
                }
            }).done(function (result) {
                $('#msgg').html(result.msgg);
                location.reload();
                console.log(result);
            });
        }
    }
    </script>
<div id="content-container">
    <div style="position: fixed;top: 50px;right: 20px;z-index: 2000;">
        <span id="msg_all">
            
        </span>
    </div>
  
    <div class="tab-base">
        <div class="panel">
             <div class="panel-heading bg-info">
            <h1 class="page-header text-overflow" >Area Name</h1>
             </div>
            <div class="panel-body">
                
                    <div class="row"> 
                        <div class="col-md-12">
                            <div>
                                <a  onclick="add_generic_name_modal($(this))" style="margin-top: 10px;margin-bottom: 10px;" class="btn btn-info btn-sm pull-right"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;Create New</a>
                            </div>
                        </div>   
                        <div class="col-md-12">  
                            <div class="table-responsive"> 
                                <table class="table table-bordered" style="background: white;">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Area Name</th>
                                           
                                            <th>Status</th>
                                            <th class="text-right">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php
                                        $i = 0;

                                        foreach ($all_shipping_area as $shipping_area) {
//                                            echo '<pre>';
//                                            print_r($generic);
                                            
                                            $i++;
                                            ?>
                                            <tr>
                                                <td><?php echo $i; ?></td>

                                                <td><?php echo $shipping_area->shipping_area; ?></td>
                                            
                                               
                                                <td>
                                                    <?php 
                                                    if($shipping_area->status == 1){
                                                        echo '<span class ="label label-success">Active</span>';
                                                    } else {
                                                        echo '<span class ="label label-danger">Inactive</span>';
                                                    }
                                                    ?>
                                                  
                                                </td>
                                               <td class="text-right">
                                                   
                                                    <?php 
                                                    if($shipping_area->status == 1){?> 
                                                       
                                                         <button onclick="inactive($(this))" shipping_area_id="<?php echo $shipping_area->shipping_area_id;?>" class="btn btn-danger btn-xs"><i class="fa fa-minus" aria-hidden="true"></i></button>
                                                   
                                                    <?php } else {?>
                                                     <button onclick="active($(this))" shipping_area_id="<?php echo $shipping_area->shipping_area_id;?>" class="btn btn-info btn-xs"><i class="fa fa-plus" aria-hidden="true"></i></button>
                                                   
                                                 <?php }   ?>
                                                   
                                                   
                                                    <button onclick="open_edit_generic_modal($(this))" shipping_area_id="<?php echo $shipping_area->shipping_area_id;?>" class="btn btn-success btn-xs"><i class="fa fa-edit" aria-hidden="true"></i></button>
                                                    <button onclick="delete_generic_name($(this))" shipping_area_id="<?php echo $shipping_area->shipping_area_id;?>" class="btn btn-danger btn-xs"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                                </td>

                                            </tr>
                                        <?php } ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>  

                </div>
                
            </div>
        </div>
    </div>
</div>
<!-- Modal Change -->
          
<div class="modal fade" id="generic_modal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header hcolor">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" id="edit_modal_title">Add Area Name</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal">
                <div class="form-group">

                    <label class="control-label col-sm-3" for="name">Area Name<span style="color:red; padding-left: 5px;">*</span> :</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control clean" id="shipping_area_name" name="shipping_area_name" placeholder="Enter Shipping Area Name">
                        <input type="hidden" class="form-control clean" id="shipping_area_id" name="shipping_area_id">
                    </div>
                </div>
               
                <div class="form-group">

                    <label class="control-label col-sm-3"></label>
                    <div class="col-sm-9">
                        <span id="msgg"></span>
                       </div>
                </div>

            </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <a type="submit" onclick="save_update_all_shipping_area_info()" class="btn btn-success">Save Shipping Area</a>
        </div>
      </div>
      
    </div>
  </div>

<script type="text/javascript">
function add_generic_name_modal(element) {
        $('#edit_modal_title').html('Add Area Name');
        $('.clean').val('');
        $('#generic_modal').modal({
          keyboard: false
        });
       
    }
function open_edit_generic_modal(element) {
      
var shipping_area_id=element.attr('shipping_area_id');
//alert(id_medicine_generic_groups);

  $.ajax({
                              type: 'POST',
                              url: '<?php echo site_url('Shipping/get_generic_info') ?>',
                              data: {shipping_area_id:shipping_area_id}
                         }).done(function(shipping_area_info){
                        console.log(shipping_area_info);
                     
                         $('input#shipping_area_id').val(shipping_area_info.shipping_area_id);
                         $('input#shipping_area_name').val(shipping_area_info.shipping_area);
                        
                         $('#edit_modal_title').html('Edit Shipping Area Name');
                        
                        });

        $('#generic_modal').modal({
          keyboard: false
        });
       
    }

</script>

<style>
    
    .hcolor{
        background-color: #0088cc;
        color: white;
    }
</style>