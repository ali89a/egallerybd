<style>
      .error {
                color:red;
                font-size:13px;
                margin-bottom:-15px
            }
</style>
<section class="content">

    <!-- SELECT2 EXAMPLE -->
    <div class="box box-default">
     
        <div class="box-header with-border">
            <h3 class="">Create Youtube</h3>
             
            <div class="">
                <a href="<?php echo site_url('Youtube/youtube_video'); ?>" class="btn btn-default" style="background-color: #00A65A;color: white;"><i class="fa fa-eye">&nbsp;</i>View Youtube Video</a>
            </div>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <form action="<?php echo site_url('Youtube/youtube_video_add')?>" method="post" class="form-horizontal well" style="margin: 0px;" enctype="multipart/form-data">
                <div class="form-group">
                    <label class="control-label col-sm-2" for="name">Title:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="name" name="name" placeholder="Enter title">   
                            <?php echo form_error('name'); ?>
                    </div>
                   
                    
                  
                </div>
                <div class="form-group">
                   
                    <label class="control-label col-sm-2" for="file">Video Link:</label>
                    <div class="col-sm-10">
                         <input type="text" class="form-control" id="file" name="file" placeholder="Enter link">  
                       
                            <?php echo form_error('file'); ?>
                          
                    </div>
                    
                   
                  
                </div>
                <div class="form-group">
                   
                    <label class="control-label col-sm-2" for="file"></label>
                    <div class="col-sm-10">
                         <span style="color:red;text-align: center;">(Requred fixed size width 260px, height 146 px)</span>
                          
                    </div>
                    
                   
                  
                </div>
             
            
                <div class="form-group">        
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success">Submit</button>
                    </div>
                </div>
             <?php //echo form_close(); ?>
            </form>
            
        </div>
        <!-- /.box-body -->
    </div>
</section>
<!-- /.content -->
