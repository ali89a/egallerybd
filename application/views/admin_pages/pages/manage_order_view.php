<script type="text/javascript">
 $(document).ready(function () {
        $('#from_date').datepicker({dateFormat: 'Y-m-d',
            changeMonth: true,
            changeYear: true}).val('');
        $('#to_date').datepicker({dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true}).val('');
    });
</script>



<?php

?>
<div class="box">
    <div class="box-header">
        <h3 class="box-title">Order Manage</h3>
    </div>

    <!-- /.box-header -->
    <div class="box-body">
        
          <div class="well">
            <form class="form-horizontal" action="<?php echo base_url();?>Common/manage_order" method="post">
                <div class="form-group">
                    <div class="col-sm-1">
                     
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="from_date" id="from_date" placeholder="From Date" required>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="to_date" id="to_date" placeholder="To Date" required>
                    </div>
                    <div class="col-sm-2">
                        <button  class="btn btn-primary btn-block"><i class="fa fa-search" aria-hidden="true"></i>&nbsp;Search</button>
                    </div>
                    <div class="col-sm-2">
                        <a href="<?php echo base_url();?>Common/manage_order"  class="btn btn-success btn-block"><i class="fa fa-refresh" aria-hidden="true"></i>&nbsp;Refresh</a>
                    </div>
                </div>
            </form>
        </div> 
        
        
        <table id="example1" class="table table-bordered table-striped text-center" >
            <thead>
                <tr>
                    <th style="font-size: 11px;">#</th>
                    <th>Order No.</th>
                    <th>Order Date</th>
                   
                    <th>Order total</th>
                    <th>Advance</th>
                    <th>Payment Status</th> 
                    <th>Manage Delivery</th> 
                    <th>Delivery Status</th> 
                     <th>Comments</th>
                     <th>Reviews</th>
                    <th class="text-right">Action</th>
                </tr>
            </thead>
            <tbody>


                <?php
                $i = 1;
                foreach ($all_order as $value) {
                    ?>

                    <tr>

                        <td><?php echo $i; ?></td>
                        <td><?php echo $value->order_id;?></td>
                        <td><?php echo $value->order_date; ?></td>
                       
                        <td>&#2547; <?php 
                        
                          $shipping_area_id=$this->db->get_where('shipping',array('ship_id'=>$value->shipping_id))->row()->shipping_area_id;
                          $amont_ship=$this->db->get_where('shipping_cost',array('shipping_area_id'=>$shipping_area_id))->row()->ship_cost_amt;
                               
                           echo $value->order_total+$amont_ship-$value->coupon_discount; 
                        
                       ?></td>
                        <td>&#2547; <?php echo $value->order_advance; ?></td>
                        <td style="font-size: 11px;" class="text-right">
                            <?php
                            if ($value->payment_status == 1) {
                                echo '<span style="color: white;background: #da4;padding: 2px 10px;">Pending</span>';
                            } elseif ($value->payment_status == 2) {
                                echo '<span style="color: white;background: red;padding: 2px 10px;">Pending</span>';
                            } elseif ($value->payment_status == 3) {
                                echo '<span style="color: white;background: #00733e;padding: 2px 10px;">Approved</span>';
                            }
                            ?>

                        </td>

                        <td style="font-size: 11px;">



                            <a href="<?php echo site_url("Common/delivered_order/$value->order_id") ?>" class="" title="Delivered Order"><button class="btn btn-success btn-xs"><i class="fa fa-check" aria-hidden="true"></i></button></a>
                            <a href="<?php echo site_url("Common/confirm_order/$value->order_id") ?>" class="" title="On Delivery Order"><button class="btn btn-info btn-xs"><i class="fa fa-road" aria-hidden="true"></i></button></a>
                            <a href="<?php echo site_url("Common/pending_order/$value->order_id") ?>" class="" title="Pending Order"><button class="btn btn-warning btn-xs"><i class="fa fa-thumbs-down" aria-hidden="true"></i></button></a>

                            <?php
                            if ($value->order_cancle_req == 1) {
                                ?>
                                <a href="<?php echo site_url("Common/delete_order/$value->order_id") ?>" title="Request Order Cancle" class="" onclick="return confirm('Are you sure?');">  <button class="btn btn-danger btn-xs"><i class="fa fa-times" aria-hidden="true"></i></button></a>
                            <?php } ?>
                        </td>
                        <td style="font-size: 11px;">
                            <?php
                            if ($value->order_status == 0) {
                                ?>
                                <span class="label label-warning">Pending</span>
                                <?php
                            } elseif ($value->order_status == 1) {
                                ?> 
                                <span class="label label-info">On Delivery</span>

                                <?php
                            } else {
                                ?> 
                                <span class="label label-success">Delivered</span>

                            <?php } ?>

                        </td>
                        <td width="100">
<!--                            <span style="color:red;"><?php echo $value->order_return_text; ?></span>-->
                                <button onclick="retrun_open_modal($(this))" order_id="<?php echo $value->order_id?>"  class="btn btn-danger btn-xs">Comment</button>
                        </td>
                          <td width="100">
<!--                            <span style="color:red;"><?php echo $value->order_review_text; ?></span>-->
                                  <button onclick="review_open_modal($(this))" order_id="<?php echo $value->order_id?>"  class="btn btn-danger btn-xs">Review</button>
                        </td>

                        <td style="font-size: 11px;">
                            <button onclick="open_modal($(this))" order_id="<?php echo $value->order_id?>"  class="btn btn-success btn-xs">Advance</button>
                              <a href="<?php echo site_url("Common/view_payment/$value->payment_id") ?>" class="" title=""><button data-toggle="tooltip" data-placement="bottom" title="Check Payment" class="btn btn-success btn-xs"><i class="fa fa-money" aria-hidden="true"></i></button></a>
                    
                            <a href="<?php echo site_url("Common/view_invoice/$value->order_id") ?>" class=""><button class="btn btn-success btn-xs"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                            <a href="<?php echo site_url("Common/download_invoice/$value->order_id") ?>" class=""> <button class="btn btn-primary btn-xs"><i class="fa fa-download" aria-hidden="true"></i></button></a>
                            <a href="<?php echo site_url("Common/delete_order/$value->order_id/$value->shipping_id/$value->payment_id") ?>" class="" onclick="return confirm('Are you sure?');">  <button class="btn btn-danger btn-xs"><i class="fa fa-trash" aria-hidden="true"></i></button></a>

                        </td>
                    </tr>

                    <?php
                    $i++;
                }
                ?>      
            </tbody>
        </table>
    </div>
    <!-- /.box-body -->
</div>


<script type="text/javascript">
    
    
function save_update_order_advance_info() {
     var order_id = $("#order_id").val();
        var order_advance = $("#order_advance").val();
           $.ajax({
                type: 'POST',
                url: '<?php echo site_url('Common/ajax_save_update_advance_info') ?>',
                data: {
                    order_id: order_id,
                    order_advance: order_advance
                  
                }
            }).done(function (result) {
                $('#msgg_advance').html(result.msgg); 
                  $('#msgg_advance').delay(2000).show().fadeOut('slow');
                location.reload();
                console.log(result);
            });
}
function save_update_order_retrun_info() {
     var order_id = $("#order_id").val();
        var order_return_text = $("#order_return_text").val();
           $.ajax({
                type: 'POST',
                url: '<?php echo site_url('Common/ajax_save_update_retrun_info') ?>',
                data: {
                    order_id: order_id,
                    order_return_text: order_return_text
                  
                }
            }).done(function (result) {
                $('#msgg_retrun').html(result.msgg); 
                $('#msgg_retrun').delay(2000).show().fadeOut('slow');
                location.reload();
                console.log(result);
            });
}
function save_update_order_review_info() {
     var order_id = $("#order_id").val();
        var order_review_text = $("#order_review_text").val();
           $.ajax({
                type: 'POST',
                url: '<?php echo site_url('Common/ajax_save_update_review_info') ?>',
                data: {
                    order_id: order_id,
                    order_review_text: order_review_text
                  
                }
            }).done(function (result) {
                $('#msgg_review').html(result.msgg); 
                $('#msgg_review').delay(2000).show().fadeOut('slow');
                location.reload();
                console.log(result);
            });
}
function review_open_modal(element) {
    
       var order_id=element.attr('order_id');
      // alert(order_id);
             $.ajax({
                              type: 'POST',
                              url: '<?php echo site_url('Common/get_order_info') ?>',
                              data: {order_id:order_id}
                         }).done(function(order_info){
                        console.log(order_info);
                       
                         $('textarea#order_review_text').val(order_info.order_review_text);
                         $('input#order_id').val(order_info.order_id);
                        });
       
       
       
       
        $('.clean').val('');
        $('#review_modal').modal({
          keyboard: false
        });
       
    }
function retrun_open_modal(element) {
    
       var order_id=element.attr('order_id');
      // alert(order_id);
             $.ajax({
                              type: 'POST',
                              url: '<?php echo site_url('Common/get_order_info') ?>',
                              data: {order_id:order_id}
                         }).done(function(order_info){
                        console.log(order_info);
                       
                         $('textarea#order_return_text').val(order_info.order_return_text);
                         $('input#order_id').val(order_info.order_id);
                        });
       
       
       
       
        $('.clean').val('');
        $('#retrun_modal').modal({
          keyboard: false
        });
       
    }
function open_modal(element) {
    
       var order_id=element.attr('order_id');
      // alert(order_id);
             $.ajax({
                              type: 'POST',
                              url: '<?php echo site_url('Common/get_order_info') ?>',
                              data: {order_id:order_id}
                         }).done(function(order_info){
                        console.log(order_info);
                       
                         $('input#order_advance').val(order_info.order_advance);
                         $('input#order_id').val(order_info.order_id);
                        });
       
       
       
       
        $('.clean').val('');
        $('#advance_modal').modal({
          keyboard: false
        });
       
    }
</script>
<!-- advance_modal-->
<div class="modal fade" id="advance_modal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header hcolor">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" id="edit_modal_title">Add Advance</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal">
                <div class="form-group">

                    <label class="control-label col-sm-4" for="name">Advance Amount<span style="color:red; padding: 5px;">*</span> :</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control cleang" id="order_advance" name="order_advance" placeholder="Enter Advance Amount">
                        <input type="hidden" class="form-control cleang" id="order_id" name="order_id">
                    </div>
                </div>
               
                <div class="form-group">

                    <label class="control-label col-sm-3" for="remark"></label>
                    <div class="col-sm-9">
                        <span id="msgg_advance"></span>
                       </div>
                </div>

            </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <a type="submit" onclick="save_update_order_advance_info()" class="btn btn-success">Save Advance</a>
        </div>
      </div>
      
    </div>
  </div>
<!-- Comments_modal-->
<div class="modal fade" id="retrun_modal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header hcolor">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" id="edit_modal_title">Add Comments</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal">
                <div class="form-group">

                    <label class="control-label col-sm-4" for="name">Comments<span style="color:red; padding: 5px;">*</span> :</label>
                    <div class="col-sm-8">
                         <textarea class="form-control cleang" id="order_return_text" name="order_return_text" rows="5"></textarea>
                        
                        <input type="hidden" class="form-control cleang" id="order_id" name="order_id">
                    </div>
                </div>
               
                <div class="form-group">

                    <label class="control-label col-sm-3" for="remark"></label>
                    <div class="col-sm-9">
                        <span id="msgg_retrun"></span>
                       </div>
                </div>

            </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <a type="submit" onclick="save_update_order_retrun_info()" class="btn btn-success">Save Retrun Remark</a>
        </div>
      </div>
      
    </div>
  </div>
<!-- review_modal-->
<div class="modal fade" id="review_modal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header hcolor">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" id="edit_modal_title">Add Review</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal">
                <div class="form-group">

                    <label class="control-label col-sm-4" for="name">Review<span style="color:red; padding: 5px;">*</span> :</label>
                    <div class="col-sm-8">
                         <textarea class="form-control cleang" id="order_review_text" name="order_review_text" rows="5"></textarea>
                        
                        <input type="hidden" class="form-control cleang" id="order_id" name="order_id">
                    </div>
                </div>
               
                <div class="form-group">

                    <label class="control-label col-sm-3" for="remark"></label>
                    <div class="col-sm-9">
                        <span id="msgg_review"></span>
                       </div>
                </div>

            </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <a type="submit" onclick="save_update_order_review_info()" class="btn btn-success">Save Review</a>
        </div>
      </div>
      
    </div>
  </div>