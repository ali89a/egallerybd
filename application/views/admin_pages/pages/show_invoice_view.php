<!DOCTYPE html>
<html>
    <body>



        <script>
            function myFunction() {
                window.print();
            }

        </script>

        <style>
            @media only print{
                .box-header {
                    display: none;
                    overflow:hidden;
                }
                .btn {
                    display: none;
                    overflow:hidden;
                }
                .box-body {
                    overflow:hidden;
                }
                ::-webkit-scrollbar { 
                    display: none; 
                }

                body{
                    background-image:url(<?php echo base_url(); ?>front_assets/img/logo.jpg") ;
                }
            }
        </style>


        <section class="">
            <div class="row">
                <div class="col-xs-12">


                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title print_hide">Show Invoice</h3>
                        </div>

                        <!-- /.box-header -->
                        <div class="box-body">
                            <table class="table">
                                <tr class="text-right">

                                <button class="btn text-right" onclick="myFunction()">Print Invoice</button>
                                </tr>
                               
                                <tr>
                                  
                                    <td colspan="2">Order#<?php echo $shipping_info->order_id; ?></td>
                                </tr>
                                <tr>
                                    <td>
                                        <address>
                                            <strong>Customer Info#:</strong><br>
                                            <?php echo $customer_info->customer_name; ?><br>
                                            <?php echo $customer_info->customer_email; ?><br>
                                            <?php echo $customer_info->address; ?>

                                        </address>
                                    </td>
                                    <td>   
                                        <address>

                                            <strong>Shipped To:</strong><br>
                                            <?php echo $shipping_info->ship_name; ?><br>
                                            <?php echo $shipping_info->ship_email; ?><br>
                                            <?php echo $shipping_info->ship_phone; ?><br>
                                            <?php echo $shipping_info->ship_address; ?><br>
                                            <?php echo $shipping_info->ship_city; ?><br>
                                            <?php echo $shipping_info->ship_zip; ?>


                                        </address>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <address>
                                            <strong>Payment Info:</strong>
                                            <br>
                                            Payment
Method:<?php
                                            if ($customer_info->payment_type == 1) {
                                                echo 'Cash On Delivery';
                                            } elseif ($customer_info->payment_type == 2) {
                                                echo 'Rocket';
                                            } elseif ($customer_info->payment_type == 3) {
                                                echo 'Bkash';
                                            }
                                            ?>

                                            <br>

                                            Transaction id:<?php
                                            if ($customer_info->trx_id) {
                                                echo $customer_info->trx_id;
                                            } else{
                                                echo 'N/A';
                                            }
                                            ?>

                                        </address>
                                    </td>
                                    <td> <address>
                                            <strong>Order Info:</strong><br>
                                            Order Date:<?php echo $customer_info->order_date; ?><br>
                                            Order Total:
                                            <?php echo $shipping_info->order_total; ?><br>
                                            <span style="color:red;">
                                                <b>Order Retrun:</b> <?php echo $shipping_info->order_return_text; ?>
                                       
                                            </span> 
                                        </address>
                                    </td>
                                </tr>
                                <tr>

                                    <td colspan="2">  
                                        <table class="table table-condensed">
                                            <thead>
                                                <tr>
                                                    <td><strong>Product Name</strong></td>
                                                    <td class="text-center"><strong>Price</strong></td>
                                                    <td class="text-center"><strong>Quantity</strong></td>
                                                    <td class="text-right"><strong>Totals</strong></td>
                                                    <td class="text-right"></td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <!-- foreach ($order->lineItems as $line) or some such thing here -->
                                                <?php
                                                foreach ($order_details_info as $value) {
//                                    echo '<pre>';
//                                    print_r($value);
//                                    exit();
                                                    ?>
                                                    <tr>
                                                        <td><?php echo $value->product_name; ?></td>
                                                        <td class="text-center">&#2547;&nbsp;<?php echo $value->product_price;?></td>
                                                        <td class="text-center"><?php echo $value->product_sales_qty; ?></td>
                                                        <td class="text-right">	&#2547;&nbsp;<?php echo $value->product_price * $value->product_sales_qty ?></td>
                                                        <td class="text-right"></td>
                                                    </tr>
                                                <?php } ?>

                                                <tr>
                                                    <td class="thick-line"></td>
                                                    <td class="thick-line"></td>
                                                    <td class="thick-line text-center"><strong>Subtotal</strong></td>
                                                    <td class="thick-line text-right">&#2547;&nbsp;<?php echo $shipping_info->order_total; ?></td>
                                                    <td class="text-right"></td>
                                                </tr>
                                                <tr>
                                                    <td class="no-line">
                                                        <?php 
//                                                      echo '<pre>';
//                                                       print_r($shipping_info);
                                                        ?>
                                                         <?php 
                                    $amont_ship=$this->db->get_where('shipping_cost',array('shipping_area_id'=>$shipping_info->shipping_area_id))->row()->ship_cost_amt;
                                  
                                    ?>
                                                    </td>
                                                    <td class="no-line"></td>
                                                    <td class="no-line text-center"><strong>Shipping Cost</strong></td>
                                                    <td class="no-line text-right">&#2547;&nbsp; <?php echo $amont_ship;?></td>
                                                    <td class="text-right"></td>
                                                </tr>
                                                <tr>
                                                    <td class="no-line">
                                                       
                                                    </td>
                                                    <td class="no-line"></td>
                                                    <td class="no-line text-center"><strong>Coupon Discount</strong></td>
                                                    <td class="no-line text-right">	&#2547;&nbsp; <?php echo $shipping_info->coupon_discount;?></td>
                                                    <td class="text-right"></td>
                                                </tr>
                                                <tr>
                                                    <td class="no-line"></td>
                                                    <td class="no-line"></td>
                                                    <td class="no-line text-center" style="font-size: 18px"><strong>Total</strong></td>
                                                    <td class="no-line text-right" style="font-size: 18px">	<strong>&#2547;&nbsp;<?php echo $shipping_info->order_total-$shipping_info->coupon_discount+$amont_ship; ?></strong></td>
                                                    <td class="text-right"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>




    </body>
</html>