
<?php 
//echo '<pre>';
//print_r($all_ad_image);
//exit();
?>


<section class="content">
    <div class="row">
        <div class="col-xs-12">


            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Advertisement  Manage</h3>
                </div>
                <div class="">
                    <a href="<?php echo site_url('Ad/advertisement_img_add'); ?>" class="btn btn-default" style="background-color: #00A65A;color: white;"><i class="fa fa-plus-circle">&nbsp;</i>Add New</a>
                </div>
                <div class="text-center">
                 <?php 
                       $excp=$this->session->userdata('ad_message');
                        if ($excp) {
                        echo "<span style='color: white; background:green;padding: 5px;'>$excp</style>";
                       
                        $this->session->unset_userdata('ad_message');}
                       ?>
                    </div>
                     <div class="text-center">
                 <?php 
                       $excp=$this->session->userdata('adup_message');
                        if ($excp) {
                        echo "<span style='color: white; background:green;padding: 5px;'>$excp</style>";
                       
                        $this->session->unset_userdata('adup_message');}
                       ?>
                    </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped text-center">
                        <thead>
                            <tr>
                                <th style="font-size: 11px; width: 16px">SL</th>
                                <th>Title</th>
                                <th>Ad File</th>
                                <th>Ad Position</th>
                                <th>STATUS</th>
                                <th>OPTION</th>
                         
                            </tr>
                        </thead>
                        <tbody>


<?php 
$i=1;
foreach ($all_ad_image as $value) {
//    echo '<pre>';
//print_r($value);
//exit();

?>

                            <tr>
                                <td><?php echo $i;?></td>
                                <td><?php echo $value->ad_title;;?></td>
                                
                                <td>
                                    <img width="100" src="<?php echo base_url(). $value->ad_file;?>" alt="" >
                                </td>
                                <td><?php 
            if ($value->ad_position==1){
                                echo 'Top';
            }
            if ($value->ad_position==2){
                                echo 'Right';
            }
                                ?></td>
                                <td style="font-size: 11px;">
                                    <?php 
                                    if($value->is_active==1){
                                        ?>
                               <a href="<?php echo site_url("Ad/ad_inactive/$value->ad_id") ?>" class="">  <input style="color: white; background-color: #00a65a" type="submit" value="Active" name="active" class="button"></a>
                                    <?php }
                                    else {
                                    ?> 
                                   <a href="<?php echo site_url("Ad/ad_active/$value->ad_id") ?>" class="">  <input style="color: white; background-color: #a94236" type="submit" value="Inactive" name="published" class="button"></a>
                                    
                                    <?php }?>
                                </td>
                                <td style="font-size: 11px; width: 220px">
                                    <a href="<?php echo site_url("Ad/ad_edit_update/$value->ad_id") ?>" class="">  <input style="color: white; background-color: #3c5e76" type="submit" value="Edit" name="edit" class="button"></a>
                                    <a href="<?php echo site_url("Ad/ad_delete/$value->ad_id") ?>" class="" onclick="return confirm('Are you sure?');">  <input style="color: white; background-color: #a94236" type="submit" value="Delete" name="delete" class="button"></a>

                                </td>
                            </tr>
                        
                   <?php 
    $i++;
}

?>      
                        </tbody>
                        
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
