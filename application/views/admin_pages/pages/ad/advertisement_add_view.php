<style>
      .error {
                color:red;
                font-size:13px;
                margin-bottom:-15px
            }
</style>
<section class="content">

    <!-- SELECT2 EXAMPLE -->
    <div class="box box-default">
     
        <div class="box-header with-border">
            <h3 class="">Create Advertisement</h3>
             
            <div class="">
                <a href="<?php echo site_url('Ad/advertisement'); ?>" class="btn btn-default" style="background-color: #00A65A;color: white;"><i class="fa fa-eye">&nbsp;</i>View Advertisement</a>
            </div>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <form action="<?php echo site_url('Ad/advertisement_img_add')?>" method="post" class="form-horizontal well" style="margin: 0px;" enctype="multipart/form-data">
                <div class="form-group">
                    <label class="control-label col-sm-1" for="name">Title:</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="name" name="name" placeholder="Enter title">   
                            <?php echo form_error('name'); ?>
                    </div>
                    <label class="control-label col-sm-1" for="name">Position:</label>
                    <div class="col-sm-3">
                              
                        <select class="form-control" id="position" name="position">
                            <option value="">--Choose Position--</option>
                            <option value="1">Top</option>
                            <option value="2">Right</option>
                        </select>
                        <?php echo form_error('position'); ?>
                    </div>
                    <label class="control-label col-sm-1" for="file">Image:</label>
                    <div class="col-sm-3">
                        <input type="file" class="form-control" id="file" name="userFiles[]" multiple required>
                           <?php echo form_error('userFiles[]'); ?>
                    </div>
                    
                  
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-8" for="name"></label>
                    <div class="col-sm-4">
                        <span style="color:red">Required Max Size  top Ad "width 585 px, height 80 px"</span>  
                        <span style="color:red">Required Max Size  Right Ad "width 585 px"</span>
                    </div>
                  
                    
                  
                </div>
            
            
                <div class="form-group">        
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success">Submit</button>
                    </div>
                </div>
             <?php //echo form_close(); ?>
            </form>
            
        </div>
        <!-- /.box-body -->
    </div>
</section>
<!-- /.content -->
