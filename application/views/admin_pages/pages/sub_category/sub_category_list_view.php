<?php 
//echo '<pre>'; 
//print_r($select_all_sub_category); 
//exit();
?>

<style>
    .my_ddown .dropdown-menu{
        min-width: 80px;
    }
</style>

<div class="box">
    <div class="box-header">
        <h3 class="box-title">Sub Category  List</h3>
    </div>
    
    
    <!-- start add model -->
  

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form class="form-horizontal" action="<?php echo base_url(); ?>Sub_Category/sub_category_save" method="POST" id="cat_add_form" enctype="multipart/form-data">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add Sub-Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="category_name">Category:</label>
                            <div class="col-sm-8">
                                <select class="form-control" name="category_id" id="category_id">
                                    <option>--select option--</option>
                                    <?php foreach ($select_all_pub_category as $value) { ?>
                                        <option value="<?php echo $value->category_id; ?>"><?php echo $value->category_name; ?></option>
                                    <?php } ?>

                                </select>
                                </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="txt_sub_category">Sub-Category Name:</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="txt_sub_category" id="txt_category" placeholder="Sub-Category Title">
                            </div>
                        </div>
<!--                        <div class="form-group">
                            <label class="control-label col-sm-2" for="icon">Icon:</label>
                            <div class="col-sm-10"> 
                                <input type="file" class="form-control" id="icon" name="icon">
                            </div>
                        </div>-->
                       
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" id="add_btn" >Save changes</button>
                </div>
            </div>
       </form> 
        </div>
    </div>
    <!-- end add model -->
    <!-- /.box-header -->
    <div class="box-body">
        <button type="button" class="btn btn-success btn-sm pull-right" style="margin-bottom: 10px;" data-toggle="modal" data-target="#exampleModal">
            <span>
                <i class="fa fa-plus-circle" aria-hidden="true"></i>

                Create Sub-Category
            </span>
        </button>
        <table id="example1" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Sl</th>
                    <th>Category Name</th>
                    <th>Sub Category Name</th>
<!--                    <th>Sub Category Icon</th>-->
                    <th>Status</th>
                    <th class="text-right">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $i = 1;
                foreach ($select_all_sub_category as $sub_category) {
  
//     echo '<pre>';
//    print_r($v_brand);
//     exit();
                    ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $sub_category->category_name ?></td>
                        <td><?php echo $sub_category->sub_cat_name ?></td>
<!--                        <td><img width="50" height="50" src="<?php echo base_url(). $sub_category->sub_cat_icon ?>"></td>-->
                        <td>
                            <?php
                            if ($sub_category->is_active == 1) {
                                echo "<span class='label label-success'>Active</span>";
                            } else {
                                echo "<span class='label label-danger'>Inactive</span>";
                            }
                            ?>
                        </td>

                        <td class="text-right">
                            
                                        <?php 
                                        if($sub_category->is_active == 1){
                                            
                                       
                                        ?>
                                        <a href="<?php echo base_url();?>Sub_Category/inactive/<?php echo $sub_category->sub_category_id?>" data-toggle="tooltip" data-placement="bottom" title="Inactive"><button class="btn btn-danger btn-xs"><span class="fa fa-lock"></span></button></a> 
                                        <?php }
 else {
                                        
                                        ?>
                                        <a href="<?php echo base_url();?>Sub_Category/active/<?php echo $sub_category->sub_category_id?>" data-toggle="tooltip" data-placement="bottom" title="Active"><button class="btn btn-success btn-xs"><span class="fa fa-unlock"></span></button></a>  
                                        
 <?php }?>
                                  
                                  <a href="<?php echo base_url();?>Sub_Category/edit_sub_category/<?php echo $sub_category->sub_category_id?>"  data-toggle="tooltip" data-placement="bottom" title="Edit"><button class="btn btn-primary btn-xs"><span class="fa fa-edit"></span></button></a>
                                    <a href="<?php echo base_url();?>Sub_Category/delete_sub_category/<?php echo $sub_category->sub_category_id?>" data-toggle="tooltip" data-placement="bottom" title="Delete" onclick="return confirm('Are you sure you want to delete this Sub-Category?');"><button class="btn btn-danger btn-xs"><span class="fa fa-trash"></span></button></a>
                               


                        </td>
                    </tr>
    <?php
    $i++;
    ?>   
                <?php } ?>


            </tbody>
            <tfoot>

            </tfoot>
        </table>
    </div>
    <!-- /.box-body -->
</div>
