<?php
//echo '<pre>'; 
//print_r($select_all_brand); 
//exit();
?>
<script type="text/javascript">
    // Ajax post
    $(document).ready(function () {
        $("#cbo_category").on('change', function () {
            var category_id = $('#cbo_category').val();
            // alert(category_id);
            console.log(category_id);


            if (category_id == '')
            {
                $("#cbo_sub_category").prop('disabled', true);
            } else
            {
                $("#cbo_sub_category").prop('disabled', false);
                $.ajax({

                    // url: "<?php echo base_url(); ?>brand/get_subcat_by_cat_id", 
                    url: "<?php echo base_url(); ?>" + "brand/get_subcat_by_cat_id",
                    type: "POST",
                    data: {'category_id': category_id},
                    dataType: 'json',
                    success: function (data) {

                        $("#cbo_sub_category").html(data);
                    },
                    error: function () {
                          $("#cbo_sub_category").html(' <option value="">Not Found</option>');
                    }
                });

            }

        });
    });
</script>
<style>
    .my_ddown .dropdown-menu{
        min-width: 80px;
    }
</style>

<div class="box">
    <div class="box-header">
        <h3 class="box-title">Sub-Category 2 List</h3>
    </div>
    
     
    <!-- start add model -->
  

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form class="form-horizontal" action="<?php echo base_url(); ?>Brand/brand_save" method="POST" id="cat_add_form" enctype="multipart/form-data">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add Sub-Category 2</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="category_name">Category Title:</label>
                            <div class="col-sm-8">
                                <select class="form-control" name="cbo_category" id="cbo_category">
                                    <option value="">--Select Category--</option>
                                    <?php foreach ($select_all_pub_category as $value) { ?>
                                        <option value="<?php echo $value->category_id; ?>"><?php echo $value->category_name; ?></option>
                                    <?php } ?>

                                </select>
                       
                                </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="category_name">Sub-Category Title:</label>
                            <div class="col-sm-8">
                                
                                <select class="form-control" name="cbo_sub_category" id="cbo_sub_category" disabled>
                                    <option value="">--Select Sub Category--</option>
                                  

                                </select>
                        </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="txt_brand">Sub-Category-2 Title:</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="txt_brand" id="txt_brand" placeholder="Sub-Category-2 Title">
                            </div>
                        </div>
<!--                        <div class="form-group">
                            <label class="control-label col-sm-4" for="icon">Icon:</label>
                            <div class="col-sm-8"> 
                                <input type="file" class="form-control" id="icon" name="icon">
                            </div>
                        </div>-->
                       
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" id="add_btn" >Save changes</button>
                </div>
            </div>
       </form> 
        </div>
    </div>
    </div>
    <!-- end add model -->
    
    <!-- /.box-header -->
    <div class="box-body">
         <button type="button" class="btn btn-success btn-sm pull-right" style="margin-bottom: 10px;" data-toggle="modal" data-target="#exampleModal">
        <span>
           <i class="fa fa-plus-circle" aria-hidden="true"></i>

            Create Sub-Category 2
        </span>
    </button>
        <table id="example1" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Sl</th>
                    <th>Category Name</th>
                    <th>Sub-Category 2 Name</th>
<!--                    <th>Sub-Category 2 Icon</th>-->
                    <th>Status</th>
                    <th class="pull-right">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $i = 1;
                foreach ($select_all_brand as $v_brand) {
  
//     echo '<pre>';
//    print_r($v_brand);
//     exit();
                    ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $v_brand->category_name ?></td>
                        <td><?php echo $v_brand->brand_name ?></td>
<!--                        <td><img width="50" height="50" src="<?php echo base_url(). $v_brand->brand_icon ?>"></td>-->
                        <td>
                            <?php
                            if ($v_brand->is_active == 1) {
                                echo "<span class='label label-success'>Active</span>";
                            } else {
                                echo "<span class='label label-danger'>Inactive</span>";
                            }
                            ?>
                        </td>

                        <td class="pull-right">
                           
                                        <?php 
                                        if($v_brand->is_active == 1){
                                            
                                       
                                        ?>
                                        <a href="<?php echo base_url();?>Brand/inactive/<?php echo $v_brand->brand_id?>" data-toggle="tooltip" data-placement="bottom" title="Inactive"><button class="btn btn-danger btn-xs"><span class="fa fa-lock"></span></button></a> 
                                        <?php }
 else {
                                        
                                        ?>
                                        <a href="<?php echo base_url();?>Brand/active/<?php echo $v_brand->brand_id?>" data-toggle="tooltip" data-placement="bottom" title="Active"><button class="btn btn-success btn-xs"><span class="fa fa-unlock"></span></button></a>  
                                        
 <?php }?>
                                   
                                 <a href="<?php echo base_url();?>Brand/edit_brand/<?php echo $v_brand->brand_id?>"  data-toggle="tooltip" data-placement="bottom" title="Edit"><button class="btn btn-primary btn-xs"><span class="fa fa-edit"></span></button></a>
                                   <a href="<?php echo base_url();?>Brand/delete_brand/<?php echo $v_brand->brand_id?>" data-toggle="tooltip" data-placement="bottom" title="Delete" onclick="return confirm('Are you sure you want to delete this brand?');"><button class="btn btn-danger btn-xs"><span class="fa fa-trash"></span></button></a>
                                


                        </td>
                    </tr>
    <?php
    $i++;
    ?>   
                <?php } ?>


            </tbody>
            <tfoot>
                <tr>
                  
                </tr>
            </tfoot>
        </table>
    </div>
    <!-- /.box-body -->
</div>
