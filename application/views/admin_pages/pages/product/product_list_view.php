<?php
$category_info=$this->db->get('category')->result();
$sub_category_info=$this->db->get('sub_category')->result();
$brand_info=$this->db->get('brand')->result();
//echo '<pre>';
//print_r($all_product);
//exit();
?>
<script>
//while(true){
//var tt=$('body div').eq(0).attr('class');
//if(typeof tt===typeof undefined){
//$('body div').eq(0).remove();
//}else{
//break;
//}
//}
//$('body div').eq(0).remove();

</script>

<section class="content">
    <div class="row">
        <div class="col-xs-12">


            <div class="box">
                <div class="box-header">
                    <h2 class="box-title">Product Manage</h2>
                </div>
                  <div class="text-center">
                 <?php 
                       $excp=$this->session->userdata('message');
                        if ($excp) {
                        echo "<span style='color: white; background:green;padding: 5px;'>$excp</style>";
                       
                        $this->session->unset_userdata('message');}
                       ?>
                    </div>
            <div class="">
            <form class="form-horizontal" action="<?php echo base_url();?>Product/search_product_view" method="post">
                <div class="form-group">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-3">
                        <select class="selectpicker" data-live-search="true" data-live-search-style="startsWith" name="category_id">
                            <option value="">-- choose Category --</option>
                            <?php 
                            foreach ($category_info as $category) {
                                
                         
                            ?>
                            <option value="<?php echo $category->category_id;?>"><?php echo $category->category_name;?></option>
<?php } ?>
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <select class="selectpicker" data-live-search="true" data-live-search-style="startsWith" name="sub_category_id">
                            <option value="">--choose Sub Category--</option>
                            <?php 
                            foreach ($sub_category_info as $sub_category) {
                                
                         
                            ?>
                            <option value="<?php echo $sub_category->sub_category_id;?>"><?php echo $sub_category->sub_cat_name;?></option>
<?php } ?>
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <select class="selectpicker" data-live-search="true" data-live-search-style="startsWith" name="brand_id">
                            <option value="">--choose Sub Category-2--</option>
                            <?php 
                            foreach ($brand_info as $brand) {
                                
                         
                            ?>
                            <option value="<?php echo $brand->brand_id;?>"><?php echo $brand->brand_name;?></option>
<?php } ?>
                        </select>
                    </div>
                
                    <div class="col-sm-3">
                     
                    </div>
                </div>
                <div class="form-group">
                     <div class="col-sm-3"></div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="search_text_name" id="search_text" placeholder="Search By Product Name">
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="search_text_code" id="search_text" placeholder="Search By Product Code">
                    </div>
                   
                    
                 
                </div>
                <div class="form-group well">
                   <div class="col-sm-3"></div>
                    
                    <div class="col-sm-3">
                        <button  class="btn btn-primary btn-block"><i class="fa fa-search" aria-hidden="true"></i>&nbsp;Search
</button>
                    </div>
                    <div class="col-sm-3">
                        <a href="<?php echo base_url();?>Product/view"  class="btn btn-primary btn-block"><i class="fa fa-refresh" aria-hidden="true"></i>&nbsp;Refresh
</a>
                    </div>
                </div>
            </form>
        </div> 
                <div class="pull-right">
                     <a href="<?php echo site_url('Product'); ?>" class="btn btn-default" style="background-color: #00A65A;color: white; margin-right: 10px!important;margin-bottom: 5px;"><i class="fa fa-plus-circle">&nbsp;</i>Add New</a>
                  
                </div>
                <div class="text-center">
               
                <!-- /.box-header -->
                <div class="box-body"> 
                   
                    <table id="table2excel" class="table2excel table table-bordered table-striped">
                        <thead>
                            <tr class="bg-info noExl">
                                <th>#</th>
                                <th>Image</th>
                                <th>Name</th>
                                <th>Code</th>
                                <th>Price</th>
                                <th>Discount</th>    
                                <th>Sale Price</th>
                                <th>Status</th>
                                <th>Latest</th>
                                <th class="text-right">Action</th>

                            </tr>
                        </thead>
                        <tbody>


                            <?php
                            $i = 1;
                            foreach ($all_product as $value) {
                                     $img=$data= explode(',', $value->product_img);
                                ?>

                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td>
                                        <a href="<?php echo site_url("Product/product_details/$value->product_id") ?>" class=""><img width="50" src=" <?php   echo base_url(). $value->product_img_master;?> "></a>
<!--                                         <img width="50" src=" <?php   echo base_url(). $img['0'];?> ">-->
                                    </td>
                                    <td><?php echo $value->product_name; ?></td>
                                    <td><?php echo $value->product_code; ?></td>
                                    <td>BDT&nbsp;<?php echo $value->product_price; ?></td>
                                    <td><?php echo $value->discount; ?>%</td>
                                     <td>BDT&nbsp;<?php echo $value->current_sale_price; ?></td>
                                    
                                 
                                    <td>
                                            <?php
                                            if ($value->is_active == 1) {
                                                ?>
                                                <a href="<?php echo site_url("Category/inactive_product/$value->product_id") ?>" class="">  <span class="label label-success">Active</span></a>
                                                <?php
                                            } else {
                                                ?> 
                                                <a href="<?php echo site_url("Category/active_product/$value->product_id") ?>" class=""> <span class="label label-danger">Inactive</span></a>

                                            <?php } ?>
                                        </td>
                                  
                                    <td>
                                            <?php
                                            if ($value->is_latest == 1) {
                                                ?>
                                                <a href="<?php echo site_url("Category/latest_inactive/$value->product_id") ?>" class="">  <span class="label label-success">Active</span></a>
                                                <?php
                                            } else {
                                                ?> 
                                                <a href="<?php echo site_url("Category/latest_active/$value->product_id") ?>" class=""> <span class="label label-danger">Inactive</span></a>

                                            <?php } ?>
                                        </td>
                                        <td class="text-right">
         <button onclick="open_modal($(this))" product_id="<?php echo $value->product_id?>"  class="btn btn-success btn-xs">discount</button>
               
                                        <a href="<?php echo site_url("Product/product_details/$value->product_id") ?>" class="btn btn-primary btn-xs">  <i class="fa fa-eye" aria-hidden="true"></i>
</a>
                                        <a href="<?php echo site_url("Product/edit_product/$value->product_id") ?>" class="btn btn-primary btn-xs">  <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
</a>
                                     
                                        <a href="<?php echo site_url("Category/delete_product/$value->product_id") ?>" class="btn btn-danger btn-xs" onclick="return confirm('Are you sure?');">  <i class="fa fa-trash-o" aria-hidden="true"></i>
</a>

                                    </td>
                                </tr>

    <?php
    $i++;
}
?>      
                        </tbody>
                      
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<script>
			$(function() {
				$(".table2excel").table2excel({
					exclude: ".noExl",
					name: "Excel Document Name",
					filename: "myFileName",
					fileext: ".xls",
					exclude_img: true,
					exclude_links: true,
					exclude_inputs: true
				});
			});
		</script>
<script type="text/javascript">
   
//    =======================
    $(document).ready(function () {
        $("#discount").on('keyup', function () {



                      var price = $("#product_price").val();
                      var discount = $("#discount").val();
                    
                      var totalvalue = price * ((100 - discount) / 100);
                      var bdtvalue = price * (discount / 100);

                      $("#current_sale_price").val(totalvalue);
                      $("#bdt_discount").val(bdtvalue);
   
              
          });
         });
//        ----------------
    
function save_update_order_advance_info() {
     var order_id = $("#order_id").val();
        var order_advance = $("#order_advance").val();
           $.ajax({
                type: 'POST',
                url: '<?php echo site_url('Common/ajax_save_update_advance_info') ?>',
                data: {
                    order_id: order_id,
                    order_advance: order_advance
                  
                }
            }).done(function (result) {
                $('#msgg_advance').html(result.msgg); 
                  $('#msgg_advance').delay(2000).show().fadeOut('slow');
                location.reload();
                console.log(result);
            });
}
function open_modal(element) {
    
       var product_id=element.attr('product_id');
     //  alert(product_id);
       
       
       
                             $.ajax({
                              type: 'POST',
                              url: '<?php echo site_url('Common/get_product_info') ?>',
                              data: {product_id:product_id}
                         }).done(function(product_info){
                        console.log(product_info);
                       
                         $('input#product_price').val(product_info.product_price);
                         $('input#product_id').val(product_info.product_id);
                         $('input#current_sale_price').val(product_info.current_sale_price);
                         $('input#discount').val(product_info.discount);
                         $('input#bdt_discount').val(product_info.bdt_discount);
                         
                          
                        
                        });
       
       
       
       
        $('.clean').val('');
        $('#advance_modal').modal({
          keyboard: false
        });
       
    }
</script>
<!-- generic_modal-->
<div class="modal fade" id="advance_modal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header hcolor">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" id="edit_modal_title">Add Discount</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" action="<?php echo base_url();?>Category/update_discount" method="post">
                <div class="form-group">

                    <label class="control-label col-sm-4" for="name">Product Price<span style="color:red; padding: 5px;">*</span> :</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control cleang" id="product_price" name="product_price" placeholder="Enter Advance Amount" disabled>
                        <input type="hidden" class="form-control cleang" id="product_id" name="product_id">
                    </div>
                </div>
                <div class="form-group">

                    <label class="control-label col-sm-4" for="name">Discount(%)<span style="color:red; padding: 5px;">*</span> :</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control cleang" id="discount" name="discount" placeholder="Enter Advance Amount">
                    </div>
                </div>
                <div class="form-group">

                    <label class="control-label col-sm-4" for="name">Discount(BDT)<span style="color:red; padding: 5px;">*</span> :</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control cleang" id="bdt_discount" name="bdt_discount" placeholder="Enter Advance Amount" disabled>
                    </div>
                </div>
                <div class="form-group">

                    <label class="control-label col-sm-4" for="name">Current Sale Price<span style="color:red; padding: 5px;">*</span> :</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control cleang" id="current_sale_price" name="current_sale_price" placeholder="Enter Advance Amount" disabled>
                    </div>
                </div>
               
                <div class="form-group">

                    <label class="control-label col-sm-3" for="remark"></label>
                    <div class="col-sm-9">
                        <span id="msgg_advance"></span>
                       </div>
                </div>

            
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-success">Save Discount</button>
        </div>
      </div>
      </form>
    </div>
  </div>
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/css/bootstrap-select.css" />
<script src="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/js/bootstrap-select.js"></script>
<script type="text/javascript">

 

$(function () {
    //Initialize Select2 Elements
    $(".select2").select2();
    });

</script>