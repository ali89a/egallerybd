<?php 
//echo '<pre>';
//print_r($all_customer);
?>



<h2 class="text-center" style="margin: 0px; padding: 20px;">Customers</h2>
  <div class="panel panel-default">
    <div class="panel-heading">
        
       
            <div class="">
            <form class="form-horizontal" action="<?php echo base_url();?>Customer/search_customer" method="post">
                <div class="form-group">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" name="search_text" id="search_text" placeholder="Search By Customer Name or Email or Contact or Address or etc">
                    </div>
                   
                    <div class="col-sm-3">
                     
                    </div>
                </div>
               
                 <div class="form-group well">
                   <div class="col-sm-3"></div>
                    
                    <div class="col-sm-3">
                        <button  class="btn btn-primary btn-block"><i class="fa fa-search" aria-hidden="true"></i>&nbsp;Search
</button>
                    </div>
                    <div class="col-sm-3">
                        <a href="<?php echo base_url();?>Customer/manage_customer"  class="btn btn-primary btn-block"><i class="fa fa-refresh" aria-hidden="true"></i>&nbsp;Refresh
</a>
                    </div>
                </div>
            </form>
        </div> 
       
        
        
    </div>
    <div class="panel-body">
     
        <button onclick="excel()" class="btn btn-primary btn-sm" id="excel"><i class="fa fa-download" aria-hidden="true"></i>&nbsp;&nbsp;Download Excel</button>
     
    <div class="table-responsive" id="table_wrapper"> 
        <table class="table table-bordered" id="mytable">
         <thead>
            <tr>
                <th>#</th>
                <th> Name</th>  
               <th>Mobile</th>
                <th> Address</th>
                <th>email</th>
                <th>Password</th> 
                <th>Birthday</th>
                <th>Blood Group</th>
                <th>Status</th>
                <th class="text-right">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php 
            $i=1;
            foreach ($all_customer as $value) {
                
            
            ?>            
            <tr>
               <td><?php echo $i;?></td>
                 <td><?php echo $value->customer_name;?></td>  
                 <td><?php echo $value->mobile;?></td>
                 <td><?php echo $value->address;?></td>
             
                <td><?php echo $value->customer_email;?></td>
                <td><?php echo $value->password;?></td>
                
                <td><?php echo $value->dob;?></td>
                <td><?php echo $value->blood_group;?></td>
                <td>
                        <?php
                        if ($value->is_active == 0) {
                            
                            echo '<span class="label label-warning">Pending</span>';
                            
                        } elseif ($value->is_active == 1) {

                            echo '<span class="label label-success">Active</span>';
                        }  elseif ($value->is_active == 2) {

                            echo '<span class="label label-danger">Inactive</span>';
                        } 
                        ?>
                    </td>
              
                <td class="text-right">
                        <?php
                        if ($value->is_active == 1) {?>
<a href="<?php echo site_url("Customer/customer_inactive/$value->customer_id") ?>" class="btn btn-danger btn-xs"><i class="fa fa-minus" aria-hidden="true"></i></a>
    <?php
                        } else{?>
<a href="<?php echo site_url("Customer/customer_active/$value->customer_id") ?>" class="btn btn-success btn-xs"><i class="fa fa-plus" aria-hidden="true"></i></a>
                        <?php }?>
                    </td>
            </tr>
          <?php 
          $i++;
            }
            ?> 
        </tbody>
    </table>
</div>
    <script>
function excel(e) {
    //e.preventDefault();

    //getting data from our table
    var data_type = 'data:application/vnd.ms-excel';
    var table_div = document.getElementById('table_wrapper');
    var table_html = table_div.outerHTML.replace(/ /g, '%20');

    var a = document.createElement('a');
    a.href = data_type + ', ' + table_html;
    a.download = 'exported_table_' + Math.floor((Math.random() * 9999999) + 1000000) + '.xls';
    a.click();
  }
</script>

    
    </div>
  </div>

<!-- generic_modal-->
<div class="modal fade" id="product_modal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header hcolor">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" id="edit_modal_title">Add Generic Name</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal">
                <div class="form-group">

                    <label class="control-label col-sm-3" for="name">Generic Name<span style="color:red; padding: 5px;">*</span> :</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control cleang" id="generic_name" name="generic_name" placeholder="Enter Generic Name">
                        <input type="hidden" class="form-control cleang" id="id_medicine_generic_groups" name="id_medicine_generic_groups" placeholder="Enter Generic Name">
                    </div>
                </div>
                <div class="form-group">

                    <label class="control-label col-sm-3" for="remark">Remarks:</label>
                    <div class="col-sm-9">
                        <textarea class="form-control cleang" id="remarks" name="remarks" placeholder="Enter Remark" rows="5"></textarea>
                    </div>
                </div>
                <div class="form-group">

                    <label class="control-label col-sm-3" for="remark"></label>
                    <div class="col-sm-9">
                        <span id="msgg_generic"></span>
                       </div>
                </div>

            </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <a type="submit" onclick="save_update_all_generic_name_info()()" class="btn btn-success">Save Generic</a>
        </div>
      </div>
      
    </div>
  </div>
<script type="text/javascript">
function pdf() {
    

       $('#mytable').tableExport({
           type:'pdf',
           escape:false
            
        });
}
function open_modal(element) {
    
       
        $('.clean').val('');
        $('#product_modal').modal({
          keyboard: false
        });
       
    }
</script>
<style>
    
    .hcolor{
        background-color: #0088cc;
        color: white;
    }
    #generic_search_result_ul{
        background-color:#0088CC;
        color:white;
        list-style:none;
        margin:0;
        padding:0;
        min-height:50px;
        max-height:200px;
        overflow-y: scroll;
    }
    .generic_search_result_li{
       padding:2px 10px;
    }
    .generic_search_result_li:hover{
        background-color:yellowgreen;
        color:white;
      cursor: pointer;
    }
</style>


