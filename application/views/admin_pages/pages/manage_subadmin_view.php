<?php

//echo '<pre>';
//print_r($all_subadmin);
?>



<h2 class="text-center" style="margin: 0px; padding: 10px;">Sub Admin</h2>
 
  <div class="panel panel-default">
    <div class="panel-heading">
      <div class="text-right">
               <a  onclick="open__modal($(this))" style="margin-top: 10px;margin-bottom: 10px;" class="btn btn-info btn-sm"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;Create New</a>
    
        </div>
    </div>
      
    <div class="panel-body">
        <div id="msg_all"></div>
    <div class="table-responsive"> 
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>#</th>
<!--                <th>Sub-Admin Image</th>-->
                <th>Sub-Admin Name</th>
                <th>Sub-Admin email</th>
                <th>Sub-Admin Password</th>
                <th>Sub-Admin Contact</th>
                <th>Sub-Admin Address</th>
               
                <th class="text-right">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php 
          //  echo '<pre>';
          //  print_r($all_vendor);
            $i=1;
            foreach ($all_subadmin as $value) {     
           
            ?>            
            <tr>
                <td><?php echo $i;?></td> 
<!--                <td><img height="50" class="img-thumbnail" src="<?php echo $value->vendor_name;?>" alt="Image"></td>
               -->
                <td><?php echo $value->vendor_name;?></td>
                <td><?php echo $value->vendor_email;?></td>
                <td><?php echo $value->vendor_password;?></td>
                <td><?php echo $value->vendor_contact;?></td>
                <td><?php echo $value->vendor_address;?></td>
              
               
              
                <td class="text-right">

                    <a class="btn btn-success btn-xs" onclick="edit_vendor_store($(this))" vendor_id="<?php echo $value->vendor_id?>"><i class="fa fa-edit" aria-hidden="true"></i></a>
                        <a class="btn btn-danger btn-xs" onclick="delete_vendor_store($(this))" vendor_id="<?php echo $value->vendor_id?>"><i class="fa fa-trash" aria-hidden="true"></i></a>
               </td>
            </tr>
          <?php 
          $i++;
            }
            ?> 
        </tbody>
    </table>
</div>
    
    
    </div>
  </div>

<!-- generic_modal-->
<div class="modal fade" id="product_modal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header hcolor">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" id="edit_modal_title">Add Sub Admin</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal">
                <div class="form-group">

                    <label class="control-label col-sm-4">Sub-Admin Name<span style="color:red; padding: 5px;">*</span> :</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control cleang" id="vendor_name" name="vendor_name" placeholder="Enter Sub-Admin Name">
                        <input type="hidden" class="form-control cleang" id="vendor_id" name="vendor_id" >
                    </div>
                </div>
                <div class="form-group">

                    <label class="control-label col-sm-4">Sub Admin Email<span style="color:red; padding: 5px;">*</span> :</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control cleang" id="vendor_email" name="vendor_email" placeholder="Enter Sub-Admin Email">
                           </div>
                </div>
               
                <div class="form-group">

                    <label class="control-label col-sm-4">Sub Admin Password<span style="color:red; padding: 5px;">*</span> :</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control cleang" id="vendor_password" name="vendor_password" placeholder="Enter Sub-Admin Password">
                        </div>
                </div>
                <div class="form-group">

                    <label class="control-label col-sm-4">Sub Admin Contact<span style="color:red; padding: 5px;">*</span> :</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control cleang" id="vendor_contact" name="vendor_contact" placeholder="Enter Sub-Admin contact">
                        </div>
                </div>
                <div class="form-group">

                    <label class="control-label col-sm-4">Sub Admin Address<span style="color:red; padding: 5px;">*</span> :</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control cleang" id="vendor_address" name="vendor_address" placeholder="Enter Sub-Admin Address">
                        </div>
                </div>
                
                <div class="form-group">

                    <label class="control-label col-sm-4"></label>
                    <div class="col-sm-8">
                        <span id="msggm"></span>
                       </div>
                </div>

            </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <a type="submit" onclick="save_update_all_subadmin_info()" class="btn btn-success"><span id="btn">Save Sub-Admin</span></a>
        </div>
      </div>
      
    </div>
  </div>
<script type="text/javascript">
    
    
    
    
function save_update_all_subadmin_info() {
   
    var vendor_id=$('#vendor_id').val(); 
    var vendor_name=$('#vendor_name').val(); 
    var vendor_email=$('#vendor_email').val(); 
    var vendor_password=$('#vendor_password').val(); 
    var vendor_contact=$('#vendor_contact').val(); 
    var vendor_address=$('#vendor_address').val(); 
    console.log(vendor_id);  
    console.log(vendor_name);  
    console.log(vendor_email);  
    console.log(vendor_contact);  
    console.log(vendor_address);  
    
    
      if (vendor_name == '' || vendor_email == '' || vendor_contact == '' || vendor_address == '' ) {

            $('#msggm').html('<div class="alert alert-danger"><strong>Error!</strong> Please Required Field Fillup</div>');
                  $('#msggm').delay(1000).show().fadeOut('slow');
} else {

            $.ajax({
                type: 'POST',
                url: '<?php echo site_url('Customer/ajax_save_update_subadmin_info') ?>',
                data: {
                    vendor_id: vendor_id,
                    vendor_name: vendor_name,
                    vendor_email: vendor_email,
                    vendor_password: vendor_password,
                    vendor_contact: vendor_contact,
                    vendor_address:vendor_address
                   
                }
            }).done(function (result) {
                $('#msggm').html(result.msgg); 
                location.reload();
                console.log(result);
            });
        }
    
    
    
}

function delete_vendor_store(element){
        
      var vendor_id=element.attr('vendor_id');

//       alert(id_medicine_generic_groups);
        if(!confirm('Are you sure you want to delete this?')){
    
    return false;
}
        $.ajax({
                type: 'POST',
                url: '<?php echo site_url('Customer/ajax_delete_vendor_name') ?>',
                data: {
                    vendor_id: vendor_id
                  
                }
            }).done(function (result) {
                $('#msg_all').html(result.msgg);
                location.reload();
                $('#msg_all').delay(2000).show().fadeOut('slow');
                console.log(result);
            });
        
        
        
        
        
    }
function edit_vendor_store(element) {

  var vendor_id=element.attr('vendor_id');
  //alert(vendor_id);
  $('.clean').val('');
  
  
   $.ajax({
                              type: 'POST',
                              url: '<?php echo site_url('Customer/get_vendor_info') ?>',
                              data: {vendor_id:vendor_id}
                         }).done(function(vendor_info){
                        console.log(vendor_info);
                       
                         $('input#vendor_id').val(vendor_info.vendor_id);      
                         $('input#vendor_name').val(vendor_info.vendor_name);
                         $('input#vendor_email').val(vendor_info.vendor_email);
                         $('input#vendor_password').val(vendor_info.vendor_password);
                         $('input#vendor_contact').val(vendor_info.vendor_contact);
                         $('input#vendor_address').val(vendor_info.vendor_address);
                        
                        $('#edit_modal_title').html('Edit Sub-Admin Name');
                        $('#btn').html('Update');
                        
                        });

  
  
  
  
  
  
        $('#product_modal').modal({
          keyboard: false
        });
}
function open__modal(element) {
    
       
        $('.clean').val('');
        $('#product_modal').modal({
          keyboard: false
        });
       
    }
</script>
<style>
    
    .hcolor{
        background-color: #0088cc;
        color: white;
    }

</style>


