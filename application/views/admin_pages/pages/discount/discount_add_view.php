<div class="box">
    <div class="box-header">
        <h3 class="box-title">Product List</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <table id="example1" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Sl</th>
                    <th><input type="checkbox"></th>
                    <th>Product Name</th>
                    <th>image</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $i = 1;
                
                foreach ($select_all_product as $v_product) {

//    echo '<pre>';
//     print_r($v_product);
//    exit();
                    ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td><input type="checkbox"></td>
                     
                        <td><?php echo $v_product->product_name ?></td>
                        <td><img width="50" height="50" src="<?php echo base_url() . $v_product->product_img ?>"></td>
                    </tr>
    <?php
    $i++;
    ?>   
                <?php } ?>


            </tbody>
            <tfoot>
                <tr>
                      <th>Sl</th>
                    <th><input type="checkbox"></th>
                    <th>Product Name</th>
                    <th>image</th>
                </tr>
            </tfoot>
        </table>
    </div>
    <!-- /.box-body -->
</div>
<!--<div class="row">
     <?php
                $i = 1;
                
                foreach ($select_all_product as $v_product) {
                    
                    
               ?>
    <div class="col-md-4">
        <table class="table" border="1">
            <tr>
                    <th>Sl</th> 
                <th></th>  
                   
                    <th>Product Name</th>
                    <th>image</th>
            </tr>
            
                 <tr>
                       <td><?php echo $i;?></td>
                        <td><input type="checkbox"></td>
                     
                        <td><?php echo $v_product->product_name ?></td>
                        <td><img width="50" height="50" src="<?php echo base_url() . $v_product->product_img ?>"></td>
                    </tr>
           
            
        </table>
    </div>
    <?php
    $i++;
    ?>  
                <?php } ?>
</div>-->
<div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Discount Entry</h3>
            </div>
            <div class="box-body">
              <div class="row">
                  <form action="Ali" method="post">
                <div class="col-xs-1">
                    <b> P.Image</b><img src="" alt="">
                </div>
                 <div class="col-xs-2">
                     <b>Product Item Code</b>
                   <?php
                        $data1 = array(
                            'type' => 'text',
                            'name' => 'txt_product_item_id',
                            'id' => 'txt_product_item_id',
                            'class' => 'form-control'
                        );

                        echo form_input($data1);
                        ?>

                </div>
                <div class="col-xs-2">
                    <b>Product Name</b>
                  <?php
                        $data2 = array(
                            'type' => 'text',
                            'name' => 'txt_product_name',
                            'id' => 'txt_category',
                            'class' => 'form-control'
                        );

                        echo form_input($data2);
                        ?>
                </div>
                <div class="col-xs-2">
                    <b>Sales Price</b>
               <?php
                        $data3 = array(
                            'type' => 'text',
                            'name' => 'txt_sales_price',
                            'id' => 'txt_sales_price',
                            'class' => 'form-control'
                        );

                        echo form_input($data3);
                        ?>
                </div>
                <div class="col-xs-1">
                    <b>Buy Price</b>
                 <?php
                        $data4 = array(
                            'type' => 'text',
                            'name' => 'txt_buy_price',
                            'id' => 'txt_buy_price',
                            'class' => 'form-control'
                        );

                        echo form_input($data4);
                        ?>
                </div>
                <div class="col-xs-1">
                    <b>Discunt</b>
             <?php
                        $data5 = array(
                            'type' => 'text',
                            'name' => 'txt_discount',
                            'id' => 'txt_discount',
                            'class' => 'form-control'
                        );

                        echo form_input($data5);
                        ?>
                </div>
                <div class="col-xs-1">
                    <b>Discunt(%)</b>
                 <?php
                        $data6 = array(
                            'type' => 'text',
                            'name' => 'txt_discount_perct',
                            'id' => 'txt_discount_perct',
                            'class' => 'form-control'
                        );

                        echo form_input($data6);
                        ?>
                </div>
                <div class="col-xs-2">
                    <b>Current Sales Price</b>
                  <?php
                        $data7 = array(
                            'type' => 'text',
                            'name' => 'txt_curr_sales_price',
                            'id' => 'txt_curr_sales_price',
                            'class' => 'form-control'
                        );

                        echo form_input($data7);
                        ?>
                </div>
                  
                    <div class="form-group">
                    <div class="col-sm-10"></div>
                    <div class="col-sm-2"> 

                        <?php echo form_submit(array('id' => 'submit', 'value' => 'Save', 'class' => 'btn btn-success pull-right')); ?>
                    

                    </div>
                </div>
              </form>
              </div>
            </div>
            <!-- /.box-body -->
          </div>

