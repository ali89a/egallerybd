<script type="text/javascript">
    // Ajax post
    $(document).ready(function () {
        $("#txt_category").on('keyup', function(){  
            var txt_category=$('#txt_category').val();
            console.log(txt_category);

            //alert(txt_category);
            jQuery.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>" + "index.php/ajax_post_controller/user_data_submit",
                dataType: 'json',
                data: {txt_category: txt_category},
                success: function (res) {
                    if (res)
                    {
// Show Entered Value
                        jQuery("div#result").show();
                        jQuery("div#value").html(res.username);
                        jQuery("div#value_pwd").html(res.pwd);
                    }
                }
            });
        });
    });
</script>
<!--<script type="text/javascript">
    $(document).ready(function(){
     $("#add_btn").click(function(){
         var category_name=$('#category_name').val();
         var icon=$('#icon').val();
        console.log('#someButton was clicked');
        alert(category_name);
        alert(icon);
     });
    }); 
</script>-->

<style>

</style>

<div class="box">
    <div class="box-header">
        <h3 class="box-title">Category List</h3>
    </div>
   
    <!-- start add model -->
  

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form class="form-horizontal" action="<?php echo base_url(); ?>Category/category_save" method="POST" id="cat_add_form" enctype="multipart/form-data">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    
                        <div class="form-group">
                
                            <label class="control-label col-sm-2" for="category_name">Title:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="txt_category" id="txt_category" placeholder="Category Title">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="icon">Icon:</label>
                            <div class="col-sm-10"> 
                                <input type="file" class="form-control" id="icn" name="icn">
                                <p style="color: red;">Must Upload min-height=200px,min-Width=200px OR 1:1 </p>
                            </div>
                        </div>
                
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" id="add_btn" >Save changes</button>
                </div>
            </div>
       </form> 
        </div>
    </div>
    <!-- end add model -->
    <!-- /.box-header -->
    <div class="box-body">
          <!-- Button trigger modal -->
          <button type="button" class="btn btn-success btn-sm pull-right" style="margin-bottom: 10px;" data-toggle="modal" data-target="#exampleModal">
        <span>
           <i class="fa fa-plus-circle" aria-hidden="true"></i>

            Create Category
        </span>
    </button>
        <table id="example1" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Sl</th>
                    <th>Category Name</th>
                    <th>Category Icon</th>
                    <th>Status</th>
                    <th class="text-right">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $i = 1;
                foreach ($select_all_category as $v_category) {
  
//     echo '<pre>';
//     print_r($v_category);
//     exit();
                    ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $v_category->category_name ?></td>
                        <td><img width="50" height="50" src="<?php echo base_url(). $v_category->category_icon ?>"></td>
                        <td>
                            <?php
                            if ($v_category->is_active == 1) {
                                echo "<span class='label label-success'>Active</span>";
                            } else {
                                echo "<span class='label label-danger'>Inactive</span>";
                            }
                            ?>
                        </td>

                        <td class="text-right">
                          
                                        <?php 
                                        if($v_category->is_active == 1){
                                            
                                       
                                        ?>
                                        <a href="<?php echo base_url();?>Category/inactive/<?php echo $v_category->category_id?>" data-toggle="tooltip" data-placement="bottom" title="Inactive"><button class="btn btn-danger btn-xs"><span class="fa fa-lock"></span></button></a> 
                                        <?php }
 else {
                                        
                                        ?>
                                        <a href="<?php echo base_url();?>Category/active/<?php echo $v_category->category_id?>" data-toggle="tooltip" data-placement="bottom" title="Active"><button class="btn btn-success btn-xs"><span class="fa fa-unlock"></span></button></a>  
                                        
 <?php }?>
                                
                                   <a href="<?php echo base_url();?>Category/edit_category/<?php echo $v_category->category_id?>"  data-toggle="tooltip" data-placement="bottom" title="Edit"><button class="btn btn-primary btn-xs"><span class="fa fa-edit"></span></button></a> 
                                 <a href="<?php echo base_url();?>Category/delete_category/<?php echo $v_category->category_id?>" data-toggle="tooltip" data-placement="bottom" title="Delete" onclick="return confirm('Are you sure you want to delete this category?');"><button class="btn btn-danger btn-xs"><span class="fa fa-trash"></span></button></a>
                              


                        </td>
                    </tr>
    <?php
    $i++;
    ?>   
                <?php } ?>


            </tbody>
            <tfoot>
                <tr>
                   <th>Sl</th>
                    <th>Category Name</th>
                    <th>Category Icon</th>
                    <th>Status</th>
                  <th class="text-right">Action</th>
                </tr>
            </tfoot>
        </table>
    </div>
    <!-- /.box-body -->
</div>


