     
  


<div class="col-md-12">


    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Payment</h3>
        </div>
        <a href="<?php echo base_url();?>Common/manage_order"><button class="btn btn-primary"><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;Manage Oder</button></a>
        <!-- /.box-header -->
        <div class="box-body">
         <div class="">
    <table class="table table-bordered">
    <thead>
      <tr>
        <th>Sl</th>
        <th>Payment Date</th>
        <th>Payment Type</th>
        <th>Trx ID</th>
         <th>Order Total</th>
        <th>Payment Status</th>
        
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>1</td>
         <td><?php echo $payment_info->payment_date;?></td>
        <td>
            <?php 
            if ($payment_info->payment_type==1) {
                echo 'Cash On Delivery';
            }
            elseif ($payment_info->payment_type==2) {
             echo 'Rocket';
        }
            elseif ($payment_info->payment_type==3) {
             echo 'Bkash';
        }
           
            ?>
        </td>
          <td><?php echo $payment_info->trx_id;?></td>
          <td>BDT&nbsp;<?php echo $payment_info->order_total;?></td>
        <td>
            <?php
            if ($payment_info->payment_status == 1) {
                echo '<span style="color: white;background: #00733e;padding: 2px 10px;">Pending</span>';
            } elseif ($payment_info->payment_status == 2) {
                echo '<span style="color: white;background: red;padding: 2px 10px;">Pending</span>';
            } elseif ($payment_info->payment_status == 3) {
                echo '<span style="color: white;background: #00733e;padding: 2px 10px;">Approved</span>';
            }
            ?>
        </td>

        
        <td>
           
            <a href="<?php echo site_url("Common/due_payment/$payment_info->payment_id") ?>" class="btn btn-danger btn-xs">Pending</a> 
            <a href="<?php echo site_url("Common/paid_payment/$payment_info->payment_id") ?>" class="btn btn-success btn-xs">Approve</a> 
            
        </td>
      </tr>
     
    </tbody>
  </table>
</div>  
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix">
           
        </div>
    </div>
</div>  <!-- /.box -->
