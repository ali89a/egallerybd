<html>
    <head>
<title>eGallerybd.com</title>
<style>
   
    body{
    font-size: 12px;
/*        border: 1px #006400 solid;*/
    }
    table{
       border-collapse: collapse; 
    }
     
</style>
</head>
    <body>
        <div class="container">
            <div class="row">
                <div style="float: left;width: 45%">
                    <h2 style="color:green">eGallerybd</h2>
                    <p  style="">
                        <b>Mobile:</b>+8801881202020 <br>
                        <b>Email:</b>info@egallerybd.com<br>
                        <b>Address:</b>Muktobangla Shopping Complex(3rd Floor)<br>

                        Mirpur-1, Dhaka-1216.
                        </p> 
                </div>
             
               
            </div>
           
            
     
            
            <div class="row" >

                <div style=" font-size: 12px;">
           <h3 align="center">Client Info</h3>  
                    <table border="1" width="100%">
                        <tbody>
                            
                               
                           
                            <tr>
                                <td>
                                    <table  width="100%">
                                        <tbody>
                                              <tr>
                                                  <td colspan="2"> <h3 style="text-decoration: underline;">Customer Info:</h3></td>
                                                
                                            </tr>
                                            <tr>
                                                <td><span class="required"></span> Name:</td>
                                                <td><?php echo $customer_info->customer_name; ?></td>
                                            </tr>

                                            <tr>
                                                <td><span class="required"></span>Email Address:</td>
                                                <td>
                                                    <?php echo $customer_info->customer_email; ?>

                                                </td>
                                            </tr>
                                              <tr>
                                                <td><span class="required"></span> Gender:</td>
                                                <td>
                                                    <?php 
                                                    if ($customer_info->gender==1) {
                                                        echo 'Male';
                                                    }elseif ( $customer_info->gender==2) {
                                                        echo 'Female'; 
                                                    }
                                                   
                                                    ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><span class="required"></span> Mobile Number:</td>
                                                <td>
                                                    <?php echo $customer_info->mobile; ?><br>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><span class="required"></span> Address:</td>
                                                <td>
                                                    <?php echo $customer_info->address; ?>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                    <td>

                                        <table width="100%">
                                            <tbody>
                                                <tr>
                                                    <td colspan="2"> <h3 style="text-decoration: underline;">Shipping Address:</h3></td>

                                                </tr>

                                                <tr>
                                                    <td><span class="required"></span> Name:</td>
                                                    <td>
                                                        <?php echo $shipping_info->ship_name; ?>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td><span class="required"></span>Email Address:</td>
                                                    <td>
                                                        <?php echo $shipping_info->ship_email; ?>

                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td><span class="required"></span> Mobile Number:</td>
                                                    <td>
                                                        <?php echo $shipping_info->ship_phone; ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><span class="required"></span> Address:</td>
                                                    <td>
                                                        <?php echo $shipping_info->ship_address; ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><span class="required"></span> City:</td>
                                                    <td>
                                                        <?php echo $shipping_info->ship_city; ?>
                                                    </td>
                                                </tr>
<?php 
if ($shipping_info->ship_zip) {
    

?>
                                                <tr>
                                                    <td><span class="required"></span> Zip Code:</td>
                                                    <td>
                                                        <?php echo $shipping_info->ship_zip; ?>
                                                    </td>
                                                </tr>

<?php }?>
                                            </tbody>
                                        </table>
                                    </td>
                            </tr>





                        </tbody>
                    </table>


                </div>




            </div>

            <br/>
            <div class="row-fluid">

                <div class="span6">
                 <h3 align="center">Payment Info</h3>
                     <table border="1" width="100%" style="margin: 10px 0px;">
                       
                        <tr>
                            <td width="15%" align="center">Payment ID: <b><?php echo $shipping_info->payment_id;?> </b></td>
                           
                            <td width="25%" align="center">Payment Method: <b>
                          
                            <?php 
                            if($customer_info->payment_type==1){
                           echo "Cash On Delivery";
                           }
                            elseif($customer_info->payment_type==2){
                           echo "Roket";
                           }
                            elseif($customer_info->payment_type==3){
                           echo "Bkash";
                           }
                            ?>
                            
                             </b>
                            </td>
                            <td width="25%" align="center">Transaction id:<b>
                          
                            <?php 
                           if($customer_info->trx_id){
                           echo $customer_info->trx_id;
                           }
                            else{
                           echo "N/A";
                           }
                            
                            
                            ?>
                            </b>
                            </td>
                            <td width="35%" align="center">Payment Date:<b>
                           <?php echo $customer_info->payment_date;?>
                            </b></td>
                        </tr>

                    </table> 
                 <?php 
                                    $amont_ship=$this->db->get_where('shipping_cost',array('shipping_area_id'=>$shipping_info->shipping_area_id))->row()->ship_cost_amt;
                                  
                                    ?>
                 
                     <h3 align="center">Order Info</h3>
                    <table border="1" width="100%" style="margin: 10px 0px;">
                       
                        <tr>
                            <td width="25%" align="center">Order ID:<b><?php echo $shipping_info->order_id; ?></b></td>
                            
                            <td width="35%" align="center">Order Total:<b>Tk&#2547;&nbsp;<?php echo $shipping_info->order_total+$amont_ship-$shipping_info->coupon_discount; ?></b></td>
                           
                            <td width="40%" align="center">Order Date:<b><?php echo $shipping_info->order_date; ?></b></td>
                           
                        </tr>

                    </table> 
                   
                </div>
            </div>

            <div class="row-fluid">
                <div class="span8">
                 <h3 align="center">Product Info</h3>
               
                    <table class="table table-condensed" style="border: 1px #000 solid" width="100%" >
                        <tr>
                            <td align="center">Product Name</td>
                            <td align="center">Product Price</td>
                            <td align="center">Product Quantity</td>
                            <td colspan="2" align="right">Subtotal</td>

                        </tr>

                        <?php foreach ($order_details_info as $value) { ?>
                            <tr>
                                <td align="center"><?php echo $value->product_name; ?></td>
                                <td align="center">Tk&#2547;&nbsp;<?php echo $value->product_price; ?></td>
                                <td align="center"><?php echo $value->product_sales_qty; ?></td>
                                 <td  align="right">Tk&#2547;&nbsp;</td>
                                <td align="right"><?php echo $value->product_price * $value->product_sales_qty ?></td>

                            </tr>
                        <?php } ?>
                            <tr>
                                <td colspan="2"></td>
                                <td  align="right" style="border-top: 1px #000 dotted;"><strong>Total</strong></td>
                                <td  align="right" style="border-top: 1px #000 dotted;">Tk&#2547;&nbsp;</td>
                                <td align="right" style="border-top: 1px #000 dotted;"><?php echo $shipping_info->order_total; ?></td>
                            </tr>
                            <tr>
                                <td colspan="2"></td>
                                <td  align="right"><strong>Shipping Charge(+)</strong></td>
                                <td  align="right" >Tk&#2547;&nbsp;</td>
                                <td align="right">
                                    <?php 
//                                    $amont_ship=$this->db->get_where('shipping_cost',array('shipping_area_id'=>$shipping_info->shipping_area_id))->row()->ship_cost_amt;
                                    echo $amont_ship;
                                    ?>
                                
                                </td>
                            </tr>
                        <tr>
                            <td colspan="2"></td>
                            <td  align="right"><strong>Coupon Discount(-)</strong></td>
                            <td  align="right">Tk&#2547;&nbsp;</td>
                            <td align="right"><?php echo $shipping_info->coupon_discount; ?></td>
                        </tr>
                        <tr>
                            <td colspan="2"></td>
                            <td  align="right"><strong>Advance(-)</strong></td>
                            <td  align="right">Tk&#2547;&nbsp;</td>
                            <td align="right"><?php echo $shipping_info->order_advance; ?></td>
                        </tr>
                        <tr>
                            <td colspan="2"></td>
                            <td  align="right"><strong>Total Due</strong></td>
                            <td  align="right">Tk&#2547;&nbsp;</td>

                            <td align="right"><span><?php echo $shipping_info->order_total-$shipping_info->order_advance-$shipping_info->coupon_discount+$amont_ship; ?></span></td>
                        </tr>
                    </table>
                    <?php 
                 if($shipping_info->order_return_text !=''){
                 ?>
                 <h5  style="color:red;font-size: 18px;padding: 5px;"><?php echo $shipping_info->order_return_text; ?></h5>
                 
                 <?php 
                 }
                 ?>
                 <div class="row" >

                <div style=" font-size: 12px;">
           <h3 align="center" style="border-top: 2px dotted #000;">Client Info</h3>  
                    <table border="1" width="100%">
                        <tbody>
                            
                               
                           
                            <tr>
                                <td>
                                    <table  width="100%">
                                        <tbody>
                                              <tr>
                                                  <td colspan="2"> <h3 style="text-decoration: underline;">Customer Info:</h3></td>
                                                
                                            </tr>
                                            <tr>
                                                <td><span class="required"></span> Name:</td>
                                                <td><?php echo $customer_info->customer_name; ?></td>
                                            </tr>

                                            <tr>
                                                <td><span class="required"></span>Email Address:</td>
                                                <td>
                                                    <?php echo $customer_info->customer_email; ?>

                                                </td>
                                            </tr>
                                              <tr>
                                                <td><span class="required"></span> Gender:</td>
                                                <td>
                                                    <?php 
                                                    if ($customer_info->gender==1) {
                                                        echo 'Male';
                                                    }elseif ( $customer_info->gender==2) {
                                                        echo 'Female'; 
                                                    }
                                                   
                                                    ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><span class="required"></span> Mobile Number:</td>
                                                <td>
                                                    <?php echo $customer_info->mobile; ?><br>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><span class="required"></span> Address:</td>
                                                <td>
                                                    <?php echo $customer_info->address; ?>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                    <td>

                                        <table width="100%">
                                            <tbody>
                                                <tr>
                                                    <td colspan="2"> <h3 style="text-decoration: underline;">Shipping Address:</h3></td>

                                                </tr>

                                                <tr>
                                                    <td><span class="required"></span> Name:</td>
                                                    <td>
                                                        <?php echo $shipping_info->ship_name; ?>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td><span class="required"></span>Email Address:</td>
                                                    <td>
                                                        <?php echo $shipping_info->ship_email; ?>

                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td><span class="required"></span> Mobile Number:</td>
                                                    <td>
                                                        <?php echo $shipping_info->ship_phone; ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><span class="required"></span> Address:</td>
                                                    <td>
                                                        <?php echo $shipping_info->ship_address; ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><span class="required"></span> City:</td>
                                                    <td>
                                                        <?php echo $shipping_info->ship_city; ?>
                                                    </td>
                                                </tr>
<?php 
if ($shipping_info->ship_zip) {
    

?>
                                                <tr>
                                                    <td><span class="required"></span> Zip Code:</td>
                                                    <td>
                                                        <?php echo $shipping_info->ship_zip; ?>
                                                    </td>
                                                </tr>

<?php }?>
                                            </tbody>
                                        </table>
                                    </td>
                            </tr>





                        </tbody>
                    </table>


                </div>




            </div>
                </div>
            </div>


        </div>



    </body>

</html>
