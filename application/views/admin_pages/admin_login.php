<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>EgalleryBD | Log in</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="<?php echo base_url() ?>admin_asset/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?php echo base_url() ?>admin_asset/dist/css/AdminLTE.min.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="<?php echo base_url() ?>admin_asset/plugins/iCheck/square/blue.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style>
            .msg_fadeout a{
                color: white!important;
                
            }
            div.fixed {
    position: fixed;
    top: 0;
    right: 0;
    width: 300px;
   
}
        </style>
    </head>
    <body class="hold-transition login-page" style="background-color: #40BC83;">
       
<!--        <div class="pull-right msg_fadeout fixed">
            <?php if ($this->session->userdata('success')) { ?>
                <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    <strong>Success!</strong> <?php echo $this->session->userdata('success'); ?>
                </div>

            <?php } else if ($this->session->userdata('error')) { ?>

                <div class="alert alert-danger">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    <strong>Error!</strong> <?php echo $this->session->userdata('error'); ?>
                </div>

            <?php } else if ($this->session->userdata('warning')) { ?>

                <div class="alert alert-warning">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    <strong>Warning!</strong> <?php echo $this->session->userdata('warning'); ?>
                </div>

            <?php } else if ($this->session->userdata('info')) { ?>

                <div class="alert alert-info">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    <strong>Info!</strong> <?php echo $this->session->userdata('info'); ?>
                </div>
            <?php } ?>

        </div>-->
        <div class="pull-right msg_fadeout fixed">
          <?php if($this->session->flashdata('success')){ ?>
        <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
        </div>

    <?php } else if($this->session->flashdata('error')){  ?>

        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
        </div>

    <?php } else if($this->session->flashdata('warning')){  ?>

        <div class="alert alert-warning">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Warning!</strong> <?php echo $this->session->flashdata('warning'); ?>
        </div>

    <?php } else if($this->session->flashdata('info')){  ?>

        <div class="alert alert-info">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>
        </div>
    <?php } ?>

        </div>
        <div class="login-box">
          
            <div class="login-logo">
                <a href="<?php echo base_url() ?>admin"><b>eGallery</b>bd<br>Admin Panel</a>
            </div>
            <!-- /.login-logo -->
            <div class="login-box-body" style="box-shadow: 0px 2px 28px 9px rgba(1,13,8,1);">
<!--                <p class="login-box-msg">Sign in to start your session</p>-->
                <img style="margin-bottom: 15px;box-shadow:1px 2px 28px 2px rgba(128,164,58,0.95); "  src="<?php echo base_url() ?>admin_asset/dist/img/logo.jpg" alt="">
                <form action="<?php echo base_url() ?>admin/admin_login_check" method="post">
                    <div class="form-group has-feedback">
                        <input type="email" name="email" class="form-control" placeholder="Email">
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password" name="password" class="form-control" placeholder="Password">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    <div class="row">
                       
                        
                        <!-- /.col -->
                        <div class="col-xs-12">
                            <button style="margin-bottom:20px; background-color: #80A43A;color: white;" type="submit" class="btn btn-default btn-block btn-flat">Sign In</button>
                        </div>
                        <!-- /.col -->
                    </div>
                </form>
                <a style="text-align: center;"  href="<?php echo base_url();?>Admin_Pass_Forgot/admin_pass_forget">I forgot my password</a>


            </div>
            <!-- /.login-box-body -->
        </div>
        <!-- /.login-box -->

        <!-- jQuery 2.2.3 -->
        <script src="<?php echo base_url() ?>admin_asset/plugins/jQuery/jquery-2.2.3.min.js"></script>
        <!-- Bootstrap 3.3.6 -->
        <script src="<?php echo base_url() ?>admin_asset/bootstrap/js/bootstrap.min.js"></script>
        <!-- iCheck -->
        <script src="<?php echo base_url() ?>admin_asset/plugins/iCheck/icheck.min.js"></script>
        <script>
            $(function () {
                $('input').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue',
                    increaseArea: '20%' // optional
                });
            });
        </script>
         <script>
        $(function() {
            $('.msg_fadeout').delay(1000).show().fadeOut('slow');
        });
        </script>
    </body>
</html>
