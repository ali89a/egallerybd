<?php
class Email extends CI_Controller{
    
    public function recovery_password($enc_email=NULL)
    {
     
         $email = str_replace("%F2","/", $enc_email );

        $email_address=$this->encrypt->decode($email);
//        echo '<pre>';
//        print_r($email_address);
//        exit();
         
        $data = array();  
        $cdata = array();
        $cdata['email_address'] = $email_address;
          //  all page common start
        $data['all_active_category'] = $this->Adeshbroad_Model->get_all_active_category();
        $data['about_us_front'] = $this->Adeshbroad_Model->get_all_about_us();
        $data['advertisement'] = $this->Adeshbroad_Model->get_all_advertisement();
        $data['youtube_video'] = $this->Adeshbroad_Model->get_youtube_video();
        $data['all_active_product'] = $this->Adeshbroad_Model->get_all_active_product();
    // all page common end
        
        $data['main_home_content'] = $this->load->view('front/password_recovery_form_view', $cdata, true);
        $this->load->view('master', $data);
    }
    public function admin_recovery_password($enc_email=NULL)
    {
     
         $email = str_replace("%F2","/", $enc_email );

        $email_address=$this->encrypt->decode($email);
//        echo '<pre>';
//        print_r($email_address);
//        exit();
         
        $data = array();  
        //$cdata = array();
        $data['email_address'] = $email_address;
       // $data['main_home_content'] = $this->load->view('front/password_recovery_form_view', $cdata, true);
        //$this->load->view('master', $data);
         $this->load->view('admin_pages/admin_password_recovery_form_view', $data);
    }
     public function update_password()
    {
        $this->Mailer_Model->update_new_password();
        $sdata=array();
        $sdata['updatemessage']="Update Your New Password Successfully !";
        $this->session->set_userdata($sdata);
        redirect("email/update_password_successfully");
    }
    public function update_password_successfully()
    {
        $data=array();
        
        $data['title']="Password Recovery Form";
      
          //  all page common start
        $data['all_active_category'] = $this->Adeshbroad_Model->get_all_active_category();
        $data['about_us_front'] = $this->Adeshbroad_Model->get_all_about_us();
        $data['advertisement'] = $this->Adeshbroad_Model->get_all_advertisement();
        $data['youtube_video'] = $this->Adeshbroad_Model->get_youtube_video();
        $data['all_active_product'] = $this->Adeshbroad_Model->get_all_active_product();
    // all page common end
        
        $data['main_home_content']=  $this->load->view('front/update_password_successfully_view',$data,true);
        $this->load->view('master',$data);
    }
    public function update_admin_password_successfully()
    {
       $this->load->view('admin_pages/admin_login');
    }
    
}