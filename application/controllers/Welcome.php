<?php


defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

    public function index() {
         $cdata['letest_products'] = $this->Home_model->select_product_by_letest_product();
         $cdata['bigoffer_products'] = $this->Home_model->select_product_by_bigoffer_products();
         $cdata['youtube_video'] = $this->Home_model->select_product_youtube_video();
         $cdata['slider_images'] = $this->Home_model->select_slider_images();
         $cdata['all_pub_category'] = $this->Home_model->get_all_pub_category();
         $cdata['visitor_count'] = $this->Adeshbroad_Model->get_visitor_count();
        $data['all_published_category'] = $this->Home_model->get_all_pub_category();
        $data['home_content'] = $this->load->view('front_pages/home_content', $cdata, TRUE);
        $this->load->view('master',$data);
    }
    
    public function product_details($id) {
         $data['all_published_category'] = $this->Home_model->get_all_pub_category();
            $cdata['visitor_count'] = $this->Adeshbroad_Model->get_visitor_count();
        $cdata['product_details_by_id'] = $this->Home_model->get_product_details_by_id($id);
        $data['home_content'] = $this->load->view('front_pages/product_details_view', $cdata, TRUE);
        $this->load->view('master',$data);
    }
    public function cart() {
         $data['all_published_category'] = $this->Home_model->get_all_pub_category();
            $cdata['visitor_count'] = $this->Adeshbroad_Model->get_visitor_count();
        $data['home_content'] = $this->load->view('front_pages/cart_view', $cdata, TRUE);
        $this->load->view('master',$data);
    }
    public function gallery() {
         $data['all_published_category'] = $this->Home_model->get_all_pub_category();
          $cdata['visitor_count'] = $this->Adeshbroad_Model->get_visitor_count();
        $data['home_content'] = $this->load->view('front_pages/gallery_view', $cdata, TRUE);
        $this->load->view('master',$data);
    }
    public function about() {
         $data['all_published_category'] = $this->Home_model->get_all_pub_category();
            $data['visitor_count'] = $this->Adeshbroad_Model->get_visitor_count();
            
             $cdata['all_career'] = $this->Adeshbroad_Model->get_all_about_new();
        $data['home_content'] = $this->load->view('front_pages/about_view',$cdata, TRUE);
        $this->load->view('master',$data);
    }
    public function career() {
         $data['all_published_category'] = $this->Home_model->get_all_pub_category();
          $data['visitor_count'] = $this->Adeshbroad_Model->get_visitor_count();
             $cdata['all_career'] = $this->Adeshbroad_Model->get_all_career();
     
        $data['home_content'] = $this->load->view('front_pages/career_view',$cdata, TRUE);
        $this->load->view('master',$data);
    }
    public function return_policy() {
         $data['all_published_category'] = $this->Home_model->get_all_pub_category();
          $data['visitor_count'] = $this->Adeshbroad_Model->get_visitor_count();
       $cdata['all_career'] = $this->Adeshbroad_Model->get_all_return_policy();
        $data['home_content'] = $this->load->view('front_pages/return_policy_view', $cdata, TRUE);
        $this->load->view('master',$data);
    }
    public function term_condition() {
         $data['visitor_count'] = $this->Adeshbroad_Model->get_visitor_count();
         $data['all_published_category'] = $this->Home_model->get_all_pub_category();
         $cdata['all_career'] = $this->Adeshbroad_Model->get_all_term_condition();
        $data['home_content'] = $this->load->view('front_pages/term_condition_view',  $cdata, TRUE);
        $this->load->view('master',$data);
    }
    public function other_business() {
         $data['visitor_count'] = $this->Adeshbroad_Model->get_visitor_count();
         $data['all_published_category'] = $this->Home_model->get_all_pub_category();
          $cdata['all_career'] = $this->Adeshbroad_Model->get_all_other_business();
        $data['home_content'] = $this->load->view('front_pages/other_business_view', $cdata, TRUE);
        $this->load->view('master',$data);
    }
    public function contact() {
         $data['visitor_count'] = $this->Adeshbroad_Model->get_visitor_count();
         $data['all_published_category'] = $this->Home_model->get_all_pub_category();
        $data['home_content'] = $this->load->view('front_pages/contact_view', '', TRUE);
        $this->load->view('master',$data);
    }
    public function show_invoice() {
         $data['visitor_count'] = $this->Adeshbroad_Model->get_visitor_count();
         $data['all_published_category'] = $this->Home_model->get_all_pub_category();
        $data['home_content'] = $this->load->view('front_pages/invoice_view', '', TRUE);
        $this->load->view('master',$data);
    }
    
     public function download_invoice($id){
   
         
       $cdata['shipping_info'] = $this->Adeshbroad_Model->get_shipping_info($id);
       $cdata['customer_info'] = $this->Adeshbroad_Model->get_customer_info($id);
        
//       echo '<pre>';
//       print_r($cdata);
//       exit();
        
        $cdata['order_details_info'] = $this->Adeshbroad_Model->get_order_details_info($id);
        // $this->load->view('admin/pages/download_invoice', $data);
        $this->load->helper('dompdf');
        $view_file=$this->load->view('front_pages/download_invoice_view', $cdata,true);
        $file_name=pdf_create($view_file, 'egallery_invoice-00'.$id);
        echo $file_name;
        
    }
        public function ajax_shipping_cost(){
        
       
        $area_code=$this->input->post('area_code');
        
        $result=$this->Adeshbroad_Model->get_shipping_cost_by_id_info($area_code);
            $ssdata=array();
            $ssdata['ship_cost_amt']=$result->ship_cost_amt;
            $this->session->set_userdata($ssdata);
        echo  $ssdata['ship_cost_amt'];
    }
       public function cancle_order_request($id)
            {
        
        $this->Adeshbroad_Model->cancle_order_request_by_id($id);
        $this->session->set_flashdata('cancelled_order_item', 'order_cancelled');
        redirect('Welcome/account');
            }
    public function account() {
        $data['visitor_count'] = $this->Adeshbroad_Model->get_visitor_count();
         $data['all_published_category'] = $this->Home_model->get_all_pub_category();
        $cdata['customer_active'] = $this->Adeshbroad_Model->get_customer_by_id($this->session->userdata('customer_id'));
        $cdata['order_customer_active'] = $this->Adeshbroad_Model->get_order_customer_by_id($this->session->userdata('customer_id'));

        $data['home_content'] = $this->load->view('front_pages/account_view', $cdata, TRUE);
        $this->load->view('master', $data);
    }
     public function update_userinfo(){
        
       $data = array(
              
                'customer_name' =>$this->input->post('customer_name'),
                'customer_email' =>$this->input->post('customer_email'),
                'mobile' =>$this->input->post('mobile'),
                'address' =>$this->input->post('address'),
                'dob' =>$this->input->post('dob'),
                'blood_group' =>$this->input->post('blood_group'),
              
                'update_by' =>$this->session->userdata('customer_id'),
                'update_time' => date('Y-m-d h:m:s')
         );
       
       //Transfering data to Model
             $this->Adeshbroad_Model->update_customer_info($this->session->userdata('customer_id'),$data);
            $data['message'] = 'Data Inserted Successfully';
//Loading View
//            $this->load->view('master', $data);
             $this->session->set_flashdata('tab_name', 'profile');
            redirect('welcome/account');
     }
     public function update_userimg(){
        
       $data = array(
              
                'update_by' =>$this->session->userdata('customer_id'),
                'update_time' => date('Y-m-d h:m:s')
         );
              if (!empty($_FILES['userFiles']['name'])) {
            $filesCount = count($_FILES['userFiles']['name']);
            for ($i = 0; $i < $filesCount; $i++) {
               // echo $i;
                $_FILES['userFile']['name'] = $_FILES['userFiles']['name'][$i];
                $_FILES['userFile']['type'] = $_FILES['userFiles']['type'][$i];
                $_FILES['userFile']['tmp_name'] = $_FILES['userFiles']['tmp_name'][$i];
                $_FILES['userFile']['error'] = $_FILES['userFiles']['error'][$i];
                $_FILES['userFile']['size'] = $_FILES['userFiles']['size'][$i];
                $uploadPath = 'upload_image/customer_image/';
                $config['upload_path'] = $uploadPath;
                $config['allowed_types'] = 'gif|jpg|JPG|jpeg|JPEG|png|txt';
                //$config['max_size']	= '303374600';
                //$config['max_width'] = '1024000';
                //$config['max_height'] = '768000';
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if ($this->upload->do_upload('userFile')) {
                    $fileData = $this->upload->data();
                    //$uploadData[$i]['file_name'] = $fileData['file_name'];
                    //$uploadData[$i]['created'] = date("Y-m-d H:i:s");
                    $uploadData[$i] = $uploadPath.$fileData['file_name'];
                }
                
            }
            //var_dump($uploadData);
            if (!empty($uploadData)) {
                $data['img'] = implode(',', $uploadData);
            }
        }
//            echo '<pre>';
//            print_r($data);
//            exit();
//Transfering data to Model
             $this->Adeshbroad_Model->update_customer_info($this->session->userdata('customer_id'),$data);
            $data['message'] = 'Data Inserted Successfully';
//Loading View
//            $this->load->view('master', $data);
             $this->session->set_flashdata('tab_name', 'profile');
            redirect('welcome/account');
        }
           public function view_order_info()
            {
       
        
        $cdata = array();
        $data = array();
        $data['error_msg'] = "";
        //javascritpt load from Bookes.js
        //$data['page_scripts'] = array('admin_asset/js/client_mail.js');
        $data['title'] = "Email Sender";
        $p_id = $this->input->post('b_id', TRUE);
        
//        echo '<pre>';
//        print_r($p_id);
//        exit();
        $cdata['shipping_info']=  $this->Adeshbroad_Model->get_shipping_info($p_id);

    $cdata['customer_info']=  $this->Adeshbroad_Model->get_customer_info($p_id);
//           echo '<pre>';
//            print_r($cdata);
//           exit();
    $cdata['order_details_info']=  $this->Adeshbroad_Model->get_order_details_info($p_id);
       // $data['view_order'] = $this->Adeshbroad_Model->get_view_order($p_id);
//        echo '<pre>';
//        print_r( $cdata['order_details_info']);
//        exit();
 $amont_ship=$this->db->get_where('shipping_cost',array('shipping_area_id'=>$cdata['shipping_info']->shipping_area_id))->row()->ship_cost_amt;
      
 $total=$cdata['customer_info']->order_total+$amont_ship-$cdata['customer_info']->coupon_discount ;
 
 $output = null;
        $output .='<div class="modal-body" >';
          $output .= ' <div class="row">
     
      
      <div class="col-sm-5 text-right">
      <p><b>Customer Info</b></p>
     <p>'.$cdata['customer_info']->customer_name . '</p>
     <p>'.$cdata['customer_info']->customer_email . '</p>
     <p>'.$cdata['customer_info']->address . '</p>
     <p>'.$cdata['customer_info']->mobile . '</p>
      </div>
       <div class="col-sm-2 "></div>
      <div class="col-sm-5 ">
      <p><b>Shipping Address</b></p>
     <p>'.$cdata['shipping_info']->ship_name . '</p>
     <p>'.$cdata['shipping_info']->ship_email . '</p>
     <p>'.$cdata['shipping_info']->ship_phone . '</p>
     <p>'.$cdata['shipping_info']->ship_alt_phone . '</p>
     <p>'.$cdata['shipping_info']->ship_address . '</p>
     <p>'.$cdata['shipping_info']->ship_city .$cdata['shipping_info']->ship_zip. '</p>
     
      </div>
    </div>';
          $output .= ' <div class="row">
     
      
      <div class="col-sm-5 text-right">
      <p><b>Payment Info</b></p>
     <p>';
      $output .= $cdata['customer_info']->payment_type==1?"Cash on Delivery":($cdata['customer_info']->payment_type==2?"Bkash":($cdata['customer_info']->payment_type==3?"Rocket":"")) . '</p>';
      
 	if($cdata['customer_info']->trx_id){
         $output .= '<p>'."Transaction Id:".$cdata['customer_info']->trx_id . '</p>';
      }
      else{
          $output .= '<p>N/A</p>';
      }
     
      
   $output .= '<p>'."Payment Date:".$cdata['customer_info']->payment_date.'</p>';
    
    
     $output .= ' </div>
       <div class="col-sm-2 "></div>
      <div class="col-sm-5 ">
      <p><b>Order Info</b></p>
     <p>'."Order Id:".$cdata['shipping_info']->order_id . '</p>
      <p>'."Order Total:".$cdata['shipping_info']->order_total . '</p>
       <p>'."Order Date:".$cdata['shipping_info']->order_date . '</p>
   
   
     
      </div>
    </div>';
          $output .= ' <div class="row ">
     
    
      <div class="col-sm-12 ">
      <p class="text-center"><b>Order summary</b><p>
   <table class="table">
    <thead>
 
      <tr>
        <th>Item</th>
        <th>Price</th>
        <th>Quantity</th>
        <th>Sub Total</th>
      </tr>
    </thead>
    <tbody>
  ';
          foreach ($cdata['order_details_info'] as $value) {
              $output .= '<tr>
        <td>'.$value->product_name.'</td>
        <td>&#2547;&nbsp;'.$value->product_price.'</td>
        <td>'.$value->product_sales_qty.'</td>
        <td>&#2547;&nbsp;'.$value->product_price* $value->product_sales_qty.'</td>
      
        
      </tr>';
          }

 $output .= '
     <tr>
       <td></td>
        <td></td>
        <td>Shipping Cost</td>
        <td>&#2547;&nbsp;'.$amont_ship . '</td>
     </tr>
     <tr>
       <td></td>
        <td></td>
        <td>Coupon Discount</td>
        <td>&#2547;&nbsp;'.$cdata['customer_info']->coupon_discount . '</td>
     </tr>
     <tr>
       <td></td>
        <td></td>
        <td>Total</td>
        <td>&#2547;&nbsp;'.$total . '</td>
     </tr>
    </tbody>
  </table>
     
      </div>
    </div>';

  
   

       

        $output .='</div>';
        echo $output;
  
    }
      public function customer_password_change() {
        $current_pass = $this->input->post('cpass');
        $data = array(
    'password' => md5($this->input->post('npass'))
        );
        $this->Adeshbroad_Model->get_customer_pass_check_by_id( $current_pass, $data);
          $this->session->set_flashdata('cancelled_order_item', 'change');
        redirect('Welcome/account');
    }
 public function add_to_cart($id=NULL) {
      
       $product_by_id= $this->Adeshbroad_Model->get_product_by_id($id);
          
       $quantity=$this->input->post('quantity',true);
//       echo '<pre>';
//       print_r($product_by_id);
//       exit();
      
       
          
          if($quantity==NULL){
             $qty=1; 
          }  else {
               $qty=$quantity;
          }
         
        $selling_price=$product_by_id->current_sale_price;
        $selling_price_discount=$product_by_id->bdt_discount;
          

        $data = array(
            'id' => $product_by_id->product_id,
            'qty' => $qty,
            'price' => $selling_price,
            'product_discount' => $selling_price_discount,
            'name' => $product_by_id->product_name,
            'image' => $product_by_id->product_img_master
        );
        //  echo '<pre>';
       //   print_r($data);
          //exit();
        $this->cart->insert($data);
//        redirect("Home/single_product/$id");
        
        $this->session->set_flashdata('success', 'Added to Cart');
        redirect("Welcome");
        
    }
     public function update_cart() {
        $qty = $this->input->post('qty', true);
        $rowid = $this->input->post('rowid', true);

//        
//        echo '<pre>';
//        print_r($qty.'-----------------------'.$rowid);
//        exit();


        $data = array(
            'rowid' => $rowid,
            'qty' => $qty
        );

        $this->cart->update($data);
      redirect('welcome/cart');
    }
    public function cart_remove($rowid) {
        $this->cart->remove($rowid);
        redirect('welcome/cart');
    }
    public function cart_destroy() {
        $this->cart->destroy();
       $this->session->unset_userdata('coupon_dis');
        redirect('Home/shopping_cart');
    }
    //=========Login & Registration=============
    public function login(){
     
         $data = array();
        $data['main_home_content'] = $this->load->view('front/login', "", true);
          $data['count_new'] =$this->Pagination_Model->record_count_new();
          $data['all_active_category']=  $this->Adeshbroad_Model->get_all_active_category();
         $data['about_us_front']=  $this->Adeshbroad_Model->get_all_about_us();
         $data['advertisement']=  $this->Adeshbroad_Model->get_all_advertisement();
          $data['youtube_video'] = $this->Adeshbroad_Model->get_youtube_video();
           $data['all_active_product']=  $this->Adeshbroad_Model->get_all_active_product();
        $this->load->view('master', $data);
    }
    public function register(){
     
         $data = array();
           $data['count_new'] =$this->Pagination_Model->record_count_new();
           $data['all_active_category']=  $this->Adeshbroad_Model->get_all_active_category();
         $data['about_us_front']=  $this->Adeshbroad_Model->get_all_about_us();
         $data['advertisement']=  $this->Adeshbroad_Model->get_all_advertisement();
          $data['youtube_video'] = $this->Adeshbroad_Model->get_youtube_video();
           $data['all_active_product']=  $this->Adeshbroad_Model->get_all_active_product();
        $data['main_home_content'] = $this->load->view('front/register', "", true);
        $this->load->view('master', $data);
    }
    public function customer_registration(){
          
        
        $pass = $this->input->post('password');
        $cpass = $this->input->post('confirm_password');
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
        $is_exists_email=$this->db->get_where('customer',array('customer_email'=>$email))->num_rows();
        if ($pass == $cpass && $is_exists_email == 0) {

                 $data = array(
                'customer_name' => $this->input->post('name'),
                'mobile' => $this->input->post('mobile'),
                'address' => $this->input->post('address'),
                'dob' => $this->input->post('dob'),
                'customer_email ' => $this->input->post('email'),
                'password' =>$this->input->post('password')
            );
//        echo '<pre>';
//        print_r($data);
//        exit();

            $this->db->insert('customer', $data);
            $customer_id = $this->db->insert_id();
            $sdata = array();
            $sdata['customer_id'] = $customer_id;
            $sdata['customer_name'] = $data['customer_name'];
            $this->session->set_userdata($sdata);
            redirect('welcome');
        } else {
            $this->session->set_flashdata('error', 'Password Not Match');
            redirect('welcome');
        }
    }
function checkout(){
        if ($this->session->userdata('customer_id')!=NULL) {
            
        
         $data['all_published_category'] = $this->Home_model->get_all_pub_category();
          $data['visitor_count'] = $this->Adeshbroad_Model->get_visitor_count();
         $data['home_content'] = $this->load->view('front_pages/shipping_payment_view', '', TRUE);
        $this->load->view('master',$data);
    } else {
        $this->session->set_flashdata('error', 'Your login or Register');
        redirect('welcome/cart');
    }
    
        }
       
        function save_shipping_info(){
            
             $data = array(
            'ship_name' => $this->input->post('customer_name'),
            'ship_email' => $this->input->post('email_address'),
            'ship_phone' => $this->input->post('phone_number'),
            'ship_city ' => $this->input->post('city'),
            'ship_address ' => $this->input->post('address'),
            'shipping_area_id' => $this->input->post('shipping_area_id'),
            'country' => $this->input->post('country'),
            'ship_zip' => $this->input->post('zip_code')
        );
//             echo '<pre>';
//       print_r($data);
//        exit();
           $this->db->insert('shipping',$data);
        $shipping_id=$this->db->insert_id();
        $sdata=array();
           $sdata['shipping_id'] = $shipping_id;  
            $this->session->set_userdata($sdata);
            redirect("welcome/payment_order");
            
        }
         function payment_order(){
        $data['all_published_category'] = $this->Home_model->get_all_pub_category();
         $data['visitor_count'] = $this->Adeshbroad_Model->get_visitor_count();
        $data['home_content'] = $this->load->view('front_pages/payment_order_view','', TRUE);
        $this->load->view('master', $data);
    }
    
    public function place_order()
    {
        $payment_type=$this->input->post('payment_type');
        $this->checkout_model->save_payment_info();
        if($payment_type=='cash_on_delivery')
        {
            $this->checkout_model->save_order_info();
            redirect("checkout/order_successfull");
        }
        else if($payment_type=='paypal')
        {
            
        }
    }                
    
    
    public function save_order(){
        
            if ($this->input->post('optradio')==2) {
            	$trx_id=$this->input->post('tnx_id');
        }
        elseif ($this->input->post('optradio')==3) {
            	$trx_id=$this->input->post('tnx_id');
        }
        else{
        $trx_id=NULL;
        }
//        echo $ship_id;
            $cdata = array(
         
            'payment_type' => $this->input->post('optradio'),
            'trx_id' => $trx_id,
            'payment_date' => date("Y-m-d H:i:s")
        );
      
        $this->db->insert('payment',$cdata);
       $payment_id=$this->db->insert_id();
//       echo $payment_id;
         $ssdata = array(
          'payment_id' => $payment_id
        ); 
         $this->session->set_userdata($ssdata);
       
       $this->Adeshbroad_Model->save_order_info();
         $this->session->unset_userdata('shipping_id');
        $this->session->unset_userdata('payment_id');
        $this->session->unset_userdata('coupon_discount');
        $this->session->unset_userdata('shipping_charge');
           redirect('welcome/order_successfull');
    }
    public function order_successfull()
    {
         $data=array();
        $data['title']="Order Successfull";
     $data['all_published_category'] = $this->Home_model->get_all_pub_category();
         $data['visitor_count'] = $this->Adeshbroad_Model->get_visitor_count();
        $data['home_content']=  $this->load->view('front_pages/invoice_view','',true);
        $this->load->view('master',$data);
    }
    function category_product($id){
         $data['all_published_category'] = $this->Home_model->get_all_pub_category();
          $data['visitor_count'] = $this->Adeshbroad_Model->get_visitor_count();
        $cdata['products'] = $this->db->get_where('product', array('category_id' => $id))->result();
        $data['home_content'] = $this->load->view('front_pages/category_product_view', $cdata, TRUE);
        $this->load->view('master', $data);
    }
    
    function brand_product($id){
         $data['all_published_category'] = $this->Home_model->get_all_pub_category(); 
         $data['visitor_count'] = $this->Adeshbroad_Model->get_visitor_count();
        $cdata['products'] = $this->db->get_where('product', array('sub_category_three_id' => $id))->result();
        $data['home_content'] = $this->load->view('front_pages/brand_product_view', $cdata, TRUE);
        $this->load->view('master', $data);
    }
    function top_brand_product($id){
         $data['all_published_category'] = $this->Home_model->get_all_pub_category(); 
         $data['visitor_count'] = $this->Adeshbroad_Model->get_visitor_count();
        $cdata['products'] = $this->db->get_where('product', array('vendor_store_id' => $id))->result();
        $data['home_content'] = $this->load->view('front_pages/top_brand_product_view', $cdata, TRUE);
        $this->load->view('master', $data);
    }
    function view_all_big_offer_product(){
         $data['all_published_category'] = $this->Home_model->get_all_pub_category();
          $data['visitor_count'] = $this->Adeshbroad_Model->get_visitor_count();
        $cdata['big_offer_products'] = $this->db->query("Select * from product where discount > 0")->result();
        $data['home_content'] = $this->load->view('front_pages/view_all_big_offer_product_view', $cdata, TRUE);
        $this->load->view('master', $data);
    }
    
    
    public function customer_logout(){
          $this->session->unset_userdata('customer_id');
            $this->session->unset_userdata('customer_name');
            redirect('welcome');
    }
    
    public function newslatter_save(){
         $data = array(
            'email' => $this->input->post('email'),
            'insert_time' =>  date("Y-m-d H:i:s")
             
            
        );

        $this->db->insert('newsletter', $data);
        echo "<p style='color:red;background-color: #449D44;padding: 10px;'>Data Inserted Successfully</p>";
         //redirect('Welcome');
    }
       public function save_review()
                {
            $id=$this->input->post('product_id');
             $data = array(
            
                'name' => $this->input->post('name'),
                'email' => $this->input->post('email'),
                'review' => $this->input->post('review'),
                'product_id' => $id,
                'product_name' => $this->input->post('product_name'),
             
                'date' => date('Y-m-d h:m:s')
            
            );
//               echo '<pre>';
//            print_r($data);
//            exit();
            $this->db->insert('review',$data);
            $sdata['rev_message'] = 'Review Inserted Successfully';
             $this->session->set_userdata($sdata);
            
             redirect("Welcome/product_details/$id");
          
                }
    
    
    
    public function forgot_password(){
        $data = array();
          $data['count_new'] =$this->Pagination_Model->record_count_new();
         $data['all_active_category']=  $this->Adeshbroad_Model->get_all_active_category();
         $data['about_us_front']=  $this->Adeshbroad_Model->get_all_about_us();
         $data['advertisement']=  $this->Adeshbroad_Model->get_all_advertisement();
          $data['youtube_video'] = $this->Adeshbroad_Model->get_youtube_video();
           $data['all_active_product']=  $this->Adeshbroad_Model->get_all_active_product();
        $data['main_home_content'] = $this->load->view('front/forgot_password_view', "", true);
        $this->load->view('master', $data);
    }
    public function customer_email_check() {
        $sdata = array();
        $email = $this->input->post('email', true);
        $result = $this->Adeshbroad_Model->customer_email_check_info($email);
        if ($result) {



            $data = array();
            $data['from_address'] = " info@tatpalli.com";
            $data['admin_full_name'] = "Tatpalli Admin";
            $data['to_address'] = $email;
            $data['subject'] = "Password Reset Email";
            $data['customer_name'] = $result->customer_name;
            $this->Mailer_Model->send_email($data, 'password_recovery_email');
            redirect('Home/send_password_successfully');
        } else {
            $sdata['mailmessage']="Your are Not a Registart Email Address";
           $this->session->set_userdata($sdata);
            redirect('Home/forgot_password');
        }
    }
    public function send_password_successfully(){
          $data = array();
            $data['count_new'] =$this->Pagination_Model->record_count_new();
           $data['all_active_category']=  $this->Adeshbroad_Model->get_all_active_category();
         $data['about_us_front']=  $this->Adeshbroad_Model->get_all_about_us();
         $data['advertisement']=  $this->Adeshbroad_Model->get_all_advertisement();
          $data['youtube_video'] = $this->Adeshbroad_Model->get_youtube_video();
           $data['all_active_product']=  $this->Adeshbroad_Model->get_all_active_product();
        $data['main_home_content'] = $this->load->view('front/send_password_successfully_view', "", true);
        $this->load->view('master', $data);
    }

    public function customer_login_check() {
          
        $email_address = $this->input->post('email');
        $password =$this->input->post('password');
      
       
        
        $result = $this->Adeshbroad_Model->customer_login_check_info($email_address, $password);
//      echo '<pre>';
//       print_r($result);
//       exit();
        $sdata = array();
        if ($result!=null) {
            $sdata['customer_id'] = $result->customer_id;
            $sdata['customer_name'] = $result->customer_name;
            $this->session->set_userdata($sdata);
        
               redirect('welcome');
        }
         else {
            $sdata['ex_message'] = "Enter Your Valid Email or Password !!!";
            $this->session->set_userdata($sdata);
            redirect('welcome');
        }
    }
   
//=========Login & Registration=============
   
  public function check_coupon_discount(){
        $coupon_code = $this->input->post('coupon_code');
        $result=$this->Adeshbroad_Model->get_coupon_discount_info($coupon_code);
        
   
        if ($result) {
            
            $sdata = array();
            $sdata['coupon_discount'] = $result->coupon_dis_tk;         
            $this->session->set_userdata($sdata);
           
        }
 else {
           $sdata = array();
            $sdata['coupon_discount'] = 0;
            $this->session->set_userdata($sdata);
        }
        redirect('welcome/cart');
    } 
    
    
    
   function ajax_search_product_name(){
//      header('Content-Type:Application/json');
       
        $search_text = $this->input->post('search_text');
        $product_info = $this->db->query('SELECT product_name,product_id FROM product WHERE product_name LIKE "%'.$search_text.'%"')->result();
        $result='';
        $result.='<ul  id="search_result_ul" class="form-control">';
        foreach ($product_info as $value) {
             $result.='<li class="search_result_li"><a href="'.base_url().'welcome/product_details/'.$value->product_id.'">'.$value->product_name.'</a></li>';
        }
        $result.='</ul>';
//        echo json_encode($generic_info);
        echo $result;
    }
        public function save_contact(){
           
        $data = array(
            'name' => $this->input->post('name'),
            'email ' => $this->input->post('email'), 
            'phone ' => $this->input->post('phone'), 
            'message ' => $this->input->post('message'),
            'date ' => date('Y-m-d H:i:s')
        
          
        );
         $sdata=array();
        $sdata['message']="Save Your Message Successfully !";
        $this->session->set_userdata($sdata);
        $this->db->insert('contact_us',$data);
        
        redirect('welcome/contact');
    }
    
}
