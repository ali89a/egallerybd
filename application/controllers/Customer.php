<?php

class Customer extends CI_Controller{
    //put your code here
     public function __construct() {
        parent::__construct();
        $res=$this->session->userdata('id');
        if ($res == NULL) {
            redirect('admin', 'refresh');
        }
    }
    public function manage_customer(){
        
        
        $data = array();
        $cdata = array();
        $data['title'] = 'Customer';
        $cdata['all_customer']=  $this->Adeshbroad_Model->get_all_customer();
        $data['admin_main_content'] = $this->load->view('admin_pages/pages/manage_customer_view',$cdata, TRUE);
        $this->load->view('admin_pages/admin_master', $data);
  
    }
    public function search_customer(){
         $search_name= $this->input->post('search_text');
        
        $data = array();
        $cdata = array();
        $data['title'] = 'Customer';
        $cdata['all_customer']=  $this->Adeshbroad_Model->get_search_all_customer($search_name);
        $data['admin_main_content'] = $this->load->view('admin_pages/pages/manage_customer_view',$cdata, TRUE);
        $this->load->view('admin_pages/admin_master', $data);
  
    }
      public function search_vendor(){
 $search_name= $this->input->post('search_name');

        $data = array();
        $cdata = array();
        $data['title'] = 'Customer';
        $cdata['all_vendor']=  $this->Adeshbroad_Model->get_all_search_vendor_store($search_name);
        $data['admin_main_content'] = $this->load->view('admin_pages/pages/manage_vendor_view',$cdata, TRUE);
        $this->load->view('admin_pages/admin_master', $data);
    }
    
    public function customer_inactive($id){
         $this->Adeshbroad_Model->inactive_customer_info($id);
        redirect('Customer/manage_customer');
    }
    public function customer_active($id){
         $this->Adeshbroad_Model->active_customer_info($id);
        redirect('Customer/manage_customer');
    }

        public function manage_vendor(){
 
        $data = array();
        $cdata = array();
        $data['title'] = 'Customer';
        $cdata['all_vendor']=  $this->Adeshbroad_Model->get_all_vendor_store();
        $data['admin_main_content'] = $this->load->view('admin_pages/pages/manage_vendor_view',$cdata, TRUE);
        $this->load->view('admin_pages/admin_master', $data);
    }
      

    
    
    public function ajax_delete_vendor_store_name(){
        
        
         header('Content-Type:Application/json');
       
        $vendor_store_id = $this->input->post('vendor_store_id');
        if($this->db->where('vendor_store_id',$vendor_store_id)->delete('vendor_store'))
                {
                    
                 $data2['msgg']='<div class="alert alert-danger"><strong>Successfully!</strong> Deleted</div>';
                }
                else
                {
                $data2['msgg']='<span style="color:red"><b>Not Deleted</b></span>'; 
                }
        
       echo json_encode($data2); 
    }
    public function ajax_delete_vendor_name(){
        
        
         header('Content-Type:Application/json');
       
        $vendor_id = $this->input->post('vendor_id');
        if($this->db->where('vendor_id',$vendor_id)->delete('vendor'))
                {
                    
                 $data2['msgg']='<div class="alert alert-danger"><strong>Successfully!</strong> Deleted</div>';
                }
                else
                {
                $data2['msgg']='<span style="color:red"><b>Not Deleted</b></span>'; 
                }
        
       echo json_encode($data2); 
    }
    public function ajax_delete_top_brand_name(){
        
        
         header('Content-Type:Application/json');
       
        $vendor_store_id = $this->input->post('vendor_store_id');
        if($this->db->where('top_brand_id',$vendor_store_id)->delete('top_brand'))
                {
                    
                 $data2['msgg']='<div class="alert alert-danger"><strong>Successfully!</strong> Deleted</div>';
                }
                else
                {
                $data2['msgg']='<span style="color:red"><b>Not Deleted</b></span>'; 
                }
        
       echo json_encode($data2); 
    }
    public function get_vendor_store_info(){
          header('Content-Type:Application/json');
         $vendor_store_id = $this->input->post('vendor_store_id');

        $vendor_info = $this->db->get_where('vendor_store', array('vendor_store_id' => $vendor_store_id))->row();
      
        echo json_encode($vendor_info);
    }
    public function get_vendor_info(){
          header('Content-Type:Application/json');
         $vendor_id = $this->input->post('vendor_id');

        $vendor_info = $this->db->get_where('vendor', array('vendor_id' => $vendor_id))->row();
      
        echo json_encode($vendor_info);
    }
    public function get_top_brand_info(){
          header('Content-Type:Application/json');
         $vendor_store_id = $this->input->post('vendor_store_id');

        $vendor_info = $this->db->get_where('top_brand', array('top_brand_id' => $vendor_store_id))->row();
      
        echo json_encode($vendor_info);
    }
    public function ajax_save_update_subadmin_info(){
        
          header('Content-Type:Application/json');
       
        $vendor_id= $this->input->post('vendor_id');
        $data['vendor_name'] = $this->input->post('vendor_name');
        $data['vendor_email'] = $this->input->post('vendor_email');
        $data['vendor_password'] = $this->input->post('vendor_password');
        $data['vendor_contact'] = $this->input->post('vendor_contact');
        $data['vendor_address'] = $this->input->post('vendor_address');
     
       if(empty($vendor_id))
       {
            
         $is_exists_vendor = $this->db->get_where('vendor', array('vendor_email' => $data['vendor_email']))->num_rows();
            if ($is_exists_vendor == 0) {    
                    $this->db->insert('vendor',$data);
                    $vendor_store_id=$this->db->insert_id();
 
                    if($vendor_store_id)
                       {
                      $data2['msgg']=  '<div class="alert alert-success"><strong>Successfully!</strong>Inserted</div>';
                       }
                    else
                       {
                     $data2['msgg']= '<div class="alert alert-danger"><strong>Error!</strong>Not Inserted</div>'; 
                   }
                   
             } else {
                $data2['msgg'] = '<div class="alert alert-danger"><strong>Error!</strong>Already Exists</div>';
            }       
        } 
        
        else
        {
                if($this->db->where('vendor_id',$vendor_id)->update('vendor',$data))
                {
                    
                 $data2['msgg']='<div class="alert alert-success"><strong>Successfully!</strong> Updated</div>';
                }
                else
                {
                $data2['msgg']='<span style="color:red"><b>Not Updated</b></span>'; 
                }
        }
        
        echo json_encode($data2);
    }
    public function ajax_save_update_vendor_info(){
        
          header('Content-Type:Application/json');
       
        $vendor_store_id= $this->input->post('vendor_store_id');
        $data['vendor_store_name'] = $this->input->post('vendor_name');
        $data['vendor_store_email'] = $this->input->post('vendor_email');
        $data['vendor_store_contact'] = $this->input->post('vendor_contact');
        $data['vendor_store_address'] = $this->input->post('vendor_address');
     
       
        if(empty($vendor_store_id))
       {
            
         $is_exists_vendor = $this->db->get_where('vendor_store', array('vendor_store_email' => $data['vendor_store_email']))->num_rows();
            if ($is_exists_vendor == 0) {    
                    $this->db->insert('vendor_store',$data);
                    $vendor_store_id=$this->db->insert_id();
 
                    if($vendor_store_id)
                       {
                      $data2['msgg']=  '<div class="alert alert-success"><strong>Successfully!</strong>Inserted</div>';
                       }
                    else
                       {
                     $data2['msgg']= '<div class="alert alert-danger"><strong>Error!</strong>Not Inserted</div>'; 
                   }
                   
             } else {
                $data2['msgg'] = '<div class="alert alert-danger"><strong>Error!</strong>Already Exists</div>';
            }       
        } 
        
        else
        {
                if($this->db->where('vendor_store_id',$vendor_store_id)->update('vendor_store',$data))
                {
                    
                 $data2['msgg']='<div class="alert alert-success"><strong>Successfully!</strong> Updated</div>';
                }
                else
                {
                $data2['msgg']='<span style="color:red"><b>Not Updated</b></span>'; 
                }
        }
        
        echo json_encode($data2);
    }
    public function ajax_save_update_top_brand_info(){
        
          header('Content-Type:Application/json');
       
        $top_brand_id= $this->input->post('vendor_store_id');
        $data['top_brand_name'] = $this->input->post('vendor_name');
        $data['top_brand_email'] = $this->input->post('vendor_email');
        $data['top_brand_contact'] = $this->input->post('vendor_contact');
        $data['top_brand_address'] = $this->input->post('vendor_address');
     
         //image upload\\
        $name = $_FILES["vendor_icon"]["name"];
        $ext = end((explode(".", $name)));
        $icon_name=implode('_',explode( " ",$data['top_brand_name']));
//        $icon_name=implode('_',preg_split( "/ (-|.|\s+|_) /", $data['test_name'] ));
        $img_size=getimagesize($_FILES["vendor_icon"]['tmp_name']);
        if($img_size[1]>=32&&$img_size[1]<=700){
        move_uploaded_file($_FILES["vendor_icon"]['tmp_name'], 'uploads/top_brand/'.$icon_name.'.' . $ext);
        }
        //---image upload ends here\\
         $data['top_brand_icon'] = "uploads/top_brand/".$icon_name.".".$ext;
        if(empty($vendor_store_id))
       {
            
         $is_exists_vendor = $this->db->get_where('top_brand', array('top_brand_email' => $data['top_brand_email']))->num_rows();
            if ($is_exists_vendor == 0) {    
                    $this->db->insert('top_brand',$data);
                    $vendor_store_id=$this->db->insert_id();
 
                    if($vendor_store_id)
                       {
                      $data2['msgg']=  '<div class="alert alert-success"><strong>Successfully!</strong>Inserted</div>';
                       }
                    else
                       {
                     $data2['msgg']= '<div class="alert alert-danger"><strong>Error!</strong>Not Inserted</div>'; 
                   }
                   
             } else {
                $data2['msgg'] = '<div class="alert alert-danger"><strong>Error!</strong>Already Exists</div>';
            }       
        } 
        
        else
        {
                if($this->db->where('top_brand_id',$top_brand_id)->update('top_brand',$data))
                {
                    
                 $data2['msgg']='<div class="alert alert-success"><strong>Successfully!</strong> Updated</div>';
                }
                else
                {
                $data2['msgg']='<span style="color:red"><b>Not Updated</b></span>'; 
                }
        }
        
        echo json_encode($data2);
    }
 
     
    
    
    
    
    public function vendor_request_accept($id) {
        $this->Adeshbroad_Model->vendor_request_accept_info($id);
        redirect('Product/view');
    }

    public function vendor_request_reject($id) {
        $this->Adeshbroad_Model->vendor_request_reject_info($id);

        redirect('Customer/manage_vendor_request');
    }

    public function manage_vendor_request(){
        
        
        $data = array();
        $cdata = array();
        $data['title'] = 'Customer';
       
        $data['admin_main_content'] = $this->load->view('admin_pages/pages/manage_vendor_request_view',$cdata, TRUE);
        $this->load->view('admin_pages/admin_master', $data);
  
    }
    public function manage_vendor_accept_product(){
        
        
        $data = array();
        $cdata = array();
        $data['title'] = 'Customer';
        $data['admin_main_content'] = $this->load->view('admin_pages/pages/manage_vendor_accept_view',$cdata, TRUE);
        $this->load->view('admin_pages/admin_master', $data);
  
    }
    public function manage_vendor_reject_product(){
        
        
        $data = array();
        $cdata = array();
        $data['title'] = 'Customer';
   
        $data['admin_main_content'] = $this->load->view('admin_pages/pages/manage_vendor_reject_view',$cdata, TRUE);
        $this->load->view('admin_pages/admin_master', $data);
  
    }
    public function manage_subadmin(){
        
        
        $data = array();
        $cdata = array();
        $data['title'] = 'Sub Admin';
        $cdata['all_subadmin']=  $this->Adeshbroad_Model->get_all_vendor();
        $data['admin_main_content'] = $this->load->view('admin_pages/pages/manage_subadmin_view',$cdata, TRUE);
        $this->load->view('admin_pages/admin_master', $data);
  
    }
}
