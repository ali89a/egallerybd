<?php

class Vendor_dashboard extends CI_Controller{
    //put your code here
  public function __construct() {
        parent::__construct();
        $res=$this->session->userdata('vendor_id');
        if ($res == NULL) {
            redirect('vendor', 'refresh');
        }
    }

    public function index() {


        $data = array();
        $data['title'] = 'Dashboard';
        $data['vendor_main_content'] = $this->load->view('vendor/dashboard','', TRUE);
        $this->load->view('vendor/vendor_master', $data);
    }
    
    
        public function get_subcat_by_cat_id(){
        
        $category_id = $this->input->post('category_id');
         $sub_categories=$this->Brand_Model->get_subcat_by_cat_id_info($category_id);
         if (count($sub_categories) > 0) {
             
             $output = '';
            $output .= '<option value="">--Select Sub Category--</option>';
            foreach ($sub_categories as $subcategory) {
                $output .= '<option value="' . $subcategory->sub_category_id . '">' . $subcategory->sub_cat_name . '</option>';
            }
            echo json_encode($output);
         }
        
    }
    public function get_brand_by_sub_cat_id(){
        
        $sub_category_id= $this->input->post('sub_category_id');
         $brands=$this->Brand_Model->get_brand_by_subcat_id_info($sub_category_id);
         if (count($brands) > 0) {
             
             $output = '';
            $output .= '<option value="">--Select Sub-Category-2--</option>';
            foreach ($brands as $brand) {
                $output .= '<option value="' . $brand->brand_id . '">' . $brand->brand_name . '</option>';
            }
            echo json_encode($output);
         }
        
    }

    public function get_sub3_by_brand_id(){
        
        $brand= $this->input->post('brand_id');
         $sub3=$this->Brand_Model->get_sub3_by_brand_id_info($brand);
         if (count($sub3) > 0) {
             
             $output = '';
            $output .= '<option value="">--Select Sub Category2--</option>';
            foreach ($sub3 as $sub_3) {
                $output .= '<option value="' . $sub_3->sub_category_three_id . '">' . $sub_3->sub_category_three_name . '</option>';
            }
            echo json_encode($output);
         }
        
    }
    
    

    public function view_product() {
          $search_text = $this->input->post('search_text');
          if ($search_text != NULL) {
            $a['vendor_product']=$this->db->select('*')
                                          ->from('product')
                                          ->where('vendor_id',$this->session->userdata('vendor_id'))
                                            ->like('product_name',$search_text,'after')
                                            ->or_like('product_code',$search_text,'after')
                                          ->get()->result();  
          }
 else {
     $a['vendor_product']=$this->db->select('*')
        ->from('product')
        ->where('vendor_id',$this->session->userdata('vendor_id'))
        ->get()->result();
 }
        $data = array();
        $data['title'] = 'Product';
        $data['vendor_main_content'] = $this->load->view('vendor/vendor_products_view',$a, TRUE);
        $this->load->view('vendor/vendor_master', $data);
        
        
    }
    public function add_product() {
        
        $data = array();
        $data['title'] = 'Add Product';
        $cdata['select_all_pub_category'] = $this->Category_Model->select_all_published_category_info();
         $cdata['select_all_pub_top_brand'] = $this->Product_Model->select_all_pub_top_brand_info();
       $cdata['select_all_pub_vendor'] = $this->Product_Model->select_all_pub_vendor_info();
       
        $data['vendor_main_content'] = $this->load->view('vendor/vendor_add_products_view',$cdata, TRUE);
        $this->load->view('vendor/vendor_master', $data);
        
        
    }
    public function edit_product($product_id) {
        $data = array();
        $data['title'] = 'Edit Product';
        $cdata = array();
         $cdata['select_all_pub_category'] = $this->Category_Model->select_all_published_category_info();
        $cdata['select_all_pub_sub_cat'] = $this->Sub_Category_Model->select_all_sub_cat_info();
       $cdata['select_all_pub_brand'] = $this->Brand_Model->select_all_brand_info();
        $cdata['select_all_pub_sub3'] = $this->Brand_Model->select_all_sub3_info();
        $cdata['select_product_by_id'] = $this->Product_Model->select_product_by_id($product_id);
         $cdata['select_all_pub_vendor'] = $this->Product_Model->select_all_pub_vendor_info();
           $cdata['select_all_pub_top_brand'] = $this->Product_Model->select_all_pub_top_brand_info();
       // $cdata['select_all_pub_category'] = $this->Category_Model->select_all_published_category_info();

//         echo '<pre>';
//         print_r($cdata);
//         exit();

     $data['vendor_main_content'] = $this->load->view('vendor/vendor_edit_products_view',$cdata, TRUE);
        $this->load->view('vendor/vendor_master', $data);
    }
    
       public function save_product() {



 
        $product_id = $this->input->post('product_id');   
        $vendor_store_id = $this->input->post('vendor_store_id');   
        $top_brand_id = $this->input->post('top_brand_id');   
        $cbo_category = $this->input->post('category_id');   
        $cbo_sub_category = $this->input->post('sub_category_id');
        $cbo_brand = $this->input->post('brand_id');
        $sub3_id = $this->input->post('sub3_id');
        $txt_product = $this->input->post('txt_product'); 
        $txt_product_item_code = $this->input->post('txt_product_item_code');
        $price = $this->input->post('price');
        $new_price = $this->input->post('new_price');
        $discount = $this->input->post('discount');
        $bdtdiscount = $this->input->post('bdtdiscount');
        $txt_short_description = $this->input->post('txt_short_description');
        $txt_long_description = $this->input->post('txt_long_description');

       
        $data= array(
            'top_brand_id' => $top_brand_id,
            'vendor_store_id' => $vendor_store_id,
            'brand_id' => $cbo_brand,
            'category_id' => $cbo_category,
            'sub_category_id' => $cbo_sub_category,
            'sub_category_three_id'=>$sub3_id,
            'product_name' => $txt_product,
            'product_short_des' => $txt_short_description,
            'product_long_des' => $txt_long_description,
            'product_price' => $price,
            'current_sale_price' => $new_price,
            'discount' => $discount,
            'bdt_discount' => $bdtdiscount,
            'product_code' => $txt_product_item_code,
            'inserted_by' => NULL,
            'insert_time' => date('Y-m-d h:m:s'),
            'updated_by' => NULL,
            'update_time' => NULL,
            'is_active' => 0,
            'is_vendor_product' => '1',
             'vendor_id' => $this->session->userdata('vendor_id'),
             'is_product' => 2,
           

        );
      


        //        start single file upload
        $this->load->library('upload');
        $config['upload_path'] = 'upload_image/product_image/master/';
        $config['allowed_types'] = 'gif|jpg|png';
        //$config['max_size']= '200';
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        $error = '';
        $fdata = array();
        if (!$this->upload->do_upload('micon')) {
            $error = $this->upload->display_errors();
            //echo $error;
            /* exit(); */
        } else {
            $fdata = $this->upload->data();
            $data['product_img_master'] = $config['upload_path'] . $fdata['file_name'];
        }
        //end
        
         //        start multiple file upload
         if (!empty($_FILES['userFiles']['name'])) {
            $filesCount = count($_FILES['userFiles']['name']);
            for ($i = 0; $i < $filesCount; $i++) {
              //  echo $i;
                $_FILES['userFile']['name'] = $_FILES['userFiles']['name'][$i];
                $_FILES['userFile']['type'] = $_FILES['userFiles']['type'][$i];
                $_FILES['userFile']['tmp_name'] = $_FILES['userFiles']['tmp_name'][$i];
                $_FILES['userFile']['error'] = $_FILES['userFiles']['error'][$i];
                $_FILES['userFile']['size'] = $_FILES['userFiles']['size'][$i];
                $uploadPath = 'upload_image/product_image/other/';
                $config['upload_path'] = $uploadPath;
                $config['allowed_types'] = 'gif|jpg|JPG|jpeg|JPEG|png|txt';
                //$config['max_size']	= '303374600';
                //$config['max_width'] = '1024000';
                //$config['max_height'] = '768000';
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if ($this->upload->do_upload('userFile')) {
                    $fileData = $this->upload->data();
                    //$uploadData[$i]['file_name'] = $fileData['file_name'];
                    //$uploadData[$i]['created'] = date("Y-m-d H:i:s");
                    $uploadData[$i] = $uploadPath.$fileData['file_name'];
                }
                
            }
            //var_dump($uploadData);
            if (!empty($uploadData)) {
              $data['product_img'] = implode(',', $uploadData);
            }
        }
        //end

if(empty($product_id)){
        $this->db->insert('product', $data);
        $this->session->set_flashdata('success', 'Successfully Saved Product');
       
} else {
    $this->db->where('product_id', $product_id)
                            ->update('product', $data);
     
        $this->session->set_flashdata('success', 'Successfully Update Product');
}
        redirect('Vendor_dashboard/view_product');
    }
    public function product_details($product_id) {
        $data['title'] = 'Product Details';
         $cdata['select_product_by_id'] = $this->Product_Model->select_product_by_id($product_id);
       // $cdata['select_all_pub_category'] = $this->Category_Model->select_all_published_category_info();

//         echo '<pre>';
//         print_r($cdata);
//         exit();

        $data['vendor_main_content'] = $this->load->view('vendor/product_details_view',$cdata, TRUE);
        $this->load->view('vendor/vendor_master', $data);
       
    }
    
    
    public function logout() {
       // $sdata = array();
//        $sdata['message'] = 'Succesfully Logout';
       $this->session->set_flashdata('success', 'Succesfully Logout');
        $this->session->unset_userdata('vendor_name');
        $this->session->unset_userdata('vendor_id');
        redirect('vendor');
    }

}
