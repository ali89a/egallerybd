<?php


class Discount extends CI_Controller{
    //put your code here
    
      public function __construct() {
        parent::__construct();
        $res=$this->session->userdata('id');
        if ($res == NULL) {
            redirect('admin', 'refresh');
        }
    }

    public function index() {


        $data = array();
        $data['title'] = 'Dashboard';
           $cdata['select_all_product'] = $this->Product_Model->select_all_product_info();
        $data['admin_main_content'] = $this->load->view('admin_pages/pages/discount/discount_add_view',$cdata, TRUE);
        $this->load->view('admin_pages/admin_master', $data);
    }
 public function view() {

        $data = array();
        $data['title'] = 'View Discount';
       
        $data['admin_main_content'] = $this->load->view('admin_pages/pages/discount/discount_list_view', '', TRUE);
        $this->load->view('admin_pages/admin_master', $data);
    }
}
