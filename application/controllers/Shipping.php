<?php


class Shipping extends CI_Controller{
    //put your code here
    
     public function __construct() {
        parent::__construct();
        $res=$this->session->userdata('id');
        if ($res == NULL) {
            redirect('admin', 'refresh');
        }
    }
    public function manage_shipping_area(){
        $data = array();
        $cdata = array();
        $data['title'] = 'Manage';
        $cdata['all_shipping_area']=  $this->Adeshbroad_Model->get_all_shipping_area();
        
        $data['admin_main_content'] = $this->load->view('admin_pages/pages/ship_cost/manage_shipping_area_view',$cdata,  TRUE);
       $this->load->view('admin_pages/admin_master', $data);
        
        
    }
    public function manage_shipping_cost(){
        $data = array();
        $cdata = array();
        $data['title'] = 'Manage';
        $cdata['all_shipping_cost']=  $this->Adeshbroad_Model->get_all_shipping_cost();
        
        $data['admin_main_content'] = $this->load->view('admin_pages/pages/ship_cost/manage_shipping_cost_view',$cdata,  TRUE);
       $this->load->view('admin_pages/admin_master', $data);
        
        
    }
     public function inactive_shipping_cost($id){
         $this->Adeshbroad_Model->shipping_cost_inactive_info($id);
        redirect('Shipping/manage_shipping_cost');
    }
     public function active_shipping_cost($id){
         $this->Adeshbroad_Model->shipping_cost_active_info($id);
        redirect('Shipping/manage_shipping_cost');
    } 
    public function delete_shipping_cost($id) {
        $this->Adeshbroad_Model->delete_shipping_cost_info($id);
        redirect('Shipping/manage_shipping_cost');
    }
     public function add_shipping_cost(){
        
        
         $this->load->library('form_validation');

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

//Validating Name Field
        
         $this->form_validation->set_rules('shipping_area_id', 'Shipping Area', 'required');
        $this->form_validation->set_rules('shipping_cost', 'Shipping Cost', 'required');
       



        if ($this->form_validation->run() == FALSE) {
            
       $data = array();
    
        $data['title'] = 'Add';
      
           $data['admin_main_content'] = $this->load->view('admin_pages/pages/ship_cost/add_shipping_cost_view','',  TRUE);
       $this->load->view('admin_pages/admin_master', $data);
        
        
        } 
        else 
            {
//Setting values for tabel columns
            $data = array(
                'shipping_area_id' => $this->input->post('shipping_area_id'),
                'ship_cost_amt' => $this->input->post('shipping_cost'),
                'insert_by' =>$this->session->userdata('id'),
                'insert_time' => date('Y-m-d h:m:s'),
                'update_by' => 0,
                'update_time' => 0,
                'is_active' => 1,
                'status' => 1
            );
//            echo '<pre>';
//            print_r($data);
//            exit();
//Transfering data to Model
            $this->db->insert('shipping_cost',$data);
            $sdata['sc_message']='Data Inserted Successfully';
           $this->session->set_userdata($sdata);
//Loading View
//            $this->load->view('master', $data);
            redirect('Shipping/manage_shipping_cost');
        }
//        ==============
       
    }
    public function edit_shipping_cost($id){
        
        
         $this->load->library('form_validation');

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

//Validating Name Field
        
         $this->form_validation->set_rules('shipping_area_id', 'Shipping Area', 'required');
        $this->form_validation->set_rules('shipping_cost', 'Shipping Cost', 'required');
       



        if ($this->form_validation->run() == FALSE) {
            
            $data = array();
    
            $data['title'] = 'Add';
            $cdata['shipping_cost_by_id']=  $this->Adeshbroad_Model->get_shipping_cost_by_id($id);
            
               $data['admin_main_content'] = $this->load->view('admin_pages/pages/ship_cost/edit_shipping_cost_view',$cdata,  TRUE);
       $this->load->view('admin_pages/admin_master', $data);
        } 
        else 
            {
//Setting values for tabel columns
            $data = array(
                'shipping_area_id' => $this->input->post('shipping_area_id'),
                'ship_cost_amt' => $this->input->post('shipping_cost'),
                'update_by' => $this->session->userdata('id'),
                'update_time' =>date('Y-m-d h:m:s'),
              
            );
//            echo '<pre>';
//            print_r($data);
//            exit();
//Transfering data to Model
               $this->Adeshbroad_Model->update_shipping_cost_by_id($id,$data);
           
           $sdata['scup_message']='Data Updated Successfully';
           $this->session->set_userdata($sdata);
//Loading View
//            $this->load->view('master', $data);
            redirect('Shipping/manage_shipping_cost');
        }
//        ==============
       
    }
    
    public function ajax_save_update_shipping_area_name_info(){
        
         header('Content-Type:Application/json');
        $shipping_area_id = $this->input->post('shipping_area_id');
      
        $data['shipping_area'] = $this->input->post('shipping_area_name');
        $data['status'] = 1;
       
              if (empty($shipping_area_id)) {
            $this->db->insert('shipping_area', $data);
            $test_id = $this->db->insert_id();

            if ($test_id) {

                $data2['msgg'] = '<div class="alert alert-success"><strong>Successfully!</strong>Inserted</div>';
            } else {
                $data2['msgg'] = '<div class="alert alert-danger"><strong>Error!</strong>Not Inserted</div>';
            }
        } else {
            if ($this->db->where('shipping_area_id',$shipping_area_id)
                            ->update('shipping_area',$data)) {

                $data2['msgg'] = '<div class="alert alert-success"><strong>Successfully!</strong> Updated</div>';
            } else {
                $data2['msgg'] = '<span style="color:red"><b>Not Updated</b></span>';
            }
        }

        echo json_encode($data2);
        
        
    }
    
      function get_generic_info(){
      header('Content-Type:Application/json');
       
        $shipping_area_id = $this->input->post('shipping_area_id');
        $shipping_area_info = $this->db->get_Where('shipping_area', array('shipping_area_id'=>$shipping_area_id))->row();
      
        
        echo json_encode($shipping_area_info);
       
    }
    
     function ajax_delete_generic_name(){
       
         header('Content-Type:Application/json');
       
         $shipping_area_id= $this->input->post('shipping_area_id');
        if($this->db->where('shipping_area_id',$shipping_area_id)->delete('shipping_area'))
                {
                    
                 $data2['msgg']='<div class="alert alert-danger"><strong>Successfully!</strong> Deleted</div>';
                }
                else
                {
                $data2['msgg']='<span style="color:red"><b>Not Deleted</b></span>'; 
                }
        
       echo json_encode($data2); 
   }
       function ajax_active_generic_name(){
       
         header('Content-Type:Application/json');
       $data['status']=1;
        $shipping_area_id= $this->input->post('shipping_area_id');
        if($this->db->where('shipping_area_id',$shipping_area_id)->update('shipping_area',$data))
                {
                    
                 $data2['msgg']='<div class="alert alert-success"><strong>Successfully!</strong> Updated Status Active</div>';
                }
                else
                {
                $data2['msgg']='<span style="color:red"><b>Not Updated</b></span>'; 
                }
        
       echo json_encode($data2); 
   }
      function ajax_inactive_generic_name(){
       
         header('Content-Type:Application/json');
       $data['status']=2;
        $shipping_area_id= $this->input->post('shipping_area_id');
        if($this->db->where('shipping_area_id',$shipping_area_id)->update('shipping_area',$data))
                {
                    
                 $data2['msgg']='<div class="alert alert-danger"><strong>Successfully!</strong> Updated Status Inactive</div>';
                }
                else
                {
                $data2['msgg']='<span style="color:red"><b>Not Updated</b></span>'; 
                }
        
       echo json_encode($data2); 
   }
}
