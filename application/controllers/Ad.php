<?php


class Ad extends CI_Controller{
    //put your code here
     public function __construct() {
        parent::__construct();
        $res=$this->session->userdata('id');
        if ($res == NULL) {
            redirect('admin', 'refresh');
        }
    }
     public function advertisement() {
            $data = array();    
            $data['title'] = 'Advertisement';
            $cdata['all_ad_image']=  $this->Adeshbroad_Model->get_all_advertisement_image();
          
              $data['admin_main_content'] = $this->load->view('admin_pages/pages/ad/advertisement_list_view', $cdata, TRUE);
       $this->load->view('admin_pages/admin_master', $data);
    }
       public function ad_inactive($id) {

        $this->Adeshbroad_Model->ad_inactive_info($id);
           redirect('Ad/advertisement');
    }

    public function ad_active($id) {
        $this->Adeshbroad_Model->ad_active_info($id);
           redirect('Ad/advertisement');
    }
    
    public function ad_delete($id){
       $this->Adeshbroad_Model->ad_delete_category_by_id($id);
          redirect('Ad/advertisement');
    }
     public function advertisement_img_add() {

       $this->load->library('form_validation');

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

//Validating Name Field
        $this->form_validation->set_rules('name', 'Title Name', 'required|min_length[2]|max_length[15]');
//        $this->form_validation->set_rules('userFiles[]', 'File', 'required');



        if ($this->form_validation->run() == FALSE) {
            
         $data = array();
        $data['title'] = 'Slider';
        $data['admin_main_content'] = $this->load->view('admin_pages/pages/ad/advertisement_add_view', '', TRUE);
       $this->load->view('admin_pages/admin_master', $data);
        
       
            
        } 
        else 
            {
//Setting values for tabel columns
         
               if (!empty($_FILES['userFiles']['name'])) {
            $filesCount = count($_FILES['userFiles']['name']);
            for ($i = 0; $i < $filesCount; $i++) {
              //  echo $i;
                $_FILES['userFile']['name'] = $_FILES['userFiles']['name'][$i];
                $_FILES['userFile']['type'] = $_FILES['userFiles']['type'][$i];
                $_FILES['userFile']['tmp_name'] = $_FILES['userFiles']['tmp_name'][$i];
                $_FILES['userFile']['error'] = $_FILES['userFiles']['error'][$i];
                $_FILES['userFile']['size'] = $_FILES['userFiles']['size'][$i];
                $uploadPath = 'upload_image/ad_image/';
                $config['upload_path'] = $uploadPath;
                $config['allowed_types'] = 'gif|jpg|JPG|jpeg|JPEG|png|txt';
                //$config['max_size']	= '303374600';
                //$config['max_width'] = '1024000';
                //$config['max_height'] = '768000';
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if ($this->upload->do_upload('userFile')) {
                    $fileData = $this->upload->data();
                    //$uploadData[$i]['file_name'] = $fileData['file_name'];
                    //$uploadData[$i]['created'] = date("Y-m-d H:i:s");
                    $uploadData[$i] = $uploadPath.$fileData['file_name'];
                }
                  $data_arr = array(
                                                    'ad_position' =>$this->input->post('position'),
                                                    'ad_title' => $this->input->post('name'),
                                                    'inserted_by' => $this->session->userdata('id'),
                                                    'insert_time' => date('Y-m-d h:m:s'),
                                                    'updated_by' => NULL,
                                                    'update_time' => NULL,
                                                    'is_active' => 1,
                                                    'is_delete' => 0,
                                                    'ad_file'=>$uploadData[$i]
                                                );
                                $this->db->insert('ad_management', $data_arr);
            }
            //var_dump($uploadData);
//            if (!empty($uploadData)) {
//                $data['file_url'] = implode(',', $uploadData);
//            }
        }
//            echo '<pre>';
//            print_r($data);
//            exit();
//Transfering data to Model
          
          $sdata['ad_message']='Data Inserted Successfully';
           $this->session->set_userdata($sdata);
//Loading View
//            $this->load->view('master', $data);
            redirect('Ad/advertisement');
        }
}
      public function ad_edit_update($id) {

       
           $this->load->library('form_validation');

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

//Validating Name Field
        $this->form_validation->set_rules('name', 'Title Name', 'required|min_length[2]|max_length[15]');
//        $this->form_validation->set_rules('userFiles[]', 'File', 'required');



        if ($this->form_validation->run() == FALSE) {
            
         $cdata = array();
         $data = array();
        $data['title'] = 'Slider';
        $cdata['select_ad'] =  $this->Adeshbroad_Model->get_advertisement_by_id($id);
        
       
        
           $data['admin_main_content'] = $this->load->view('admin_pages/pages/ad/ad_edit_view', $cdata, TRUE);
       $this->load->view('admin_pages/admin_master', $data);
            
        } 
        else 
            {
//Setting values for tabel columns
            $data = array(
                'ad_title' => $this->input->post('name'),
                'ad_position' =>$this->input->post('position'),
                'updated_by' =>$this->session->userdata('id'),
                'update_time' => date('Y-m-d h:m:s'),
         );
              if (!empty($_FILES['userFiles']['name'])) {
            $filesCount = count($_FILES['userFiles']['name']);
            for ($i = 0; $i < $filesCount; $i++) {
                //echo $i;
                $_FILES['userFile']['name'] = $_FILES['userFiles']['name'][$i];
                $_FILES['userFile']['type'] = $_FILES['userFiles']['type'][$i];
                $_FILES['userFile']['tmp_name'] = $_FILES['userFiles']['tmp_name'][$i];
                $_FILES['userFile']['error'] = $_FILES['userFiles']['error'][$i];
                $_FILES['userFile']['size'] = $_FILES['userFiles']['size'][$i];
                $uploadPath = 'upload_image/product_image/';
                $config['upload_path'] = $uploadPath;
                $config['allowed_types'] = 'gif|jpg|JPG|jpeg|JPEG|png|txt';
                //$config['max_size']	= '303374600';
                //$config['max_width'] = '1024000';
                //$config['max_height'] = '768000';
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if ($this->upload->do_upload('userFile')) {
                    $fileData = $this->upload->data();
                    //$uploadData[$i]['file_name'] = $fileData['file_name'];
                    //$uploadData[$i]['created'] = date("Y-m-d H:i:s");
                    $uploadData[$i] = $uploadPath.$fileData['file_name'];
                }
                
            }
            //var_dump($uploadData);
            if (!empty($uploadData)) {
                $data['ad_file'] = implode(',', $uploadData);
            }
        }
//            echo '<pre>';
//            print_r($data);
//            exit();
//Transfering data to Model
             $this->Adeshbroad_Model->update_advertisement($id,$data);
             $sdata['adup_message']='Data Updated Successfully';
           $this->session->set_userdata($sdata);
//Loading View
//            $this->load->view('master', $data);
            redirect('Ad/advertisement');
        }
    }
}
