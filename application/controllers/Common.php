<?php

class Common extends CI_Controller {

    //put your code here
     public function __construct() {
        parent::__construct();
        $res=$this->session->userdata('id');
        if ($res == NULL) {
            redirect('admin', 'refresh');
        }
    }
    
    
//    ==============Order Start=============
    
    public function get_product_info(){
        
        
         header('Content-Type:Application/json');
       
        $product_id = $this->input->post('product_id');
        $product_info = $this->db->get_Where('product', array('product_id'=>$product_id))->row();
      
        
        echo json_encode($product_info);
    }
    public function get_order_info(){
        
        
         header('Content-Type:Application/json');
       
        $order_id = $this->input->post('order_id');
        $order_info = $this->db->get_Where('customer_order', array('order_id'=>$order_id))->row();
      
        
        echo json_encode($order_info);
    }
    function ajax_save_update_advance_info(){
       
        header('Content-Type:Application/json');
        $order_id= $this->input->post('order_id');
        $data['order_advance'] = $this->input->post('order_advance');
    
        if(empty($data['order_advance']))
       {
             $data2['msgg'] = '<div class="alert alert-danger"><strong>Error!</strong>Already Exists</div>';      
        } 
        else
        {
                if($this->db->where('order_id',$order_id)->update('customer_order',$data))
                {
                    
                 $data2['msgg']='<div class="alert alert-success"><strong>Successfully!</strong> Updated</div>';
                }
                else
                {
                $data2['msgg']='<span style="color:red"><b>Not Updated</b></span>'; 
                }
        }
        
        echo json_encode($data2);
       
    }
    function ajax_save_update_retrun_info(){
       
        header('Content-Type:Application/json');
        $order_id= $this->input->post('order_id');
        $data['order_return_text'] = $this->input->post('order_return_text');
    
        if(empty($data['order_return_text']))
       {
             $data2['msgg'] = '<div class="alert alert-danger"><strong>Error!</strong>Already Exists</div>';      
        } 
        else
        {
                if($this->db->where('order_id',$order_id)->update('customer_order',$data))
                {
                    
                 $data2['msgg']='<div class="alert alert-success"><strong>Successfully!</strong> Updated</div>';
                }
                else
                {
                $data2['msgg']='<span style="color:red"><b>Not Updated</b></span>'; 
                }
        }
        
        echo json_encode($data2);
       
    }
    function ajax_save_update_review_info(){
       
        header('Content-Type:Application/json');
        $order_id= $this->input->post('order_id');
        $data['order_review_text'] = $this->input->post('order_review_text');
    
        if(empty($data['order_review_text']))
       {
             $data2['msgg'] = '<div class="alert alert-danger"><strong>Error!</strong>Already Exists</div>';      
        } 
        else
        {
                if($this->db->where('order_id',$order_id)->update('customer_order',$data))
                {
                    
                 $data2['msgg']='<div class="alert alert-success"><strong>Successfully!</strong> Updated</div>';
                }
                else
                {
                $data2['msgg']='<span style="color:red"><b>Not Updated</b></span>'; 
                }
        }
        
        echo json_encode($data2);
       
    }
    public function manage_order()
        {
   

        $data = array();
        $cdata = array();
        $data['title'] = 'Manage Order';
        $cdata['all_order']=  $this->Adeshbroad_Model->get_all_order();
        $data['admin_main_content'] = $this->load->view('admin_pages/pages/manage_order_view',$cdata, TRUE);
        $this->load->view('admin_pages/admin_master', $data);
    }
    
     public function delete_order($id,$sid,$pid){
     
        $this->Adeshbroad_Model->delect_order_info($id);
        $this->Adeshbroad_Model->delect_order_details_info($id);
        $this->Adeshbroad_Model->delect_payment_info($pid);
        $this->Adeshbroad_Model->delect_shipping_info($sid);
        redirect('Common/manage_order');
    }
      public function confirm_order($id){
        $this->Adeshbroad_Model->confirm_order_info($id);
        redirect('Common/manage_order');
    }
    public function delivered_order($id){
        $this->Adeshbroad_Model->delivered_order_info($id);
        redirect('Common/manage_order');
    }
    public function pending_order($id){
        $this->Adeshbroad_Model->pending_order_info($id);
       
         redirect('Common/manage_order');
    }
    public function due_payment($id){
        $this->Adeshbroad_Model->due_payment_info($id);
        redirect("Common/manage_order");
    }
    public function paid_payment($id){
        $this->Adeshbroad_Model->paid_payment_info($id);
        redirect("Common/manage_order");
    }
      public function view_payment($id){
        
        $data = array();
        $cdata = array();
        $data['title'] = 'view_payment';
        $cdata['payment_info']=  $this->Adeshbroad_Model->payment_info_by_id($id);
         $data['admin_main_content'] = $this->load->view('admin_pages/pages/manage_payment_view',$cdata, TRUE);
        $this->load->view('admin_pages/admin_master', $data);
    }
    
    public function view_invoice($id)
        {
//    echo $id;

        $data = array();
        $cdata = array();
        $data['title'] = 'Category';
    $cdata['shipping_info']=  $this->Adeshbroad_Model->get_shipping_info($id);

    $cdata['customer_info']=  $this->Adeshbroad_Model->get_customer_info($id);
//            echo '<pre>';
//            print_r($cdata);
//            exit();
    $cdata['order_details_info']=  $this->Adeshbroad_Model->get_order_details_info($id);
      
         $data['admin_main_content'] = $this->load->view('admin_pages/pages/show_invoice_view',$cdata, TRUE);
         $this->load->view('admin_pages/admin_master', $data);
    }
       public function download_invoice($id){
   
         
       $cdata['shipping_info'] = $this->Adeshbroad_Model->get_shipping_info($id);
       $cdata['customer_info'] = $this->Adeshbroad_Model->get_customer_info($id);
        
//       echo '<pre>';
//       print_r($cdata);
//       exit();
        
        $cdata['order_details_info'] = $this->Adeshbroad_Model->get_order_details_info($id);
        // $this->load->view('admin/pages/download_invoice', $data);
        $this->load->helper('dompdf');
        $view_file=$this->load->view('admin_pages/pages/download_invoice_view', $cdata,true);
        $file_name=pdf_create($view_file, 'eGallerybd_Invoice-00'.$id);
        echo $file_name;
        
    }
    
    
//    ==============Order End=============
    public function newsletter() {
        $data = array();
        $data['title'] = 'Newsletter';
        $cdata['all_newsletter'] = $this->Adeshbroad_Model->get_all_newsletter();
        $data['admin_main_content'] = $this->load->view('admin_pages/pages/newsletter_list_view', $cdata, TRUE);
        $this->load->view('admin_pages/admin_master', $data);
    }

    public function newsletter_email_delete($id) {
        $this->Adeshbroad_Model->newsletter_email_delete_by_id($id);
        redirect('Common/newsletter');
    }

    public function review_delete($id) {
        $this->Adeshbroad_Model->delete_review_by_id($id);
        $sdata['rd_message'] = 'Data Deleted Successfully';
        $this->session->set_userdata($sdata);
        redirect('Common/manage_review');
    }

    public function manage_review() {
        $data = array();
        $cdata = array();
        $data['title'] = 'Manage';
        $cdata['all_review'] = $this->Adeshbroad_Model->get_all_review();
        $data['admin_main_content'] = $this->load->view('admin_pages/pages/review_list_view', $cdata, TRUE);
        $this->load->view('admin_pages/admin_master', $data);
    }

 

    public function contact() {


        $data = array();
        $cdata = array();
        $data['title'] = 'Dashboard';
        $cdata['all_contact'] = $this->Adeshbroad_Model->get_all_contact();
        $data['admin_main_content'] = $this->load->view('admin_pages/pages/contact_us_list_view', $cdata, TRUE);
        $this->load->view('admin_pages/admin_master', $data);
    } public function contact_delete($id) {


    
         $this->Adeshbroad_Model->delete_contact_by_id($id);
         redirect('Common/contact');
       
    }

       public function other_business() {
        $data = array();
        $data['title'] = 'term_condition';
        $cdata['all_career'] = $this->Adeshbroad_Model->get_all_other_business();
        $data['admin_main_content'] = $this->load->view('admin_pages/pages/career/other_business_list_view', $cdata, TRUE);
       $this->load->view('admin_pages/admin_master', $data);
     }
       public function new_about() {
        $data = array();
        $data['title'] = 'About';
        $cdata['all_career'] = $this->Adeshbroad_Model->get_all_about_new();
        $data['admin_main_content'] = $this->load->view('admin_pages/pages/career/about_list_view', $cdata, TRUE);
       $this->load->view('admin_pages/admin_master', $data);
     }
        public function update_new_about() {
        $about_new_id = $this->input->post('about_new_id');
        $data = array(
            'about_new_text' => $this->input->post('about_new_text'),
            'inserted_by' => $this->session->userdata('id'),
            'status' => 1
        );
        if (empty($about_new_id)) {
            $this->db->insert('about_new', $data);
        } else {
            $this->db->where('about_new_id', $about_new_id)->update('about_new', $data);
        }

        redirect('Common/new_about');
    }

    public function update_other_business() {
          $other_business_id =$this->input->post('other_business_id');
            $data = array(
                'other_business_text' => $this->input->post('other_business_text'),
                'inserted_by' =>$this->session->userdata('id'),
                'status' => 1
            );
            
             if (empty($other_business_id)) {
            $this->db->insert('other_business', $data);
        } else {
             $this->db->where('other_business_id',$other_business_id)->update('other_business',$data);
        }
            
         
          redirect('Common/other_business');
      }
       public function term_condition() {
        $data = array();
        $data['title'] = 'term_condition';
        $cdata['all_career'] = $this->Adeshbroad_Model->get_all_term_condition();
        $data['admin_main_content'] = $this->load->view('admin_pages/pages/career/term_condition_list_view', $cdata, TRUE);
       $this->load->view('admin_pages/admin_master', $data);
     }
        public function update_term_condition() {
          $term_condition_id =$this->input->post('term_condition_id');
            $data = array(
                'term_condition_text' => $this->input->post('term_condition_text'),
                'inserted_by' => $this->session->userdata('id'),
                'status' => 1
            );
            
             if (empty($term_condition_id)) {
            $this->db->insert('term_condition', $data);
        } else {
            $this->db->where('term_condition_id', $term_condition_id)->update('term_condition', $data);
        }


        redirect('Common/term_condition');
      }
       public function return_policy() {
        $data = array();
        $data['title'] = 'return_policy';
        $cdata['all_career'] = $this->Adeshbroad_Model->get_all_return_policy();
        $data['admin_main_content'] = $this->load->view('admin_pages/pages/career/return_policy_list_view', $cdata, TRUE);
       $this->load->view('admin_pages/admin_master', $data);
     }
        public function update_return_policy() {
          $return_policy_id =$this->input->post('return_policy_id');
            $data = array(
                'return_policy_text' => $this->input->post('return_policy_text'),
                'inserted_by' => $this->session->userdata('id'),
                'status' => 1
            );
            
            
             if (empty($return_policy_id)) {
            $this->db->insert('return_policy', $data);
        } else {
            $this->db->where('return_policy_id',$return_policy_id)->update('return_policy',$data);
        }
            
            
          
          redirect('Common/return_policy');
      }
     
     
       public function career() {
        $data = array();
        $data['title'] = 'Dashboard';
        $cdata['all_career'] = $this->Adeshbroad_Model->get_all_career();
        $data['admin_main_content'] = $this->load->view('admin_pages/pages/career/career_list_view', $cdata, TRUE);
       $this->load->view('admin_pages/admin_master', $data);
     }
      public function update_career() {
          $career_id =$this->input->post('career_id');
            $data = array(
                'career_text' => $this->input->post('career_text'),
                'inserted_by' => $this->session->userdata('id'),
                'status' => 1
            );
            
             if (empty($career_id)) {
            $this->db->insert('career', $data);
        } else {
            $this->db->where('career_id',$career_id)->update('career',$data);
        }
          
          redirect('Common/career');
      }
   
       public function about() {


        $data = array();
        $data['title'] = 'Dashboard';
        $cdata['all_about'] = $this->Adeshbroad_Model->get_all_about();
        
        
        
          $data['admin_main_content'] = $this->load->view('admin_pages/pages/about/about_us_list_view', $cdata, TRUE);
       $this->load->view('admin_pages/admin_master', $data);
        
        
     
    }
 public function ab_inactive($id) {

        $this->Adeshbroad_Model->ab_inactive_info($id);
           redirect('Common/about');
    }
    
     public function ab_active($id) {
        $this->Adeshbroad_Model->ab_active_info($id);
           redirect('Common/about');
    }
    
    public function ab_delete($id){
       $this->Adeshbroad_Model->ab_delete_category_by_id($id);
          redirect('Common/about');
    }
    public function about_us_add() {

        $this->load->library('form_validation');

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

//Validating Name Field
        $this->form_validation->set_rules('txt_short_description', 'Short Description', 'required|min_length[5]|max_length[400]');
        $this->form_validation->set_rules('txt_long_description', 'Long Description', 'required');



        if ($this->form_validation->run() == FALSE) {

            $data = array();
            $data['title'] = 'Dashboard';
            
             $data['admin_main_content'] = $this->load->view('admin_pages/pages/about/about_us_add_view', '', TRUE);
             $this->load->view('admin_pages/admin_master', $data);
          
        } else {
//Setting values for tabel columns
            $data = array(
                'short_des' => $this->input->post('txt_short_description'),
                'long_des' => $this->input->post('txt_long_description'),
                'inserted_by' => $this->session->userdata('id'),
                'insert_time' => date('Y-m-d h:m:s'),
                'updated_by' => 0,
                'update_time' => 0,
                'is_active' => 1
            );
//            echo '<pre>';
//            print_r($data);
//            exit();
//Transfering data to Model
            $this->db->insert('about_us', $data);
            $data['message'] = 'Data Inserted Successfully';
//Loading View
//            $this->load->view('master', $data);
            redirect('Common/about');
        }
    }

    public function about_us_edit($id) {

        $this->load->library('form_validation');

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

//Validating Name Field
        $this->form_validation->set_rules('txt_short_description', 'Short Description', 'required|min_length[5]|max_length[400]');
        $this->form_validation->set_rules('txt_long_description', 'Long Description', 'required');



        if ($this->form_validation->run() == FALSE) {

            $data = array();
            $data['title'] = 'Dashboard';
            $cdata['about_us_info'] = $this->Adeshbroad_Model->get_about_us_info_by_id($id);
            
            
          $data['admin_main_content'] = $this->load->view('admin_pages/pages/about/about_us_edit_view', $cdata, TRUE);
       $this->load->view('admin_pages/admin_master', $data);
          
        } else {
//Setting values for tabel columns
            $data = array(
                'short_des' => $this->input->post('txt_short_description'),
                'long_des' => $this->input->post('txt_long_description'),
                'updated_by' => $this->session->userdata('id'),
                'update_time' => date('Y-m-d h:m:s'),
            );
//            echo '<pre>';
//            print_r($data);
//            exit();
//Transfering data to Model
            $this->Adeshbroad_Model->update_about_us_info_by_id($id, $data);
            // $this->db->insert('about_us',$data);
            $data['message'] = 'Data Inserted Successfully';
//Loading View
//            $this->load->view('master', $data);
            redirect('Common/about');
        }
    }

}
