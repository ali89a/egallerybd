<?php

class Slider_Manage extends CI_Controller {

    //put your code here
 public function __construct() {
        parent::__construct();
        $res=$this->session->userdata('id');
        if ($res == NULL) {
            redirect('admin', 'refresh');
        }
    }
    public function slider() {
        $data = array();
        $cdata = array();
        $data['title'] = 'Slider';
        $cdata['all_slider_image'] = $this->Adeshbroad_Model->get_all_slider_image();
      
          $data['admin_main_content'] = $this->load->view('admin_pages/pages/slider/slide_list_view', $cdata, TRUE);
       $this->load->view('admin_pages/admin_master', $data);
    }

    public function slider_inactive($id) {

        $this->Adeshbroad_Model->slider_inactive_info($id);
        redirect('Slider_Manage/slider');
    }

    public function slider_active($id) {
        $this->Adeshbroad_Model->slider_active_info($id);
        redirect('Slider_Manage/slider');
    }

    public function slider_delete($id) {
        $this->Adeshbroad_Model->slider_delete_category_by_id($id);
        redirect('Slider_Manage/slider');
    }

    public function slider_img_add() {

        $this->load->library('form_validation');

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

//Validating Name Field
        $this->form_validation->set_rules('name', 'Title Name', 'required|min_length[2]|max_length[15]');
//        $this->form_validation->set_rules('userFiles[]', 'File', 'required');



        if ($this->form_validation->run() == FALSE) {

            $data = array();
            $data['title'] = 'Slider';
           $data['admin_main_content'] = $this->load->view('admin_pages/pages/slider/slider_img_add_view', '', TRUE);
       $this->load->view('admin_pages/admin_master', $data);
        } else {
//Setting values for tabel columns

            if (!empty($_FILES['userFiles']['name'])) {
                $filesCount = count($_FILES['userFiles']['name']);
                for ($i = 0; $i < $filesCount; $i++) {
                    // echo $i;
                    $_FILES['userFile']['name'] = $_FILES['userFiles']['name'][$i];
                    $_FILES['userFile']['type'] = $_FILES['userFiles']['type'][$i];
                    $_FILES['userFile']['tmp_name'] = $_FILES['userFiles']['tmp_name'][$i];
                    $_FILES['userFile']['error'] = $_FILES['userFiles']['error'][$i];
                    $_FILES['userFile']['size'] = $_FILES['userFiles']['size'][$i];
                    $uploadPath = 'upload_image/slider_image/';
                    $config['upload_path'] = $uploadPath;
                    $config['allowed_types'] = 'gif|jpg|JPG|jpeg|JPEG|png|txt';
                    //$config['max_size']	= '303374600';
                    //$config['max_width'] = '1024000';
                    //$config['max_height'] = '768000';
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    if ($this->upload->do_upload('userFile')) {
                        $fileData = $this->upload->data();
                        //$uploadData[$i]['file_name'] = $fileData['file_name'];
                        //$uploadData[$i]['created'] = date("Y-m-d H:i:s");
                        $uploadData[$i] = $uploadPath . $fileData['file_name'];
                    }
                    $data_arr = array(
                        'photo_type' => 1,
                        'title' => $this->input->post('name'),
                        'inserted_by' => $this->session->userdata('id'),
                        'insert_time' => date('Y-m-d h:m:s'),
                        'updated_by' => NULL,
                        'update_time' => NULL,
                        'is_active' => 1,
                        'is_delete' => 0,
                        'file_url' => $uploadData[$i]
                    );
                    $this->db->insert('lib_photo', $data_arr);
                }
                //var_dump($uploadData);
//            if (!empty($uploadData)) {
//                $data['file_url'] = implode(',', $uploadData);
//            }
            }
//            echo '<pre>';
//            print_r($data);
//            exit();
//Transfering data to Model
            $ssdata['slider_message'] = 'Data Inserted Successfully';
            $this->session->set_userdata($ssdata);

//Loading View
//            $this->load->view('master', $data);
            redirect('Slider_Manage/slider');
        }
    }

    public function slider_edit_update($id) {


        $this->load->library('form_validation');

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

//Validating Name Field
        $this->form_validation->set_rules('name', 'Title Name', 'required|min_length[2]|max_length[15]');
//        $this->form_validation->set_rules('userFiles[]', 'File', 'required');



        if ($this->form_validation->run() == FALSE) {

            $cdata = array();
            $data = array();
            $data['title'] = 'Slider';
            $cdata['select_slider_img'] = $this->Adeshbroad_Model->get_slider_img_by_id($id);

            $data['admin_main_content'] = $this->load->view('admin_pages/pages/slider/slider_img_edit_view', $cdata, TRUE);
            $this->load->view('admin_pages/admin_master', $data);
        } else {
//Setting values for tabel columns
            $data = array(
                'title' => $this->input->post('name'),
                'updated_by' => $this->session->userdata('id'),
                'update_time' => date('Y-m-d h:m:s'),
            );
            if (!empty($_FILES['userFiles']['name'])) {
                $filesCount = count($_FILES['userFiles']['name']);
                for ($i = 0; $i < $filesCount; $i++) {
                    // echo $i;
                    $_FILES['userFile']['name'] = $_FILES['userFiles']['name'][$i];
                    $_FILES['userFile']['type'] = $_FILES['userFiles']['type'][$i];
                    $_FILES['userFile']['tmp_name'] = $_FILES['userFiles']['tmp_name'][$i];
                    $_FILES['userFile']['error'] = $_FILES['userFiles']['error'][$i];
                    $_FILES['userFile']['size'] = $_FILES['userFiles']['size'][$i];
                    $uploadPath = 'upload_image/product_image/';
                    $config['upload_path'] = $uploadPath;
                    $config['allowed_types'] = 'gif|jpg|JPG|jpeg|JPEG|png|txt';
                    //$config['max_size']	= '303374600';
                    //$config['max_width'] = '1024000';
                    //$config['max_height'] = '768000';
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    if ($this->upload->do_upload('userFile')) {
                        $fileData = $this->upload->data();
                        //$uploadData[$i]['file_name'] = $fileData['file_name'];
                        //$uploadData[$i]['created'] = date("Y-m-d H:i:s");
                        $uploadData[$i] = $uploadPath . $fileData['file_name'];
                    }
                }
                //var_dump($uploadData);
                if (!empty($uploadData)) {
                    $data['file_url'] = implode(',', $uploadData);
                }
            }
//            echo '<pre>';
//            print_r($data);
//            exit();
//Transfering data to Model
            $this->Adeshbroad_Model->update_slider($id, $data);
            $sdata['sli_message'] = 'Data Updated Successfully';
            $this->session->set_userdata($sdata);
//Loading View
//            $this->load->view('master', $data);
            redirect('Slider_Manage/slider');
        }
    }

}
