<?php

class Brand extends CI_Controller {

    //put your code here
    public function __construct() {
        parent::__construct();
        $res = $this->session->userdata('id');
        if ($res == NULL) {
            redirect('admin', 'refresh');
        }
    }

    public function index() {

        $data = array();
        $cdata = array();
        $data['title'] = 'Add Brand';
       $cdata['select_all_pub_category'] = $this->Category_Model->select_all_published_category_info();
        $data['admin_main_content'] = $this->load->view('admin_pages/pages/brand/brand_add_view', $cdata, TRUE);
        $this->load->view('admin_pages/admin_master', $data);
    }

    public function brand_save() {

        $data = array();


//        //        start
//        $this->load->library('upload');
//        $config['upload_path'] = 'upload_image/icons/brand_icon/';
//        $config['allowed_types'] = 'gif|jpg|png';
//        //$config['max_size']= '200';
//        $this->load->library('upload', $config);
//        $this->upload->initialize($config);
//        $error = '';
//        $fdata = array();
//        if (!$this->upload->do_upload('icon')) {
//            $error = $this->upload->display_errors();
//          //  echo $error;
//            /* exit(); */
//        } else {
//            $fdata = $this->upload->data();
//            $data['brand_icon'] = $config['upload_path'] . $fdata['file_name'];
//        }
//        //end

        $txt_bname = $this->input->post('txt_brand');
        $cbo_category = $this->input->post('cbo_category');
        $cbo_sub_category = $this->input->post('cbo_sub_category');
        $data_arr = array(
            'brand_name' => $txt_bname,
            'category_id' => $cbo_category,
            'sub_category_id' => $cbo_sub_category,
            'inserted_by' => $this->session->userdata('id'),
            'insert_time' => date('Y-m-d h:m:s'),
            'updated_by' => 0,
            'update_time' => 0,
            'is_active' => 1,
            'is_delete' => 0,
            
        );
        $t_data = $data_arr;
//         echo '<pre>';
//        print_r($t_data);
//        exit(); 
        $this->db->insert('brand', $t_data);
      $this->session->set_flashdata('success', 'Saved Brand');
        redirect('Brand/view');
    }

    public function view() {

        $data = array();
        $data['title'] = 'View Brand';
        $cdata = array();
         $cdata['select_all_pub_category'] = $this->Category_Model->select_all_published_category_info();
        $cdata['select_all_brand'] = $this->Brand_Model->select_all_brand_info();
        $data['admin_main_content'] = $this->load->view('admin_pages/pages/brand/brand_list_view', $cdata, TRUE);
        $this->load->view('admin_pages/admin_master', $data);
    }

    public function inactive($brand_id) {

        $this->Brand_Model->inactive_brand_info($brand_id);
        redirect('Brand/view');
    }

    public function active($brand_id) {
        $this->Brand_Model->active_brand_info($brand_id);
        redirect('Brand/view');
    }

    public function edit_brand($brand_id) {
        $data = array();
        $data['title'] = 'Edit Brand';
        $cdata = array();
        $cdata['select_brand_by_id'] = $this->Brand_Model->select_brand_by_id($brand_id);
       $cdata['all_ctegory'] = $this->Adeshbroad_Model->get_category();
       $cdata['all_sub_ctegory'] = $this->Adeshbroad_Model->get_sub_category();



//         echo '<pre>';
//         print_r($cdata);
//         exit();

        $data['admin_main_content'] = $this->load->view('admin_pages/pages/brand/brand_edit_view', $cdata, TRUE);
        $this->load->view('admin_pages/admin_master', $data);
    }

    public function update_brand() {

//        $data = array();
//
//
//        //        start
//        $this->load->library('upload');
//        $config['upload_path'] = 'upload_image/icons/brand_icon/';
//        $config['allowed_types'] = 'gif|jpg|png';
//        //$config['max_size']= '200';
//        $this->load->library('upload', $config);
//        $this->upload->initialize($config);
//        $error = '';
//        $fdata = array();
//        if (!$this->upload->do_upload('icn')) {
//            $error = $this->upload->display_errors();
//            echo $error;
//            /* exit(); */
//        } else {
//            $fdata = $this->upload->data();
//            $data['brand_icon'] = $config['upload_path'] . $fdata['file_name'];
//        }
//        //end


        $txt_bname = $this->input->post('txt_brand');
        $brand_id = $this->input->post('brand_id');

        //  echo $brand_id;
        $cbo_category = $this->input->post('category_id');
        $sub_category_id = $this->input->post('sub_category_id');
        $data_arr = array(
            'brand_name' => $txt_bname,
            'category_id' => $cbo_category,  
            'sub_category_id' => $sub_category_id,  
            'updated_by' => $this->session->userdata('id'),
            'update_time' =>date('Y-m-d h:m:s'),
            'is_active' => 1,
            'is_delete' => 0,
 
        );
        $t_data = $data_arr;
//        echo '<pre>';
//         print_r($t_data);
//        exit();


        $this->Brand_Model->update_brand_info($t_data, $brand_id);
        $sdata = array();
        $sdata['message'] = "Successfully Update Brand";
        $this->session->set_userdata($sdata);
        redirect('Brand/view');
    }

    public function delete_brand($brand_id) {

        $this->Brand_Model->delete_brand_by_id($brand_id);
        redirect('Brand/view');
    }
    public function get_subcat_by_cat_id(){
        
        $category_id = $this->input->post('category_id');
         $sub_categories=$this->Brand_Model->get_subcat_by_cat_id_info($category_id);
         if (count($sub_categories) > 0) {
             
             $output = '';
            $output .= '<option value="">--Select Sub Category--</option>';
            foreach ($sub_categories as $subcategory) {
                $output .= '<option value="' . $subcategory->sub_category_id . '">' . $subcategory->sub_cat_name . '</option>';
            }
            echo json_encode($output);
         }
        
    }
    public function get_brand_by_sub_cat_id(){
        
        $sub_category_id= $this->input->post('sub_category_id');
         $brands=$this->Brand_Model->get_brand_by_subcat_id_info($sub_category_id);
         if (count($brands) > 0) {
             
             $output = '';
            $output .= '<option value="">--Select Sub Category2--</option>';
            foreach ($brands as $brand) {
                $output .= '<option value="' . $brand->brand_id . '">' . $brand->brand_name . '</option>';
            }
            echo json_encode($output);
         }
        
    }
	
	public function get_sub3_by_brand_id(){
        
        $brand= $this->input->post('brand_id');
         $sub3=$this->Brand_Model->get_sub3_by_brand_id_info($brand);
         if (count($sub3) > 0) {
             
             $output = '';
            $output .= '<option value="">--Select Sub Category-3--</option>';
            foreach ($sub3 as $sub_3) {
                $output .= '<option value="' . $sub_3->sub_category_three_id . '">' . $sub_3->sub_category_three_name . '</option>';
            }
            echo json_encode($output);
         }
        
    }

    public function top_brand_view(){
        
         $data = array();
        $data['title'] = 'View Brand';
        $cdata = array();
          $cdata['all_top_brand']=  $this->Adeshbroad_Model->get_all_top_brand();
        $data['admin_main_content'] = $this->load->view('admin_pages/top_brand/top_brand_list_view', $cdata, TRUE);
        $this->load->view('admin_pages/admin_master', $data);
    }
            public function search_top_brand(){
            $search_name= $this->input->post('search_name');

        $data = array();
        $cdata = array();
        $data['title'] = 'Customer';
        $cdata['all_top_brand']=  $this->Adeshbroad_Model->get_all_search_top_brand($search_name);
       $data['admin_main_content'] = $this->load->view('admin_pages/top_brand/top_brand_list_view', $cdata, TRUE);
         $this->load->view('admin_pages/admin_master', $data);
    }
}
