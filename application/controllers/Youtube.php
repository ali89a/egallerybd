<?php


class Youtube extends CI_Controller{
    //put your code here
    
     public function __construct() {
        parent::__construct();
        $res=$this->session->userdata('id');
        if ($res == NULL) {
            redirect('admin', 'refresh');
        }
    }
    public function youtube_video() {
            $data = array();    
            $data['title'] = 'youtube video';
            $cdata['all_youtube_video']=  $this->Adeshbroad_Model->get_all_youtube_video();
            $data['admin_main_content'] = $this->load->view('admin_pages/pages/youtube/youtube_video_list_view', $cdata, TRUE);
       $this->load->view('admin_pages/admin_master', $data);
    }   public function youtube_active($id) {
        $this->Adeshbroad_Model->youtube_active_info($id);
           redirect('Youtube/youtube_video');
    }
        public function youtube_inactive($id) {

        $this->Adeshbroad_Model->youtube_inactive_info($id);
           redirect('Youtube/youtube_video');
    }
     public function youtube_video_delete($id){
       $this->Adeshbroad_Model->youtube_video_delete_by_id($id);
          redirect('Youtube/youtube_video');
    }
     public function youtube_video_add() {

       $this->load->library('form_validation');

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

//Validating Name Field
        $this->form_validation->set_rules('name', 'Title Name', 'required|min_length[5]|max_length[15]');
        $this->form_validation->set_rules('file', 'Link', 'required');
//        $this->form_validation->set_rules('userFiles[]', 'File', 'required');



        if ($this->form_validation->run() == FALSE) {
            
         $data = array();
        $data['title'] = 'Slider';
         $data['admin_main_content'] = $this->load->view('admin_pages/pages/youtube/youtube_video_add_view', '', TRUE);
       $this->load->view('admin_pages/admin_master', $data);
         
            
        } 
        else 
            {
//Setting values for tabel columns
         
           $data_arr = array(
                'title' => $this->input->post('name'),
                'file' => $this->input->post('file'),
                'inserted_by' => $this->session->userdata('id'),
                'insert_time' => date('Y-m-d h:m:s'),
                'updated_by' => NULL,
                'update_time' => NULL,
                'is_active' => 1,
                'is_delete' => 0,
            );
           
//            echo '<pre>';
//            print_r($data_arr);
//            exit();
           $this->db->insert('youtube_video', $data_arr);
//Transfering data to Model
          
           
             $sdata['yt_message']='Data Inserted Successfully';
           $this->session->set_userdata($sdata);
//Loading View
//            $this->load->view('master', $data);
            redirect('Youtube/youtube_video');
        }
}
    public function youtube_video_edit_update($id) {

       $this->load->library('form_validation');

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->form_validation->set_rules('name', 'Title Name', 'required|min_length[5]|max_length[15]');
        $this->form_validation->set_rules('file', 'Link', 'required');
    



        if ($this->form_validation->run() == FALSE) {
            
        $data = array();
        $data['title'] = 'Slider';
        $cdata['select_youtube_video'] =  $this->Adeshbroad_Model->get_youtube_video_by_id($id);
     
         $data['admin_main_content'] = $this->load->view('admin_pages/pages/youtube/youtube_video_edit_view', $cdata, TRUE);
       $this->load->view('admin_pages/admin_master', $data);
       
            
        } 
        else 
            {
//Setting values for tabel columns
         $id=$this->input->post('id');
           $data_arr = array(
                'title' => $this->input->post('name'),
                'file' => $this->input->post('file'),
                'updated_by' => $this->session->userdata('id'),
                'update_time' =>date('Y-m-d h:m:s'),
               
            );
//           
//            echo '<pre>';
//            print_r($id);
//            exit();
           $this->Adeshbroad_Model->update_save_youtube_video_by_id($id,$data_arr);
           
//Transfering data to Model
          
            $sdata['ytup_message']='Data Updated Successfully';
           $this->session->set_userdata($sdata);
//Loading View
//            $this->load->view('master', $data);
            redirect('Youtube/youtube_video');
        }
}
}
