<?php


class Coupon extends CI_Controller{
    //put your code here
     public function __construct() {
        parent::__construct();
        $res=$this->session->userdata('id');
        if ($res == NULL) {
            redirect('admin', 'refresh');
        }
    }
     public function manage_coupon(){
        $data = array();
        $cdata = array();
        $data['title'] = 'Manage';
        $cdata['all_coupon_discount']=  $this->Adeshbroad_Model->get_all_coupon_discount();

          $data['admin_main_content'] = $this->load->view('admin_pages/pages/coupon/manage_coupon_view', $cdata, TRUE);
       $this->load->view('admin_pages/admin_master', $data);
    }
    
    public function active_coupon($id) {
        $this->Adeshbroad_Model->coupon_active_info($id);
        redirect('Coupon/manage_coupon');
    }

    public function inactive_coupon($id) {
        $this->Adeshbroad_Model->coupon_inactive_info($id);
        redirect('Coupon/manage_coupon');
    }
    public function delete_coupon($id) {
        $this->Adeshbroad_Model->delete_coupon_info($id);
        redirect('Coupon/manage_coupon');
    }
      public function add_coupon(){
        
        
         $this->load->library('form_validation');

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

//Validating Name Field
        $this->form_validation->set_rules('coupon_number', 'Coupon Number ', 'required');
        $this->form_validation->set_rules('discount', 'Discount Name', 'required');



        if ($this->form_validation->run() == FALSE) {
            
       $data = array();
    
        $data['title'] = 'Add';
      
          $data['admin_main_content'] = $this->load->view('admin_pages/pages/coupon/add_coupon_view', '', TRUE);
       $this->load->view('admin_pages/admin_master', $data);
        
        
            
        } 
        else 
            {
//Setting values for tabel columns
            $data = array(
                'coupon_number' => $this->input->post('coupon_number'),
                'coupon_dis_tk' => $this->input->post('discount'),
                'insert_by' =>$this->session->userdata('id'),
                'insert_time' => date('Y-m-d h:m:s'),
                'update_by' => 0,
                'update_time' => 0,
                'is_active' => 1,
                'status' => 1
            );
//            echo '<pre>';
//            print_r($data);
//            exit();
//Transfering data to Model
            $this->db->insert('coupon',$data);
            $sdata['co_message']='Data Inserted Successfully';
           $this->session->set_userdata($sdata);
//Loading View
//            $this->load->view('master', $data);
            redirect('Coupon/manage_coupon');
        }
//        ==============
       
    }
    public function edit_coupon($id) {
       
         $this->load->library('form_validation');

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

//Validating Name Field
        $this->form_validation->set_rules('coupon_number', 'Coupon Number ', 'required');
        $this->form_validation->set_rules('discount', 'Discount Name', 'required');



        if ($this->form_validation->run() == FALSE) {
            
       $data = array();
       $cdata = array();
       $data['title'] = 'Add';
       $cdata['coupon_by_id']=  $this->Adeshbroad_Model->get_coupon_by_id($id);
       $data['admin_main_content'] = $this->load->view('admin_pages/pages/coupon/edit_coupon_view', $cdata, TRUE);
       $this->load->view('admin_pages/admin_master', $data);
            
        } 
        else 
            {
//Setting values for tabel columns
                $id =$this->input->post('coupon_id');
            $data = array(
            
                'coupon_number' => $this->input->post('coupon_number'),
                'coupon_dis_tk' => $this->input->post('discount'),
                'update_by' =>$this->session->userdata('id'),
                'update_time' => date('Y-m-d h:m:s')
            
            );
            
//           echo '<pre>';
//          print_r($id);
//            exit();
//Transfering data to Model
           // $this->db->insert('coupon',$data);
            $this->Adeshbroad_Model->update_coupon_by_id($id,$data);
            $data['message'] = 'Data Inserted Successfully';
//Loading View
//            $this->load->view('master', $data);
            redirect('Coupon/manage_coupon');
        }
    }
}
