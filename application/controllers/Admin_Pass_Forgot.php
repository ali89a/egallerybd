<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Admin_Pass_Forgot
 *
 * @author User
 */
class Admin_Pass_Forgot extends CI_Controller{
    //put your code here
    
     public function index() 
            {}
        
     public function admin_pass_forget() 
            {
        
      $this->load->view('admin_pages/admin_pass_forgot_view');
            }
    public function admin_email_check() 
            {
        
        $sdata = array();
        $email = $this->input->post('email', true);
      
        $result = $this->admin_model->admin_email_check_info($email);
//        echo '<pre>';
//        print_r($result);
//        exit();
        if ($result) {



            $data = array();
            $data['from_address'] = " info@egallerybd.com";
            $data['admin_full_name'] = "EgalleryBD Admin";
            $data['to_address'] = $email;
            $data['subject'] = "Password Reset Email";
            $data['admin_name'] = $result->admin_name;
            $this->admin_model->send_email($data, 'admin_password_recovery_email');
            redirect('Admin_Pass_Forgot/send_password_successfully');
        } else {
            $sdata['mailmessage'] = "Your are Not a Registart Email Address";
            $this->session->set_userdata($sdata);
            redirect('Admin_Pass_Forgot/admin_pass_forget');
        }
    }
    public function send_password_successfully(){
    
        //echo 'success';
        redirect('Admin');
    }
    public function update_admin_password(){
        
         $new_password=$this->input->post('password',true);
        $com_new_password=$this->input->post('com_password',true);
         $email_address=$this->input->post('email',true);
        if ($new_password==$com_new_password) {
         
              $this->Mailer_Model->update_new_admin_password($new_password,$email_address);
        $sdata=array();
        $sdata['updatemessage']="Update Your New Password Successfully !";
        $this->session->set_userdata($sdata);
        redirect("email/update_admin_password_successfully");
            
        } else {
              $sdata=array();
        $sdata['updatemessage']="Not Match Password !";
        $this->session->set_userdata($sdata);
        }
       
    }
    
}
