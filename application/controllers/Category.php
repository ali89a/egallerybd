<?php

class Category extends CI_Controller {

    //put your code here
    public function __construct() {
        parent::__construct();
        $res = $this->session->userdata('id');
        if ($res == NULL) {
            redirect('admin', 'refresh');
        }
    }

    public function index() {

        $data = array();
        $data['title'] = 'Add Category';
        $data['admin_main_content'] = $this->load->view('admin_pages/pages/category/category_add_view', '', TRUE);
        $this->load->view('admin_pages/admin_master', $data);
    }

    public function view() {

        $data = array();
        $data['title'] = 'View Category';
        $cdata = array();
        $cdata['select_all_category'] = $this->Category_Model->select_all_category_info();
        $data['admin_main_content'] = $this->load->view('admin_pages/pages/category/category_list_view', $cdata, TRUE);
        $this->load->view('admin_pages/admin_master', $data);
    }

    public function category_save() {

        $data = array();

      
        //        start
        $this->load->library('upload');
        $config['upload_path'] = 'upload_image/icons/category_icon/';
        $config['allowed_types'] = 'gif|jpg|png';
        //$config['max_size']= '200';
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        $error = '';
        $fdata = array();
        if (!$this->upload->do_upload('icn')) {
            $error = $this->upload->display_errors();
        //    echo $error;
            /* exit(); */
        } else {
            $fdata = $this->upload->data();
            $data['category_icon'] = $config['upload_path'] . $fdata['file_name'];
        }
        //end

        $txt_cname = $this->input->post('txt_category');
        $data_arr = array(
            'category_name' => $txt_cname,
            'inserted_by' => $this->session->userdata('id'),
            'insert_time' => date('Y-m-d h:m:s'),
            'updated_by' => 0,
            'update_time' => 0,
            'is_active' => 1,
            'is_delete' => 0,
        );
        $t_data = $data_arr + $data;
//          echo '<pre>';
//        print_r($t_data);
//        exit();
        $this->db->insert('category', $t_data);
      
       $this->session->set_flashdata('success', 'Saved Category');
        redirect('Category/view');
    }

    public function inactive($category_id) {
        $this->Category_Model->inactive_category_info($category_id);
        redirect('Category/view');
    }

    public function active($category_id) {
        $this->Category_Model->active_category_info($category_id);
        redirect('Category/view');
    }

    public function edit_category($category_id) {
        $data = array();
        $data['title'] = 'Edit Category';
        $cdata = array();
        $cdata['select_category_by_id'] = $this->Category_Model->select_category_by_id($category_id);


//         echo '<pre>';
//         print_r($cdata);
//         exit();

        $data['admin_main_content'] = $this->load->view('admin_pages/pages/category/category_edit_view', $cdata, TRUE);
        $this->load->view('admin_pages/admin_master', $data);
    }

    public function update_category() {


        $data = array();


        //        start
        $this->load->library('upload');
        $config['upload_path'] = 'upload_image/icons/category_icon/';
        $config['allowed_types'] = 'gif|jpg|png';
        //$config['max_size']= '200';
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        $error = '';
        $fdata = array();
        if (!$this->upload->do_upload('icn')) {
            $error = $this->upload->display_errors();
            echo $error;
            /* exit(); */
        } else {
            $fdata = $this->upload->data();
            $data['category_icon'] = $config['upload_path'] . $fdata['file_name'];
        }
        //end


        $txt_cname = $this->input->post('txt_category');
        $category_id = $this->input->post('category_id');
        // echo $category_id;
        $data_arr = array(
            'category_name' => $txt_cname,
            'updated_by' =>$this->session->userdata('id'),
            'update_time' => date('Y-m-d h:m:s'),
            'is_active' => 1,
            'is_delete' => 0,
        );
        $t_data = $data_arr + $data;



//echo '<pre>';
//print_r($t_data);
//exit();

        $this->Category_Model->update_category_info($t_data, $category_id);
        $sdata = array();
        $sdata['message'] = "Successfully Update Category";
        $this->session->set_userdata($sdata);
        redirect('Category/view');
    }

    public function delete_category($id) {

        $this->Category_Model->delete_category_by_id($id);
        redirect('Category/view');
    }
    
    
      public function save_product() {

        $product_id = $this->input->post('product_id');
        $vendor_store_id = $this->input->post('vendor_store_id');
        $top_brand_id = $this->input->post('top_brand_id');
        $cbo_category = $this->input->post('category_id');
        $cbo_sub_category = $this->input->post('sub_category_id');
         $sub3_id = $this->input->post('sub3_id');
        $cbo_brand = $this->input->post('brand_id');
        $txt_product = $this->input->post('txt_product');
        $txt_product_item_code = $this->input->post('txt_product_item_code');
        $price = $this->input->post('price');
        $n_p=$this->input->post('new_price');
        if(!empty($n_p)){
            
               $new_price = $n_p;
        }else{
             $new_price =$price;
        }
     
        
        $discount = $this->input->post('discount');
        $bdtdiscount = $this->input->post('bdtdiscount');
        $txt_short_description = $this->input->post('txt_short_description');
        $txt_long_description = $this->input->post('txt_long_description');

        $data = array(
            'top_brand_id' => $top_brand_id,
            'vendor_store_id' => $vendor_store_id,
            'brand_id' => $cbo_brand,
            'category_id' => $cbo_category,
            'sub_category_id' => $cbo_sub_category,
            'sub_category_three_id' => $sub3_id,
            'product_name' => $txt_product,
            'product_short_des' => $txt_short_description,
            'product_long_des' => $txt_long_description,
            'product_price' => $price,
            'current_sale_price' => $new_price,
            'discount' => $discount,
            'bdt_discount' => $bdtdiscount,
            'product_code' => $txt_product_item_code,
            'inserted_by' => $this->session->userdata('id'),
            'insert_time' => date('Y-m-d h:m:s'),
            'updated_by' => NULL,
            'update_time' => NULL,
            'is_active' => 1,
            'is_vendor_product' => '0',
            'is_product' => 1,
        );

        

        //        start single file upload
        $this->load->library('upload');
        $config['upload_path'] = 'upload_image/product_image/master/';
        $config['allowed_types'] = 'gif|jpg|png';
        //$config['max_size']= '200';
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        $error = '';
        $fdata = array();
        if (!$this->upload->do_upload('micon')) {
            $error = $this->upload->display_errors();
            //    echo $error;
            /* exit(); */
        } else {
            $fdata = $this->upload->data();
            $data['product_img_master'] = $config['upload_path'] . $fdata['file_name'];
        }
        //end
        //        start multiple file upload
        if (!empty($_FILES['userFiles']['name'])) {
            $filesCount = count($_FILES['userFiles']['name']);
            for ($i = 0; $i < $filesCount; $i++) {
                //  echo $i;
                $_FILES['userFile']['name'] = $_FILES['userFiles']['name'][$i];
                $_FILES['userFile']['type'] = $_FILES['userFiles']['type'][$i];
                $_FILES['userFile']['tmp_name'] = $_FILES['userFiles']['tmp_name'][$i];
                $_FILES['userFile']['error'] = $_FILES['userFiles']['error'][$i];
                $_FILES['userFile']['size'] = $_FILES['userFiles']['size'][$i];
                $uploadPath = 'upload_image/product_image/other/';
                $config['upload_path'] = $uploadPath;
                $config['allowed_types'] = 'gif|jpg|JPG|jpeg|JPEG|png|txt';
                //$config['max_size']	= '303374600';
                //$config['max_width'] = '1024000';
                //$config['max_height'] = '768000';
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if ($this->upload->do_upload('userFile')) {
                    $fileData = $this->upload->data();
                    //$uploadData[$i]['file_name'] = $fileData['file_name'];
                    //$uploadData[$i]['created'] = date("Y-m-d H:i:s");
                    $uploadData[$i] = $uploadPath . $fileData['file_name'];
                }
            }
            //var_dump($uploadData);
            if (!empty($uploadData)) {
                $data['product_img'] = implode(',', $uploadData);
            }
        }
        //end

        
//         echo '<pre>';
//      print_r($data);
//    exit();
        if (empty($product_id)) {
            $this->db->insert('product', $data);
            $this->session->set_flashdata('success', 'Successfully Saved Product');
        } else {

            $this->db->where('product_id', $product_id)
                    ->update('product', $data);

            $this->session->set_flashdata('success', 'Successfully Update Product');
        }
        
//         echo '<pre>';
//      print_r($data);
//    exit();
       redirect('Product/view');
    }
      public function latest_inactive($id) {

        $this->Product_Model->latest_inactive_info($id);
        redirect('Product/view');
    }

    public function latest_active($id) {
        $this->Product_Model->latest_active_info($id);
        redirect('Product/view');
    }
      public function inactive_product($product_id) {

        $this->Product_Model->inactive_product_info($product_id);
        redirect('Product/view');
    }

    public function active_product($product_id) {
        $this->Product_Model->active_product_info($product_id);
        redirect('Product/view');
    }

    public function delete_product($product_id) {

        $this->Product_Model->delete_product_by_id($product_id);
        redirect('Product/view');
    }
    public function update_discount() {


        $product_id = $this->input->post('product_id');
        $data['product_price'] = $this->input->post('product_price');
        $data['discount'] = $this->input->post('discount');
        $data['bdt_discount'] = $this->input->post('bdt_discount');
        $data['current_sale_price'] = $this->input->post('current_sale_price');


        $this->db->where('product_id', $product_id)
                ->update('product', $data);
        $this->session->set_flashdata('success', 'Successfully Update Discount');

        redirect('Product/view');
    }

}
