<?php


class Brand_Model extends CI_Model {

    //put your code here

    public function save_brand_info($data) {
        $this->db->insert('brand', $data);
    }

    public function select_all_brand_info() {

        $this->db->select('brand.*,category.category_name');
        $this->db->from('brand');
        $this->db->join('category','category.category_id = brand.category_id','left');
        $this->db->where('brand.is_delete',0); 
        $this->db->order_by("brand_id","desc");
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }
    
    public function select_all_sub3_info() {

        $this->db->select('sub_category_three.*,category.category_name');
        $this->db->from('sub_category_three');
        $this->db->join('category','category.category_id = sub_category_three.category_id','left');
        //$this->db->where('sub_category_three.is_delete',0); 
        $this->db->order_by("sub_category_three_id","desc");
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }
 public function get_subcat_by_cat_id_info($category_id) {
        $this->db->select('*');
        $this->db->from('sub_category');
      $this->db->where('is_active', 1);
        $this->db->where('is_delete', 0);
        $this->db->where('category_id',$category_id);
         $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }
 public function get_brand_by_subcat_id_info($sub_category_id) {
        $this->db->select('*');
        $this->db->from('brand');
      $this->db->where('is_active', 1);
        $this->db->where('is_delete', 0);
        $this->db->where('sub_category_id',$sub_category_id);
         $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }
 public function get_sub3_by_brand_id_info($brand) {
        $this->db->select('*');
        $this->db->from('sub_category_three');
      $this->db->where('is_active', 1);
        //$this->db->where('is_delete', 0);
        $this->db->where('brand_id',$brand);
         $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }
    public function select_all_published_sub_category_info() {
        $this->db->select('*');
        $this->db->from('sub_category');
        $this->db->where('is_active', 1);
        $this->db->where('is_delete', 0);
        $query_result = $this->db->get();
        $result = $query_result->result();
        $res=array(''=>'no value');
        if(count($result)!=0)
        {
            $res='';
            foreach ($result as $sub_cat)
            {
//                echo '<pre>';
//                print_r($sub_cat);
//                exit();
                $res[$sub_cat->sub_category_id]=$sub_cat->sub_cat_name;
            }
        }
        return $res;
    }
    public function select_all_published_brand_info() {
        $this->db->select('*');
        $this->db->from('brand');     
        $this->db->where('is_active', 1);
        $this->db->where('is_delete', 0);
        $query_result = $this->db->get();
        $result = $query_result->result();
        foreach ($result as $v_cat)
        {
//            echo '<pre>';
//            print_r($v_cat);
//            exit();
            $res[$v_cat->brand_id]=$v_cat->brand_name;
        }
        return $res;
    }
    

    public function inactive_brand_info($brand_id) {
        $this->db->select('*');
        $this->db->from('brand');
        $this->db->where('brand_id',$brand_id);
        $this->db->set('is_active', 0);
        $this->db->update('brand');
    }
    public function inactive_info($brand_id) {
        $this->db->select('*');
        $this->db->from('brand');
        $this->db->where('sub_category_three_id',$brand_id);
        $this->db->set('is_active', 0);
        $this->db->update('sub_category_three');
    }
    public function active_info($brand_id) {
        $this->db->select('*');
        $this->db->from('brand');
        $this->db->where('sub_category_three_id',$brand_id);
        $this->db->set('is_active', 1);
        $this->db->update('sub_category_three');
    }
        public function delete_by_id($brand_id) {
        
        $this->db->where('sub_category_three_id', $brand_id);
        $this->db->delete('sub_category_three');
    }

    public function active_brand_info($brand_id) {
        $this->db->select('*');
        $this->db->from('brand');
        $this->db->where('brand_id', $brand_id);
        $this->db->set('is_active', 1);
        $this->db->update('brand');
    }

    public function select_brand_by_id($brand_id) {

        $this->db->select('*');
        $this->db->from('brand');
        $this->db->where('brand_id', $brand_id);
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }

    public function update_brand_info($data, $brand_id) {
        $this->db->select('*');
        $this->db->from('brand');
        $this->db->where('brand_id', $brand_id);
        $this->db->update('brand', $data);
    }

    public function delete_brand_by_id($brand_id) {
         $this->db->select('*');
        $this->db->from('brand');
        $this->db->where('brand_id', $brand_id);
        $this->db->set('is_delete', 1);
        $this->db->update('brand');
    }
    
    //=====================
    
    
    //=====================

}
