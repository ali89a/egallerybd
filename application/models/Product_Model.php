<?php

class Product_Model extends CI_Model{
    //put your code here
    


    public function select_all_pub_top_brand_info() {
        $this->db->select('*');
        $this->db->from('top_brand');
        $query_result = $this->db->get();
        $result = $query_result->result(); 
        return $result;
    }
    public function select_all_pub_vendor_info() {
        $this->db->select('*');
        $this->db->from('vendor_store');
        $query_result = $this->db->get();
        $result = $query_result->result(); 
        return $result;
    }
    public function select_all_published_brand_info() {
        $this->db->select('*');
        $this->db->from('brand');
        $this->db->where('is_active', 1);
        $this->db->where('is_delete', 0);
        $query_result = $this->db->get();
        $result = $query_result->result(); 
        return $result;
    }
    
    //---------------------------
    public function select_all_product_info()
    {
        $this->db->select('product.*,category.category_name,brand.brand_name');
        $this->db->from('product');
        $this->db->join('category','category.category_id = product.category_id','left');
        $this->db->join('brand','brand.brand_id = product.brand_id','left');
        $this->db->order_by("product.product_id","desc");
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }
    public function select_all_search_product_info()
    {
        
        $category_id= $this->input->post('category_id');
        $sub_category_id= $this->input->post('sub_category_id');
        $brand_id= $this->input->post('brand_id');
        $search_text_name= $this->input->post('search_text_name');
        $search_text_code= $this->input->post('search_text_code');
 
        $this->db->select('product.*,category.category_name,brand.brand_name');
        $this->db->from('product');
        $this->db->join('category','category.category_id = product.category_id','left');
        $this->db->join('brand','brand.brand_id = product.brand_id','left');
        $this->db->order_by("product.product_id","desc");
        
         if(!empty($category_id)){
          $this->db->like('product.category_id', $category_id);   
         }
         if(!empty($sub_category_id)){
           $this->db->like('product.sub_category_id', $sub_category_id);  
         }
         if(!empty($brand_id)){
           $this->db->like('product.brand_id', $brand_id);  
         }
         if(!empty($search_text_name)){
           $this->db->like('product.product_name', $search_text_name);  
         }
         if(!empty($search_text_code)){
           $this->db->like('product.product_code', $search_text_code);  
         }
        
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }
    
     public function inactive_product_info($product_id) {
        $this->db->select('*');
        $this->db->from('product');
        $this->db->where('product_id',$product_id);
        $this->db->set('is_active', 0);
        $this->db->update('product');
    }

    public function active_product_info($product_id) {
        $this->db->select('*');
        $this->db->from('product');
        $this->db->where('product_id', $product_id);
        $this->db->set('is_active', 1);
        $this->db->update('product');
    }
     public function delete_product_by_id($product_id) {
        
        $this->db->where('product_id', $product_id);
        $this->db->delete('product');
    }
    
    public function select_product_by_id($product_id){
        
          $this->db->select('*');
        $this->db->from('product');
        $this->db->where('product_id', $product_id);
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }
    
    public function update_product_info($data,$product_id){
         $this->db->select('*');
        $this->db->from('product');
        $this->db->where('product_id', $product_id);
        $this->db->update('product', $data);
    }    
    public function latest_inactive_info($id){
      
       $this->db->select('*');
        $this->db->from('product');
        $this->db->where('product_id',$id);
        $this->db->set('is_latest', 0);
        $this->db->update('product');
    }
    public function latest_active_info($id){
      
      $this->db->select('*');
        $this->db->from('product');
        $this->db->where('product_id',$id);
        $this->db->set('is_latest', 1);
        $this->db->update('product');
        
    }
}
