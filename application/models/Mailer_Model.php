<?php

class Mailer_model extends CI_Model {

    /**
     * send_email
     * @author Md. Shafiul Alam
     * @param --- $data - information to place in the mail 
     * $templateName - html template to use in mail body          
     * @return --- none
     * modified by ----- Shafiul Alam
     * date --- 12/04/2009 (mm/dd/yyyy  )
     */
    function send_email($data, $templateName) {
//        echo "<pre>";
//        print_r($data);

        $this->load->library('email');
        $this->email->set_mailtype('html');
        $this->email->from($data['from_address'], $data['admin_full_name']);
        $this->email->to($data['to_address']);
        //$this->email->cc($data['cc_address']);
        $this->email->subject($data['subject']);
        $body = $this->load->view('mail_scripts/' . $templateName, $data, true);
        //echo $body;
        //exit();
        $this->email->message($body);
        $this->email->send();
        $this->email->clear();
    }
    public function update_new_password()
    {
        $new_password=$this->input->post('password',true);
        $email_address=$this->input->post('email',true);
        
//      echo $new_password.'---------------'.$email_address;
//        
//       exit();
        $this->db->set('password',md5($new_password));
        $this->db->where('customer_email',$email_address);
        $this->db->update('customer');
        
        
    }
    public function update_new_admin_password($new_password,$email_address)
    {
       
        
//      echo $new_password.'---------------'.$email_address;
//        
//       exit();
        $this->db->set('password',md5($new_password));
        $this->db->where('email',$email_address);
        $this->db->update('admin');
        
        
    }
}
