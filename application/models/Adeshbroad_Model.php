<?php



class Adeshbroad_Model extends CI_Model{
    //put your code here
    
    
      public function get_visitor_count() {
        $this->db->select('*');
        $this->db->from('visitor_count');
        $this->db->where('ip_1', $this->input->server('REMOTE_ADDR'));
        $this->db->where('visited_url', $this->input->server('REQUEST_URI'));
        $query_result = $this->db->get();
        $result = $query_result->result();

        if (count($result) == 1) {
            $this->db->where('ip_1', $this->input->server('REMOTE_ADDR'));
            $this->db->where('visited_url', $this->input->server('REQUEST_URI'));
            $this->db->set('last_visit_date', date('Y-m-d h:m:i'));
            $this->db->set('total_visit', 'total_visit+1', FALSE);
            $this->db->update('visitor_count');
        } else {
            $data = array(
                'ip_1' => $this->input->server('REMOTE_ADDR'),
                'ip_2' => $this->input->server('REMOTE_ADDR'),
                'ip_3' => $this->input->server('REMOTE_ADDR'),
                'last_visit_date' => date('Y-m-d h:m:i'),
                'visited_url' => $this->input->server('REQUEST_URI'),
                'total_visit' => 1,
            );
            $this->db->insert('visitor_count', $data);
        }

        $query_result = $this->db->query('SELECT sum(total_visit) as total_count from visitor_count');
        $result = $query_result->row();
        return $result;
    }
     public function get_shipping_cost_by_id_info($area_code){
       
          $this->db->select("*");
        $this->db->from('shipping_cost');
        $this->db->where('shipping_area_id', $area_code);
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
     }
     
      public function  get_loggedin_customer_info($id){
       
          $this->db->select("*");
        $this->db->from('customer');
        $this->db->where('customer_id', $id);
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
     }
     
    
     
      //    about us
    public function ab_inactive_info($id){
      
       $this->db->select('*');
        $this->db->from('about_us');
        $this->db->where('about_us_id',$id);
        $this->db->set('is_active', 0);
        $this->db->update('about_us');
    }
 
    public function ab_active_info($id){
      
       $this->db->select('*');
        $this->db->from('about_us');
        $this->db->where('about_us_id',$id);
        $this->db->set('is_active', 1);
        $this->db->update('about_us');
    }
     public function ab_delete_category_by_id($id){
      
        $this->db->where('about_us_id', $id);
        $this->db->delete('about_us');
    }
     public function get_about_us_info_by_id($id){
        $this->db->select('*');
        $this->db->from('about_us');
        $this->db->where('about_us_id',$id);
      
        $query_result=  $this->db->get();
        $result=$query_result->row();
        return $result;
        
    }
    public function update_about_us_info_by_id($id,$data){
       
      
      $this->db->select('*');
        $this->db->from('about_us');
        $this->db->where('about_us_id', $id);
        $this->db->update('about_us', $data);
        
    }
    
    //    about us
     public function get_coupon_discount_info($coupon_code)
             {
         $this->db->select("*");
        $this->db->from('coupon');
        $this->db->where('coupon_number', $coupon_code);
        $this->db->where('status', 1);
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }
     public function get_product_by_search_keyword($search_keyword){
         

        $this->db->select("*");
        $this->db->from('product');
        $this->db->like('product_name', $search_keyword, 'both');
        $this->db->where('active', 1);
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }
     public function check_email_address($email_address)
    {
        $this->db->select("*");
        $this->db->from('customer');
        $this->db->where('customer_email',$email_address);
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }
    
    public function get_customer_by_id($id){
        
       $this->db->select('*');
        $this->db->from('customer');
        $this->db->where('customer_id',$id);
        $query_result=  $this->db->get();
        $result=$query_result->row();
        return $result;
    }
    public function get_order_customer_by_id($id){
        
       $this->db->select('*');
        $this->db->from('customer_order');
        $this->db->order_by('order_id',"desc");
        $this->db->where('customer_id',$id);
        $query_result=  $this->db->get();
        $result=$query_result->result();
        return $result;
    }
    public function get_all_active_gallery_image_by_product(){
        
        $this->db->select('*');
        $this->db->from('product');
        $this->db->where('active',1);
        $query_result=  $this->db->get();
        $result=$query_result->result();
        return $result;
    }
    public function get_shipping_info($id){
        
       $this->db->select('*');
        $this->db->from('shipping');
        $this->db->join('customer_order', 'shipping.ship_id = customer_order.shipping_id', 'left');
       $this->db->where('customer_order.order_id', $id);
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }
    public function get_all_customer(){
        
       $this->db->select('*');
        $this->db->from('customer');
        $query_result=  $this->db->get();
        $result=$query_result->result();
        return $result;
    }
    public function get_search_all_customer($search_name=''){
        
        $this->db->select('*');
        $this->db->from('customer');
        $this->db->like('customer_name', $search_name);
        $this->db->or_like('customer_email', $search_name);
        $this->db->or_like('address', $search_name);
        $this->db->or_like('mobile', $search_name);
        $this->db->or_like('dob', $search_name);
        $this->db->or_like('blood_group', $search_name);
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }
    public function get_all_vendor(){
        
       $this->db->select('*');
        $this->db->from('vendor');
        $query_result=  $this->db->get();
        $result=$query_result->result();
        return $result;
    }
    public function get_all_top_brand(){
        
       $this->db->select('*');
        $this->db->from('top_brand');
        $query_result=  $this->db->get();
        $result=$query_result->result();
        return $result;
    }
    public function get_all_vendor_store(){
        
       $this->db->select('*');
        $this->db->from('vendor_store');
        $query_result=  $this->db->get();
        $result=$query_result->result();
        return $result;
    }
    public function get_all_search_top_brand($search_name=''){
        
       $this->db->select('*');
        $this->db->from('top_brand');
        
        $this->db->like('top_brand_name',$search_name);
        $this->db->or_like('top_brand_email',$search_name);
        $this->db->or_like('top_brand_contact',$search_name);
        
       
        $query_result=  $this->db->get();
        $result=$query_result->result();
        return $result;
    }
    public function get_all_search_vendor_store($search_name=''){
        
       $this->db->select('*');
        $this->db->from('vendor_store');
        
        $this->db->like('vendor_store_name',$search_name);
        $this->db->or_like('vendor_store_email',$search_name);
        $this->db->or_like('vendor_store_contact',$search_name);
        
       
        $query_result=  $this->db->get();
        $result=$query_result->result();
        return $result;
    }
    public function get_customer_info($id){
        
       $this->db->select('*');
        $this->db->from('customer');
          $this->db->join('customer_order', 'customer.customer_id = customer_order.customer_id', 'left'); 
          $this->db->join('payment', 'customer_order.payment_id = payment.payment_id', 'left'); 
        $this->db->where('customer_order.order_id',$id);
        $query_result=  $this->db->get();
        $result=$query_result->row();
        return $result;
    }
    public function get_customer_pass_check_by_id($pass,$data){
        
//        echo '<pre>';
//        print_r($data);
//        exit();
        
        
      $this->db->select('*');
        $this->db->from('customer');
        $this->db->where('customer_id', $this->session->userdata('customer_id'));
        $this->db->where('password', md5($pass));
        $this->db->update('customer', $data);
    }
    public function get_all_coupon_discount(){
        $this->db->select('*');
        $this->db->from('coupon');
        $this->db->order_by('coupon_id',"desc");
      
        $query_result=  $this->db->get();
        $result=$query_result->result();
        return $result;
        
    }
    public function coupon_inactive_info($id){
          $this->db->select('*');
        $this->db->from('coupon');
        $this->db->where('coupon_id',$id);
        $this->db->set('status', 0);
        $this->db->update('coupon');
    }
    public function shipping_cost_inactive_info($id){
          $this->db->select('*');
        $this->db->from('shipping_cost');
        $this->db->where('shipping_cost_id',$id);
        $this->db->set('status', 0);
        $this->db->update('shipping_cost');
    }
    public function shipping_cost_active_info($id){
          $this->db->select('*');
        $this->db->from('shipping_cost');
        $this->db->where('shipping_cost_id',$id);
        $this->db->set('status', 1);
        $this->db->update('shipping_cost');
    }
    public function coupon_active_info($id){
          $this->db->select('*');
        $this->db->from('coupon');
        $this->db->where('coupon_id',$id);
        $this->db->set('status', 1);
        $this->db->update('coupon');
    }
    public function delete_coupon_info($id){
           $this->db->where('coupon_id', $id);
        $this->db->delete('coupon');
        
       
    }
    public function delete_shipping_cost_info($id){
           $this->db->where('shipping_cost_id', $id);
        $this->db->delete('shipping_cost');
        
       
    }
    public function get_coupon_by_id($id){
       $this->db->select('*');
       $this->db->from('coupon');
       $this->db->where('coupon_id', $id);
       $query_result=  $this->db->get();
       $result=$query_result->row();
       return $result;
        
       
    }
    public function get_shipping_cost_by_id($id){
       $this->db->select('*');
       $this->db->from('shipping_cost');
       $this->db->where('shipping_cost_id', $id);
       $query_result=  $this->db->get();
       $result=$query_result->row();
       return $result;
        
       
    }
    public function update_coupon_by_id($id,$data){
        $this->db->select('*');
        $this->db->from('coupon');
        $this->db->where('coupon_id', $id);
        $this->db->update('coupon', $data);
        
    }
    public function update_shipping_cost_by_id($id,$data){
        $this->db->select('*');
        $this->db->from('shipping_cost');
        $this->db->where('shipping_cost_id', $id);
        $this->db->update('shipping_cost', $data);
        
    }

    public function get_all_shipping_area(){
        $this->db->select('*');
        $this->db->from('shipping_area');
        $this->db->order_by('shipping_area_id',"desc");
        $query_result=  $this->db->get();
        $result=$query_result->result();
        return $result;
        
    }
    public function get_all_shipping_cost(){
        $this->db->select('*');
        $this->db->from('shipping_cost');
        $this->db->order_by('shipping_cost_id',"desc");
        $query_result= $this->db->get();
        $result=$query_result->result();
        return $result;
        
    }
    public function get_category(){
        $this->db->select('*');
        $this->db->from('category');
        $this->db->order_by('category_id',"desc");
      
        $query_result=  $this->db->get();
        $result=$query_result->result();
        return $result;
        
    }
    public function get_brand(){
        $this->db->select('*');
        $this->db->from('brand');
        $this->db->order_by('brand_id',"desc");
      
        $query_result=  $this->db->get();
        $result=$query_result->result();
        return $result;
        
    }
    public function select_sub_category_three_by_id($id){
        $this->db->select('*');
        $this->db->from('sub_category_three');
        $this->db->where('sub_category_three_id',$id);
      
        $query_result=  $this->db->get();
        $result=$query_result->row();
        return $result;
        
    }
    public function get_sub_category(){
        $this->db->select('*');
        $this->db->from('sub_category');
        $this->db->order_by('sub_category_id',"desc");
      
        $query_result=  $this->db->get();
        $result=$query_result->result();
        return $result;
        
    }
     public function get_all_review(){
        $this->db->select('*');
        $this->db->from('review');
        $this->db->order_by('review_id',"desc");
        $query_result=  $this->db->get();
        $result=$query_result->result();
        return $result;
        
    }
      public function delete_review_by_id($id){
      
        $this->db->where('review_id', $id);
        $this->db->delete('review');
    }
    public function get_all_order(){
        
         $from_date=$this->input->post('from_date');
         $to_date=$this->input->post('to_date');
        
        if($from_date==NULL || $to_date==null){
        
        $this->db->select('*');
        $this->db->from('customer_order');
        $this->db->join('payment', 'customer_order.payment_id = payment.payment_id', 'left');
        $this->db->order_by('customer_order.order_id',"desc");
        $query_result=  $this->db->get();
        $result=$query_result->result();
        return $result;
        
        }
        else{
         $this->db->select('*');
        $this->db->from('customer_order'); 
        $this->db->join('payment', 'customer_order.payment_id = payment.payment_id', 'left');
        $this->db->where('customer_order.order_date <=',$to_date);
        $this->db->where('customer_order.order_date >=',$from_date);
        $this->db->order_by('customer_order.order_id',"desc");
         $query_result=  $this->db->get();
        $result=$query_result->result();
        return $result;
        
        }
        
    }
    public function get_product_by_category_id($id){
        $this->db->select('*');
        $this->db->from('product');
        $this->db->where('category_id',$id);
         $this->db->where('active',1);
        $query_result=  $this->db->get();
        $result=$query_result->result();
        return $result;
        
    }
    public function get_all_active_category(){
        $this->db->select('*');
        $this->db->from('category');
        $this->db->order_by('category_id',"desc");
        $this->db->where('is_active',1);
        $query_result=  $this->db->get();
        $result=$query_result->result();
        return $result;
        
    }
    public function get_all_other_business(){
        $this->db->select('*');
        $this->db->from('other_business');
        //$this->db->order_by('career_id',"desc");
        $query_result=  $this->db->get();
        $result=$query_result->row();
        return $result;
        
    }
    public function get_all_about_new(){
        $this->db->select('*');
        $this->db->from('about_new');
        //$this->db->order_by('career_id',"desc");
        $query_result=  $this->db->get();
        $result=$query_result->row();
        return $result;
        
    }
    public function get_all_term_condition(){
        $this->db->select('*');
        $this->db->from('term_condition');
        //$this->db->order_by('career_id',"desc");
        $query_result=  $this->db->get();
        $result=$query_result->row();
        return $result;
        
    }
    public function get_all_return_policy(){
        $this->db->select('*');
        $this->db->from('return_policy');
        //$this->db->order_by('career_id',"desc");
        $query_result=  $this->db->get();
        $result=$query_result->row();
        return $result;
        
    }
    public function get_all_career(){
        $this->db->select('*');
        $this->db->from('career');
        //$this->db->order_by('career_id',"desc");
        $query_result=  $this->db->get();
        $result=$query_result->row();
        return $result;
        
    }
    public function get_all_about(){
        $this->db->select('*');
        $this->db->from('about_us');
        $this->db->order_by('about_us_id',"desc");
        $query_result=  $this->db->get();
        $result=$query_result->result();
        return $result;
        
    }
    public function delete_contact_by_id($id){
          $this->db->where('contact_us_id', $id);
        $this->db->delete('contact_us');
    }
    public function get_all_contact(){
        $this->db->select('*');
        $this->db->from('contact_us');
        $this->db->order_by('contact_us_id',"desc");
        $query_result=  $this->db->get();
        $result=$query_result->result();
        return $result;
        
    }
    public function get_all_about_us(){
        $this->db->select('*');
        $this->db->from('about_us');
        $this->db->order_by('about_us_id',"desc");
        $this->db->where('is_active',1);
        $query_result=  $this->db->get();
        $result=$query_result->result();
        return $result;
        
    }
    public function get_all_advertisement(){
        $this->db->select('*');
        $this->db->from('ad_management');
        $this->db->order_by('ad_id',"desc");
        $this->db->where('is_active',1);
        $query_result=  $this->db->get();
        $result=$query_result->row();
        return $result;
        
    }
    public function get_youtube_video(){
        $this->db->select('*');
        $this->db->from('youtube_video');
        $this->db->order_by('youtube_video_id',"desc");
        $this->db->where('is_active',1);
        $query_result=  $this->db->get();
        $result=$query_result->row();
        return $result;
        
    }
    public function get_all_slider_image(){
        $this->db->select('*');
        $this->db->from(' lib_photo');
        $this->db->order_by('photo_id',"desc");    
        $query_result=  $this->db->get();
        $result=$query_result->result();
        return $result;
        
    }
    public function get_all_advertisement_image(){
        $this->db->select('*');
        $this->db->from('ad_management');
        $this->db->order_by('ad_id',"desc");    
        $query_result=  $this->db->get();
        $result=$query_result->result();
        return $result;
        
    }
    public function get_all_newsletter(){
        $this->db->select('*');
        $this->db->from('newsletter');
        $this->db->order_by('newsletter_id',"desc");    
        $query_result=  $this->db->get();
        $result=$query_result->result();
        return $result;
        
    }
    public function get_all_youtube_video(){
        $this->db->select('*');
        $this->db->from('youtube_video');
        $this->db->order_by('youtube_video_id',"desc");    
        $query_result=  $this->db->get();
        $result=$query_result->result();
        return $result;
        
    }
    public function get_all_active_slider_image(){
        $this->db->select('*');
        $this->db->from(' lib_photo');
        $this->db->order_by('photo_id',"desc");
        $this->db->where('is_active',1);
        $query_result=  $this->db->get();
        $result=$query_result->result();
        return $result;
        
    }
    public function get_slider_img_by_id($id){
        $this->db->select('*');
        $this->db->from(' lib_photo');
        $this->db->where('photo_id',$id); 
        $query_result=  $this->db->get();
        $result=$query_result->row();
        return $result;
        
    }
    public function get_advertisement_by_id($id){
        $this->db->select('*');
        $this->db->from(' ad_management');
        $this->db->where('ad_id',$id); 
        $query_result=  $this->db->get();
        $result=$query_result->row();
        return $result;
        
    }
     public function customer_login_check_info($email_address, $password)
    {

        $this->db->select('*');
        $this->db->from('customer');
        $this->db->where("(customer_email = '$email_address' OR mobile = '$email_address')");
        $this->db->where('password',$password);
        $query_result=$this->db->get();
        $result=$query_result->row();
        return $result;
    }
    public function customer_email_check_info($email){
            $this->db->select('*');
        $this->db->from('customer');
        $this->db->where('customer_email',$email);
        $query_result=$this->db->get();
        $result=$query_result->row();
        return $result;
    }
 
    public function get_category_by_id($id){
        $this->db->select('*');
        $this->db->from('category');
        $this->db->where('category_id',$id);
      
        $query_result=  $this->db->get();
        $result=$query_result->row();
        return $result;
        
    }
    public function get_youtube_video_by_id($id){
        $this->db->select('*');
        $this->db->from('youtube_video');
        $this->db->where('youtube_video_id',$id);
      
        $query_result=  $this->db->get();
        $result=$query_result->row();
        return $result;
        
    }
 
    public function update_category($id,$data){
      
          $this->db->select('*');
        $this->db->from('category');
        $this->db->where('category_id', $id);
        $this->db->update('category', $data);
    }
    public function update_save_youtube_video_by_id($id,$data){
      
          $this->db->select('*');
        $this->db->from('youtube_video');
        $this->db->where('youtube_video_id', $id);
        $this->db->update('youtube_video', $data);
    }
    public function update_slider($id,$data){
      
          $this->db->select('*');
        $this->db->from('lib_photo');
        $this->db->where('photo_id', $id);
        $this->db->update('lib_photo', $data);
    }
    public function update_advertisement($id,$data){
      
          $this->db->select('*');
        $this->db->from('ad_management');
        $this->db->where('ad_id', $id);
        $this->db->update('ad_management', $data);
    }
    public function update_customer_info($id,$data){
      
          $this->db->select('*');
        $this->db->from('customer');
        $this->db->where('customer_id', $id);
        $this->db->update('customer', $data);
    }

    public function delete_category_by_id($id){
      
        $this->db->where('category_id', $id);
        $this->db->delete('category');
    }
    public function inactive_info($id){
      
        $this->db->select('*');
        $this->db->from('category');
        $this->db->where('category_id',$id);
        $this->db->set('is_active', 0);
        $this->db->update('category');
    }
    public function active_info($id){
      
      $this->db->select('*');
        $this->db->from('category');
        $this->db->where('category_id',$id);
        $this->db->set('is_active', 1);
        $this->db->update('category');
        
    }
    public function slider_delete_category_by_id($id){
      
        $this->db->where('photo_id', $id);
        $this->db->delete('lib_photo');
    }
    public function newsletter_email_delete_by_id($id){
      
        $this->db->where('newsletter_id', $id);
        $this->db->delete('newsletter');
    }
 
    public function ad_delete_category_by_id($id){
      
        $this->db->where('ad_id', $id);
        $this->db->delete('ad_management');
    }
    public function youtube_video_delete_by_id($id){
      
        $this->db->where('youtube_video_id', $id);
        $this->db->delete('youtube_video');
    }
    public function slider_inactive_info($id){
      
       $this->db->select('*');
        $this->db->from('lib_photo');
        $this->db->where('photo_id',$id);
        $this->db->set('is_active', 0);
        $this->db->update('lib_photo');
    }
    public function slider_active_info($id){
      
      $this->db->select('*');
        $this->db->from('lib_photo');
        $this->db->where('photo_id',$id);
        $this->db->set('is_active', 1);
        $this->db->update('lib_photo');
        
    }
    public function ad_inactive_info($id){
      
       $this->db->select('*');
        $this->db->from('ad_management');
        $this->db->where('ad_id',$id);
        $this->db->set('is_active', 0);
        $this->db->update('ad_management');
    }
    public function youtube_inactive_info($id){
      
       $this->db->select('*');
        $this->db->from('youtube_video');
        $this->db->where('youtube_video_id',$id);
        $this->db->set('is_active', 0);
        $this->db->update('youtube_video');
    }
    public function ad_active_info($id){
      
        $this->db->select('*');
        $this->db->from('ad_management');
        $this->db->where('ad_id',$id);
        $this->db->set('is_active', 1);
        $this->db->update('ad_management');
        
    }
    public function youtube_active_info($id){
      
        $this->db->select('*');
        $this->db->from('youtube_video');
        $this->db->where('youtube_video_id',$id);
        $this->db->set('is_active', 1);
        $this->db->update('youtube_video');
        
    }
      public function get_all_active_shipping_cost(){
        $this->db->select('*');
        $this->db->from('shipping_cost');
        $this->db->order_by('shipping_cost_id',"ASC");
        $this->db->where('status',1);
        $query_result=  $this->db->get();
        $result=$query_result->result();
        return $result;
        
    }
    
    
//    ====Product=============
    
    
     
       public function get_all_active_product(){
        $this->db->select('*');
        $this->db->from('product');
//        $this->db->order_by('product_id',"Asc");
        $this->db->where('active',1);
        $query_result=  $this->db->get();
        $result=$query_result->result();
        return $result;
        
    }
       public function get_product(){
        $this->db->select('*');
        $this->db->from('product');
         $this->db->join('category', 'product.category_id = category.category_id', 'left'); 
        $this->db->order_by('product.product_id',"desc");
//      $this->db->where('is_active',1);
        $query_result=  $this->db->get();
        $result=$query_result->result();
        return $result;
        
    }
       public function get_all_latest_product(){
        $this->db->select('*');
        $this->db->from('product');
        $this->db->order_by('product_id',"desc");
        $this->db->where('is_latest',1);
        $this->db->where('active',1);
        $query_result=  $this->db->get();
        $result=$query_result->result();
        return $result;
        
    }
       public function get_topseller_all_product(){
        $this->db->select('*');
        $this->db->from('product');
        $this->db->order_by('product_id',"desc");
        $this->db->where('is_top_sales',1);
        $this->db->where('active',1);
        $query_result=  $this->db->get();
        $result=$query_result->result();
        return $result;
        
    }
       public function get_product_by_id($id){
        $this->db->select('*');
        $this->db->from('product');
        $this->db->where('product_id',$id);
         $query_result=  $this->db->get();
        $result=$query_result->row();
        return $result;
        
    }
       public function get_order_details_info($id){
        $this->db->select('*');
        $this->db->from('customer_order_details');
        $this->db->where('order_id',$id);
         $query_result=  $this->db->get();
        $result=$query_result->result();
        return $result;
        
    }
        public function update_product($id,$data){
      
          $this->db->select('*');
        $this->db->from('product');
        $this->db->where('product_id', $id);
        $this->db->update('product', $data);
    }
        public function cancle_order_request_by_id($id){
      
       $this->db->select('*');
        $this->db->from('customer_order');
        $this->db->where('order_id',$id);
        $this->db->set('order_cancle_req', 1);
        $this->db->update('customer_order');
    }
        public function confirm_order_info($id){
      
       $this->db->select('*');
        $this->db->from('customer_order');
        $this->db->where('order_id',$id);
        $this->db->set('order_status', 1);
        $this->db->update('customer_order');
    }
     public function pending_order_info($id){
      
       $this->db->select('*');
        $this->db->from('customer_order');
        $this->db->where('order_id',$id);
        $this->db->set('order_status', 0);
        $this->db->update('customer_order');
    }
        public function due_payment_info($id){
      
       $this->db->select('*');
        $this->db->from('payment');
        $this->db->where('payment_id',$id);
        $this->db->set('payment_status',2);
        $this->db->update('payment');
    }
        public function paid_payment_info($id){
      
       $this->db->select('*');
        $this->db->from('payment');
        $this->db->where('payment_id',$id);
        $this->db->set('payment_status',3);
        $this->db->update('payment');
    }
        public function delivered_order_info($id){
      
       $this->db->select('*');
        $this->db->from('customer_order');
        $this->db->where('order_id',$id);
        $this->db->set('order_status', 2);
        $this->db->update('customer_order');
    }
        public function payment_info_by_id($id){
      
       $this->db->select('*');
        $this->db->from('payment');
        $this->db->join('customer_order', 'payment.payment_id = customer_order.payment_id', 'left');
        $this->db->where('payment.payment_id',$id);
        $query_result=  $this->db->get();
        $result=$query_result->row();
        return $result;
    }
        public function discount_update($id,$data)
                {
      
          $this->db->select('*');
        $this->db->from('product');
        $this->db->where('product_id', $id);
        $this->db->update('product', $data);
    }
        public function all_discount_update($id,$data)
                {
      
            
            
               
          $product_id= explode("*", $id);
          
    
        foreach ($product_id as $value) {
            
//                echo '<pre>';
//        print_r($value);
//        exit();
        
             $this->db->select('*');
            $this->db->from('product');
            $this->db->where('product_id', $value);
            $this->db->update('product', $data);
        }

     
    }
        public function save_order_info()
     {
         $odata=array();
         $odata['customer_id']=$this->session->userdata('customer_id');
         $odata['shipping_id']=$this->session->userdata('shipping_id');
         $odata['payment_id']=$this->session->userdata('payment_id');
         $odata['order_date']=date("m/d/Y");  
      
         if($this->session->userdata('coupon_discount')>0){
         $odata['coupon_discount']= $this->session->userdata('coupon_discount');  
         }
         $odata['shipping_charge']= $this->session->userdata('shipping_charge');  
         $odata['order_total']=$this->cart->total();
//         $odata['order_comments']=$this->input->post('order_comments');
         
         $this->db->insert('customer_order',$odata);
         $order_id=$this->db->insert_id();
         
         
            $osdata = array(
         
            'order_id' => $order_id
          
        ); 
          
          $this->session->set_userdata($osdata);
         $contents=$this->cart->contents();
         $oddata=array();
         foreach($contents as $v_contents)
         {
             $oddata['order_id']=$order_id;
             $oddata['product_id']=$v_contents['id'];
             $oddata['product_name']=$v_contents['name'];
             $oddata['product_price']=$v_contents['price'];
             $oddata['product_sales_qty']=$v_contents['qty'];
             $this->db->insert('customer_order_details',$oddata);
         }

         $this->cart->destroy();
         
     }
      
     public function delect_order_info($id){
      
        $this->db->where('order_id', $id);
        $this->db->delete('customer_order');
    }
     public function delect_order_details_info($id){
      
        $this->db->where('order_id', $id);
        $this->db->delete('customer_order_details');
    }
     public function delect_payment_info($id){
      
        $this->db->where('payment_id', $id);
        $this->db->delete('payment');
    }
     public function delect_shipping_info($id){
      
        $this->db->where('ship_id', $id);
        $this->db->delete('shipping');
    }
   
    
    public function get_view_order($p_id){
          $this->db->select('*');
            $this->db->from('customer_order');
            $this->db->where('order_id', $p_id);
           $query_result=  $this->db->get();
        $result=$query_result->row();
        return $result;
    }
       public function vendor_request_accept_info($id){
      
       $this->db->select('*');
        $this->db->from('product');
        $this->db->where('product_id',$id);
        $this->db->set('is_vendor_product', 2);
        $this->db->set('is_product', 1);
        $this->db->update('product');
    }
       public function vendor_request_reject_info($id){
      
       $this->db->select('*');
        $this->db->from('product');
        $this->db->where('product_id',$id);
        $this->db->set('is_vendor_product', 3);
        $this->db->update('product');
    }
       public function inactive_customer_info($id){
      
       $this->db->select('*');
        $this->db->from('customer');
        $this->db->where('customer_id',$id);
        $this->db->set('is_active', 0);
        $this->db->update('customer');
    }
       public function active_customer_info($id){
      
       $this->db->select('*');
        $this->db->from('customer');
        $this->db->where('customer_id',$id);
        $this->db->set('is_active', 1);
        $this->db->update('customer');
    }
}
