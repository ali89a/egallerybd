<?php



class Home_model extends CI_Model{
    //put your code here
    function select_product_by_bigoffer_products(){
        
        $this->db->select('*');
        $this->db->from('product');
        
        $this->db->where('is_active',1);
        $this->db->order_by("discount","desc");
         $this->db->limit(1);
        $result=$this->db->get()->result();
        return $result;
    }
    function select_product_by_letest_product(){
        
        $this->db->select('*');
        $this->db->from('product');
        $this->db->where('is_latest',1);
        $this->db->where('is_active',1);
        $this->db->where('is_product',1);
        $this->db->order_by("product_id","desc");
        $result=$this->db->get()->result();
        return $result;
    }
    function get_four_product_by_cat_id($category_id){
        
        $this->db->select('*');
        $this->db->from('product');
        $this->db->where('category_id',$category_id);
        $this->db->where('is_active',1);
       $this->db->where('is_product',1);
        $result=$this->db->get()->result();
        return $result;
    }
    function select_slider_images(){
        
        $this->db->select('*');
        $this->db->from('lib_photo');
       $this->db->limit(10);
        $this->db->where('is_active',1);
        $result=$this->db->get()->result();
        return $result;
    }
    function select_product_youtube_video(){
        
        $this->db->select('*');
        $this->db->from('youtube_video');
         $this->db->order_by("youtube_video_id","desc");
//       $this->db->limit(1);
        $this->db->where('is_active',1);
        $result=$this->db->get()->row();
        return $result;
    }
    function get_product_details_by_id($id){
        
        $this->db->select('*');
        $this->db->from('product');
        $this->db->where('product_id', $id);
        $result = $this->db->get()->row();
        return $result;
    }
    function get_all_pub_category(){
        
        $this->db->select('*');
        $this->db->from('category');
        $this->db->where('is_active', 1);
        $result = $this->db->get()->result();
        return $result;
    }
    function get_sub_category_by_cat_id($id){
        
        $this->db->select('*');
        $this->db->from('sub_category');
        $this->db->where('category_id', $id);
        $this->db->where('is_active', 1);
        $result = $this->db->get()->result();
        return $result;
    }
    function get_brand_by_sub_cat_id($id){
        
        $this->db->select('*');
        $this->db->from('brand');
        $this->db->where('sub_category_id', $id);
        $this->db->where('is_active', 1);
        $result = $this->db->get()->result();
        return $result;
    }
    function get_sub_category_three_by_brand_id($id){
        
        $this->db->select('*');
        $this->db->from('sub_category_three');
        $this->db->where('brand_id', $id);
        $this->db->where('is_active', 1);
        $result = $this->db->get()->result();
        return $result;
    }
}
