<?php


class Admin_Model extends CI_Model {
    //put your code here
    public function check_admin_login_info($email,$password)
    {
        $this->db->select('*');
        $this->db->from('admin');
        $this->db->where('email',$email);
        $this->db->where('password',md5($password));
        $query_result=$this->db->get();
        $result=$query_result->row();
        return $result;
    }
       public function admin_email_check_info($email){
            $this->db->select('*');
        $this->db->from('admin');
        $this->db->where('email',$email);
        $query_result=$this->db->get();
        $result=$query_result->row();
        return $result;
    }
    function send_email($data, $templateName) {
//        echo "<pre>";
//        print_r($data);

        $this->load->library('email');
        $this->email->set_mailtype('html');
        $this->email->from($data['from_address'], $data['admin_full_name']);
        $this->email->to($data['to_address']);
        //$this->email->cc($data['cc_address']);
        $this->email->subject($data['subject']);
        $body = $this->load->view('mail_scripts/' . $templateName, $data, true);
    // echo $body;
    //    exit();
        $this->email->message($body);
       $this->email->send();
        $this->email->clear();
    }
}
