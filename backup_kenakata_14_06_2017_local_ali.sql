-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 14, 2017 at 07:38 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kenakata`
--

-- --------------------------------------------------------

--
-- Table structure for table `about_new`
--

CREATE TABLE `about_new` (
  `about_new_id` int(11) NOT NULL,
  `about_new_text` text NOT NULL,
  `status` int(11) NOT NULL,
  `inserted_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `about_new`
--

INSERT INTO `about_new` (`about_new_id`, `about_new_text`, `status`, `inserted_by`) VALUES
(1, '<h2 style="font-style:italic"><strong>iiiiiiAbout &nbsp;text of the printing and typesetting industry.</strong> Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</h2>\r\n', 1, 11);

-- --------------------------------------------------------

--
-- Table structure for table `about_us`
--

CREATE TABLE `about_us` (
  `about_us_id` int(11) NOT NULL,
  `short_des` text NOT NULL,
  `long_des` text NOT NULL,
  `is_active` int(11) NOT NULL,
  `inserted_by` int(11) NOT NULL,
  `insert_time` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `update_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `about_us`
--

INSERT INTO `about_us` (`about_us_id`, `short_des`, `long_des`, `is_active`, `inserted_by`, `insert_time`, `updated_by`, `update_time`) VALUES
(4, '<p></p><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.\r\n\r\n<br></p><p></p>', '<p>=====Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.\r\n\r\n\r\n\r\n\r\n</p>', 1, 10, '2017-03-19 06:03:05', 11, '2017-06-12 07:06:52');

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `admin_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `admin_type` enum('admin','super_admin','user') NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `admin_name`, `email`, `password`, `admin_type`, `image`) VALUES
(10, 'Mohammad Ali', 'mdali89a@gmail.com', '25f9e794323b453885f5181f1b624d0b', 'admin', 'upload_image/admin_image/admin.jpg'),
(11, 'AAdmin', 'admin@gmail.com', '75d23af433e0cea4c0e45a56dba18b30', 'admin', 'upload_image/admin_image/admin.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `ad_management`
--

CREATE TABLE `ad_management` (
  `ad_id` int(11) NOT NULL,
  `ad_title` varchar(255) NOT NULL,
  `ad_start_date` date NOT NULL,
  `ad_end_date` date NOT NULL,
  `ad_file` varchar(255) NOT NULL,
  `page_id` varchar(255) NOT NULL,
  `ad_position` int(11) NOT NULL,
  `inserted_by` int(11) NOT NULL,
  `insert_time` datetime NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `is_active` int(11) NOT NULL,
  `is_delete` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ad_management`
--

INSERT INTO `ad_management` (`ad_id`, `ad_title`, `ad_start_date`, `ad_end_date`, `ad_file`, `page_id`, `ad_position`, `inserted_by`, `insert_time`, `updated_by`, `update_time`, `is_active`, `is_delete`) VALUES
(3, 'Demos', '0000-00-00', '0000-00-00', 'upload_image/ad_image/banner-3.jpg', '', 1, 10, '2017-03-13 09:03:05', 10, '2017-03-19 06:03:26', 1, 0),
(8, 'Sharee', '0000-00-00', '0000-00-00', 'upload_image/product_image/add_image_(1).jpg', '', 2, 11, '2017-06-12 09:06:22', 11, '2017-06-13 08:06:59', 1, 0),
(9, 'ali', '0000-00-00', '0000-00-00', 'upload_image/ad_image/add_image_(3).jpg', '', 2, 11, '2017-06-14 08:06:38', NULL, NULL, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `brand`
--

CREATE TABLE `brand` (
  `brand_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `sub_category_id` int(11) NOT NULL,
  `brand_name` varchar(255) NOT NULL,
  `brand_icon` varchar(255) NOT NULL,
  `inserted_by` int(11) NOT NULL,
  `insert_time` datetime NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `is_delete` int(11) NOT NULL DEFAULT '0',
  `store_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `brand`
--

INSERT INTO `brand` (`brand_id`, `category_id`, `sub_category_id`, `brand_name`, `brand_icon`, `inserted_by`, `insert_time`, `updated_by`, `update_time`, `is_active`, `is_delete`, `store_id`) VALUES
(1, 1, 2, 'Benarosi', 'upload_image/icons/brand_icon/Capture1.PNG', 11, '2017-04-21 01:04:24', 11, '2017-04-21 05:04:30', 1, 1, 0),
(2, 2, 2, 'ali', 'upload_image/icons/brand_icon/6c3e599dbfbbfc4029063095845de219-peploading.gif', 11, '2017-04-21 04:04:34', 0, '0000-00-00 00:00:00', 1, 1, 0),
(3, 10, 3, 'Webcam', '', 11, '2017-04-21 07:04:44', 0, '0000-00-00 00:00:00', 1, 0, 0),
(4, 10, 3, 'CC/IP Camara', '', 11, '2017-04-21 07:04:14', 0, '0000-00-00 00:00:00', 1, 0, 0),
(5, 10, 3, 'Doorbell', '', 11, '2017-04-21 07:04:29', 0, '0000-00-00 00:00:00', 1, 0, 0),
(6, 10, 4, 'Anty Spy RF Signal Detector', '', 11, '2017-04-21 07:04:54', 0, '0000-00-00 00:00:00', 1, 0, 0),
(7, 10, 5, 'Pendrive', '', 11, '2017-04-21 07:04:12', 0, '0000-00-00 00:00:00', 1, 0, 0),
(8, 10, 6, 'LED LCD Projector', '', 11, '2017-04-21 07:04:31', 0, '0000-00-00 00:00:00', 1, 0, 0),
(9, 10, 7, 'Nokia', '', 11, '2017-04-21 07:04:48', 0, '0000-00-00 00:00:00', 1, 0, 0),
(10, 10, 7, 'China Mobile', '', 11, '2017-04-21 07:04:10', 0, '0000-00-00 00:00:00', 1, 0, 0),
(11, 10, 8, 'LED Television', '', 11, '2017-04-21 07:04:49', 0, '0000-00-00 00:00:00', 1, 0, 0),
(12, 11, 9, 'Mens Wallet', '', 11, '2017-04-21 07:04:45', 0, '0000-00-00 00:00:00', 1, 0, 0),
(13, 11, 10, 'Indian Sharee', '', 11, '2017-04-21 07:04:01', 0, '0000-00-00 00:00:00', 1, 0, 0),
(14, 11, 10, 'Skirt Scarf Belt', '', 11, '2017-04-21 07:04:19', 0, '0000-00-00 00:00:00', 1, 0, 0),
(15, 11, 10, 'Tangail Sharee', '', 11, '2017-04-21 07:04:43', 0, '0000-00-00 00:00:00', 1, 0, 0),
(16, 12, 12, 'Air Condition', '', 11, '2017-04-21 07:04:08', 0, '0000-00-00 00:00:00', 1, 0, 0),
(17, 12, 12, 'Television', '', 11, '2017-04-21 07:04:27', 0, '0000-00-00 00:00:00', 1, 0, 0),
(18, 12, 13, 'Coffee Maker', '', 11, '2017-04-21 07:04:45', 0, '0000-00-00 00:00:00', 1, 0, 0),
(19, 12, 13, 'Blender', '', 11, '2017-04-21 07:04:27', 0, '0000-00-00 00:00:00', 1, 0, 0),
(20, 12, 13, 'Freezer', '', 11, '2017-04-21 07:04:46', 0, '0000-00-00 00:00:00', 1, 0, 0),
(21, 12, 13, 'Cookers and Fryers', '', 11, '2017-04-21 07:04:08', 0, '0000-00-00 00:00:00', 1, 0, 0),
(22, 12, 13, 'Air-Fryer', '', 11, '2017-04-21 07:04:31', 0, '0000-00-00 00:00:00', 1, 0, 0),
(23, 12, 13, 'Washing Machine', '', 11, '2017-04-21 07:04:50', 0, '0000-00-00 00:00:00', 1, 0, 0),
(24, 12, 13, 'Rice Cooker', '', 11, '2017-04-21 07:04:09', 0, '0000-00-00 00:00:00', 1, 0, 0),
(25, 12, 13, 'Microwave Oven', '', 11, '2017-04-21 07:04:25', 0, '0000-00-00 00:00:00', 1, 0, 0),
(26, 13, 15, 'Nokia', '', 11, '2017-04-21 07:04:14', 0, '0000-00-00 00:00:00', 1, 0, 0),
(27, 13, 15, 'HTC', '', 11, '2017-04-21 07:04:33', 0, '0000-00-00 00:00:00', 1, 0, 0),
(28, 13, 15, 'Apple', '', 11, '2017-04-21 07:04:58', 0, '0000-00-00 00:00:00', 1, 0, 0),
(29, 13, 15, 'Apple', '', 11, '2017-04-21 07:04:35', 0, '0000-00-00 00:00:00', 1, 0, 0),
(30, 13, 15, 'Maximus', '', 11, '2017-04-21 07:04:04', 0, '0000-00-00 00:00:00', 1, 0, 0),
(31, 13, 15, 'Others', '', 11, '2017-04-21 07:04:26', 0, '0000-00-00 00:00:00', 1, 0, 0),
(32, 13, 16, 'Headphone/Earphone', '', 11, '2017-04-21 07:04:47', 0, '0000-00-00 00:00:00', 1, 0, 0),
(33, 13, 16, 'Speaker', '', 11, '2017-04-21 07:04:01', 0, '0000-00-00 00:00:00', 1, 0, 0),
(34, 13, 17, 'Phone Stand', '', 11, '2017-04-21 07:04:24', 0, '0000-00-00 00:00:00', 1, 0, 0),
(35, 13, 17, 'Virtual Reality Glasses', '', 11, '2017-04-21 07:04:54', 0, '0000-00-00 00:00:00', 1, 0, 0),
(36, 13, 17, 'Phone Stand(Wall Hanging)', '', 11, '2017-04-21 07:04:19', 0, '0000-00-00 00:00:00', 1, 0, 0),
(37, 13, 17, 'Power Bank', '', 11, '2017-04-21 07:04:33', 0, '0000-00-00 00:00:00', 1, 0, 0),
(38, 14, 18, 'Dell', '', 11, '2017-04-21 07:04:15', 0, '0000-00-00 00:00:00', 1, 0, 0),
(39, 14, 18, 'Asus', '', 11, '2017-04-21 07:04:39', 0, '0000-00-00 00:00:00', 1, 0, 0),
(40, 14, 18, 'Lenovo', '', 11, '2017-04-21 07:04:55', 0, '0000-00-00 00:00:00', 1, 0, 0),
(41, 14, 19, 'CPU', '', 11, '2017-04-21 07:04:13', 0, '0000-00-00 00:00:00', 1, 0, 0),
(42, 14, 19, 'Monitor', '', 11, '2017-04-21 07:04:21', 0, '0000-00-00 00:00:00', 1, 0, 0),
(43, 14, 19, 'Complete PC', '', 11, '2017-04-21 07:04:43', 0, '0000-00-00 00:00:00', 1, 0, 0),
(44, 14, 20, 'Mouse', '', 11, '2017-04-21 08:04:11', 0, '0000-00-00 00:00:00', 1, 0, 0),
(45, 14, 20, 'Keyboard', '', 11, '2017-04-21 08:04:29', 0, '0000-00-00 00:00:00', 1, 0, 0),
(46, 14, 20, 'Mouse & Keyboard Combo', '', 11, '2017-04-21 08:04:59', 0, '0000-00-00 00:00:00', 1, 0, 0),
(47, 14, 21, 'Hard Disk', '', 11, '2017-04-21 08:04:20', 0, '0000-00-00 00:00:00', 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `career`
--

CREATE TABLE `career` (
  `career_id` int(11) NOT NULL,
  `career_text` text NOT NULL,
  `status` int(11) NOT NULL,
  `inserted_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `career`
--

INSERT INTO `career` (`career_id`, `career_text`, `status`, `inserted_by`) VALUES
(1, '<h2 style="font-style:italic"><strong>Adummy text of the printing and typesetting industry.</strong> Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</h2>\r\n', 1, 11);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(255) NOT NULL,
  `category_icon` varchar(255) NOT NULL,
  `inserted_by` int(11) NOT NULL,
  `insert_time` datetime NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=active,0=inactive',
  `is_delete` int(11) NOT NULL DEFAULT '0',
  `store_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`category_id`, `category_name`, `category_icon`, `inserted_by`, `insert_time`, `updated_by`, `update_time`, `is_active`, `is_delete`, `store_id`) VALUES
(10, 'Electronics', 'upload_image/icons/category_icon/ec1.jpg', 11, '2017-04-21 07:04:07', 11, '2017-05-22 05:05:35', 1, 0, NULL),
(11, 'Fashion & Apparel', 'upload_image/icons/category_icon/fc.jpg', 11, '2017-04-21 07:04:42', 11, '2017-05-22 05:05:45', 1, 0, NULL),
(12, 'Home & Kitchen Appliance', 'upload_image/icons/category_icon/ho.jpg', 11, '2017-04-21 07:04:04', 11, '2017-05-22 06:05:14', 1, 0, NULL),
(13, 'Phone & Phone Accessories', 'upload_image/icons/category_icon/mb.png', 11, '2017-04-21 07:04:31', 11, '2017-05-22 06:05:10', 1, 0, NULL),
(14, 'PC & It''s Accessories', 'upload_image/icons/category_icon/pc.jpg', 11, '2017-04-21 07:04:44', 11, '2017-05-22 05:05:52', 1, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE `contact_us` (
  `contact_us_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `is_active` int(11) NOT NULL DEFAULT '1',
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contact_us`
--

INSERT INTO `contact_us` (`contact_us_id`, `name`, `email`, `phone`, `message`, `status`, `is_active`, `date`) VALUES
(1, 'Mohammad Ali', 'mdali89a@gmail.com', '01710355789', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don''t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn''t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.', 1, 1, '2017-03-07 13:41:10'),
(2, 'Mohammad', 'mdali89b@gmail.com', '+8801710355789', 'fdbhvgfgvbcbcbcvccvcvcvcv', 1, 1, '2017-03-13 12:24:53'),
(3, 'suvfbhiu', 'sahashourav@yahoo.com', '01711057846', 'fxngtxmkm', 1, 1, '2017-03-19 05:19:23'),
(4, 'Demos', 'a@gmail.com', '01710355755', 'ldfk;dlldl;lf;dlf;dl;l', 1, 1, '2017-06-02 08:52:44'),
(5, 'kkk', 'vendor@gmail.com', '+8801710355789', 'yuyiuiuiui', 1, 1, '2017-06-02 09:00:27');

-- --------------------------------------------------------

--
-- Table structure for table `coupon`
--

CREATE TABLE `coupon` (
  `coupon_id` int(11) NOT NULL,
  `coupon_number` varchar(255) NOT NULL,
  `coupon_dis_perc` int(11) NOT NULL,
  `coupon_dis_tk` int(11) NOT NULL,
  `insert_by` int(11) NOT NULL,
  `insert_time` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `update_time` datetime NOT NULL,
  `is_active` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `coupon`
--

INSERT INTO `coupon` (`coupon_id`, `coupon_number`, `coupon_dis_perc`, `coupon_dis_tk`, `insert_by`, `insert_time`, `update_by`, `update_time`, `is_active`, `status`) VALUES
(2, '2', 0, 50, 10, '2017-03-08 05:03:11', 0, '0000-00-00 00:00:00', 0, 0),
(3, '3', 0, 100, 10, '2017-03-08 11:03:22', 0, '0000-00-00 00:00:00', 1, 0),
(5, '5', 0, 45, 10, '2017-03-18 08:03:52', 0, '0000-00-00 00:00:00', 1, 1),
(6, '5', 0, 65, 10, '2017-03-18 08:03:57', 10, '2017-03-19 06:03:41', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `customer_id` int(11) NOT NULL,
  `customer_number` int(11) NOT NULL,
  `customer_name` varchar(255) NOT NULL,
  `customer_email` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `img` varchar(255) NOT NULL,
  `blood_group` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `contact_person` varchar(255) NOT NULL,
  `inserted_by` int(11) NOT NULL,
  `insert_time` datetime NOT NULL,
  `update_by` int(11) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `is_active` int(11) NOT NULL,
  `dob` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`customer_id`, `customer_number`, `customer_name`, `customer_email`, `username`, `password`, `gender`, `img`, `blood_group`, `address`, `mobile`, `contact_person`, `inserted_by`, `insert_time`, `update_by`, `update_time`, `is_active`, `dob`) VALUES
(1, 0, ' Mohammad Ali', ' mdali89a@gmail.com', '', '202cb962ac59075b964b07152d234b70', '1', 'upload_image/product_image/10-blue-sky-backdrop.jpg', 'A+', 'Rajabazar,dhaka', ' 01710355789', '', 0, '0000-00-00 00:00:00', 1, '2017-06-13 08:06:51', 1, '1993-03-29'),
(6, 0, 'abc', 'bemolesh@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', '1', 'upload_image/product_image/always-htmlg1.png', 'O+', 'styg busya v', '01754481299', '', 0, '0000-00-00 00:00:00', 6, '2017-03-20 12:03:44', 1, '0000-00-00'),
(7, 0, 'Mohammad Ali', 'serviceprovider@friendsit.com', '', '202cb962ac59075b964b07152d234b70', '', '', 'A-', 'Dhaka', '01710355789', '', 0, '0000-00-00 00:00:00', NULL, NULL, 1, '1991-12-12');

-- --------------------------------------------------------

--
-- Table structure for table `customer_order`
--

CREATE TABLE `customer_order` (
  `order_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `shipping_id` int(11) NOT NULL,
  `payment_id` int(11) NOT NULL,
  `order_total` float NOT NULL,
  `order_advance` int(11) NOT NULL,
  `coupon_discount` int(11) NOT NULL,
  `shipping_charge` int(11) DEFAULT NULL,
  `order_status` varchar(255) NOT NULL DEFAULT '0' COMMENT '0=Panding,1=Confirmed,2=Delivered',
  `order_cancle_req` int(11) NOT NULL DEFAULT '0',
  `order_date` varchar(500) NOT NULL,
  `order_return_text` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customer_order`
--

INSERT INTO `customer_order` (`order_id`, `customer_id`, `shipping_id`, `payment_id`, `order_total`, `order_advance`, `coupon_discount`, `shipping_charge`, `order_status`, `order_cancle_req`, `order_date`, `order_return_text`) VALUES
(6, 1, 6, 6, 2299, 0, 0, NULL, '0', 0, '05/11/2017', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.'),
(8, 1, 8, 8, 2748, 0, 45, NULL, '0', 0, '05/24/2017', ''),
(9, 1, 9, 9, 4599, 100, 45, NULL, '1', 0, '05/24/2017', ''),
(10, 1, 10, 10, 1196, 120, 45, NULL, '2', 1, '05/25/2017', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.'),
(12, 1, 12, 12, 4300, 0, 45, NULL, '0', 0, '06/12/2017', 'fgdfgdfgdf');

-- --------------------------------------------------------

--
-- Table structure for table `customer_order_details`
--

CREATE TABLE `customer_order_details` (
  `order_details_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_price` float NOT NULL,
  `product_sales_qty` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customer_order_details`
--

INSERT INTO `customer_order_details` (`order_details_id`, `order_id`, `product_id`, `product_name`, `product_price`, `product_sales_qty`) VALUES
(6, 3, 9, 'aaaaaaaa', 1000, 1),
(7, 4, 7, 'Nokia 216 DS', 2000, 1),
(8, 5, 9, 'aaaaaaaa', 1000, 1),
(9, 6, 7, 'Nokia 216 DS', 2000, 1),
(10, 6, 5, 'T-Shirt', 299, 1),
(12, 8, 5, 'T-Shirt', 299, 1),
(13, 8, 3, 'Nokia 130 Dual SIM', 2150, 1),
(14, 8, 8, 'AC 233333', 299, 1),
(15, 9, 5, 'T-Shirt', 299, 1),
(16, 9, 3, 'Nokia 130 Dual SIM', 2150, 2),
(17, 10, 5, 'T-Shirt', 299, 4),
(20, 12, 3, 'Nokia 130 Dual SIM', 2150, 2);

-- --------------------------------------------------------

--
-- Table structure for table `discount`
--

CREATE TABLE `discount` (
  `discount_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `sales_price` float NOT NULL,
  `buy_price` float NOT NULL,
  `discount` float NOT NULL,
  `discount_percent` float NOT NULL,
  `current_sale_price` float NOT NULL,
  `product_status` int(11) NOT NULL COMMENT '1=Real,2=Clearing',
  `inserted_by` int(11) NOT NULL,
  `insert_time` datetime NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `is_active` int(11) NOT NULL,
  `is_delete` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `discount`
--

INSERT INTO `discount` (`discount_id`, `product_id`, `sales_price`, `buy_price`, `discount`, `discount_percent`, `current_sale_price`, `product_status`, `inserted_by`, `insert_time`, `updated_by`, `update_time`, `is_active`, `is_delete`) VALUES
(1, 1, 250, 100, 20, 8, 230, 2, 10, '2017-01-06 10:01:20', NULL, NULL, 1, 0),
(2, 1, 250, 200, 20, 8, 230, 2, 10, '2017-01-06 10:01:58', NULL, NULL, 1, 1),
(3, 1, 250, 100, 20, 8, 230, 2, 10, '2017-01-06 10:01:53', NULL, NULL, 0, 0),
(4, 1, 250, 200, 20, 8, 230, 1, 10, '2017-01-09 04:01:58', NULL, NULL, 1, 0),
(5, 1, 250, 200, 40, 16, 210, 1, 10, '2017-01-09 05:01:03', 10, '2017-01-09 05:01:31', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `lib_photo`
--

CREATE TABLE `lib_photo` (
  `photo_id` int(11) NOT NULL,
  `mst_id` int(11) NOT NULL COMMENT 'mst_id means product_id',
  `title` varchar(255) NOT NULL,
  `file_url` varchar(500) NOT NULL,
  `photo_type` int(11) NOT NULL COMMENT '1=slider_image,2=product_image',
  `inserted_by` int(11) NOT NULL,
  `insert_time` datetime NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL COMMENT '1=active,0=inactive',
  `is_delete` tinyint(1) NOT NULL COMMENT '1=delete,0=no delete',
  `store_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `lib_photo`
--

INSERT INTO `lib_photo` (`photo_id`, `mst_id`, `title`, `file_url`, `photo_type`, `inserted_by`, `insert_time`, `updated_by`, `update_time`, `is_active`, `is_delete`, `store_id`) VALUES
(71, 0, 'Ali', 'upload_image/slider_image/01.jpg', 1, 11, '2017-04-25 06:04:27', NULL, NULL, 1, 0, 0),
(43, 0, 'Slider', 'upload_image/product_image/1002.jpg', 1, 10, '2017-02-25 10:02:22', 11, '2017-04-25 06:04:25', 1, 0, 0),
(41, 0, 'Slider', 'upload_image/product_image/02.jpg', 1, 10, '2017-02-25 10:02:22', 11, '2017-04-25 06:04:57', 1, 0, 0),
(42, 0, 'Slider', 'upload_image/product_image/0188.jpg', 1, 10, '2017-02-25 10:02:22', 11, '2017-04-25 06:04:42', 1, 0, 0),
(72, 0, 'Ali', 'upload_image/slider_image/02.jpg', 1, 11, '2017-04-25 06:04:27', NULL, NULL, 1, 0, 0),
(73, 0, 'Ali', 'upload_image/slider_image/03.jpg', 1, 11, '2017-04-25 06:04:27', NULL, NULL, 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `newsletter`
--

CREATE TABLE `newsletter` (
  `newsletter_id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `insert_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `newsletter`
--

INSERT INTO `newsletter` (`newsletter_id`, `email`, `insert_time`) VALUES
(1, 'mdali89a@gmail.com', '2017-03-13 11:20:04'),
(2, 'admin@gmail.com', '2017-03-13 11:22:52'),
(3, 'mdali89b@gmail.com', '2017-03-13 11:23:31'),
(4, 'admin@gmail.com', '2017-03-13 11:24:24'),
(5, 'mdali89a@gmail.com', '2017-03-13 11:31:20'),
(6, 'mdali89a@gmail.com', '2017-03-13 11:31:23'),
(7, 'mdali89a@gmail.com', '2017-03-13 11:31:49'),
(8, 'mdali89a@gmail.com', '2017-03-13 11:31:53'),
(9, 'mdali89a@gmail.com', '2017-03-13 11:32:09'),
(10, 'mdali89a@gmail.com', '2017-03-13 11:32:38'),
(11, 'mdali89a@gmail.com', '2017-03-13 11:37:07'),
(12, 'mdali89a@gmail.com', '2017-03-13 11:37:15'),
(13, 'mdali89a@gmail.com', '2017-03-13 11:37:18'),
(14, 'mdali89a@gmail.com', '2017-03-13 11:37:31'),
(15, 'mdali89a@gmail.com', '2017-03-13 11:37:31'),
(16, 'mdali89a@gmail.com', '2017-03-13 11:38:28'),
(17, 'mdali89a@gmail.com', '2017-03-13 11:40:12'),
(18, 'mdali89a@gmail.com', '2017-03-13 11:41:52'),
(19, 'mdali89a@gmail.com', '2017-03-13 11:41:53'),
(20, 'mdali89a@gmail.com', '2017-03-13 11:43:03'),
(21, 'mdali89a@gmail.com', '2017-03-13 11:44:19'),
(22, 'mdali89a@gmail.com', '2017-03-13 11:44:36'),
(23, 'mdali89a@gmail.com', '2017-03-13 11:44:39'),
(24, 'mdali89b@gmail.com', '2017-03-13 11:44:46'),
(25, 'mdali89b@gmail.com', '2017-03-13 11:44:47'),
(26, 'mdali89b@gmail.com', '2017-03-13 11:44:48'),
(27, 'mdali89b@gmail.com', '2017-03-13 11:44:49'),
(28, 'mdali89b@gmail.com', '2017-03-13 11:44:49'),
(29, 'mdali89b@gmail.com', '2017-03-13 11:44:49'),
(30, 'mdali89b@gmail.com', '2017-03-13 11:44:49'),
(31, 'mdali89b@gmail.com', '2017-03-13 11:44:49'),
(32, 'mdali89b@gmail.com', '2017-03-13 11:44:50'),
(33, 'mdali89b@gmail.com', '2017-03-13 11:44:50'),
(34, 'mdali89b@gmail.com', '2017-03-13 11:44:56'),
(35, 'mdali89a@gmail.com', '2017-03-13 11:47:09'),
(36, 'mdali89a@gmail.com', '2017-03-13 11:47:15'),
(37, 'mdali89a@gmail.com', '2017-03-13 11:47:25'),
(38, 'admin@gmail.com', '2017-03-13 11:49:29'),
(40, 'mdali89a@gmail.com', '2017-03-13 11:49:58'),
(42, 'admin@gmail.com', '2017-03-13 11:50:01'),
(43, 'admin@gmail.com', '2017-03-13 11:50:03'),
(44, 'mdali89a@gmail.com', '2017-03-13 11:50:11'),
(49, 'mdali89a@gmail.com', '2017-03-13 11:50:19');

-- --------------------------------------------------------

--
-- Table structure for table `other_business`
--

CREATE TABLE `other_business` (
  `other_business_id` int(11) NOT NULL,
  `other_business_text` text NOT NULL,
  `status` int(11) NOT NULL,
  `inserted_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `other_business`
--

INSERT INTO `other_business` (`other_business_id`, `other_business_text`, `status`, `inserted_by`) VALUES
(1, '<h1>Qother_business update_term_condition<strong><span style="font-size:36px">return_policy_text </span>Adummy text of the printing and typesetting industry.</strong> Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</h1>\r\n', 1, 11);

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `payment_id` int(11) NOT NULL,
  `payment_type` int(11) NOT NULL COMMENT '1=Cash On Delivery,2=Rocket,3=Bkash',
  `trx_id` varchar(255) DEFAULT NULL,
  `payment_status` varchar(255) NOT NULL DEFAULT '1' COMMENT '1=Pending,2=Due,3=Paid',
  `payment_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `payment`
--

INSERT INTO `payment` (`payment_id`, `payment_type`, `trx_id`, `payment_status`, `payment_date`) VALUES
(3, 3, '567', '1', '2017-05-18 19:39:33'),
(4, 3, '12133', '1', '2017-05-19 06:29:06'),
(5, 1, NULL, '1', '2017-05-19 06:42:55'),
(6, 1, NULL, '1', '2017-05-21 19:02:29'),
(7, 1, NULL, '1', '2017-05-21 19:04:42'),
(8, 1, NULL, '3', '2017-05-24 13:53:09'),
(9, 1, NULL, '1', '2017-05-24 20:38:13'),
(10, 2, '123456', '3', '2017-05-25 18:43:54'),
(11, 2, '1234567890', '1', '2017-05-28 19:53:23'),
(12, 1, NULL, '1', '2017-06-12 12:35:02');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `product_id` int(11) NOT NULL,
  `product_code` varchar(50) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `top_brand_id` int(11) NOT NULL DEFAULT '0',
  `category_id` int(11) NOT NULL,
  `sub_category_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_short_des` text,
  `product_long_des` text NOT NULL,
  `product_img_master` varchar(500) NOT NULL,
  `product_img` varchar(500) DEFAULT NULL,
  `discount` float DEFAULT NULL,
  `bdt_discount` float DEFAULT NULL,
  `product_price` float NOT NULL,
  `current_sale_price` float NOT NULL,
  `current_quantity` int(11) NOT NULL,
  `is_latest` int(11) NOT NULL DEFAULT '0' COMMENT '0=inactive,1=active',
  `inserted_by` int(11) DEFAULT NULL,
  `insert_time` datetime NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '0',
  `vendor_id` int(11) DEFAULT NULL,
  `vendor_store_id` int(11) DEFAULT NULL,
  `is_vendor_product` enum('1','2','3','0') NOT NULL COMMENT '1=pending,2=accepted,3=rejected,0=No Vendor Product'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`product_id`, `product_code`, `brand_id`, `top_brand_id`, `category_id`, `sub_category_id`, `product_name`, `product_short_des`, `product_long_des`, `product_img_master`, `product_img`, `discount`, `bdt_discount`, `product_price`, `current_sale_price`, `current_quantity`, `is_latest`, `inserted_by`, `insert_time`, `updated_by`, `update_time`, `is_active`, `vendor_id`, `vendor_store_id`, `is_vendor_product`) VALUES
(1, 'n111000', 9, 4, 10, 7, 'N70', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n', 'upload_image/product_image/master/EG-014.jpg', 'upload_image/product_image/other/EG-02.jpg,upload_image/product_image/other/EG-03.jpg,upload_image/product_image/other/EG-04.jpg', 10, 5000, 50000, 45000, 0, 1, 11, '2017-06-13 11:06:02', NULL, NULL, 1, NULL, 1, '0'),
(2, '4500', 10, 8, 10, 7, 'N72', '<p>yyituyhuiyuiyiyiuty7rtuytuyuytg<strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged</p>\r\n', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged<strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged</p>\r\n', 'upload_image/product_image/master/EG-031.jpg', NULL, 10, 507, 5070, 4563, 0, 0, NULL, '2017-06-14 05:06:27', NULL, NULL, 0, 4, 2, '1');

-- --------------------------------------------------------

--
-- Table structure for table `return_policy`
--

CREATE TABLE `return_policy` (
  `return_policy_id` int(11) NOT NULL,
  `return_policy_text` text NOT NULL,
  `status` int(11) NOT NULL,
  `inserted_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `return_policy`
--

INSERT INTO `return_policy` (`return_policy_id`, `return_policy_text`, `status`, `inserted_by`) VALUES
(1, '<h1 style="font-style:italic"><strong><span style="font-size:36px">return_policy_text </span>Adummy text of the printing and typesetting industry.</strong> Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</h1>\r\n', 1, 11);

-- --------------------------------------------------------

--
-- Table structure for table `review`
--

CREATE TABLE `review` (
  `review_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `review` text NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `review`
--

INSERT INTO `review` (`review_id`, `name`, `email`, `review`, `product_id`, `product_name`, `date`) VALUES
(1, 'Mohammad Ali', 'vendor@gmail.com', '-------------------------', 5, 'T-Shirt', '2017-05-27 01:05:03'),
(2, 'Banarashi', 'serviceprovider@friendsit.com', 'aaaaaaaaaaaaaaaaaaaaaa', 5, 'T-Shirt', '2017-05-27 01:05:32');

-- --------------------------------------------------------

--
-- Table structure for table `shipping`
--

CREATE TABLE `shipping` (
  `ship_id` int(11) NOT NULL,
  `ship_name` varchar(255) NOT NULL,
  `ship_email` varchar(255) NOT NULL,
  `ship_phone` varchar(255) NOT NULL,
  `ship_alt_phone` varchar(255) NOT NULL,
  `ship_city` varchar(255) NOT NULL,
  `ship_zip` varchar(255) NOT NULL,
  `ship_address` text NOT NULL,
  `shipping_area_id` int(11) NOT NULL,
  `country` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `shipping`
--

INSERT INTO `shipping` (`ship_id`, `ship_name`, `ship_email`, `ship_phone`, `ship_alt_phone`, `ship_city`, `ship_zip`, `ship_address`, `shipping_area_id`, `country`) VALUES
(3, 'Mohammad Ali', 'mdali89a@gmail.com', '01710355789', '', 'Mohammad Ali', '', 'Rajabazar,dhaka', 0, 'Bangladesh'),
(4, 'Mohammad Ali', 'mdali89a@gmail.com', '01710355789', '', 'Mohammad Ali', '', 'Rajabazar,dhaka', 0, 'Bangladesh'),
(5, 'Mohammad Ali', 'mdali89a@gmail.com', '01710355789', '', 'Mohammad Ali', '', 'Rajabazar,dhaka', 1, 'Bangladesh'),
(6, 'Mohammad Ali', 'mdali89a@gmail.com', '01710355789', '', 'Mohammad Ali', '', 'Rajabazar,dhaka', 1, 'Bangladesh'),
(7, 'Mohammad Ali', 'mdali89a@gmail.com', '01710355789', '', 'Mohammad Ali', '', 'Rajabazar,dhaka', 0, 'Bangladesh'),
(8, 'Mohammad Ali', 'mdali89a@gmail.com', '01710355789', '', 'Mohammad Ali', '123', 'Rajabazar,dhaka', 1, 'Bangladesh'),
(9, 'Mohammad Ali', 'mdali89a@gmail.com', '01710355789', '', 'Mohammad Ali', '', 'Rajabazar,dhaka', 1, 'Bangladesh'),
(10, 'Mohammad Ali', 'mdali89a@gmail.com', '01710355789', '', 'Mohammad Ali', '', 'Rajabazar,dhaka', 1, 'Bangladesh'),
(11, 'Mohammad Ali', 'mdali89a@gmail.com', '01710355789', '', 'Mohammad Ali', '1207', 'Rajabazar,dhaka', 1, 'Bangladesh'),
(12, 'Mohammad Ali', 'mdali89a@gmail.com', '01710355789', '', 'Mohammad Ali', '1220', 'Rajabazar,dhaka', 3, 'Bangladesh');

-- --------------------------------------------------------

--
-- Table structure for table `shipping_area`
--

CREATE TABLE `shipping_area` (
  `shipping_area_id` int(11) NOT NULL,
  `shipping_area` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '1=Active,2=Inactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `shipping_area`
--

INSERT INTO `shipping_area` (`shipping_area_id`, `shipping_area`, `status`) VALUES
(1, 'Dhaka', 1),
(3, 'Comilla', 1);

-- --------------------------------------------------------

--
-- Table structure for table `shipping_cost`
--

CREATE TABLE `shipping_cost` (
  `shipping_cost_id` int(11) NOT NULL,
  `shipping_area_id` int(11) NOT NULL,
  `ship_cost_amt` int(11) NOT NULL,
  `insert_by` int(11) NOT NULL,
  `insert_time` datetime NOT NULL,
  `is_active` int(11) NOT NULL,
  `update_by` int(11) NOT NULL,
  `update_time` datetime NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `shipping_cost`
--

INSERT INTO `shipping_cost` (`shipping_cost_id`, `shipping_area_id`, `ship_cost_amt`, `insert_by`, `insert_time`, `is_active`, `update_by`, `update_time`, `status`) VALUES
(18, 3, 100, 11, '2017-06-06 10:06:48', 1, 11, '2017-06-12 12:06:26', 1),
(19, 1, 50, 11, '2017-06-12 12:06:10', 1, 0, '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sub_category`
--

CREATE TABLE `sub_category` (
  `sub_category_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `sub_cat_name` varchar(255) NOT NULL,
  `sub_cat_icon` varchar(255) NOT NULL,
  `inserted_by` int(11) NOT NULL,
  `insert_time` datetime NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `is_delete` int(11) NOT NULL DEFAULT '0',
  `store_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sub_category`
--

INSERT INTO `sub_category` (`sub_category_id`, `category_id`, `sub_cat_name`, `sub_cat_icon`, `inserted_by`, `insert_time`, `updated_by`, `update_time`, `is_active`, `is_delete`, `store_id`) VALUES
(1, 3, 'aki2', 'upload_image/icons/sub_cat_icon/6c3e599dbfbbfc4029063095845de219-peploading.gif', 11, '2017-04-21 12:04:39', 11, '2017-04-21 01:04:25', 1, 1, 0),
(2, 2, 'Tat Saree', 'upload_image/icons/sub_cat_icon/Capture.PNG', 11, '2017-04-21 01:04:54', 0, '0000-00-00 00:00:00', 1, 1, 0),
(3, 10, 'Camera', '', 11, '2017-04-21 07:04:00', 0, '0000-00-00 00:00:00', 1, 0, 0),
(4, 10, 'Anty Spy Item', '', 11, '2017-04-21 07:04:20', 0, '0000-00-00 00:00:00', 1, 0, 0),
(5, 10, 'Memory Device', '', 11, '2017-04-21 07:04:51', 0, '0000-00-00 00:00:00', 1, 0, 0),
(6, 10, 'Projector', '', 11, '2017-04-21 07:04:32', 0, '0000-00-00 00:00:00', 1, 0, 0),
(7, 10, 'Mobile', '', 11, '2017-04-21 07:04:49', 0, '0000-00-00 00:00:00', 1, 0, 0),
(8, 10, 'Home Theaters', '', 11, '2017-04-21 07:04:16', 0, '0000-00-00 00:00:00', 1, 0, 0),
(9, 11, 'Male Fashion', '', 11, '2017-04-21 07:04:45', 0, '0000-00-00 00:00:00', 1, 0, 0),
(10, 11, 'Female Fashion', '', 11, '2017-04-21 07:04:13', 0, '0000-00-00 00:00:00', 1, 0, 0),
(11, 11, 'Kids Fashion', '', 11, '2017-04-21 07:04:30', 0, '0000-00-00 00:00:00', 1, 0, 0),
(12, 12, 'Home Appliance', '', 11, '2017-04-21 07:04:40', 0, '0000-00-00 00:00:00', 1, 0, 0),
(13, 12, 'Kitchen Appliance', '', 11, '2017-04-21 07:04:06', 0, '0000-00-00 00:00:00', 1, 0, 0),
(14, 12, 'Bathroom Appliance', '', 11, '2017-04-21 07:04:25', 0, '0000-00-00 00:00:00', 1, 0, 0),
(15, 13, 'Cell Phone', '', 11, '2017-04-21 07:04:06', 0, '0000-00-00 00:00:00', 1, 0, 0),
(16, 13, 'Media Player', '', 11, '2017-04-21 07:04:23', 0, '0000-00-00 00:00:00', 1, 0, 0),
(17, 13, 'Accessories', '', 11, '2017-04-21 07:04:37', 0, '0000-00-00 00:00:00', 1, 0, 0),
(18, 14, 'Laptop/Notebook', '', 11, '2017-04-21 07:04:43', 0, '0000-00-00 00:00:00', 1, 0, 0),
(19, 14, 'Desktop Computer', '', 11, '2017-04-21 07:04:07', 0, '0000-00-00 00:00:00', 1, 0, 0),
(20, 14, 'Accessories', '', 11, '2017-04-21 07:04:24', 0, '0000-00-00 00:00:00', 1, 0, 0),
(21, 14, 'Memory Device', '', 11, '2017-04-21 07:04:40', 0, '0000-00-00 00:00:00', 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `term_condition`
--

CREATE TABLE `term_condition` (
  `term_condition_id` int(11) NOT NULL,
  `term_condition_text` text NOT NULL,
  `status` int(11) NOT NULL,
  `inserted_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `term_condition`
--

INSERT INTO `term_condition` (`term_condition_id`, `term_condition_text`, `status`, `inserted_by`) VALUES
(1, '<h1>update_term_condition<strong><span style="font-size:36px">return_policy_text </span>Adummy text of the printing and typesetting industry.</strong> Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</h1>\r\n', 1, 11);

-- --------------------------------------------------------

--
-- Table structure for table `testimonial`
--

CREATE TABLE `testimonial` (
  `testimonial_id` int(11) NOT NULL,
  `name` varchar(500) NOT NULL,
  `massage` text NOT NULL,
  `image` varchar(500) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `top_brand`
--

CREATE TABLE `top_brand` (
  `top_brand_id` int(11) NOT NULL,
  `top_brand_name` varchar(255) NOT NULL,
  `top_brand_email` varchar(255) NOT NULL,
  `top_brand_contact` varchar(500) NOT NULL,
  `top_brand_address` text NOT NULL,
  `top_brand_icon` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `top_brand`
--

INSERT INTO `top_brand` (`top_brand_id`, `top_brand_name`, `top_brand_email`, `top_brand_contact`, `top_brand_address`, `top_brand_icon`) VALUES
(4, 'aaaa', 'llllllllll', '01710234567', 'rrrrrrrrrrrrrrrrrrrrr', 'uploads/vendor_icons/qqqqqqq.png'),
(6, 'fghjjhj', 'fgdhfgfghjgf', 'gfhfghgfhgh', 'gfhgfhghgfhgh', 'uploads/vendor_icons/fghjjhj.png'),
(7, 'aa', 'aa@gmail.com', '017103578899', 'Dhaka', 'uploads/vendor_icons/aa.png'),
(8, 'eeeee', 'zz@gmail.com', '01710355785', 'hghhhhhhh', 'uploads/vendor_icons/eeeee.png'),
(9, 'tyty', 'tytyt', 'tytytytyt', 'ytyty', 'uploads/vendor_icons/tyty.png'),
(10, 'jtry', 'dxf', 'uyu', 'ertet', 'uploads/vendor_icons/jtry.jpg'),
(11, 'iuo', 'tyiuiut', 'ttttttyuiy', 'yi87g', 'uploads/vendor_icons/iuo.png'),
(12, 'kjsfa', 'jkldkshdks', 'kjdfj', 'jolfjl;jfljls', 'uploads/vendor_icons/kjsfa.png'),
(13, 'ytytry', 'tytrytry', 'ertreytyt', 'werqwre4ew', 'uploads/vendor_icons/ytytry.png');

-- --------------------------------------------------------

--
-- Table structure for table `user_info`
--

CREATE TABLE `user_info` (
  `user_info_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `store_id` int(11) NOT NULL,
  `photo` varchar(500) NOT NULL,
  `last_login` int(11) NOT NULL,
  `permission_page` int(11) NOT NULL,
  `inserted_by` int(11) NOT NULL,
  `insert_time` datetime NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `is_active` int(11) NOT NULL,
  `is_delete` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_info`
--

INSERT INTO `user_info` (`user_info_id`, `user_id`, `user_name`, `user_password`, `email`, `store_id`, `photo`, `last_login`, `permission_page`, `inserted_by`, `insert_time`, `updated_by`, `update_time`, `is_active`, `is_delete`) VALUES
(1, 123, 'Tomal', '12345', 'tomal@gmail.com', 3, 'upload_image/admin_image/a.jpg', 0, 0, 10, '2017-01-03 08:01:07', NULL, NULL, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `vendor`
--

CREATE TABLE `vendor` (
  `vendor_id` int(11) NOT NULL,
  `vendor_name` varchar(500) NOT NULL,
  `vendor_email` varchar(500) NOT NULL,
  `vendor_password` varchar(500) NOT NULL,
  `vendor_contact` varchar(500) DEFAULT NULL,
  `vendor_address` varchar(500) DEFAULT NULL,
  `vendor_image` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vendor`
--

INSERT INTO `vendor` (`vendor_id`, `vendor_name`, `vendor_email`, `vendor_password`, `vendor_contact`, `vendor_address`, `vendor_image`) VALUES
(2, 'Taj', 'taj1@gmail.com', '123', '01710355789', 'Dhaka', NULL),
(3, 'kholil', 'k@gmail.com', '123456789', '01710355789', 'Dhaka', NULL),
(4, 'tuhin', 't@gmail.com', '202cb962ac59075b964b07152d234b70', '01710355789', 'Dhaka', NULL),
(5, 'Taj', 'email@gmail.com', 'd41d8cd98f00b204e9800998ecf8427e', '01710355789', 'Dhaka', NULL),
(6, 'Taj', 'taj@gmail.com', 'd41d8cd98f00b204e9800998ecf8427e', '01710355789', 'Dhaka', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vendor_store`
--

CREATE TABLE `vendor_store` (
  `vendor_store_id` int(11) NOT NULL,
  `vendor_store_name` varchar(255) NOT NULL,
  `vendor_store_email` varchar(255) NOT NULL,
  `vendor_store_contact` varchar(500) NOT NULL,
  `vendor_store_address` text NOT NULL,
  `vendor_icon` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vendor_store`
--

INSERT INTO `vendor_store` (`vendor_store_id`, `vendor_store_name`, `vendor_store_email`, `vendor_store_contact`, `vendor_store_address`, `vendor_icon`) VALUES
(1, 'Alimujaman', 'a@gmail.com', '01710355789', 'Dhaka', 'uploads/vendor_icons/Alimujaman.jpg'),
(2, 'rtyrty', 'd@tre.com', '0178659989899', 'hjgjgkk', 'uploads/vendor_icons/rtyrty.png');

-- --------------------------------------------------------

--
-- Table structure for table `visitor_count`
--

CREATE TABLE `visitor_count` (
  `visitor_count_id` int(11) NOT NULL,
  `ip_1` varchar(255) NOT NULL,
  `ip_2` varchar(255) NOT NULL,
  `ip_3` varchar(255) NOT NULL,
  `last_visit_date` datetime NOT NULL,
  `visited_url` text NOT NULL,
  `total_visit` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `visitor_count`
--

INSERT INTO `visitor_count` (`visitor_count_id`, `ip_1`, `ip_2`, `ip_3`, `last_visit_date`, `visited_url`, `total_visit`) VALUES
(104, '207.46.13.123', '207.46.13.123', '207.46.13.123', '2017-02-24 02:02:44', '/', 1),
(105, '113.11.61.56', '113.11.61.56', '113.11.61.56', '2017-02-24 04:02:04', '/', 26),
(106, '103.225.231.42', '103.225.231.42', '103.225.231.42', '2017-05-01 01:05:44', '/', 430),
(107, '103.225.231.42', '103.225.231.42', '103.225.231.42', '2017-03-31 10:03:53', '/Welcome.aspx', 2),
(108, '202.134.13.133', '202.134.13.133', '202.134.13.133', '2017-02-24 07:02:42', '/', 2),
(109, '66.249.65.195', '66.249.65.195', '66.249.65.195', '2017-05-23 10:05:36', '/', 19),
(110, '103.210.17.79', '103.210.17.79', '103.210.17.79', '2017-02-26 11:02:11', '/', 139),
(111, '104.130.124.209', '104.130.124.209', '104.130.124.209', '2017-02-24 08:02:32', '/', 2),
(112, '95.211.138.129', '95.211.138.129', '95.211.138.129', '2017-03-29 10:03:22', '/', 2),
(113, '103.210.17.79', '103.210.17.79', '103.210.17.79', '2017-02-24 06:02:28', '/Welcome.aspx', 3),
(114, '103.231.162.174', '103.231.162.174', '103.231.162.174', '2017-03-27 09:03:11', '/', 71),
(115, '66.249.65.254', '66.249.65.254', '66.249.65.254', '2017-05-24 03:05:21', '/', 25),
(116, '199.58.164.116', '199.58.164.116', '199.58.164.116', '2017-02-24 05:02:28', '/', 1),
(117, '103.230.104.6', '103.230.104.6', '103.230.104.6', '2017-02-24 09:02:14', '/', 2),
(118, '103.230.106.7', '103.230.106.7', '103.230.106.7', '2017-02-24 09:02:33', '/', 2),
(119, '119.147.146.60', '119.147.146.60', '119.147.146.60', '2017-02-24 09:02:32', '/', 1),
(120, '103.230.106.7', '103.230.106.7', '103.230.106.7', '2017-02-24 09:02:33', '/?keywords=mouse', 2),
(121, '202.134.11.135', '202.134.11.135', '202.134.11.135', '2017-03-01 10:03:11', '/', 10),
(122, '113.11.61.44', '113.11.61.44', '113.11.61.44', '2017-02-25 12:02:46', '/', 36),
(123, '103.10.55.84', '103.10.55.84', '103.10.55.84', '2017-03-18 11:03:58', '/', 22),
(124, '103.73.106.251', '103.73.106.251', '103.73.106.251', '2017-02-25 07:02:54', '/', 6),
(125, '107.167.107.249', '107.167.107.249', '107.167.107.249', '2017-02-25 02:02:43', '/', 1),
(126, '66.249.65.251', '66.249.65.251', '66.249.65.251', '2017-05-23 09:05:45', '/', 21),
(127, '180.76.15.29', '180.76.15.29', '180.76.15.29', '2017-02-25 04:02:39', '/', 1),
(128, '8.37.235.253', '8.37.235.253', '8.37.235.253', '2017-02-25 06:02:13', '/', 1),
(129, '66.102.7.192', '66.102.7.192', '66.102.7.192', '2017-02-25 06:02:27', '/', 1),
(130, '66.102.6.134', '66.102.6.134', '66.102.6.134', '2017-04-09 11:04:21', '/', 2),
(131, '66.102.6.138', '66.102.6.138', '66.102.6.138', '2017-02-25 06:02:27', '/', 1),
(132, '119.30.47.143', '119.30.47.143', '119.30.47.143', '2017-02-25 06:02:28', '/', 2),
(133, '202.134.10.136', '202.134.10.136', '202.134.10.136', '2017-02-25 07:02:09', '/', 3),
(134, '103.60.175.35', '103.60.175.35', '103.60.175.35', '2017-02-25 07:02:27', '/', 1),
(135, '59.152.98.165', '59.152.98.165', '59.152.98.165', '2017-02-25 07:02:29', '/', 2),
(136, '202.134.11.158', '202.134.11.158', '202.134.11.158', '2017-02-25 07:02:39', '/', 6),
(137, '107.167.105.196', '107.167.105.196', '107.167.105.196', '2017-02-25 07:02:49', '/', 2),
(138, '203.112.220.201', '203.112.220.201', '203.112.220.201', '2017-04-02 01:04:00', '/', 12),
(139, '103.230.6.250', '103.230.6.250', '103.230.6.250', '2017-02-25 08:02:32', '/', 2),
(140, '103.55.145.162', '103.55.145.162', '103.55.145.162', '2017-02-25 09:02:56', '/', 1),
(141, '103.14.26.94', '103.14.26.94', '103.14.26.94', '2017-02-25 10:02:11', '/', 2),
(142, '150.70.173.9', '150.70.173.9', '150.70.173.9', '2017-02-25 10:02:28', '/', 1),
(143, '150.70.173.43', '150.70.173.43', '150.70.173.43', '2017-02-25 10:02:28', '/', 1),
(144, '150.70.173.21', '150.70.173.21', '150.70.173.21', '2017-03-17 01:03:28', '/', 3),
(145, '64.233.173.17', '64.233.173.17', '64.233.173.17', '2017-04-29 10:04:10', '/', 3),
(146, '64.233.173.7', '64.233.173.7', '64.233.173.7', '2017-04-07 03:04:00', '/', 13),
(147, '64.233.173.15', '64.233.173.15', '64.233.173.15', '2017-04-07 03:04:03', '/', 3),
(148, '64.233.173.3', '64.233.173.3', '64.233.173.3', '2017-04-08 09:04:35', '/', 12),
(149, '64.233.173.16', '64.233.173.16', '64.233.173.16', '2017-04-08 09:04:35', '/', 5),
(150, '64.233.173.5', '64.233.173.5', '64.233.173.5', '2017-04-07 03:04:03', '/', 11),
(151, '103.198.136.45', '103.198.136.45', '103.198.136.45', '2017-02-25 12:02:40', '/', 1),
(152, '168.235.201.218', '168.235.201.218', '168.235.201.218', '2017-02-25 01:02:32', '/', 1),
(153, '180.211.197.20', '180.211.197.20', '180.211.197.20', '2017-02-25 01:02:36', '/', 3),
(154, '180.76.15.32', '180.76.15.32', '180.76.15.32', '2017-05-03 05:05:39', '/', 3),
(155, '202.134.10.138', '202.134.10.138', '202.134.10.138', '2017-02-25 09:02:14', '/', 2),
(156, '210.4.75.222', '210.4.75.222', '210.4.75.222', '2017-05-27 01:05:31', '/', 131),
(157, '202.134.11.129', '202.134.11.129', '202.134.11.129', '2017-05-22 10:05:25', '/', 4),
(158, '27.147.132.93', '27.147.132.93', '27.147.132.93', '2017-02-26 03:02:13', '/', 5),
(159, '104.223.112.67', '104.223.112.67', '104.223.112.67', '2017-02-26 05:02:20', '/', 1),
(160, '202.134.11.155', '202.134.11.155', '202.134.11.155', '2017-02-26 06:02:00', '/', 3),
(161, '117.192.128.72', '117.192.128.72', '117.192.128.72', '2017-02-26 06:02:02', '/', 1),
(162, '202.134.11.152', '202.134.11.152', '202.134.11.152', '2017-02-28 07:02:57', '/', 5),
(163, '202.134.11.130', '202.134.11.130', '202.134.11.130', '2017-02-26 07:02:53', '/', 2),
(164, '31.13.98.112', '31.13.98.112', '31.13.98.112', '2017-02-26 08:02:51', '/', 1),
(165, '54.81.174.179', '54.81.174.179', '54.81.174.179', '2017-02-26 08:02:52', '/', 1),
(166, '103.244.185.36', '103.244.185.36', '103.244.185.36', '2017-03-12 09:03:57', '/', 29),
(167, '103.54.43.254', '103.54.43.254', '103.54.43.254', '2017-02-26 09:02:25', '/', 2),
(168, '207.46.13.170', '207.46.13.170', '207.46.13.170', '2017-02-26 10:02:14', '/', 1),
(169, '107.167.108.190', '107.167.108.190', '107.167.108.190', '2017-02-26 10:02:42', '/', 2),
(170, '54.248.153.153', '54.248.153.153', '54.248.153.153', '2017-02-26 11:02:02', '/', 1),
(171, '54.248.153.153', '54.248.153.153', '54.248.153.153', '2017-02-26 11:02:02', '/', 1),
(172, '54.248.153.153', '54.248.153.153', '54.248.153.153', '2017-02-26 11:02:02', '/', 1),
(173, '54.248.153.153', '54.248.153.153', '54.248.153.153', '2017-02-26 11:02:02', '/', 1),
(174, '192.241.225.199', '192.241.225.199', '192.241.225.199', '2017-05-19 12:05:39', '/', 4),
(175, '69.63.188.103', '69.63.188.103', '69.63.188.103', '2017-02-26 11:02:17', '/', 1),
(176, '202.134.11.140', '202.134.11.140', '202.134.11.140', '2017-02-26 01:02:11', '/', 7),
(177, '107.167.106.106', '107.167.106.106', '107.167.106.106', '2017-02-26 01:02:15', '/', 3),
(178, '188.166.61.92', '188.166.61.92', '188.166.61.92', '2017-02-26 06:02:43', '/', 1),
(179, '202.134.11.159', '202.134.11.159', '202.134.11.159', '2017-02-28 12:02:00', '/', 9),
(180, '202.134.10.137', '202.134.10.137', '202.134.10.137', '2017-02-26 11:02:05', '/', 16),
(181, '146.23.203.234', '146.23.203.234', '146.23.203.234', '2017-03-26 11:03:03', '/', 6),
(182, '113.11.61.24', '113.11.61.24', '113.11.61.24', '2017-02-27 07:02:05', '/', 26),
(183, '103.246.38.196', '103.246.38.196', '103.246.38.196', '2017-04-02 05:04:48', '/', 8),
(184, '146.23.203.233', '146.23.203.233', '146.23.203.233', '2017-04-02 05:04:48', '/', 8),
(185, '107.167.105.229', '107.167.105.229', '107.167.105.229', '2017-02-27 05:02:11', '/', 2),
(186, '113.35.251.98', '113.35.251.98', '113.35.251.98', '2017-03-27 02:03:30', '/', 2),
(187, '103.225.231.42', '103.225.231.42', '103.225.231.42', '2017-02-27 06:02:14', '/?keywords=100055', 1),
(188, '103.225.231.42', '103.225.231.42', '103.225.231.42', '2017-02-27 06:02:53', '/?keywords=card', 1),
(189, '210.4.75.222', '210.4.75.222', '210.4.75.222', '2017-02-27 06:02:59', '/?keywords=saree', 1),
(190, '210.4.75.222', '210.4.75.222', '210.4.75.222', '2017-02-27 07:02:03', '/Welcome.aspx', 1),
(191, '103.244.185.35', '103.244.185.35', '103.244.185.35', '2017-03-04 07:03:53', '/', 102),
(192, '202.134.14.133', '202.134.14.133', '202.134.14.133', '2017-04-21 01:04:18', '/', 8),
(193, '72.79.57.172', '72.79.57.172', '72.79.57.172', '2017-02-27 02:02:51', '/', 1),
(194, '185.47.63.74', '185.47.63.74', '185.47.63.74', '2017-02-27 09:02:16', '/', 2),
(195, '113.11.61.10', '113.11.61.10', '113.11.61.10', '2017-02-28 08:02:23', '/', 43),
(196, '149.202.98.161', '149.202.98.161', '149.202.98.161', '2017-02-28 01:02:49', '/', 1),
(197, '178.217.187.39', '178.217.187.39', '178.217.187.39', '2017-02-28 02:02:04', '/', 1),
(198, '118.179.160.74', '118.179.160.74', '118.179.160.74', '2017-02-28 02:02:44', '/', 6),
(199, '107.21.1.8', '107.21.1.8', '107.21.1.8', '2017-05-12 01:05:46', '/', 4),
(200, '45.32.129.200', '45.32.129.200', '45.32.129.200', '2017-02-28 05:02:24', '/', 1),
(201, '199.58.164.123', '199.58.164.123', '199.58.164.123', '2017-02-28 06:02:54', '/', 1),
(202, '202.134.14.157', '202.134.14.157', '202.134.14.157', '2017-02-28 09:02:45', '/', 3),
(203, '103.217.108.144', '103.217.108.144', '103.217.108.144', '2017-03-01 09:03:54', '/', 8),
(204, '202.134.9.139', '202.134.9.139', '202.134.9.139', '2017-02-28 10:02:14', '/', 5),
(205, '45.250.20.112', '45.250.20.112', '45.250.20.112', '2017-02-28 11:02:54', '/', 2),
(206, '202.134.11.134', '202.134.11.134', '202.134.11.134', '2017-03-04 07:03:18', '/', 55),
(207, '103.197.154.26', '103.197.154.26', '103.197.154.26', '2017-03-02 09:03:26', '/', 3),
(208, '77.247.181.165', '77.247.181.165', '77.247.181.165', '2017-02-28 08:02:36', '/', 1),
(209, '212.47.253.223', '212.47.253.223', '212.47.253.223', '2017-02-28 10:02:27', '/', 1),
(210, '45.250.20.14', '45.250.20.14', '45.250.20.14', '2017-03-01 12:03:46', '/', 18),
(211, '113.11.61.20', '113.11.61.20', '113.11.61.20', '2017-03-01 03:03:42', '/', 4),
(212, '180.76.15.17', '180.76.15.17', '180.76.15.17', '2017-04-02 09:04:16', '/', 2),
(213, '64.120.30.143', '64.120.30.143', '64.120.30.143', '2017-03-01 03:03:43', '/', 1),
(214, '94.242.246.24', '94.242.246.24', '94.242.246.24', '2017-03-01 05:03:03', '/', 1),
(215, '103.230.105.9', '103.230.105.9', '103.230.105.9', '2017-03-01 08:03:43', '/', 1),
(216, '173.234.164.2', '173.234.164.2', '173.234.164.2', '2017-03-01 09:03:35', '/', 1),
(217, '203.76.110.195', '203.76.110.195', '203.76.110.195', '2017-03-01 09:03:51', '/', 1),
(218, '202.134.13.143', '202.134.13.143', '202.134.13.143', '2017-03-01 12:03:46', '/', 2),
(219, '180.76.15.146', '180.76.15.146', '180.76.15.146', '2017-05-16 10:05:36', '/', 4),
(220, '103.219.115.98', '103.219.115.98', '103.219.115.98', '2017-03-09 12:03:53', '/', 17),
(221, '103.242.21.236', '103.242.21.236', '103.242.21.236', '2017-03-02 07:03:14', '/', 1),
(222, '45.250.20.12', '45.250.20.12', '45.250.20.12', '2017-03-04 12:03:01', '/', 87),
(223, '176.9.50.43', '176.9.50.43', '176.9.50.43', '2017-04-02 03:04:16', '/', 2),
(224, '113.11.61.52', '113.11.61.52', '113.11.61.52', '2017-03-03 06:03:06', '/', 125),
(225, '202.134.14.130', '202.134.14.130', '202.134.14.130', '2017-03-03 06:03:57', '/', 1),
(226, '124.6.235.140', '124.6.235.140', '124.6.235.140', '2017-03-03 04:03:43', '/', 85),
(227, '109.83.13.11', '109.83.13.11', '109.83.13.11', '2017-03-03 11:03:34', '/', 6),
(228, '103.210.16.212', '103.210.16.212', '103.210.16.212', '2017-03-03 11:03:28', '/', 2),
(229, '176.10.99.203', '176.10.99.203', '176.10.99.203', '2017-03-03 12:03:32', '/', 1),
(230, '202.134.14.134', '202.134.14.134', '202.134.14.134', '2017-03-04 02:03:46', '/', 5),
(231, '119.30.39.227', '119.30.39.227', '119.30.39.227', '2017-03-03 10:03:12', '/', 2),
(232, '103.25.250.220', '103.25.250.220', '103.25.250.220', '2017-03-03 10:03:17', '/', 1),
(233, '103.225.231.42', '103.225.231.42', '103.225.231.42', '2017-03-03 10:03:20', '/?keywords=1000100', 1),
(234, '103.225.231.42', '103.225.231.42', '103.225.231.42', '2017-03-03 10:03:21', '/?keywords=xyz123', 1),
(235, '182.160.102.91', '182.160.102.91', '182.160.102.91', '2017-03-03 10:03:27', '/', 2),
(236, '103.230.106.5', '103.230.106.5', '103.230.106.5', '2017-03-03 11:03:06', '/', 1),
(237, '202.134.10.130', '202.134.10.130', '202.134.10.130', '2017-03-03 11:03:08', '/', 1),
(238, '113.11.61.37', '113.11.61.37', '113.11.61.37', '2017-03-04 08:03:08', '/', 174),
(239, '103.225.231.42', '103.225.231.42', '103.225.231.42', '2017-03-04 12:03:09', '/?keywords=nokia+11', 1),
(240, '103.78.224.14', '103.78.224.14', '103.78.224.14', '2017-03-04 12:03:49', '/', 1),
(241, '207.46.13.56', '207.46.13.56', '207.46.13.56', '2017-03-04 01:03:02', '/', 1),
(242, '180.76.15.160', '180.76.15.160', '180.76.15.160', '2017-03-04 01:03:24', '/', 1),
(243, '113.11.61.37', '113.11.61.37', '113.11.61.37', '2017-03-04 03:03:09', '/?keywords=nokia', 1),
(244, '113.11.61.37', '113.11.61.37', '113.11.61.37', '2017-03-04 03:03:09', '/?keywords=nokia+11', 1),
(245, '202.134.11.134', '202.134.11.134', '202.134.11.134', '2017-03-04 04:03:50', '/?keywords=nokia', 1),
(246, '202.134.11.134', '202.134.11.134', '202.134.11.134', '2017-03-04 04:03:50', '/?keywords=nokia+11', 1),
(247, '202.134.11.134', '202.134.11.134', '202.134.11.134', '2017-03-04 04:03:51', '/?keywords=nokia+130+ds', 1),
(248, '113.11.61.37', '113.11.61.37', '113.11.61.37', '2017-03-04 05:03:05', '/?keywords=mypro', 1),
(249, '113.11.61.37', '113.11.61.37', '113.11.61.37', '2017-03-04 05:03:05', '/?keywords=my+pro', 1),
(250, '175.139.243.90', '175.139.243.90', '175.139.243.90', '2017-03-04 05:03:45', '/', 1),
(251, '206.225.80.193', '206.225.80.193', '206.225.80.193', '2017-03-04 06:03:57', '/', 2),
(252, '66.249.82.114', '66.249.82.114', '66.249.82.114', '2017-03-04 06:03:57', '/', 1),
(253, '207.46.13.127', '207.46.13.127', '207.46.13.127', '2017-03-04 01:03:12', '/', 2),
(254, '103.60.172.174', '103.60.172.174', '103.60.172.174', '2017-03-04 10:03:44', '/', 6),
(255, '119.30.38.214', '119.30.38.214', '119.30.38.214', '2017-03-04 11:03:43', '/', 1),
(256, '202.134.11.151', '202.134.11.151', '202.134.11.151', '2017-03-05 09:03:49', '/', 3),
(257, '66.102.6.136', '66.102.6.136', '66.102.6.136', '2017-04-03 04:04:38', '/', 2),
(258, '124.6.235.142', '124.6.235.142', '124.6.235.142', '2017-03-04 03:03:41', '/', 5),
(259, '185.146.168.19', '185.146.168.19', '185.146.168.19', '2017-03-04 06:03:20', '/', 1),
(260, '199.58.86.209', '199.58.86.209', '199.58.86.209', '2017-03-04 08:03:42', '/', 1),
(261, '63.243.252.193', '63.243.252.193', '63.243.252.193', '2017-03-04 09:03:33', '/', 1),
(262, '113.11.61.30', '113.11.61.30', '113.11.61.30', '2017-03-05 06:03:32', '/', 34),
(263, '103.26.138.157', '103.26.138.157', '103.26.138.157', '2017-03-05 01:03:30', '/', 2),
(264, '54.184.230.113', '54.184.230.113', '54.184.230.113', '2017-03-05 08:03:55', '/', 1),
(265, '66.102.7.222', '66.102.7.222', '66.102.7.222', '2017-03-11 03:03:34', '/', 2),
(266, '45.250.20.11', '45.250.20.11', '45.250.20.11', '2017-03-05 10:03:12', '/', 1),
(267, '103.205.134.59', '103.205.134.59', '103.205.134.59', '2017-03-20 10:03:03', '/', 3),
(268, '64.246.165.210', '64.246.165.210', '64.246.165.210', '2017-03-05 12:03:56', '/', 1),
(269, '64.120.30.131', '64.120.30.131', '64.120.30.131', '2017-03-05 01:03:06', '/', 1),
(270, '216.239.90.19', '216.239.90.19', '216.239.90.19', '2017-03-05 03:03:15', '/', 1),
(271, '88.198.23.179', '88.198.23.179', '88.198.23.179', '2017-03-05 04:03:42', '/', 1),
(272, '69.63.188.107', '69.63.188.107', '69.63.188.107', '2017-03-05 07:03:43', '/', 1),
(273, '158.69.229.6', '158.69.229.6', '158.69.229.6', '2017-03-05 09:03:03', '/', 1),
(274, '113.11.61.21', '113.11.61.21', '113.11.61.21', '2017-03-06 03:03:28', '/', 1),
(275, '62.212.73.49', '62.212.73.49', '62.212.73.49', '2017-05-10 03:05:39', '/', 5),
(276, '45.55.21.202', '45.55.21.202', '45.55.21.202', '2017-04-05 01:04:59', '/', 3),
(277, '66.220.145.243', '66.220.145.243', '66.220.145.243', '2017-03-06 05:03:19', '/', 1),
(278, '139.162.70.227', '139.162.70.227', '139.162.70.227', '2017-03-06 05:03:34', '/', 1),
(279, '40.77.167.134', '40.77.167.134', '40.77.167.134', '2017-03-06 09:03:51', '/', 1),
(280, '144.48.108.210', '144.48.108.210', '144.48.108.210', '2017-03-07 12:03:41', '/', 2),
(281, '66.249.73.167', '66.249.73.167', '66.249.73.167', '2017-05-23 05:05:36', '/', 14),
(282, '113.11.61.9', '113.11.61.9', '113.11.61.9', '2017-03-07 04:03:06', '/', 2),
(283, '52.86.176.3', '52.86.176.3', '52.86.176.3', '2017-03-07 04:03:24', '/', 1),
(284, '66.249.73.163', '66.249.73.163', '66.249.73.163', '2017-05-13 10:05:43', '/', 16),
(285, '104.131.69.218', '104.131.69.218', '104.131.69.218', '2017-03-07 08:03:58', '/', 1),
(286, '124.6.235.139', '124.6.235.139', '124.6.235.139', '2017-03-09 10:03:39', '/', 2),
(287, '180.76.15.24', '180.76.15.24', '180.76.15.24', '2017-04-13 12:04:52', '/', 2),
(288, '66.249.73.159', '66.249.73.159', '66.249.73.159', '2017-05-20 11:05:45', '/', 7),
(289, '113.11.61.116', '113.11.61.116', '113.11.61.116', '2017-03-07 10:03:46', '/', 4),
(290, '77.247.181.163', '77.247.181.163', '77.247.181.163', '2017-03-07 11:03:12', '/', 1),
(291, '157.55.39.169', '157.55.39.169', '157.55.39.169', '2017-03-08 07:03:45', '/', 1),
(292, '122.144.8.89', '122.144.8.89', '122.144.8.89', '2017-03-09 04:03:25', '/', 9),
(293, '77.222.103.76', '77.222.103.76', '77.222.103.76', '2017-03-09 04:03:53', '/', 1),
(294, '103.25.250.131', '103.25.250.131', '103.25.250.131', '2017-03-09 08:03:35', '/', 1),
(295, '69.164.206.42', '69.164.206.42', '69.164.206.42', '2017-03-09 10:03:59', '/', 1),
(296, '45.32.129.1', '45.32.129.1', '45.32.129.1', '2017-03-09 01:03:01', '/', 1),
(297, '66.249.69.222', '66.249.69.222', '66.249.69.222', '2017-03-09 06:03:53', '/', 1),
(298, '180.76.15.22', '180.76.15.22', '180.76.15.22', '2017-04-17 03:04:00', '/', 2),
(299, '66.249.69.226', '66.249.69.226', '66.249.69.226', '2017-03-09 08:03:48', '/', 1),
(300, '161.69.163.20', '161.69.163.20', '161.69.163.20', '2017-03-10 02:03:23', '/', 1),
(301, '69.164.111.198', '69.164.111.198', '69.164.111.198', '2017-03-16 05:03:53', '/', 4),
(302, '113.11.61.53', '113.11.61.53', '113.11.61.53', '2017-03-10 04:03:49', '/', 1),
(303, '202.134.11.146', '202.134.11.146', '202.134.11.146', '2017-03-10 08:03:59', '/', 1),
(304, '45.250.20.21', '45.250.20.21', '45.250.20.21', '2017-03-30 01:03:17', '/', 13),
(305, '107.167.108.62', '107.167.108.62', '107.167.108.62', '2017-03-10 10:03:32', '/', 1),
(306, '103.43.151.233', '103.43.151.233', '103.43.151.233', '2017-03-12 12:03:47', '/', 3),
(307, '103.205.134.60', '103.205.134.60', '103.205.134.60', '2017-03-21 09:03:50', '/', 9),
(308, '45.250.20.20', '45.250.20.20', '45.250.20.20', '2017-03-22 01:03:52', '/', 8),
(309, '167.114.234.155', '167.114.234.155', '167.114.234.155', '2017-03-10 11:03:11', '/', 2),
(310, '103.54.43.10', '103.54.43.10', '103.54.43.10', '2017-05-01 01:05:35', '/', 176),
(311, '202.134.11.145', '202.134.11.145', '202.134.11.145', '2017-04-04 10:04:34', '/', 3),
(312, '103.231.231.228', '103.231.231.228', '103.231.231.228', '2017-03-11 01:03:58', '/', 1),
(313, '103.208.134.137', '103.208.134.137', '103.208.134.137', '2017-03-11 04:03:09', '/', 7),
(314, '104.45.18.178', '104.45.18.178', '104.45.18.178', '2017-03-11 03:03:30', '/', 1),
(315, '13.76.241.210', '13.76.241.210', '13.76.241.210', '2017-03-11 03:03:30', '/', 1),
(316, '103.208.134.131', '103.208.134.131', '103.208.134.131', '2017-03-11 03:03:33', '/', 3),
(317, '199.30.231.1', '199.30.231.1', '199.30.231.1', '2017-03-11 03:03:34', '/', 1),
(318, '192.185.4.40', '192.185.4.40', '192.185.4.40', '2017-03-11 03:03:39', '/', 2),
(319, '37.247.121.179', '37.247.121.179', '37.247.121.179', '2017-03-11 03:03:39', '/', 1),
(320, '192.0.83.67', '192.0.83.67', '192.0.83.67', '2017-03-11 03:03:39', '/', 1),
(321, '37.34.62.205', '37.34.62.205', '37.34.62.205', '2017-03-11 03:03:41', '/', 1),
(322, '180.234.81.7', '180.234.81.7', '180.234.81.7', '2017-03-11 04:03:27', '/', 1),
(323, '27.131.15.178', '27.131.15.178', '27.131.15.178', '2017-03-11 04:03:56', '/', 1),
(324, '202.134.13.136', '202.134.13.136', '202.134.13.136', '2017-03-11 06:03:11', '/', 3),
(325, '103.205.134.62', '103.205.134.62', '103.205.134.62', '2017-04-18 02:04:12', '/', 11),
(326, '23.99.101.118', '23.99.101.118', '23.99.101.118', '2017-05-07 10:05:22', '/', 2),
(327, '202.134.9.157', '202.134.9.157', '202.134.9.157', '2017-03-11 12:03:56', '/', 2),
(328, '162.210.196.100', '162.210.196.100', '162.210.196.100', '2017-03-11 03:03:59', '/', 1),
(329, '103.62.143.126', '103.62.143.126', '103.62.143.126', '2017-03-11 11:03:26', '/', 1),
(330, '149.202.175.151', '149.202.175.151', '149.202.175.151', '2017-03-12 06:03:12', '/', 1),
(331, '54.145.177.53', '54.145.177.53', '54.145.177.53', '2017-03-12 07:03:00', '/', 1),
(332, '64.233.173.138', '64.233.173.138', '64.233.173.138', '2017-03-22 04:03:19', '/', 3),
(333, '144.48.0.2', '144.48.0.2', '144.48.0.2', '2017-03-13 03:03:29', '/', 6),
(334, '162.210.196.97', '162.210.196.97', '162.210.196.97', '2017-03-12 10:03:21', '/', 1),
(335, '119.30.45.137', '119.30.45.137', '119.30.45.137', '2017-03-12 07:03:08', '/', 1),
(336, '141.8.143.185', '141.8.143.185', '141.8.143.185', '2017-03-12 08:03:21', '/', 1),
(337, '173.252.123.134', '173.252.123.134', '173.252.123.134', '2017-04-10 06:04:24', '/', 2),
(338, '103.4.146.91', '103.4.146.91', '103.4.146.91', '2017-03-13 03:03:00', '/', 3),
(339, '104.237.130.93', '104.237.130.93', '104.237.130.93', '2017-03-13 03:03:05', '/', 1),
(340, '198.58.107.211', '198.58.107.211', '198.58.107.211', '2017-03-13 03:03:05', '/', 1),
(341, '107.167.108.77', '107.167.108.77', '107.167.108.77', '2017-03-13 10:03:20', '/', 1),
(342, '45.116.251.206', '45.116.251.206', '45.116.251.206', '2017-03-13 11:03:36', '/', 1),
(343, '103.49.171.61', '103.49.171.61', '103.49.171.61', '2017-03-13 01:03:51', '/', 1),
(344, '180.76.15.143', '180.76.15.143', '180.76.15.143', '2017-03-13 04:03:56', '/', 1),
(345, '173.234.164.9', '173.234.164.9', '173.234.164.9', '2017-05-16 03:05:06', '/', 2),
(346, '66.220.145.245', '66.220.145.245', '66.220.145.245', '2017-03-14 11:03:38', '/', 1),
(347, '209.222.77.220', '209.222.77.220', '209.222.77.220', '2017-03-14 12:03:08', '/', 1),
(348, '69.58.178.56', '69.58.178.56', '69.58.178.56', '2017-05-20 05:05:28', '/', 6),
(349, '180.76.15.8', '180.76.15.8', '180.76.15.8', '2017-04-18 09:04:55', '/', 3),
(350, '103.229.86.234', '103.229.86.234', '103.229.86.234', '2017-03-14 10:03:05', '/', 1),
(351, '103.242.219.123', '103.242.219.123', '103.242.219.123', '2017-03-15 04:03:13', '/', 5),
(352, '89.145.95.39', '89.145.95.39', '89.145.95.39', '2017-05-21 08:05:02', '/', 3),
(353, '58.187.10.2', '58.187.10.2', '58.187.10.2', '2017-03-15 01:03:01', '/', 1),
(354, '113.11.61.126', '113.11.61.126', '113.11.61.126', '2017-03-15 01:03:40', '/', 1),
(355, '103.198.137.245', '103.198.137.245', '103.198.137.245', '2017-03-15 04:03:06', '/', 2),
(356, '45.64.166.178', '45.64.166.178', '45.64.166.178', '2017-03-15 02:03:52', '/', 4),
(357, '119.30.47.192', '119.30.47.192', '119.30.47.192', '2017-03-15 09:03:55', '/', 1),
(358, '180.148.210.74', '180.148.210.74', '180.148.210.74', '2017-03-16 10:03:39', '/', 1),
(359, '192.241.230.25', '192.241.230.25', '192.241.230.25', '2017-03-16 01:03:51', '/', 1),
(360, '202.46.57.173', '202.46.57.173', '202.46.57.173', '2017-03-16 02:03:15', '/', 1),
(361, '113.11.61.45', '113.11.61.45', '113.11.61.45', '2017-03-17 12:03:31', '/', 1),
(362, '113.11.61.45', '113.11.61.45', '113.11.61.45', '2017-03-17 12:03:31', '/', 1),
(363, '113.11.61.45', '113.11.61.45', '113.11.61.45', '2017-03-17 12:03:35', '/', 1),
(364, '113.11.61.45', '113.11.61.45', '113.11.61.45', '2017-03-17 12:03:35', '/', 1),
(365, '113.11.61.45', '113.11.61.45', '113.11.61.45', '2017-03-17 12:03:36', '/', 1),
(366, '113.11.61.45', '113.11.61.45', '113.11.61.45', '2017-03-17 12:03:36', '/', 1),
(367, '113.11.61.45', '113.11.61.45', '113.11.61.45', '2017-03-17 12:03:48', '/', 1),
(368, '150.70.173.45', '150.70.173.45', '150.70.173.45', '2017-03-17 01:03:08', '/', 1),
(369, '150.70.173.50', '150.70.173.50', '150.70.173.50', '2017-03-17 01:03:08', '/', 1),
(370, '113.11.61.45', '113.11.61.45', '113.11.61.45', '2017-03-17 01:03:36', '/', 1),
(371, '113.11.61.45', '113.11.61.45', '113.11.61.45', '2017-03-17 01:03:36', '/', 1),
(372, '113.11.61.45', '113.11.61.45', '113.11.61.45', '2017-03-17 07:03:19', '/', 1),
(373, '113.11.61.45', '113.11.61.45', '113.11.61.45', '2017-03-17 07:03:20', '/', 1),
(374, '113.11.61.45', '113.11.61.45', '113.11.61.45', '2017-03-17 07:03:20', '/', 1),
(375, '113.11.61.45', '113.11.61.45', '113.11.61.45', '2017-03-17 07:03:21', '/', 1),
(376, '113.11.61.45', '113.11.61.45', '113.11.61.45', '2017-03-17 07:03:21', '/', 1),
(377, '113.11.61.45', '113.11.61.45', '113.11.61.45', '2017-03-17 07:03:22', '/', 1),
(378, '66.102.6.14', '66.102.6.14', '66.102.6.14', '2017-03-17 07:03:22', '/', 1),
(379, '113.11.61.45', '113.11.61.45', '113.11.61.45', '2017-03-17 07:03:22', '/', 1),
(380, '113.11.61.45', '113.11.61.45', '113.11.61.45', '2017-03-17 07:03:24', '/', 1),
(381, '113.11.61.45', '113.11.61.45', '113.11.61.45', '2017-03-17 07:03:24', '/', 1),
(382, '113.11.61.45', '113.11.61.45', '113.11.61.45', '2017-03-17 07:03:25', '/', 1),
(383, '107.167.110.12', '107.167.110.12', '107.167.110.12', '2017-03-17 09:03:30', '/', 1),
(384, '5.255.250.65', '5.255.250.65', '5.255.250.65', '2017-05-05 08:05:16', '/', 42),
(385, '157.55.39.204', '157.55.39.204', '157.55.39.204', '2017-03-17 12:03:46', '/', 1),
(386, '119.30.45.131', '119.30.45.131', '119.30.45.131', '2017-03-18 12:03:42', '/', 1),
(387, '103.78.224.3', '103.78.224.3', '103.78.224.3', '2017-03-18 01:03:24', '/', 1),
(388, '103.196.234.9', '103.196.234.9', '103.196.234.9', '2017-03-18 04:03:22', '/', 9),
(389, '107.167.107.117', '107.167.107.117', '107.167.107.117', '2017-03-18 03:03:08', '/', 1),
(390, '123.108.244.238', '123.108.244.238', '123.108.244.238', '2017-03-18 03:03:22', '/', 1),
(391, '128.75.89.206', '128.75.89.206', '128.75.89.206', '2017-03-18 05:03:57', '/', 1),
(392, '119.30.47.6', '119.30.47.6', '119.30.47.6', '2017-03-18 06:03:50', '/', 1),
(393, '113.11.61.77', '113.11.61.77', '113.11.61.77', '2017-03-18 08:03:53', '/', 2),
(394, '104.156.230.231', '104.156.230.231', '104.156.230.231', '2017-04-05 02:04:13', '/', 3),
(395, '45.64.166.54', '45.64.166.54', '45.64.166.54', '2017-03-20 01:03:26', '/', 53),
(396, '40.77.167.132', '40.77.167.132', '40.77.167.132', '2017-03-18 02:03:01', '/', 1),
(397, '66.249.65.124', '66.249.65.124', '66.249.65.124', '2017-03-20 07:03:48', '/', 3),
(398, '64.71.171.87', '64.71.171.87', '64.71.171.87', '2017-03-18 11:03:59', '/', 2),
(399, '64.71.171.71', '64.71.171.71', '64.71.171.71', '2017-03-19 03:03:48', '/', 3),
(400, '64.233.173.153', '64.233.173.153', '64.233.173.153', '2017-03-19 08:03:47', '/', 2),
(401, '64.233.173.151', '64.233.173.151', '64.233.173.151', '2017-03-19 08:03:45', '/', 2),
(402, '103.62.141.10', '103.62.141.10', '103.62.141.10', '2017-03-19 12:03:41', '/', 2),
(403, '107.167.106.71', '107.167.106.71', '107.167.106.71', '2017-03-19 01:03:16', '/', 1),
(404, '167.114.231.35', '167.114.231.35', '167.114.231.35', '2017-03-19 02:03:13', '/', 2),
(405, '100.38.192.102', '100.38.192.102', '100.38.192.102', '2017-03-19 08:03:21', '/', 1),
(406, '163.47.156.99', '163.47.156.99', '163.47.156.99', '2017-03-19 11:03:28', '/', 2),
(407, '69.171.230.104', '69.171.230.104', '69.171.230.104', '2017-03-19 11:03:31', '/', 1),
(408, '37.216.29.142', '37.216.29.142', '37.216.29.142', '2017-03-20 12:03:48', '/', 1),
(409, '180.76.15.34', '180.76.15.34', '180.76.15.34', '2017-04-22 05:04:10', '/', 2),
(410, '157.55.39.95', '157.55.39.95', '157.55.39.95', '2017-03-20 02:03:04', '/', 1),
(411, '107.167.99.144', '107.167.99.144', '107.167.99.144', '2017-03-20 04:03:00', '/', 1),
(412, '66.249.65.67', '66.249.65.67', '66.249.65.67', '2017-04-26 12:04:42', '/', 3),
(413, '180.76.15.140', '180.76.15.140', '180.76.15.140', '2017-04-25 01:04:01', '/', 2),
(414, '43.250.81.85', '43.250.81.85', '43.250.81.85', '2017-03-21 04:03:55', '/', 5),
(415, '45.79.66.231', '45.79.66.231', '45.79.66.231', '2017-03-22 02:03:41', '/', 1),
(416, '193.201.225.109', '193.201.225.109', '193.201.225.109', '2017-03-22 03:03:17', '/', 1),
(417, '45.127.245.138', '45.127.245.138', '45.127.245.138', '2017-03-22 04:03:19', '/', 1),
(418, '103.222.20.74', '103.222.20.74', '103.222.20.74', '2017-03-22 04:03:47', '/', 1),
(419, '157.55.39.162', '157.55.39.162', '157.55.39.162', '2017-03-22 06:03:08', '/', 1),
(420, '107.167.106.132', '107.167.106.132', '107.167.106.132', '2017-03-22 08:03:21', '/', 3),
(421, '199.58.164.120', '199.58.164.120', '199.58.164.120', '2017-03-22 12:03:33', '/', 1),
(422, '27.147.243.46', '27.147.243.46', '27.147.243.46', '2017-03-22 01:03:51', '/', 1),
(423, '199.58.164.124', '199.58.164.124', '199.58.164.124', '2017-05-22 10:05:59', '/', 3),
(424, '95.27.238.255', '95.27.238.255', '95.27.238.255', '2017-03-23 05:03:00', '/', 1),
(425, '199.58.164.117', '199.58.164.117', '199.58.164.117', '2017-03-27 07:03:10', '/', 2),
(426, '207.46.13.68', '207.46.13.68', '207.46.13.68', '2017-03-23 08:03:21', '/', 1),
(427, '203.76.221.88', '203.76.221.88', '203.76.221.88', '2017-03-23 09:03:27', '/', 2),
(428, '64.246.165.140', '64.246.165.140', '64.246.165.140', '2017-03-23 02:03:18', '/', 1),
(429, '38.100.21.66', '38.100.21.66', '38.100.21.66', '2017-03-23 11:03:49', '/', 1),
(430, '38.100.21.66', '38.100.21.66', '38.100.21.66', '2017-03-23 11:03:49', '/', 1),
(431, '157.55.39.245', '157.55.39.245', '157.55.39.245', '2017-03-26 05:03:43', '/', 2),
(432, '43.250.81.61', '43.250.81.61', '43.250.81.61', '2017-03-24 04:03:09', '/', 1),
(433, '103.244.13.51', '103.244.13.51', '103.244.13.51', '2017-03-24 05:03:21', '/', 1),
(434, '202.125.65.170', '202.125.65.170', '202.125.65.170', '2017-03-24 06:03:20', '/', 2),
(435, '66.249.75.18', '66.249.75.18', '66.249.75.18', '2017-03-24 02:03:07', '/', 1),
(436, '103.25.249.107', '103.25.249.107', '103.25.249.107', '2017-03-24 03:03:37', '/', 1),
(437, '75.149.221.170', '75.149.221.170', '75.149.221.170', '2017-03-24 09:03:05', '/', 1),
(438, '74.115.214.154', '74.115.214.154', '74.115.214.154', '2017-03-24 10:03:38', '/', 1),
(439, '148.62.14.156', '148.62.14.156', '148.62.14.156', '2017-03-25 03:03:23', '/', 2),
(440, '43.250.81.83', '43.250.81.83', '43.250.81.83', '2017-04-11 04:04:20', '/', 33),
(441, '104.209.188.207', '104.209.188.207', '104.209.188.207', '2017-03-25 04:03:54', '/', 1),
(442, '103.15.141.158', '103.15.141.158', '103.15.141.158', '2017-04-22 06:04:25', '/', 27),
(443, '213.191.16.251', '213.191.16.251', '213.191.16.251', '2017-03-25 05:03:20', '/', 1),
(444, '119.30.39.53', '119.30.39.53', '119.30.39.53', '2017-03-25 08:03:44', '/', 1),
(445, '103.9.115.210', '103.9.115.210', '103.9.115.210', '2017-04-07 01:04:20', '/', 17),
(446, '199.58.164.114', '199.58.164.114', '199.58.164.114', '2017-04-23 06:04:54', '/', 2),
(447, '180.76.15.158', '180.76.15.158', '180.76.15.158', '2017-03-29 02:03:25', '/', 2),
(448, '74.115.214.156', '74.115.214.156', '74.115.214.156', '2017-03-25 09:03:50', '/', 1),
(449, '62.210.246.163', '62.210.246.163', '62.210.246.163', '2017-03-25 10:03:26', '/', 1),
(450, '103.204.83.202', '103.204.83.202', '103.204.83.202', '2017-04-09 01:04:09', '/', 43),
(451, '172.241.151.25', '172.241.151.25', '172.241.151.25', '2017-03-26 01:03:02', '/', 1),
(452, '45.251.231.54', '45.251.231.54', '45.251.231.54', '2017-03-26 08:03:54', '/', 1),
(453, '54.224.46.242', '54.224.46.242', '54.224.46.242', '2017-03-26 10:03:30', '/', 1),
(454, '141.8.143.172', '141.8.143.172', '141.8.143.172', '2017-04-26 01:04:10', '/', 4),
(455, '103.25.251.70', '103.25.251.70', '103.25.251.70', '2017-03-26 11:03:28', '/', 1),
(456, '172.241.151.18', '172.241.151.18', '172.241.151.18', '2017-04-23 09:04:46', '/', 3),
(457, '173.252.84.199', '173.252.84.199', '173.252.84.199', '2017-03-27 01:03:43', '/', 1),
(458, '103.242.21.234', '103.242.21.234', '103.242.21.234', '2017-03-27 01:03:44', '/', 2),
(459, '172.241.151.29', '172.241.151.29', '172.241.151.29', '2017-03-27 04:03:07', '/', 1),
(460, '202.134.11.144', '202.134.11.144', '202.134.11.144', '2017-03-27 09:03:00', '/', 1),
(461, '77.123.2.4', '77.123.2.4', '77.123.2.4', '2017-03-28 12:03:04', '/', 1),
(462, '176.126.252.12', '176.126.252.12', '176.126.252.12', '2017-05-06 03:05:34', '/', 2),
(463, '173.234.234.216', '173.234.234.216', '173.234.234.216', '2017-03-28 09:03:38', '/', 1),
(464, '43.250.81.26', '43.250.81.26', '43.250.81.26', '2017-03-28 10:03:14', '/', 4),
(465, '157.55.39.119', '157.55.39.119', '157.55.39.119', '2017-03-28 01:03:34', '/', 2),
(466, '144.48.108.58', '144.48.108.58', '144.48.108.58', '2017-03-28 12:03:10', '/', 1),
(467, '91.210.146.53', '91.210.146.53', '91.210.146.53', '2017-03-28 01:03:30', '/', 1),
(468, '180.76.15.7', '180.76.15.7', '180.76.15.7', '2017-03-28 10:03:58', '/', 1),
(469, '168.235.196.95', '168.235.196.95', '168.235.196.95', '2017-03-29 12:03:53', '/', 1),
(470, '52.87.86.158', '52.87.86.158', '52.87.86.158', '2017-03-29 02:03:57', '/', 1),
(471, '103.205.68.10', '103.205.68.10', '103.205.68.10', '2017-03-29 02:03:59', '/', 1),
(472, '109.163.234.8', '109.163.234.8', '109.163.234.8', '2017-03-29 04:03:42', '/', 1),
(473, '157.55.39.39', '157.55.39.39', '157.55.39.39', '2017-03-29 05:03:58', '/', 1),
(474, '66.249.73.45', '66.249.73.45', '66.249.73.45', '2017-03-29 09:03:22', '/', 1),
(475, '173.252.123.187', '173.252.123.187', '173.252.123.187', '2017-03-29 10:03:44', '/', 1),
(476, '54.165.35.250', '54.165.35.250', '54.165.35.250', '2017-03-29 10:03:44', '/', 1),
(477, '103.214.200.82', '103.214.200.82', '103.214.200.82', '2017-05-11 02:05:49', '/', 16),
(478, '103.26.136.89', '103.26.136.89', '103.26.136.89', '2017-03-30 06:03:01', '/', 3),
(479, '182.48.94.10', '182.48.94.10', '182.48.94.10', '2017-03-30 06:03:04', '/', 3),
(480, '200.225.139.101', '200.225.139.101', '200.225.139.101', '2017-03-30 06:03:16', '/', 1),
(481, '123.108.244.211', '123.108.244.211', '123.108.244.211', '2017-03-30 09:03:36', '/', 2),
(482, '123.108.246.230', '123.108.246.230', '123.108.246.230', '2017-03-30 11:03:13', '/', 1),
(483, '207.46.13.184', '207.46.13.184', '207.46.13.184', '2017-03-30 12:03:45', '/', 1),
(484, '103.19.253.202', '103.19.253.202', '103.19.253.202', '2017-03-30 12:03:59', '/', 1),
(485, '192.140.252.118', '192.140.252.118', '192.140.252.118', '2017-03-30 01:03:03', '/', 1),
(486, '103.197.155.57', '103.197.155.57', '103.197.155.57', '2017-03-30 01:03:04', '/', 1),
(487, '43.245.122.171', '43.245.122.171', '43.245.122.171', '2017-03-30 01:03:05', '/', 1),
(488, '103.217.111.252', '103.217.111.252', '103.217.111.252', '2017-03-30 01:03:06', '/', 1),
(489, '103.3.225.130', '103.3.225.130', '103.3.225.130', '2017-03-30 01:03:53', '/', 1),
(490, '182.48.94.34', '182.48.94.34', '182.48.94.34', '2017-03-30 02:03:26', '/', 1),
(491, '103.77.62.41', '103.77.62.41', '103.77.62.41', '2017-03-30 02:03:38', '/', 2),
(492, '103.237.76.19', '103.237.76.19', '103.237.76.19', '2017-03-30 02:03:43', '/', 1),
(493, '103.230.106.26', '103.230.106.26', '103.230.106.26', '2017-03-30 04:03:17', '/', 1),
(494, '89.78.139.103', '89.78.139.103', '89.78.139.103', '2017-03-30 04:03:18', '/', 1),
(495, '8.37.234.93', '8.37.234.93', '8.37.234.93', '2017-03-30 04:03:24', '/', 1),
(496, '95.147.106.235', '95.147.106.235', '95.147.106.235', '2017-03-30 05:03:01', '/', 3),
(497, '180.234.43.125', '180.234.43.125', '180.234.43.125', '2017-03-30 05:03:12', '/', 1),
(498, '182.48.64.186', '182.48.64.186', '182.48.64.186', '2017-03-30 06:03:22', '/', 2),
(499, '119.30.47.103', '119.30.47.103', '119.30.47.103', '2017-03-30 10:03:08', '/', 1),
(500, '103.205.135.53', '103.205.135.53', '103.205.135.53', '2017-03-31 03:03:35', '/', 2),
(501, '43.243.135.13', '43.243.135.13', '43.243.135.13', '2017-03-31 04:03:55', '/', 2),
(502, '103.49.171.40', '103.49.171.40', '103.49.171.40', '2017-03-31 07:03:27', '/', 2),
(503, '45.251.231.36', '45.251.231.36', '45.251.231.36', '2017-03-31 11:03:37', '/', 2),
(504, '45.250.20.16', '45.250.20.16', '45.250.20.16', '2017-03-31 12:03:02', '/', 2),
(505, '159.253.145.183', '159.253.145.183', '159.253.145.183', '2017-03-31 12:03:52', '/', 2),
(506, '61.6.109.67', '61.6.109.67', '61.6.109.67', '2017-03-31 12:03:54', '/', 2),
(507, '103.225.94.11', '103.225.94.11', '103.225.94.11', '2017-03-31 01:03:49', '/', 2),
(508, '5.255.250.83', '5.255.250.83', '5.255.250.83', '2017-03-31 03:03:10', '/', 1),
(509, '180.234.88.164', '180.234.88.164', '180.234.88.164', '2017-04-01 12:04:19', '/', 4),
(510, '5.108.163.202', '5.108.163.202', '5.108.163.202', '2017-04-01 04:04:24', '/', 1),
(511, '103.78.188.246', '103.78.188.246', '103.78.188.246', '2017-04-01 06:04:50', '/', 12),
(512, '8.37.225.226', '8.37.225.226', '8.37.225.226', '2017-04-01 07:04:02', '/', 2),
(513, '27.123.246.30', '27.123.246.30', '27.123.246.30', '2017-05-12 08:05:40', '/', 7),
(514, '45.250.20.17', '45.250.20.17', '45.250.20.17', '2017-04-01 08:04:16', '/', 1),
(515, '192.241.206.112', '192.241.206.112', '192.241.206.112', '2017-04-01 08:04:59', '/', 2),
(516, '116.58.204.249', '116.58.204.249', '116.58.204.249', '2017-04-01 09:04:32', '/', 1),
(517, '43.245.123.10', '43.245.123.10', '43.245.123.10', '2017-04-01 09:04:36', '/', 1),
(518, '137.101.140.17', '137.101.140.17', '137.101.140.17', '2017-04-01 12:04:03', '/', 4),
(519, '43.245.122.139', '43.245.122.139', '43.245.122.139', '2017-04-01 12:04:06', '/', 3),
(520, '45.251.228.227', '45.251.228.227', '45.251.228.227', '2017-04-01 12:04:52', '/', 4),
(521, '138.197.0.251', '138.197.0.251', '138.197.0.251', '2017-04-01 02:04:37', '/', 1),
(522, '107.167.116.57', '107.167.116.57', '107.167.116.57', '2017-04-01 07:04:30', '/', 2),
(523, '107.167.106.231', '107.167.106.231', '107.167.106.231', '2017-04-01 07:04:30', '/', 2),
(524, '202.134.11.138', '202.134.11.138', '202.134.11.138', '2017-04-01 09:04:15', '/', 2),
(525, '69.64.59.39', '69.64.59.39', '69.64.59.39', '2017-04-01 11:04:03', '/', 1),
(526, '14.1.100.234', '14.1.100.234', '14.1.100.234', '2017-05-15 01:05:13', '/', 11),
(527, '207.46.13.225', '207.46.13.225', '207.46.13.225', '2017-04-02 12:04:40', '/', 1),
(528, '149.56.229.17', '149.56.229.17', '149.56.229.17', '2017-04-02 02:04:28', '/', 1),
(529, '103.229.87.4', '103.229.87.4', '103.229.87.4', '2017-04-02 05:04:42', '/', 2),
(530, '82.145.222.139', '82.145.222.139', '82.145.222.139', '2017-04-02 05:04:48', '/', 2),
(531, '119.30.45.98', '119.30.45.98', '119.30.45.98', '2017-04-02 11:04:45', '/', 3),
(532, '103.250.71.46', '103.250.71.46', '103.250.71.46', '2017-04-02 12:04:43', '/', 2),
(533, '176.10.99.200', '176.10.99.200', '176.10.99.200', '2017-04-02 01:04:08', '/', 1),
(534, '119.30.45.239', '119.30.45.239', '119.30.45.239', '2017-04-02 06:04:22', '/', 8),
(535, '168.235.194.70', '168.235.194.70', '168.235.194.70', '2017-04-03 12:04:11', '/', 4),
(536, '103.25.250.176', '103.25.250.176', '103.25.250.176', '2017-04-03 12:04:14', '/', 2),
(537, '69.171.230.110', '69.171.230.110', '69.171.230.110', '2017-04-03 02:04:15', '/', 2),
(538, '69.171.230.106', '69.171.230.106', '69.171.230.106', '2017-04-03 02:04:15', '/', 1),
(539, '69.171.230.100', '69.171.230.100', '69.171.230.100', '2017-04-03 02:04:15', '/', 1),
(540, '69.171.230.116', '69.171.230.116', '69.171.230.116', '2017-04-03 02:04:15', '/', 1),
(541, '66.220.146.24', '66.220.146.24', '66.220.146.24', '2017-04-03 02:04:15', '/', 1),
(542, '66.220.146.184', '66.220.146.184', '66.220.146.184', '2017-04-03 02:04:15', '/', 1),
(543, '66.220.148.165', '66.220.148.165', '66.220.148.165', '2017-04-22 01:04:59', '/', 2),
(544, '66.220.151.89', '66.220.151.89', '66.220.151.89', '2017-04-03 02:04:15', '/', 1),
(545, '66.220.151.86', '66.220.151.86', '66.220.151.86', '2017-04-03 02:04:15', '/', 1),
(546, '173.252.80.119', '173.252.80.119', '173.252.80.119', '2017-04-03 02:04:15', '/', 1),
(547, '66.220.146.182', '66.220.146.182', '66.220.146.182', '2017-04-03 02:04:15', '/', 1),
(548, '8.37.230.170', '8.37.230.170', '8.37.230.170', '2017-04-03 02:04:15', '/', 2),
(549, '64.74.215.15', '64.74.215.15', '64.74.215.15', '2017-04-03 03:04:13', '/', 1),
(550, '108.61.123.78', '108.61.123.78', '108.61.123.78', '2017-04-03 11:04:17', '/', 1),
(551, '103.204.83.28', '103.204.83.28', '103.204.83.28', '2017-04-03 02:04:27', '/', 2),
(552, '192.36.27.7', '192.36.27.7', '192.36.27.7', '2017-04-03 07:04:20', '/', 1),
(553, '66.249.73.39', '66.249.73.39', '66.249.73.39', '2017-04-04 09:04:37', '/', 1),
(554, '207.46.13.119', '207.46.13.119', '207.46.13.119', '2017-04-04 12:04:27', '/', 1),
(555, '94.142.242.84', '94.142.242.84', '94.142.242.84', '2017-04-04 10:04:52', '/', 1),
(556, '183.13.12.231', '183.13.12.231', '183.13.12.231', '2017-04-05 01:04:03', '/', 1),
(557, '27.109.26.226', '27.109.26.226', '27.109.26.226', '2017-04-05 01:04:55', '/', 2),
(558, '43.245.123.105', '43.245.123.105', '43.245.123.105', '2017-04-05 02:04:03', '/', 2),
(559, '137.59.48.138', '137.59.48.138', '137.59.48.138', '2017-04-05 04:04:34', '/', 6),
(560, '209.126.119.35', '209.126.119.35', '209.126.119.35', '2017-04-05 04:04:29', '/', 1),
(561, '202.134.11.156', '202.134.11.156', '202.134.11.156', '2017-04-05 08:04:40', '/', 2),
(562, '46.105.109.115', '46.105.109.115', '46.105.109.115', '2017-04-05 09:04:48', '/', 1),
(563, '141.8.143.222', '141.8.143.222', '141.8.143.222', '2017-04-10 06:04:08', '/', 2),
(564, '5.199.130.188', '5.199.130.188', '5.199.130.188', '2017-04-06 02:04:22', '/', 1),
(565, '54.87.147.115', '54.87.147.115', '54.87.147.115', '2017-04-06 03:04:00', '/', 1),
(566, '27.147.204.22', '27.147.204.22', '27.147.204.22', '2017-04-06 03:04:39', '/', 2),
(567, '168.235.194.136', '168.235.194.136', '168.235.194.136', '2017-04-06 03:04:39', '/', 2),
(568, '184.72.76.14', '184.72.76.14', '184.72.76.14', '2017-04-06 04:04:55', '/', 1),
(569, '168.235.194.196', '168.235.194.196', '168.235.194.196', '2017-04-06 08:04:00', '/', 4),
(570, '107.167.107.148', '107.167.107.148', '107.167.107.148', '2017-04-06 08:04:42', '/', 2),
(571, '180.76.15.155', '180.76.15.155', '180.76.15.155', '2017-04-06 10:04:33', '/', 1),
(572, '103.242.21.248', '103.242.21.248', '103.242.21.248', '2017-04-06 11:04:07', '/', 2),
(573, '66.249.73.42', '66.249.73.42', '66.249.73.42', '2017-04-06 11:04:31', '/', 1),
(574, '8.37.231.83', '8.37.231.83', '8.37.231.83', '2017-04-06 02:04:26', '/', 2),
(575, '103.82.9.0', '103.82.9.0', '103.82.9.0', '2017-04-06 05:04:00', '/', 1),
(576, '107.167.104.96', '107.167.104.96', '107.167.104.96', '2017-04-06 09:04:15', '/', 2),
(577, '119.30.39.160', '119.30.39.160', '119.30.39.160', '2017-04-06 09:04:40', '/', 2),
(578, '123.108.244.160', '123.108.244.160', '123.108.244.160', '2017-04-07 12:04:31', '/', 2),
(579, '188.48.218.149', '188.48.218.149', '188.48.218.149', '2017-04-07 12:04:47', '/', 2),
(580, '103.225.231.42', '103.225.231.42', '103.225.231.42', '2017-04-07 01:04:33', '/?keywords=10010001', 1),
(581, '119.30.35.49', '119.30.35.49', '119.30.35.49', '2017-04-07 12:04:32', '/', 2),
(582, '54.208.201.249', '54.208.201.249', '54.208.201.249', '2017-04-18 07:04:24', '/', 2),
(583, '78.129.132.172', '78.129.132.172', '78.129.132.172', '2017-04-08 12:04:00', '/', 1),
(584, '37.123.133.148', '37.123.133.148', '37.123.133.148', '2017-04-08 07:04:52', '/', 1),
(585, '207.46.13.126', '207.46.13.126', '207.46.13.126', '2017-04-08 08:04:39', '/', 1),
(586, '150.70.173.52', '150.70.173.52', '150.70.173.52', '2017-04-08 01:04:43', '/', 1),
(587, '103.205.134.58', '103.205.134.58', '103.205.134.58', '2017-04-08 01:04:58', '/', 3),
(588, '27.147.204.141', '27.147.204.141', '27.147.204.141', '2017-04-08 02:04:19', '/', 2),
(589, '103.84.36.122', '103.84.36.122', '103.84.36.122', '2017-04-08 02:04:33', '/', 2),
(590, '125.16.216.9', '125.16.216.9', '125.16.216.9', '2017-04-08 07:04:30', '/', 6),
(591, '54.255.142.40', '54.255.142.40', '54.255.142.40', '2017-04-08 04:04:45', '/', 1),
(592, '62.212.68.156', '62.212.68.156', '62.212.68.156', '2017-05-06 09:05:50', '/', 2),
(593, '103.210.19.22', '103.210.19.22', '103.210.19.22', '2017-04-08 08:04:54', '/', 2),
(594, '27.147.203.132', '27.147.203.132', '27.147.203.132', '2017-04-08 08:04:59', '/', 2),
(595, '52.205.128.116', '52.205.128.116', '52.205.128.116', '2017-04-09 01:04:12', '/', 1),
(596, '103.82.9.3', '103.82.9.3', '103.82.9.3', '2017-04-09 03:04:53', '/', 1),
(597, '157.55.39.40', '157.55.39.40', '157.55.39.40', '2017-04-09 10:04:11', '/', 1),
(598, '43.225.150.73', '43.225.150.73', '43.225.150.73', '2017-04-10 02:04:53', '/', 5),
(599, '74.63.246.42', '74.63.246.42', '74.63.246.42', '2017-04-10 01:04:42', '/', 2),
(600, '180.76.15.10', '180.76.15.10', '180.76.15.10', '2017-04-10 02:04:52', '/', 1),
(601, '173.252.123.143', '173.252.123.143', '173.252.123.143', '2017-04-10 06:04:24', '/', 1),
(602, '173.252.123.137', '173.252.123.137', '173.252.123.137', '2017-04-10 06:04:24', '/', 1),
(603, '173.252.123.137', '173.252.123.137', '173.252.123.137', '2017-04-10 06:04:24', '/', 1),
(604, '173.252.123.130', '173.252.123.130', '173.252.123.130', '2017-04-10 06:04:24', '/', 1),
(605, '173.252.123.129', '173.252.123.129', '173.252.123.129', '2017-04-10 06:04:24', '/', 1),
(606, '173.252.123.138', '173.252.123.138', '173.252.123.138', '2017-04-10 06:04:24', '/', 1),
(607, '173.252.123.132', '173.252.123.132', '173.252.123.132', '2017-05-10 06:05:37', '/', 2),
(608, '66.220.156.148', '66.220.156.148', '66.220.156.148', '2017-04-10 06:04:24', '/', 1),
(609, '66.220.156.149', '66.220.156.149', '66.220.156.149', '2017-04-10 06:04:24', '/', 1),
(610, '66.220.156.150', '66.220.156.150', '66.220.156.150', '2017-05-10 06:05:37', '/', 3),
(611, '207.46.13.4', '207.46.13.4', '207.46.13.4', '2017-04-10 06:04:49', '/', 1),
(612, '54.255.181.60', '54.255.181.60', '54.255.181.60', '2017-04-10 09:04:15', '/', 1),
(613, '43.245.235.6', '43.245.235.6', '43.245.235.6', '2017-05-27 10:05:35', '/', 349),
(614, '64.246.161.30', '64.246.161.30', '64.246.161.30', '2017-04-10 11:04:16', '/', 1),
(615, '139.59.42.239', '139.59.42.239', '139.59.42.239', '2017-04-10 12:04:12', '/', 1),
(616, '103.196.235.150', '103.196.235.150', '103.196.235.150', '2017-04-10 02:04:42', '/', 4),
(617, '180.76.15.14', '180.76.15.14', '180.76.15.14', '2017-04-10 04:04:39', '/', 1),
(618, '192.36.27.4', '192.36.27.4', '192.36.27.4', '2017-05-04 11:05:56', '/', 2),
(619, '103.82.9.1', '103.82.9.1', '103.82.9.1', '2017-04-11 02:04:13', '/', 1),
(620, '198.199.64.237', '198.199.64.237', '198.199.64.237', '2017-04-11 02:04:26', '/', 1),
(621, '103.198.138.244', '103.198.138.244', '103.198.138.244', '2017-04-11 12:04:50', '/', 8),
(622, '89.234.68.92', '89.234.68.92', '89.234.68.92', '2017-04-11 08:04:35', '/', 1),
(623, '89.234.68.69', '89.234.68.69', '89.234.68.69', '2017-04-11 08:04:36', '/', 1),
(624, '103.25.251.103', '103.25.251.103', '103.25.251.103', '2017-04-11 12:04:32', '/', 1),
(625, '51.15.37.18', '51.15.37.18', '51.15.37.18', '2017-04-11 08:04:52', '/', 1),
(626, '103.242.219.125', '103.242.219.125', '103.242.219.125', '2017-04-12 01:04:25', '/', 4),
(627, '144.48.108.226', '144.48.108.226', '144.48.108.226', '2017-04-29 05:04:16', '/', 46),
(628, '54.173.28.195', '54.173.28.195', '54.173.28.195', '2017-04-12 07:04:06', '/', 1),
(629, '40.77.167.115', '40.77.167.115', '40.77.167.115', '2017-04-12 08:04:43', '/', 1),
(630, '37.59.254.151', '37.59.254.151', '37.59.254.151', '2017-04-12 01:04:40', '/', 1),
(631, '80.11.19.74', '80.11.19.74', '80.11.19.74', '2017-04-12 05:04:22', '/', 1),
(632, '74.115.214.152', '74.115.214.152', '74.115.214.152', '2017-04-12 08:04:13', '/', 1),
(633, '172.241.151.23', '172.241.151.23', '172.241.151.23', '2017-04-21 08:04:28', '/', 3),
(634, '103.230.104.25', '103.230.104.25', '103.230.104.25', '2017-04-13 01:04:16', '/', 2),
(635, '218.101.110.63', '218.101.110.63', '218.101.110.63', '2017-04-13 03:04:50', '/', 1),
(636, '176.126.252.11', '176.126.252.11', '176.126.252.11', '2017-04-13 06:04:07', '/', 1),
(637, '174.138.78.160', '174.138.78.160', '174.138.78.160', '2017-04-13 06:04:33', '/', 1),
(638, '103.254.86.236', '103.254.86.236', '103.254.86.236', '2017-04-18 01:04:00', '/', 4),
(639, '103.15.141.94', '103.15.141.94', '103.15.141.94', '2017-04-13 07:04:53', '/', 2),
(640, '180.92.239.142', '180.92.239.142', '180.92.239.142', '2017-04-13 09:04:34', '/', 6),
(641, '103.220.204.142', '103.220.204.142', '103.220.204.142', '2017-04-14 03:04:40', '/', 8),
(642, '103.76.45.62', '103.76.45.62', '103.76.45.62', '2017-04-14 04:04:31', '/', 2),
(643, '8.37.232.250', '8.37.232.250', '8.37.232.250', '2017-04-14 05:04:28', '/', 2),
(644, '180.148.215.166', '180.148.215.166', '180.148.215.166', '2017-04-14 05:04:34', '/', 2),
(645, '103.67.156.31', '103.67.156.31', '103.67.156.31', '2017-04-14 06:04:15', '/', 1),
(646, '103.239.254.2', '103.239.254.2', '103.239.254.2', '2017-04-14 09:04:07', '/', 2),
(647, '103.242.23.191', '103.242.23.191', '103.242.23.191', '2017-04-14 11:04:21', '/', 6),
(648, '110.76.128.234', '110.76.128.234', '110.76.128.234', '2017-04-14 11:04:39', '/', 2),
(649, '116.58.205.112', '116.58.205.112', '116.58.205.112', '2017-04-14 11:04:40', '/', 2),
(650, '107.167.103.121', '107.167.103.121', '107.167.103.121', '2017-04-14 11:04:58', '/', 1),
(651, '103.73.106.240', '103.73.106.240', '103.73.106.240', '2017-04-14 12:04:34', '/', 4),
(652, '103.51.230.254', '103.51.230.254', '103.51.230.254', '2017-04-14 12:04:36', '/', 2),
(653, '117.103.81.182', '117.103.81.182', '117.103.81.182', '2017-04-14 01:04:01', '/', 2),
(654, '45.114.88.254', '45.114.88.254', '45.114.88.254', '2017-04-14 02:04:44', '/', 2),
(655, '59.152.13.254', '59.152.13.254', '59.152.13.254', '2017-04-14 03:04:19', '/', 1),
(656, '107.21.253.49', '107.21.253.49', '107.21.253.49', '2017-04-14 03:04:59', '/', 1),
(657, '54.204.20.249', '54.204.20.249', '54.204.20.249', '2017-04-14 03:04:59', '/', 1),
(658, '27.147.201.6', '27.147.201.6', '27.147.201.6', '2017-04-14 04:04:00', '/', 6),
(659, '103.229.80.6', '103.229.80.6', '103.229.80.6', '2017-04-14 09:04:49', '/', 2),
(660, '118.100.0.70', '118.100.0.70', '118.100.0.70', '2017-04-15 01:04:14', '/', 4),
(661, '103.205.134.61', '103.205.134.61', '103.205.134.61', '2017-04-15 05:04:28', '/', 2),
(662, '180.76.15.137', '180.76.15.137', '180.76.15.137', '2017-04-15 11:04:41', '/', 1),
(663, '43.250.81.84', '43.250.81.84', '43.250.81.84', '2017-04-20 08:04:53', '/', 96),
(664, '103.84.36.22', '103.84.36.22', '103.84.36.22', '2017-04-16 12:04:39', '/', 7),
(665, '74.115.214.135', '74.115.214.135', '74.115.214.135', '2017-04-16 12:04:42', '/', 1),
(666, '172.241.151.21', '172.241.151.21', '172.241.151.21', '2017-04-16 01:04:42', '/', 1),
(667, '119.30.47.199', '119.30.47.199', '119.30.47.199', '2017-04-16 03:04:31', '/', 4),
(668, '8.37.234.106', '8.37.234.106', '8.37.234.106', '2017-04-17 12:04:31', '/', 7);
INSERT INTO `visitor_count` (`visitor_count_id`, `ip_1`, `ip_2`, `ip_3`, `last_visit_date`, `visited_url`, `total_visit`) VALUES
(669, '103.67.159.147', '103.67.159.147', '103.67.159.147', '2017-04-16 11:04:53', '/', 1),
(670, '74.115.214.141', '74.115.214.141', '74.115.214.141', '2017-04-17 05:04:06', '/', 1),
(671, '74.115.214.155', '74.115.214.155', '74.115.214.155', '2017-04-17 05:04:06', '/', 1),
(672, '202.191.127.36', '202.191.127.36', '202.191.127.36', '2017-04-19 01:04:42', '/', 8),
(673, '173.252.84.196', '173.252.84.196', '173.252.84.196', '2017-04-17 07:04:47', '/', 1),
(674, '66.220.156.151', '66.220.156.151', '66.220.156.151', '2017-04-17 07:04:47', '/', 1),
(675, '103.19.253.194', '103.19.253.194', '103.19.253.194', '2017-04-17 07:04:47', '/', 2),
(676, '142.54.161.58', '142.54.161.58', '142.54.161.58', '2017-04-18 03:04:07', '/', 1),
(677, '202.134.14.155', '202.134.14.155', '202.134.14.155', '2017-04-18 03:04:07', '/', 2),
(678, '193.70.14.60', '193.70.14.60', '193.70.14.60', '2017-04-18 03:04:22', '/', 1),
(679, '103.250.71.130', '103.250.71.130', '103.250.71.130', '2017-04-18 05:04:03', '/', 1),
(680, '217.75.194.10', '217.75.194.10', '217.75.194.10', '2017-04-18 09:04:41', '/', 2),
(681, '50.22.90.226', '50.22.90.226', '50.22.90.226', '2017-04-27 09:04:20', '/', 6),
(682, '107.191.102.245', '107.191.102.245', '107.191.102.245', '2017-04-18 03:04:29', '/', 1),
(683, '216.119.142.90', '216.119.142.90', '216.119.142.90', '2017-04-18 05:04:02', '/', 1),
(684, '107.167.113.13', '107.167.113.13', '107.167.113.13', '2017-04-18 11:04:39', '/', 1),
(685, '103.230.106.23', '103.230.106.23', '103.230.106.23', '2017-04-19 02:04:13', '/', 2),
(686, '40.77.167.67', '40.77.167.67', '40.77.167.67', '2017-04-19 06:04:39', '/', 1),
(687, '38.100.21.63', '38.100.21.63', '38.100.21.63', '2017-04-19 08:04:41', '/', 1),
(688, '51.140.100.124', '51.140.100.124', '51.140.100.124', '2017-04-19 09:04:36', '/', 1),
(689, '172.241.151.22', '172.241.151.22', '172.241.151.22', '2017-04-29 05:04:43', '/', 2),
(690, '137.59.155.18', '137.59.155.18', '137.59.155.18', '2017-04-20 08:04:43', '/', 8),
(691, '180.148.212.46', '180.148.212.46', '180.148.212.46', '2017-04-20 12:04:58', '/', 4),
(692, '173.234.234.213', '173.234.234.213', '173.234.234.213', '2017-04-20 02:04:24', '/', 1),
(693, '40.77.167.3', '40.77.167.3', '40.77.167.3', '2017-04-21 03:04:47', '/', 1),
(694, '193.201.224.239', '193.201.224.239', '193.201.224.239', '2017-04-21 04:04:39', '/', 1),
(695, '73.157.93.235', '73.157.93.235', '73.157.93.235', '2017-04-21 07:04:27', '/', 2),
(696, '199.58.164.121', '199.58.164.121', '199.58.164.121', '2017-04-21 11:04:16', '/', 1),
(697, '103.210.19.16', '103.210.19.16', '103.210.19.16', '2017-04-21 01:04:18', '/', 3),
(698, '69.171.225.38', '69.171.225.38', '69.171.225.38', '2017-04-21 01:04:54', '/', 1),
(699, '69.171.225.34', '69.171.225.34', '69.171.225.34', '2017-04-21 01:04:54', '/', 1),
(700, '66.220.151.90', '66.220.151.90', '66.220.151.90', '2017-04-21 01:04:54', '/', 1),
(701, '180.76.15.15', '180.76.15.15', '180.76.15.15', '2017-05-12 06:05:44', '/', 2),
(702, '173.252.84.200', '173.252.84.200', '173.252.84.200', '2017-04-22 01:04:58', '/', 1),
(703, '173.252.84.198', '173.252.84.198', '173.252.84.198', '2017-04-22 01:04:59', '/', 1),
(704, '43.250.81.86', '43.250.81.86', '43.250.81.86', '2017-04-23 08:04:00', '/', 27),
(705, '206.180.171.149', '206.180.171.149', '206.180.171.149', '2017-04-22 06:04:19', '/', 1),
(706, '168.235.207.22', '168.235.207.22', '168.235.207.22', '2017-04-23 12:04:42', '/', 8),
(707, '1.144.97.165', '1.144.97.165', '1.144.97.165', '2017-04-22 09:04:02', '/', 1),
(708, '168.235.196.24', '168.235.196.24', '168.235.196.24', '2017-04-22 11:04:31', '/', 2),
(709, '14.1.100.234', '14.1.100.234', '14.1.100.234', '2017-04-23 04:04:46', '/?keywords=xiaomi', 1),
(710, '207.46.13.189', '207.46.13.189', '207.46.13.189', '2017-04-23 05:04:52', '/', 1),
(711, '119.30.39.73', '119.30.39.73', '119.30.39.73', '2017-04-23 09:04:20', '/', 1),
(712, '202.134.11.141', '202.134.11.141', '202.134.11.141', '2017-04-23 10:04:16', '/', 3),
(713, '199.87.154.255', '199.87.154.255', '199.87.154.255', '2017-04-24 01:04:26', '/', 1),
(714, '103.26.247.66', '103.26.247.66', '103.26.247.66', '2017-04-24 10:04:54', '/', 7),
(715, '8.37.225.172', '8.37.225.172', '8.37.225.172', '2017-04-24 01:04:41', '/', 2),
(716, '103.244.14.185', '103.244.14.185', '103.244.14.185', '2017-05-20 07:05:59', '/', 319),
(717, '199.58.164.118', '199.58.164.118', '199.58.164.118', '2017-04-25 06:04:24', '/', 1),
(718, '69.171.225.50', '69.171.225.50', '69.171.225.50', '2017-04-25 01:04:13', '/', 1),
(719, '69.171.225.44', '69.171.225.44', '69.171.225.44', '2017-04-25 01:04:13', '/', 1),
(720, '66.220.151.88', '66.220.151.88', '66.220.151.88', '2017-04-25 01:04:13', '/', 1),
(721, '66.249.65.127', '66.249.65.127', '66.249.65.127', '2017-04-26 06:04:17', '/', 2),
(722, '207.46.13.90', '207.46.13.90', '207.46.13.90', '2017-04-25 06:04:02', '/', 1),
(723, '103.19.253.178', '103.19.253.178', '103.19.253.178', '2017-04-26 10:04:17', '/', 2),
(724, '113.190.62.106', '113.190.62.106', '113.190.62.106', '2017-04-26 10:04:30', '/', 4),
(725, '104.192.74.36', '104.192.74.36', '104.192.74.36', '2017-04-26 11:04:42', '/', 2),
(726, '168.235.201.118', '168.235.201.118', '168.235.201.118', '2017-04-27 02:04:39', '/', 2),
(727, '78.184.37.214', '78.184.37.214', '78.184.37.214', '2017-04-27 12:04:54', '/', 1),
(728, '216.145.17.190', '216.145.17.190', '216.145.17.190', '2017-04-27 03:04:12', '/', 1),
(729, '180.76.15.23', '180.76.15.23', '180.76.15.23', '2017-04-27 03:04:13', '/', 1),
(730, '202.83.126.128', '202.83.126.128', '202.83.126.128', '2017-04-28 12:04:25', '/', 2),
(731, '168.235.194.161', '168.235.194.161', '168.235.194.161', '2017-04-28 06:04:54', '/', 2),
(732, '118.89.175.109', '118.89.175.109', '118.89.175.109', '2017-04-29 03:04:35', '/', 1),
(733, '207.46.13.167', '207.46.13.167', '207.46.13.167', '2017-04-29 04:04:32', '/', 1),
(734, '43.224.116.20', '43.224.116.20', '43.224.116.20', '2017-04-29 07:04:54', '/', 10),
(735, '43.224.118.65', '43.224.118.65', '43.224.118.65', '2017-04-29 07:04:53', '/', 3),
(736, '103.25.251.182', '103.25.251.182', '103.25.251.182', '2017-04-29 10:04:10', '/', 1),
(737, '93.115.95.201', '93.115.95.201', '93.115.95.201', '2017-04-29 06:04:21', '/', 1),
(738, '162.242.156.106', '162.242.156.106', '162.242.156.106', '2017-04-30 02:04:22', '/', 2),
(739, '103.31.178.146', '103.31.178.146', '103.31.178.146', '2017-05-04 01:05:25', '/', 98),
(740, '113.11.61.16', '113.11.61.16', '113.11.61.16', '2017-04-30 12:04:45', '/', 5),
(741, '157.55.39.174', '157.55.39.174', '157.55.39.174', '2017-04-30 02:04:29', '/', 1),
(742, '180.76.15.147', '180.76.15.147', '180.76.15.147', '2017-04-30 03:04:52', '/', 1),
(743, '144.76.78.12', '144.76.78.12', '144.76.78.12', '2017-04-30 08:04:48', '/', 1),
(744, '103.78.226.249', '103.78.226.249', '103.78.226.249', '2017-04-30 11:04:30', '/', 2),
(745, '173.234.234.198', '173.234.234.198', '173.234.234.198', '2017-05-01 06:05:00', '/', 1),
(746, '107.167.105.195', '107.167.105.195', '107.167.105.195', '2017-05-01 08:05:04', '/', 2),
(747, '173.234.234.194', '173.234.234.194', '173.234.234.194', '2017-05-01 12:05:01', '/', 1),
(748, '103.43.150.139', '103.43.150.139', '103.43.150.139', '2017-05-02 10:05:40', '/', 4),
(749, '119.30.35.115', '119.30.35.115', '119.30.35.115', '2017-05-02 02:05:01', '/', 2),
(750, '173.252.90.124', '173.252.90.124', '173.252.90.124', '2017-05-03 03:05:28', '/', 1),
(751, '173.252.91.249', '173.252.91.249', '173.252.91.249', '2017-05-03 03:05:28', '/', 1),
(752, '172.241.151.27', '172.241.151.27', '172.241.151.27', '2017-05-03 03:05:00', '/', 1),
(753, '144.48.108.213', '144.48.108.213', '144.48.108.213', '2017-05-15 05:05:16', '/', 92),
(754, '89.248.172.48', '89.248.172.48', '89.248.172.48', '2017-05-09 06:05:16', '/', 2),
(755, '27.147.204.196', '27.147.204.196', '27.147.204.196', '2017-05-04 05:05:24', '/', 2),
(756, '93.115.95.204', '93.115.95.204', '93.115.95.204', '2017-05-04 03:05:52', '/', 1),
(757, '180.76.15.138', '180.76.15.138', '180.76.15.138', '2017-05-04 05:05:30', '/', 1),
(758, '45.55.215.154', '45.55.215.154', '45.55.215.154', '2017-05-05 08:05:36', '/', 1),
(759, '8.37.225.204', '8.37.225.204', '8.37.225.204', '2017-05-05 11:05:25', '/', 2),
(760, '103.26.247.106', '103.26.247.106', '103.26.247.106', '2017-05-06 07:05:33', '/', 11),
(761, '40.77.167.112', '40.77.167.112', '40.77.167.112', '2017-05-06 09:05:27', '/', 1),
(762, '103.253.47.102', '103.253.47.102', '103.253.47.102', '2017-05-07 03:05:14', '/', 2),
(763, '100.43.85.8', '100.43.85.8', '100.43.85.8', '2017-05-16 12:05:33', '/', 3),
(764, '40.78.146.128', '40.78.146.128', '40.78.146.128', '2017-05-07 02:05:15', '/', 1),
(765, '103.60.160.254', '103.60.160.254', '103.60.160.254', '2017-05-08 12:05:35', '/', 2),
(766, '180.76.15.156', '180.76.15.156', '180.76.15.156', '2017-05-08 03:05:34', '/', 1),
(767, '103.35.168.30', '103.35.168.30', '103.35.168.30', '2017-05-08 06:05:03', '/', 4),
(768, '54.242.22.165', '54.242.22.165', '54.242.22.165', '2017-05-08 02:05:04', '/', 1),
(769, '180.76.15.27', '180.76.15.27', '180.76.15.27', '2017-05-08 05:05:32', '/', 1),
(770, '139.194.5.56', '139.194.5.56', '139.194.5.56', '2017-05-08 07:05:52', '/', 1),
(771, '173.252.98.87', '173.252.98.87', '173.252.98.87', '2017-05-08 11:05:27', '/', 1),
(772, '103.78.52.34', '103.78.52.34', '103.78.52.34', '2017-05-09 04:05:55', '/', 2),
(773, '176.10.104.240', '176.10.104.240', '176.10.104.240', '2017-05-09 11:05:07', '/', 1),
(774, '208.113.166.5', '208.113.166.5', '208.113.166.5', '2017-05-10 05:05:07', '/', 4),
(775, '208.113.166.5', '208.113.166.5', '208.113.166.5', '2017-05-10 05:05:07', '/index.php?option=com_user&task=register', 1),
(776, '173.252.123.128', '173.252.123.128', '173.252.123.128', '2017-05-10 06:05:37', '/', 1),
(777, '37.187.78.81', '37.187.78.81', '37.187.78.81', '2017-05-21 11:05:38', '/', 4),
(778, '46.161.57.92', '46.161.57.92', '46.161.57.92', '2017-05-10 05:05:41', '/', 1),
(779, '45.55.160.105', '45.55.160.105', '45.55.160.105', '2017-05-11 02:05:44', '/', 1),
(780, '103.91.144.2', '103.91.144.2', '103.91.144.2', '2017-05-11 02:05:51', '/', 2),
(781, '103.35.110.101', '103.35.110.101', '103.35.110.101', '2017-05-11 05:05:26', '/', 5),
(782, '172.241.151.26', '172.241.151.26', '172.241.151.26', '2017-05-11 03:05:21', '/', 1),
(783, '172.241.151.24', '172.241.151.24', '172.241.151.24', '2017-05-14 09:05:33', '/', 2),
(784, '157.55.39.248', '157.55.39.248', '157.55.39.248', '2017-05-11 09:05:50', '/', 1),
(785, '173.234.164.11', '173.234.164.11', '173.234.164.11', '2017-05-18 06:05:30', '/', 4),
(786, '54.146.50.69', '54.146.50.69', '54.146.50.69', '2017-05-12 07:05:09', '/', 1),
(787, '63.243.252.182', '63.243.252.182', '63.243.252.182', '2017-05-12 07:05:18', '/', 1),
(788, '94.23.171.176', '94.23.171.176', '94.23.171.176', '2017-05-12 11:05:29', '/', 1),
(789, '207.46.13.181', '207.46.13.181', '207.46.13.181', '2017-05-13 04:05:57', '/', 1),
(790, '82.80.249.211', '82.80.249.211', '82.80.249.211', '2017-05-13 06:05:15', '/', 1),
(791, '82.80.230.228', '82.80.230.228', '82.80.230.228', '2017-05-13 06:05:15', '/', 2),
(792, '82.80.249.164', '82.80.249.164', '82.80.249.164', '2017-05-13 07:05:00', '/', 1),
(793, '82.80.249.200', '82.80.249.200', '82.80.249.200', '2017-05-13 07:05:02', '/', 2),
(794, '82.80.249.202', '82.80.249.202', '82.80.249.202', '2017-05-13 07:05:06', '/', 1),
(795, '103.209.20.254', '103.209.20.254', '103.209.20.254', '2017-05-13 08:05:16', '/', 2),
(796, '173.234.164.3', '173.234.164.3', '173.234.164.3', '2017-05-14 07:05:27', '/', 1),
(797, '173.234.164.5', '173.234.164.5', '173.234.164.5', '2017-05-14 09:05:18', '/', 1),
(798, '64.185.231.58', '64.185.231.58', '64.185.231.58', '2017-05-15 03:05:48', '/', 1),
(799, '115.127.80.4', '115.127.80.4', '115.127.80.4', '2017-05-15 05:05:20', '/', 2),
(800, '52.36.251.200', '52.36.251.200', '52.36.251.200', '2017-05-15 09:05:13', '/', 3),
(801, '180.76.15.144', '180.76.15.144', '180.76.15.144', '2017-05-15 12:05:40', '/', 1),
(802, '103.56.4.118', '103.56.4.118', '103.56.4.118', '2017-05-15 02:05:26', '/', 2),
(803, '64.246.165.190', '64.246.165.190', '64.246.165.190', '2017-05-16 02:05:19', '/', 1),
(804, '5.189.188.111', '5.189.188.111', '5.189.188.111', '2017-05-16 10:05:13', '/', 1),
(805, '199.58.164.122', '199.58.164.122', '199.58.164.122', '2017-05-16 11:05:17', '/', 1),
(806, '40.77.167.45', '40.77.167.45', '40.77.167.45', '2017-05-17 02:05:24', '/', 2),
(807, '182.64.178.254', '182.64.178.254', '182.64.178.254', '2017-05-17 05:05:40', '/', 1),
(808, '95.213.184.251', '95.213.184.251', '95.213.184.251', '2017-05-18 09:05:03', '/', 1),
(809, '173.252.90.100', '173.252.90.100', '173.252.90.100', '2017-05-18 11:05:24', '/', 1),
(810, '173.252.88.152', '173.252.88.152', '173.252.88.152', '2017-05-18 11:05:24', '/', 1),
(811, '103.229.84.118', '103.229.84.118', '103.229.84.118', '2017-05-18 12:05:42', '/', 7),
(812, '66.240.192.138', '66.240.192.138', '66.240.192.138', '2017-05-19 03:05:33', '/', 1),
(813, '163.172.4.153', '163.172.4.153', '163.172.4.153', '2017-05-26 07:05:02', '/', 2),
(814, '38.100.21.62', '38.100.21.62', '38.100.21.62', '2017-05-19 11:05:49', '/', 1),
(815, '173.252.122.120', '173.252.122.120', '173.252.122.120', '2017-05-19 12:05:34', '/', 1),
(816, '173.234.234.219', '173.234.234.219', '173.234.234.219', '2017-05-19 01:05:24', '/', 1),
(817, '180.76.15.136', '180.76.15.136', '180.76.15.136', '2017-05-19 03:05:11', '/', 1),
(818, '180.76.15.26', '180.76.15.26', '180.76.15.26', '2017-05-20 01:05:46', '/', 1),
(819, '103.231.160.30', '103.231.160.30', '103.231.160.30', '2017-05-26 01:05:44', '/', 4),
(820, '40.77.167.138', '40.77.167.138', '40.77.167.138', '2017-05-24 10:05:29', '/', 3),
(821, '199.58.164.119', '199.58.164.119', '199.58.164.119', '2017-05-22 01:05:21', '/', 1),
(822, '79.137.85.189', '79.137.85.189', '79.137.85.189', '2017-05-22 09:05:06', '/', 1),
(823, '107.178.115.136', '107.178.115.136', '107.178.115.136', '2017-05-22 11:05:40', '/', 1),
(824, '119.30.39.177', '119.30.39.177', '119.30.39.177', '2017-05-23 12:05:17', '/', 1),
(825, '119.30.32.129', '119.30.32.129', '119.30.32.129', '2017-05-23 12:05:31', '/', 1),
(826, '202.134.11.139', '202.134.11.139', '202.134.11.139', '2017-05-23 11:05:18', '/', 2),
(827, '103.210.19.23', '103.210.19.23', '103.210.19.23', '2017-05-23 01:05:46', '/', 1),
(828, '103.210.19.23', '103.210.19.23', '103.210.19.23', '2017-05-23 01:05:46', '/', 1),
(829, '180.76.15.161', '180.76.15.161', '180.76.15.161', '2017-05-23 09:05:57', '/', 1),
(830, '149.202.90.29', '149.202.90.29', '149.202.90.29', '2017-05-24 12:05:37', '/', 1),
(831, '103.76.196.146', '103.76.196.146', '103.76.196.146', '2017-05-24 04:05:54', '/', 2),
(832, '8.37.234.59', '8.37.234.59', '8.37.234.59', '2017-05-24 09:05:06', '/', 2),
(833, '27.123.246.21', '27.123.246.21', '27.123.246.21', '2017-05-24 09:05:07', '/', 2),
(834, '155.133.82.122', '155.133.82.122', '155.133.82.122', '2017-05-24 01:05:59', '/', 1),
(835, '8.37.225.214', '8.37.225.214', '8.37.225.214', '2017-05-25 12:05:51', '/', 2),
(836, '103.79.216.2', '103.79.216.2', '103.79.216.2', '2017-05-25 02:05:31', '/', 2),
(837, '8.37.225.206', '8.37.225.206', '8.37.225.206', '2017-05-25 09:05:57', '/', 6),
(838, '103.210.19.18', '103.210.19.18', '103.210.19.18', '2017-05-26 01:05:46', '/', 31),
(839, '173.252.90.118', '173.252.90.118', '173.252.90.118', '2017-05-25 11:05:25', '/', 2),
(840, '173.252.91.252', '173.252.91.252', '173.252.91.252', '2017-05-25 11:05:25', '/', 1),
(841, '104.156.231.254', '104.156.231.254', '104.156.231.254', '2017-05-25 03:05:50', '/', 1),
(842, '180.76.15.157', '180.76.15.157', '180.76.15.157', '2017-05-25 04:05:18', '/', 1),
(843, '66.249.65.193', '66.249.65.193', '66.249.65.193', '2017-05-26 01:05:27', '/', 1),
(844, '157.55.39.187', '157.55.39.187', '157.55.39.187', '2017-05-26 05:05:07', '/', 1),
(845, '54.251.2.104', '54.251.2.104', '54.251.2.104', '2017-05-26 12:05:53', '/', 2),
(846, '144.48.161.31', '144.48.161.31', '144.48.161.31', '2017-05-26 12:05:53', '/', 1),
(847, '66.220.155.248', '66.220.155.248', '66.220.155.248', '2017-05-26 12:05:54', '/', 2),
(848, '66.220.155.249', '66.220.155.249', '66.220.155.249', '2017-05-26 12:05:54', '/', 1),
(849, '45.127.246.8', '45.127.246.8', '45.127.246.8', '2017-05-26 12:05:57', '/', 1),
(850, '110.76.128.134', '110.76.128.134', '110.76.128.134', '2017-05-26 12:05:59', '/', 1),
(851, '103.49.202.50', '103.49.202.50', '103.49.202.50', '2017-05-26 12:05:59', '/', 1),
(852, '137.59.5.249', '137.59.5.249', '137.59.5.249', '2017-05-26 01:05:00', '/', 1),
(853, '103.205.68.18', '103.205.68.18', '103.205.68.18', '2017-05-26 01:05:04', '/', 1),
(854, '103.77.62.3', '103.77.62.3', '103.77.62.3', '2017-05-26 01:05:04', '/', 1),
(855, '180.234.87.108', '180.234.87.108', '180.234.87.108', '2017-05-26 01:05:05', '/', 2),
(856, '103.49.202.53', '103.49.202.53', '103.49.202.53', '2017-05-26 01:05:07', '/', 1),
(857, '103.237.38.177', '103.237.38.177', '103.237.38.177', '2017-05-26 01:05:10', '/', 1),
(858, '103.78.52.227', '103.78.52.227', '103.78.52.227', '2017-05-26 02:05:11', '/', 3),
(859, '103.220.204.129', '103.220.204.129', '103.220.204.129', '2017-05-26 01:05:16', '/', 1),
(860, '180.148.210.162', '180.148.210.162', '180.148.210.162', '2017-05-26 01:05:19', '/', 1),
(861, '103.58.92.14', '103.58.92.14', '103.58.92.14', '2017-05-27 10:05:40', '/', 2),
(862, '103.196.234.130', '103.196.234.130', '103.196.234.130', '2017-05-26 01:05:21', '/', 1),
(863, '103.205.135.13', '103.205.135.13', '103.205.135.13', '2017-05-26 01:05:21', '/', 1),
(864, '203.76.121.203', '203.76.121.203', '203.76.121.203', '2017-05-26 01:05:27', '/', 1),
(865, '103.242.219.100', '103.242.219.100', '103.242.219.100', '2017-05-26 01:05:27', '/', 1),
(866, '103.25.250.20', '103.25.250.20', '103.25.250.20', '2017-05-26 01:05:33', '/', 4),
(867, '103.43.148.102', '103.43.148.102', '103.43.148.102', '2017-05-26 01:05:36', '/', 1),
(868, '103.19.255.209', '103.19.255.209', '103.19.255.209', '2017-05-26 01:05:45', '/', 1),
(869, '103.86.198.3', '103.86.198.3', '103.86.198.3', '2017-05-26 01:05:46', '/', 1),
(870, '103.73.106.241', '103.73.106.241', '103.73.106.241', '2017-05-26 01:05:47', '/', 1),
(871, '180.234.51.108', '180.234.51.108', '180.234.51.108', '2017-05-26 01:05:56', '/', 1),
(872, '103.49.168.26', '103.49.168.26', '103.49.168.26', '2017-05-26 01:05:59', '/', 1),
(873, '103.203.94.82', '103.203.94.82', '103.203.94.82', '2017-05-26 02:05:00', '/', 1),
(874, '103.60.160.114', '103.60.160.114', '103.60.160.114', '2017-05-26 02:05:07', '/', 1),
(875, '182.48.95.26', '182.48.95.26', '182.48.95.26', '2017-05-26 02:05:08', '/', 1),
(876, '103.56.7.30', '103.56.7.30', '103.56.7.30', '2017-05-26 02:05:14', '/', 1),
(877, '110.76.129.222', '110.76.129.222', '110.76.129.222', '2017-05-26 02:05:20', '/', 1),
(878, '103.43.150.2', '103.43.150.2', '103.43.150.2', '2017-05-26 02:05:21', '/', 1),
(879, '103.205.135.42', '103.205.135.42', '103.205.135.42', '2017-05-26 03:05:01', '/', 3),
(880, '45.33.45.144', '45.33.45.144', '45.33.45.144', '2017-05-26 03:05:19', '/', 1),
(881, '103.19.253.123', '103.19.253.123', '103.19.253.123', '2017-05-26 03:05:25', '/', 1),
(882, '139.59.250.222', '139.59.250.222', '139.59.250.222', '2017-05-26 03:05:39', '/', 1),
(883, '103.73.106.233', '103.73.106.233', '103.73.106.233', '2017-05-26 04:05:03', '/', 1),
(884, '59.152.97.73', '59.152.97.73', '59.152.97.73', '2017-05-26 04:05:52', '/', 1),
(885, '94.197.121.204', '94.197.121.204', '94.197.121.204', '2017-05-26 07:05:23', '/', 1),
(886, '103.213.236.62', '103.213.236.62', '103.213.236.62', '2017-05-26 07:05:29', '/', 1),
(887, '103.41.245.157', '103.41.245.157', '103.41.245.157', '2017-05-26 07:05:45', '/', 1),
(888, '103.83.167.3', '103.83.167.3', '103.83.167.3', '2017-05-26 07:05:49', '/', 1),
(889, '103.220.204.132', '103.220.204.132', '103.220.204.132', '2017-05-26 08:05:01', '/', 1),
(890, '103.200.39.7', '103.200.39.7', '103.200.39.7', '2017-05-26 08:05:32', '/', 1),
(891, '103.242.216.214', '103.242.216.214', '103.242.216.214', '2017-05-26 08:05:50', '/', 1),
(892, '103.231.162.142', '103.231.162.142', '103.231.162.142', '2017-05-26 09:05:08', '/', 1),
(893, '103.76.45.42', '103.76.45.42', '103.76.45.42', '2017-05-26 09:05:17', '/', 1),
(894, '43.225.150.110', '43.225.150.110', '43.225.150.110', '2017-05-26 09:05:43', '/', 1),
(895, '59.152.89.162', '59.152.89.162', '59.152.89.162', '2017-05-26 10:05:00', '/', 1),
(896, '103.220.205.10', '103.220.205.10', '103.220.205.10', '2017-05-26 10:05:31', '/', 1),
(897, '124.6.235.134', '124.6.235.134', '124.6.235.134', '2017-05-26 10:05:58', '/', 1),
(898, '103.77.62.12', '103.77.62.12', '103.77.62.12', '2017-05-27 02:05:59', '/', 2),
(899, '103.78.54.84', '103.78.54.84', '103.78.54.84', '2017-05-26 11:05:35', '/', 1),
(900, '103.225.231.26', '103.225.231.26', '103.225.231.26', '2017-05-27 01:05:17', '/', 1),
(901, '103.213.239.46', '103.213.239.46', '103.213.239.46', '2017-05-27 01:05:26', '/', 1),
(902, '157.55.39.136', '157.55.39.136', '157.55.39.136', '2017-05-27 01:05:41', '/', 1),
(903, '103.204.208.182', '103.204.208.182', '103.204.208.182', '2017-05-27 01:05:47', '/', 1),
(904, '103.43.151.22', '103.43.151.22', '103.43.151.22', '2017-05-27 02:05:00', '/', 1),
(905, '110.76.128.21', '110.76.128.21', '110.76.128.21', '2017-05-27 02:05:55', '/', 1),
(906, '103.230.180.106', '103.230.180.106', '103.230.180.106', '2017-05-27 04:05:22', '/', 1),
(907, '110.70.26.19', '110.70.26.19', '110.70.26.19', '2017-05-27 05:05:09', '/', 1),
(908, '103.35.168.162', '103.35.168.162', '103.35.168.162', '2017-05-27 11:05:35', '/', 2),
(909, '66.249.73.145', '66.249.73.145', '66.249.73.145', '2017-05-27 06:05:41', '/', 1),
(910, '45.124.171.177', '45.124.171.177', '45.124.171.177', '2017-05-27 07:05:34', '/', 1),
(911, '14.200.78.85', '14.200.78.85', '14.200.78.85', '2017-05-27 08:05:17', '/', 1),
(912, '103.218.27.210', '103.218.27.210', '103.218.27.210', '2017-05-27 09:05:23', '/', 1),
(913, '182.48.81.78', '182.48.81.78', '182.48.81.78', '2017-05-27 10:05:00', '/', 1),
(914, '14.1.100.74', '14.1.100.74', '14.1.100.74', '2017-05-27 10:05:26', '/', 1),
(915, '103.85.33.22', '103.85.33.22', '103.85.33.22', '2017-05-27 10:05:58', '/', 1),
(916, '174.95.53.180', '174.95.53.180', '174.95.53.180', '2017-05-27 01:05:19', '/', 18),
(917, '103.19.253.54', '103.19.253.54', '103.19.253.54', '2017-05-27 01:05:13', '/', 1),
(918, '119.30.32.133', '119.30.32.133', '119.30.32.133', '2017-05-27 02:05:40', '/', 2),
(919, '113.11.43.143', '113.11.43.143', '113.11.43.143', '2017-05-27 07:05:08', '/', 1),
(920, '113.11.43.143', '113.11.43.143', '113.11.43.143', '2017-05-27 07:05:08', '/', 1),
(921, '113.11.43.143', '113.11.43.143', '113.11.43.143', '2017-05-27 07:05:08', '/', 1),
(922, '113.11.43.143', '113.11.43.143', '113.11.43.143', '2017-05-27 07:05:08', '/', 1),
(923, '113.11.43.143', '113.11.43.143', '113.11.43.143', '2017-05-27 07:05:08', '/', 1),
(924, '113.11.43.143', '113.11.43.143', '113.11.43.143', '2017-05-27 07:05:09', '/', 1),
(925, '180.76.15.159', '180.76.15.159', '180.76.15.159', '2017-05-27 09:05:20', '/', 1),
(926, '103.77.60.6', '103.77.60.6', '103.77.60.6', '2017-05-27 10:05:25', '/', 1),
(927, '43.224.116.21', '43.224.116.21', '43.224.116.21', '2017-05-28 03:05:23', '/', 12),
(928, '::1', '::1', '::1', '2017-06-14 03:06:59', '/kenakata/', 445),
(929, '::1', '::1', '::1', '2017-06-13 07:06:28', '/kenakata/Welcome.aspx', 31),
(930, '::1', '::1', '::1', '2017-06-13 07:06:55', '/kenakata/welcome/cart', 15),
(931, '::1', '::1', '::1', '2017-06-12 09:06:14', '/kenakata/welcome/about', 14),
(932, '::1', '::1', '::1', '2017-06-12 12:06:31', '/kenakata/welcome/cart.aspx', 4),
(933, '::1', '::1', '::1', '2017-06-12 12:06:34', '/kenakata/welcome/checkout', 3),
(934, '::1', '::1', '::1', '2017-06-12 12:06:34', '/kenakata/welcome/payment_order.aspx', 2),
(935, '::1', '::1', '::1', '2017-06-12 12:06:35', '/kenakata/welcome/order_successfull.aspx', 6),
(936, '::1', '::1', '::1', '2017-06-13 08:06:05', '/kenakata/welcome/view_all_big_offer_product', 14),
(937, '::1', '::1', '::1', '2017-06-13 09:06:27', '/kenakata/welcome/account.aspx', 52),
(938, '::1', '::1', '::1', '2017-06-13 09:06:30', '/kenakata/welcome/account', 55),
(939, '::1', '::1', '::1', '2017-06-13 06:06:47', '/kenakata/welcome/contact', 31),
(940, '::1', '::1', '::1', '2017-06-13 06:06:47', '/kenakata/welcome/gallery', 15),
(941, '::1', '::1', '::1', '2017-06-01 10:06:12', '/kenakata/welcome/product_details/6', 2),
(942, '::1', '::1', '::1', '2017-06-13 07:06:51', '/kenakata/welcome/product_details/img/index.ico', 4),
(943, '::1', '::1', '::1', '2017-06-12 09:06:13', '/kenakata/welcome/top_brand_product/4', 4),
(944, '::1', '::1', '::1', '2017-06-13 08:06:08', '/kenakata/welcome/top_brand_product/img/index.ico', 6),
(945, '::1', '::1', '::1', '2017-06-02 09:06:33', '/kenakata/welcome/top_brand_product/6', 4),
(946, '::1', '::1', '::1', '2017-06-02 09:06:00', '/kenakata/welcome/contact.aspx', 7),
(947, '::1', '::1', '::1', '2017-06-12 08:06:33', '/kenakata/welcome/career', 13),
(948, '::1', '::1', '::1', '2017-06-12 08:06:32', '/kenakata/welcome/return_policy', 7),
(949, '::1', '::1', '::1', '2017-06-12 08:06:47', '/kenakata/welcome/term_condition', 6),
(950, '::1', '::1', '::1', '2017-06-12 09:06:00', '/kenakata/welcome/other_business', 13),
(951, '::1', '::1', '::1', '2017-06-13 08:06:08', '/kenakata/welcome/top_brand_product/11', 3),
(952, '::1', '::1', '::1', '2017-06-06 07:06:40', '/kenakata/welcome/brand_product/16', 1),
(953, '::1', '::1', '::1', '2017-06-12 09:06:48', '/kenakata/welcome/brand_product/img/index.ico', 3),
(954, '::1', '::1', '::1', '2017-06-12 12:06:28', '/kenakata/welcome/product_details/3', 1),
(955, '::1', '::1', '::1', '2017-06-12 09:06:48', '/kenakata/welcome/brand_product/4', 1),
(956, '::1', '::1', '::1', '2017-06-12 09:06:48', '/kenakata/welcome/brand_product/6', 1),
(957, '::1', '::1', '::1', '2017-06-13 06:06:47', '/kenakata/welcome/product_details/12', 1),
(958, '::1', '::1', '::1', '2017-06-13 07:06:51', '/kenakata/welcome/product_details/4', 1);

-- --------------------------------------------------------

--
-- Table structure for table `youtube_video`
--

CREATE TABLE `youtube_video` (
  `youtube_video_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `file` text NOT NULL,
  `inserted_by` int(11) NOT NULL,
  `insert_time` datetime NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `is_active` int(11) NOT NULL,
  `is_delete` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `youtube_video`
--

INSERT INTO `youtube_video` (`youtube_video_id`, `title`, `file`, `inserted_by`, `insert_time`, `updated_by`, `update_time`, `is_active`, `is_delete`) VALUES
(7, 'Sharee', '4DAf42rY5Ss', 10, '2017-03-20 10:03:46', 11, '2017-06-12 06:06:21', 1, 0),
(9, 'Sharee', 'N9phCHjI6cI', 11, '2017-06-12 06:06:17', NULL, NULL, 1, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about_new`
--
ALTER TABLE `about_new`
  ADD PRIMARY KEY (`about_new_id`);

--
-- Indexes for table `about_us`
--
ALTER TABLE `about_us`
  ADD PRIMARY KEY (`about_us_id`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ad_management`
--
ALTER TABLE `ad_management`
  ADD PRIMARY KEY (`ad_id`);

--
-- Indexes for table `brand`
--
ALTER TABLE `brand`
  ADD PRIMARY KEY (`brand_id`);

--
-- Indexes for table `career`
--
ALTER TABLE `career`
  ADD PRIMARY KEY (`career_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`contact_us_id`);

--
-- Indexes for table `coupon`
--
ALTER TABLE `coupon`
  ADD PRIMARY KEY (`coupon_id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `customer_order`
--
ALTER TABLE `customer_order`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `customer_order_details`
--
ALTER TABLE `customer_order_details`
  ADD PRIMARY KEY (`order_details_id`);

--
-- Indexes for table `discount`
--
ALTER TABLE `discount`
  ADD PRIMARY KEY (`discount_id`);

--
-- Indexes for table `lib_photo`
--
ALTER TABLE `lib_photo`
  ADD PRIMARY KEY (`photo_id`);

--
-- Indexes for table `newsletter`
--
ALTER TABLE `newsletter`
  ADD PRIMARY KEY (`newsletter_id`);

--
-- Indexes for table `other_business`
--
ALTER TABLE `other_business`
  ADD PRIMARY KEY (`other_business_id`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`payment_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `return_policy`
--
ALTER TABLE `return_policy`
  ADD PRIMARY KEY (`return_policy_id`);

--
-- Indexes for table `review`
--
ALTER TABLE `review`
  ADD PRIMARY KEY (`review_id`);

--
-- Indexes for table `shipping`
--
ALTER TABLE `shipping`
  ADD PRIMARY KEY (`ship_id`);

--
-- Indexes for table `shipping_area`
--
ALTER TABLE `shipping_area`
  ADD PRIMARY KEY (`shipping_area_id`);

--
-- Indexes for table `shipping_cost`
--
ALTER TABLE `shipping_cost`
  ADD PRIMARY KEY (`shipping_cost_id`);

--
-- Indexes for table `sub_category`
--
ALTER TABLE `sub_category`
  ADD PRIMARY KEY (`sub_category_id`);

--
-- Indexes for table `term_condition`
--
ALTER TABLE `term_condition`
  ADD PRIMARY KEY (`term_condition_id`);

--
-- Indexes for table `testimonial`
--
ALTER TABLE `testimonial`
  ADD PRIMARY KEY (`testimonial_id`);

--
-- Indexes for table `top_brand`
--
ALTER TABLE `top_brand`
  ADD PRIMARY KEY (`top_brand_id`);

--
-- Indexes for table `user_info`
--
ALTER TABLE `user_info`
  ADD PRIMARY KEY (`user_info_id`);

--
-- Indexes for table `vendor`
--
ALTER TABLE `vendor`
  ADD PRIMARY KEY (`vendor_id`);

--
-- Indexes for table `vendor_store`
--
ALTER TABLE `vendor_store`
  ADD PRIMARY KEY (`vendor_store_id`);

--
-- Indexes for table `visitor_count`
--
ALTER TABLE `visitor_count`
  ADD PRIMARY KEY (`visitor_count_id`);

--
-- Indexes for table `youtube_video`
--
ALTER TABLE `youtube_video`
  ADD PRIMARY KEY (`youtube_video_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about_new`
--
ALTER TABLE `about_new`
  MODIFY `about_new_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `about_us`
--
ALTER TABLE `about_us`
  MODIFY `about_us_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `ad_management`
--
ALTER TABLE `ad_management`
  MODIFY `ad_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `brand`
--
ALTER TABLE `brand`
  MODIFY `brand_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT for table `career`
--
ALTER TABLE `career`
  MODIFY `career_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `contact_us_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `coupon`
--
ALTER TABLE `coupon`
  MODIFY `coupon_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `customer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `customer_order`
--
ALTER TABLE `customer_order`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `customer_order_details`
--
ALTER TABLE `customer_order_details`
  MODIFY `order_details_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `discount`
--
ALTER TABLE `discount`
  MODIFY `discount_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `lib_photo`
--
ALTER TABLE `lib_photo`
  MODIFY `photo_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;
--
-- AUTO_INCREMENT for table `newsletter`
--
ALTER TABLE `newsletter`
  MODIFY `newsletter_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT for table `other_business`
--
ALTER TABLE `other_business`
  MODIFY `other_business_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `payment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `return_policy`
--
ALTER TABLE `return_policy`
  MODIFY `return_policy_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `review`
--
ALTER TABLE `review`
  MODIFY `review_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `shipping`
--
ALTER TABLE `shipping`
  MODIFY `ship_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `shipping_area`
--
ALTER TABLE `shipping_area`
  MODIFY `shipping_area_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `shipping_cost`
--
ALTER TABLE `shipping_cost`
  MODIFY `shipping_cost_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `sub_category`
--
ALTER TABLE `sub_category`
  MODIFY `sub_category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `term_condition`
--
ALTER TABLE `term_condition`
  MODIFY `term_condition_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `testimonial`
--
ALTER TABLE `testimonial`
  MODIFY `testimonial_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `top_brand`
--
ALTER TABLE `top_brand`
  MODIFY `top_brand_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `user_info`
--
ALTER TABLE `user_info`
  MODIFY `user_info_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `vendor`
--
ALTER TABLE `vendor`
  MODIFY `vendor_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `vendor_store`
--
ALTER TABLE `vendor_store`
  MODIFY `vendor_store_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `visitor_count`
--
ALTER TABLE `visitor_count`
  MODIFY `visitor_count_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=959;
--
-- AUTO_INCREMENT for table `youtube_video`
--
ALTER TABLE `youtube_video`
  MODIFY `youtube_video_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
